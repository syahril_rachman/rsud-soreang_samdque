(function ($, window, undefined) {
    var samdque = {};

    samdque.userTable = function ($element, listUrl, csrf) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[1, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columns: [
                {
                    data: 'id',
                    name: 'id',
                    "orderable": false,
                    "searchable": false
                },
                {data: 'empname', name: 'empname'},
                {data: 'username', name: 'username'},
                {data: 'roles[0].display_name', name: 'roles[0].display_name'},
                {data: 'loket_name', name: 'loket_name'},
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var btn_edit = '<a href="/user/manage/' + row.id + '" title="Ubah data" class="btn btn-outline-primary btn-icon sq-24"><i class="icon icon-edit"></i></a>';
                        var btn_delete = '<a href="javascript:;" title="Hapus data" class="btn btn-outline-danger btn-icon sq-24 btn-delete" data-id="' + row.id + '"><i class="icon icon-trash"></i></a>';
                        return btn_edit + ' ' + btn_delete;
                    }
                }
            ]
        });
    };

    samdque.runningTextTable = function ($element, listUrl, csrf) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[1, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columns: [
                {
                    data: 'settingid',
                    name: 'settingid',
                    "orderable": false,
                    "searchable": false
                },
                {data: 'settingvalue', name: 'settingvalue'},
                {
                    data: 'setting',
                    name: 'setting',
                    "orderable": false,
                    "searchable": false,
                    'mRender': function (data) {
                        return data['font'] + ' px';
                    }
                },
                {
                    data: 'settingstat', name: 'settingstat', 'mRender': function (data) {
                    var status;
                    if (data == 'A') {
                        status = '<span class="label label-outline-primary">Aktif</span>';
                    } else if (data = 'NA') {
                        status = '<span class="label label-outline-danger">Non Aktif</span>';
                    }
                    return status;
                }
                },
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var btn_edit = '<a href="/other/runningtext/manage/' + row.settingid + '" title="Ubah data" class="btn btn-outline-primary btn-icon sq-24"><i class="icon icon-edit"></i></a>';
                        var btn_delete = '<a href="javascript:;" title="Hapus data" class="btn btn-outline-danger btn-icon sq-24 btn-delete" data-id="' + row.settingid + '"><i class="icon icon-trash"></i></a>';
                        return btn_edit + ' ' + btn_delete;
                    }
                }
            ]
        });
    };

    samdque.bannerTable = function ($element, listUrl, csrf, url) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[1, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columns: [
                {
                    data: 'settingid',
                    name: 'settingid',
                    "orderable": false,
                    "searchable": false
                },
                {
                    data: 'settingvalue', name: 'settingvalue', 'mRender': function (data) {
                    return '<img src="' + url + data + '" width="150px">'
                }
                },
                {
                    data: 'settingstat', name: 'settingstat', 'mRender': function (data) {
                    var status;
                    if (data == 'A') {
                        status = '<span class="label label-outline-primary">Aktif</span>';
                    } else if (data = 'NA') {
                        status = '<span class="label label-outline-danger">Non Aktif</span>';
                    }
                    return status;
                }
                },
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var btn_edit = '<a href="/other/banner/manage/' + row.settingid + '" title="Ubah data" class="btn btn-outline-primary btn-icon sq-24"><i class="icon icon-edit"></i></a>';
                        var btn_delete = '<a href="javascript:;" title="Hapus data" class="btn btn-outline-danger btn-icon sq-24 btn-delete" data-id="' + row.settingid + '"><i class="icon icon-trash"></i></a>';
                        return btn_edit + ' ' + btn_delete;
                    }
                }
            ]
        });
    };

    samdque.advTable = function ($element, listUrl, csrf, url) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[1, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columns: [
                {
                    data: 'settingid',
                    name: 'settingid',
                    "orderable": false,
                    "searchable": false
                },
                {
                    data: 'settingvalue', name: 'settingvalue', 'mRender': function (data) {
                    return '<img src="' + url + data + '" width="150px">'
                }
                },
                {
                    data: 'settingstat', name: 'settingstat', 'mRender': function (data) {
                    var status;
                    if (data == 'A') {
                        status = '<span class="label label-outline-primary">Aktif</span>';
                    } else if (data = 'NA') {
                        status = '<span class="label label-outline-danger">Non Aktif</span>';
                    }
                    return status;
                }
                },
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var btn_edit = '<a href="/other/adv/manage/' + row.settingid + '" title="Ubah data" class="btn btn-outline-primary btn-icon sq-24"><i class="icon icon-edit"></i></a>';
                        var btn_delete = '<a href="javascript:;" title="Hapus data" class="btn btn-outline-danger btn-icon sq-24 btn-delete" data-id="' + row.settingid + '"><i class="icon icon-trash"></i></a>';
                        return btn_edit + ' ' + btn_delete;
                    }
                }
            ]
        });
    };

    samdque.patientTable = function ($element, listUrl, csrf) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[1, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columns: [
                {
                    data: 'id',
                    name: 'id',
                    "orderable": false,
                    "searchable": false
                },
                {data: 'IdBankNomor', name: 'IdBankNomor'},
                {data: 'NamaPasien', name: 'NamaPasien'},
                {
                    data: 'IdJenisKelamin', name: 'IdJenisKelamin', 'mRender': function (data) {
                    var gender;
                    if (data == 'L') {
                        gender = 'Laki-laki';
                    } else if (data == 'P') {
                        gender = 'Perempuan'
                    }
                    return gender;
                }
                },
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var btn_edit = '<a href="/main/patient/detail/' + row.IdBankNomor + '" title="Ubah data" class="btn btn-outline-primary btn-icon sq-24"><i class="icon icon-edit"></i></a>';
                        var btn_delete = '<a href="javascript:;" title="Hapus data" class="btn btn-outline-danger btn-icon sq-24 btn-delete" data-id="' + row.id + '"><i class="icon icon-trash"></i></a>';
                        return btn_edit + ' ' + btn_delete;
                    }
                }
            ]
        });
    };

    samdque.visitTable = function ($element, listUrl, csrf, link) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[1, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columns: [
                {
                    data: 'noqueue', name: 'noqueue', 'mRender': function (data) {
                    var pad = "000";
                    var number_queue = pad.substring(0, pad.length - data.length) + data;

                    return link + number_queue;
                }
                },
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var status = row.status;
                        var span;
                        switch (status) {
                            case "WT":
                                span = '<span class="label label-default">Online</span>';
                                break;
                            case "PD":
                                span = '<span class="label label-warning">Pending</span>';
                                break;
                            case "DN":
                                span = '<span class="label label-primary">Done</span>';
                                break;
                            case "PR":
                                span = '<span class="label label-success">On Process</span>';
                                break;
                            case "CL":
                                span = '<span class="label label-danger">Canceled</span>';
                                break;
                        }

                        return span;

                    }
                }
            ]
        });
    };

    samdque.kioskTable = function ($element, listUrl, csrf) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[0, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columnDefs: [
                {"className": "pull-left", "targets": "0"},
                {"className": "pull-right", "targets": "1"}
            ],
            columns: [
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "width": "50%",
                    "mRender": function (data, type, row) {
                        var str = row.noqueue;
                        var pad = "000";
                        var nq = pad.substring(0, pad.length - str.length) + str;

                        var span = "";
                        switch (row.status) {
                            case "WT":
                                span = '<span class="label label-default">Online</span>';
                                break;
                            case "PD":
                                span = '<span class="label label-warning">Pending</span>';
                                break;
                            case "DN":
                                span = '<span class="label label-primary">Done</span>';
                                break;
                            case "PR":
                                span = '<span class="label label-success">On Process</span>';
                                break;
                            case "CL":
                                span = '<span class="label label-danger">Canceled</span>';
                                break;
                        }

                        var metadesc = metad[row.metavalue - 2];
                        var metalink = metal[row.metavalue - 2];
                        nq = metalink + nq;

                        return "<strong>#" + nq + "</strong> " + span + " <span class='label label-default'>" +
                            metadesc + "</span><br/>" + row.timequeue;
                    }
                },
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "width": "50%",
                    "mRender": function (data, type, row) {
                        var d1 = '';
                        var d2 = '';

                        if (row.is_calling == 1) {
                            d1 = "disabled='disabled' ";
                        }
                        if (row.status == "PR") {
                            d2 = "disabled='disabled' ";
                        }

                        return "<button " +
                            "type='button' " +
                            "value='Pendaftaran Kiosk' " +
                            "class='btn btn-primary btn-icon sq-24 btn-bullhorn' " +
                            "id='bl-" + row.noqueue + "-" + row.metavalue + "-" + row.regid + "' " +
                            d1 +
                            "title='Panggil Antrian'>" +
                            "<i class='icon icon-bullhorn'></i>" +
                            "</button> " +
                            "<button " +
                            "type='button' " +
                            "value='Pendaftaran Kiosk' " +
                            "class='btn btn-danger btn-icon sq-24 btn-cancel' " +
                            "id='cl-" + row.noqueue + "-" + row.metavalue + "-" + row.regid + "' " +
                            // d2 +
                            "title='Batal Antrian'>" +
                            "<i class='icon icon-trash'></i>" +
                            "</button> " +
                            "<button " +
                            "type='button' " +
                            "value='Pendaftaran Kiosk' " +
                            "class='btn btn-warning btn-icon sq-24 btn-pending' " +
                            "id='pd-" + row.noqueue + "-" + row.metavalue + "-" + row.regid + "' " +
                            "title='Ubah Status: Pending'>" +
                            "<i class='icon icon-bookmark-o'></i>" +
                            "</button> " +
                            "<button " +
                            "type='button' " +
                            "value='Pendaftaran Kiosk' " +
                            "class='btn btn-info btn-icon sq-24 btn-online' " +
                            "id='ol-" + row.noqueue + "-" + row.metavalue + "-" + row.regid + "' " +
                            "title='Ubah Status: On Line'>" +
                            "<i class='icon icon-bookmark'></i>" +
                            "</button> ";
                            
                            // "<button " +
                            // "type='button' " +
                            // "value='Pendaftaran Kiosk' " +
                            // "class='btn btn-default btn-icon sq-24 btn-scan' " +
                            // "id='sc-" + row.noqueue + "-" + row.metavalue + "-" + row.regid + "' " +
                            // "title='Scan Barcode'>" +
                            // "<i class='icon icon-barcode'></i>" +
                            // "</button> " +
                            // "<button " +
                            // "type='button' " +
                            // "value='Pendaftaran Kiosk' " +
                            // "class='btn btn-default btn-icon sq-24 btn-register' " +
                            // "id='rg-" + row.noqueue + "-" + row.metavalue + "-" + row.regid + "' " +
                            // "title='Pendaftaran Manual'>" +
                            // "<i class='icon icon-user-plus'></i>" +
                            // "</button> ";
                    }
                }
            ]
        });
    };

    function orderNumber($datatable) {
        $datatable.on('order.dt search.dt draw.dt', function () {
            var page = $datatable.page.info().page;
            $datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + (page * 10);
            });
        }).draw();
    }

    function refreshTable($visit2, $visit3, $visit4, $visit5, date) {
        $visit2.ajax.url('/main/visit/get-list?value=1&&' + date).load();
        $visit3.ajax.url('/main/visit/get-list?value=2&&' + date).load();
        $visit4.ajax.url('/main/visit/get-list?value=3&&' + date).load();
        $visit5.ajax.url('/main/visit/get-list?value=4&&' + date).load();
    }

    $(document).ready(function () {
        var $userTable = samdque.userTable($('#table_user'), '/user/list', $('meta[name="csrf-token"]').attr('content'));
        if ($userTable) {
            orderNumber($userTable);
        }

        var $runningtext = samdque.runningTextTable($('#table_running_text'), '/other/runningtext/get-list', $('meta[name="csrf-token"]').attr('content'));
        if ($runningtext) {
            orderNumber($runningtext);
        }

        var $banner = samdque.bannerTable($('#banner_table'), '/other/banner/get-list', $('meta[name="csrf-token"]').attr('content'), $('#banner_table').data('url'));
        if ($banner) {
            orderNumber($banner);
        }

        var $adv = samdque.advTable($('#adv_table'), '/other/adv/get-list', $('meta[name="csrf-token"]').attr('content'), $('#adv_table').data('url'));
        if ($adv) {
            orderNumber($adv);
        }

        var $patient = samdque.patientTable($('#patient_table'), '/main/patient/get-list', $('meta[name="csrf-token"]').attr('content'));
        if ($patient) {
            orderNumber($patient);
        }

        var $kiosk = samdque.kioskTable($('#kiosk_table'), '/main/kiosk/get-list', $('meta[name="csrf-token"]').attr('content'));

        /*visit table
         * 2 = BPJS lt 1
         * 3 = BPJS lt 2
         * 4 = SKTM
         * 5 = UMUM*/
        var $visit2 = samdque.visitTable($('#table_2'), '/main/visit/get-list?value=2', $('meta[name="csrf-token"]').attr('content'), $('#table_2').data('link'));
        var $visit3 = samdque.visitTable($('#table_3'), '/main/visit/get-list?value=3', $('meta[name="csrf-token"]').attr('content'), $('#table_3').data('link'));
        var $visit4 = samdque.visitTable($('#table_4'), '/main/visit/get-list?value=4', $('meta[name="csrf-token"]').attr('content'), $('#table_4').data('link'));
        var $visit5 = samdque.visitTable($('#table_5'), '/main/visit/get-list?value=5', $('meta[name="csrf-token"]').attr('content'), $('#table_5').data('link'));

        /*socket io*/
        var socket = io.connect(full_url + ':8890');
        socket.on('message', function (data) {
            if (data == 'runningtext') {
                $runningtext.ajax.reload(null, false);
            }
            if (data == 'banner') {
                $banner.ajax.reload(null, false);
            }
            if (data == 'adv') {
                $adv.ajax.reload(null, false);
            }
            if (data == 'user') {
                $userTable.ajax.reload(null, false)
            }
            if (data == 'patient') {
                $patient.ajax.reload(null, false)
            }
            if (data == 'btn-yes') {
                var btn_yes = $('#btn-sound-yes');
                if(btn_yes){
                    btn_yes.prop('disabled', false);
                }
            }
/*                        if (data == 'kiosk'){
             $kiosk.ajax.reload(null, false)
             }*/
        });
        socket.on('kiosk-update', function (data) {
            var table = $($kiosk.table().node()).hasClass('userlevel-'+data);            
            if (table) {
                $kiosk.ajax.reload(null, false)
            }
        });


        $('#btn-datenow').on('click', function () {
            $this = $(this);
            var form = $this.closest('form');
            var data = form.serialize();
            refreshTable($visit2, $visit3, $visit4, $visit5, data)
        });

    });

})(jQuery, window);