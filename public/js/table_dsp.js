(function ($, window, undefined) {
    var samdque = {};    

    samdque.dsprunningTextTable = function ($element, listUrl, csrf) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[1, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columns: [
                {
                    data: 'id',
                    name: 'id',
                    "orderable": false,
                    "searchable": false
                },
                {data: 'description', name: 'description'},
                {data: 'setting', name: 'setting',  "orderable": false, "searchable": false, 
                    'mRender' : function (data) {
                        return data + ' px';
                }},
                {
                    data: 'status', name: 'status', 'mRender': function (data) {
                    var status;
                    if (data == 'A') {
                        status = '<span class="label label-outline-primary">Aktif</span>';
                    } else if (data = 'NA') {
                        status = '<span class="label label-outline-danger">Non Aktif</span>';
                    }
                    return status;
                }
                },
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var btn_edit = '<a href="/dsp/runningtext/manage/' + row.id + '" title="Ubah data" class="btn btn-outline-primary btn-icon sq-24"><i class="icon icon-edit"></i></a>';
                        var btn_delete = '<a href="javascript:;" title="Hapus data" class="btn btn-outline-danger btn-icon sq-24 btn-delete" data-id="' + row.id + '"><i class="icon icon-trash"></i></a>';
                        return btn_edit + ' ' + btn_delete;
                    }
                }
            ]
        });
    };

    samdque.dspbannerTable = function ($element, listUrl, csrf, url) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[1, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columns: [
                {
                    data: 'id',
                    name: 'id',
                    "orderable": false,
                    "searchable": false
                },
                {
                    data: 'file', name: 'file', 'mRender': function (data) {
                        return '<img src="' + url + data + '" width="150px">'
                    }
                },
                {data: 'description', name: 'description'},
                {
                    data: 'status', name: 'status', 'mRender': function (data) {
                    var status;
                    if (data == 'A') {
                        status = '<span class="label label-outline-primary">Aktif</span>';
                    } else if (data = 'NA') {
                        status = '<span class="label label-outline-danger">Non Aktif</span>';
                    }
                    return status;
                }
                },
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var btn_edit = '<a href="/dsp/banner/manage/' + row.id + '" title="Ubah data" class="btn btn-outline-primary btn-icon sq-24"><i class="icon icon-edit"></i></a>';
                        var btn_delete = '<a href="javascript:;" title="Hapus data" class="btn btn-outline-danger btn-icon sq-24 btn-delete" data-id="' + row.id + '"><i class="icon icon-trash"></i></a>';
                        return btn_edit + ' ' + btn_delete;
                    }
                }
            ]
        });
    };

    samdque.dspadvTable = function ($element, listUrl, csrf, url) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[1, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columns: [
                {
                    data: 'id',
                    name: 'id',
                    "orderable": false,
                    "searchable": false
                },
                {
                    data: 'file', name: 'file', 'mRender': function (data) {
                        return '<img src="' + url + data + '" width="150px">'
                    }
                },
                {data: 'description', name: 'description'},
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var status = row.status;
                        
                        if (status == 'A') {
                            status = '<button class="btn btn-outline-primary btn-icon btn-status" style="padding: 3px;" id="st-'+row.id+'">Aktif</button>';
                            // status = '<span class="label label-outline-primary">Aktif</span>';
                        } else if (status = 'NA') {
                            status = '<button class="btn btn-outline-danger btn-icon btn-status" style="padding: 3px;" id="st-'+row.id+'">Non Aktif </button>';
                            // status = '<span class="label label-outline-danger">Non Aktif</span>';
                        }
                        return status;
                    }
                },
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var btn_edit = '<a href="/dsp/adv/manage/' + row.id + '" title="Ubah data" class="btn btn-outline-primary btn-icon sq-24"><i class="icon icon-edit"></i></a>';
                        var btn_delete = '<a href="javascript:;" title="Hapus data" class="btn btn-outline-danger btn-icon sq-24 btn-delete" data-id="' + row.id + '"><i class="icon icon-trash"></i></a>';
                        return btn_edit + ' ' + btn_delete;
                    }
                }
            ]
        });
    };

    samdque.dspvidTable = function ($element, listUrl, csrf, url) {
        if (!$element.length) return null;
        return $element.DataTable({
            processing: true,
            serverSide: true,
            "deferRender": true,
            responsive: true,
            ajax: {
                'url': listUrl,
                'type': 'POST',
                'headers': {
                    'X-CSRF-TOKEN': csrf
                }
            },
            dom: 'lBfrtip',
            "order": [[1, 'asc']],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            columns: [
                {
                    data: 'id',
                    name: 'id',
                    "orderable": false,
                    "searchable": false
                },
                {data: 'title', name: 'title'},
                {data: 'description', name: 'description'},
                {
                    data: 'status', name: 'status', 'mRender': function (data) {
                    var status;
                    if (data == 'A') {
                        status = '<span class="label label-outline-primary">Aktif</span>';
                    } else if (data = 'NA') {
                        status = '<span class="label label-outline-danger">Non Aktif</span>';
                    }
                    return status;
                }
                },
                {
                    "data": 'id',
                    "defaultContent": '',
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {
                        var btn_edit = '<a href="/dsp/vid/manage/' + row.id + '" title="Ubah data" class="btn btn-outline-primary btn-icon sq-24"><i class="icon icon-edit"></i></a>';
                        var btn_delete = '<a href="javascript:;" title="Hapus data" class="btn btn-outline-danger btn-icon sq-24 btn-delete" data-id="' + row.id + '"><i class="icon icon-trash"></i></a>';
                        return btn_edit + ' ' + btn_delete;
                    }
                }
            ]
        });
    };

    function orderNumber($datatable) {
        $datatable.on('order.dt search.dt draw.dt', function () {
            var page = $datatable.page.info().page;
            $datatable.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + (page * 10);
            });
        }).draw();
    }

    function toggleStatus(id) {
        alert(id);
    }

    $(document).ready(function () {        

        var $runningtext = samdque.dsprunningTextTable($('#dsp_table_running_text'), '/dsp/runningtext/get-list', $('meta[name="csrf-token"]').attr('content'));
        if ($runningtext) {
            orderNumber($runningtext);
        }

        var $banner = samdque.dspbannerTable($('#dsp_banner_table'), '/dsp/banner/get-list', $('meta[name="csrf-token"]').attr('content'), $('#dsp_banner_table').data('url'));
        if ($banner) {
            orderNumber($banner);
        }

        var $adv = samdque.dspadvTable($('#dsp_adv_table'), '/dsp/adv/get-list', $('meta[name="csrf-token"]').attr('content'), $('#dsp_adv_table').data('url'));
        if ($adv) {
            orderNumber($adv);
        }

        var $vid = samdque.dspvidTable($('#dsp_vid_table'), '/dsp/vid/get-list', $('meta[name="csrf-token"]').attr('content'), $('#dsp_vid_table').data('url'));
        if ($vid) {
            orderNumber($vid);
        }

        /*socket io*/
        var socket = io.connect(full_url + ':8890');
        socket.on('message', function (data) {
            if (data == 'dsp_runningtext') {
                $runningtext.ajax.reload(null, false);
            }
            if (data == 'dsp_banner') {
                $banner.ajax.reload(null, false);
            }
            if (data == 'dsp_adv') {
                $adv.ajax.reload(null, false);
            }
            if (data == 'dsp_vid') {
                $vid.ajax.reload(null, false);
            }
        });  

        $(document).on('click', '.btn-status', function(){
            var id = $(this).attr('id').split('-')[1];        

            alert(id);
        });      
    });

})(jQuery, window);