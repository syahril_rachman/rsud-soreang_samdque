<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sm_kunjungan extends Model
{   
	protected  $table = "sm_kunjungans";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'visitidx', 'IdBankNomor', 'IdRuangPerujuk', 'NamaRuangPerujuk',
        'AliasNamaRuangPerujuk', 'IdDokterPerujuk', 'NamaDokterPerujuk',
        'TglPesan', 'TglAntrian', 'TglAmbilHasil',
        'position', 'type', 'noqueue'
    ];
}
