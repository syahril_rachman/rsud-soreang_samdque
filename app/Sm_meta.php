<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sm_meta extends Model
{
    protected  $table = "sm_metas";
    protected $primaryKey = 'value';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value', 'parameter', 'description', 'link'
    ];
}
