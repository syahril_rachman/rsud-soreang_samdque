<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dsp_setting extends Model
{
    protected  $table = "dsp_settings";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'type', 'status', 'file', 'setting'
    ];

    public function getSettingAttribute($value)
    {
        return json_decode($value, true);
    }
}
