<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sm_ruangan extends Model
{
    protected  $table = "sm_ruangans";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idRuangPelayanan', 'namaRuangPelayanan', 'aliasRuangPelayanan'
    ];
}
