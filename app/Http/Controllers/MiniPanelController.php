<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Service\Pendaftaran_Service;
use \App\Sm_meta;
use Carbon\Carbon;
use LRedis;

class MiniPanelController extends Controller
{
    private $redis;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LRedis $lredis)
    {
        $this->middleware('auth');
        $this->redis = $lredis::connection();
    }

    private $stat = array(
        'WT' => '<span class="label label-default">Online</span>',
        'PD' => '<span class="label label-warning">Pending</span>',
        'DN' => '<span class="label label-primary">Done</span>',
        'PR' => '<span class="label label-success">On Process</span>',
        'CL' => '<span class="label label-danger">Canceled</span>',
    );

    public function miniPanel() {
        $user = Auth::user();

        $queue = null;
        $link = null;
        $restq = 0;
        if($user){            
        	$queue = Pendaftaran_Service::getOnProcessQueuebyLoket($user->id);            
            if ($queue) {
                $link = Sm_meta::find($queue->metavalue)->link;
                $restq = Pendaftaran_Service::getRestQueueCount($queue->metavalue);
            }
        }
        
        $init = false;
        return view('main.mini_panel', compact(['queue', 'user', 'link', 'init', 'restq']));
    }
	
	public function queue_detail() {
		$user = Auth::user();

        $queue = null;
        $link = null;
        if($user){
        	$queue = Pendaftaran_Service::getOnProcessQueuebyLoket($user->id);
            if ($queue) {
                $link = Sm_meta::find($queue->metavalue)->link;
            }
        }

        return $queue;
	}

    public function nextQueue(Request $request) {    	
    	$prevqueue = $request['regid'];    	
    	$user = Auth::user();

        $queue = null;
        $link = null;
        $restq = 0;
        if($user){        	
        	$update = array('status' => 'DN', 'updated_by' => $user->id);        	
        	if (!$prevqueue||Pendaftaran_Service::updatePendaftaran($prevqueue, $update)) {
        		$queue = Pendaftaran_Service::getNextQueue($user->userlevel);        		
	            if ($queue) {
	            	$setonprocess = array('status' => 'PR', 'updated_by' => $user->id);
	            	Pendaftaran_Service::updatePendaftaran($queue->regid, $setonprocess);
	                $link = Sm_meta::find($queue->metavalue)->link;	                                    
	            }
        	}        	
        }

        return response()->json(['queue' => $queue, 'user' => $user, 'link' => $link, 'restq' => $restq]);
        // $init = true;
        // return view('main.mini_panel', compact(['queue', 'user', 'link', 'init']));
    }

    public function pendingQueue() {

    }

    public function getRestq($metavalue) {
        $restq = Pendaftaran_Service::getRestQueueCount($metavalue);

        return response()->json(['restq' => $restq]);
    }
}
