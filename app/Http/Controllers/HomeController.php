<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Service\Date_Service;
use \App\Service\Meta_Service;
use \App\Service\Pendaftaran_Service;
use \App\Sm_ruangan;
use \App\Sm_meta;
use \App\Sm_pendaftaran;
use Carbon\Carbon;
use Datatables;
use LRedis;

class HomeController extends Controller
{
    private $redis;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LRedis $lredis)
    {
        $this->middleware('auth');
        $this->redis = $lredis::connection();
    }

    private $stat = array(
        'WT' => '<span class="label label-default">Online</span>',
        'PD' => '<span class="label label-warning">Pending</span>',
        'DN' => '<span class="label label-primary">Done</span>',
        'PR' => '<span class="label label-success">On Process</span>',
        'CL' => '<span class="label label-danger">Canceled</span>',
    );

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $cbn = Carbon::now('Asia/Jakarta');
        $date = $cbn->format('Y-m-d');

        // $now = Date_Service::getModedDate(date('N'), date('j'), date('m'), date('Y'));
        $now = Date_Service::getModedDate($cbn->dayOfWeek, $cbn->day, $cbn->month, $cbn->year);

        $stat = $this->stat;
        $room = Sm_ruangan::get();
        $metatype = Meta_Service::metalist('FIRST');
        $pageTitle = 'Beranda';
        $mnuHome = 'active';
        $qlist = Pendaftaran_Service::getAllQueueKiosk($date);

        $metas = Meta_Service::metalist('FIRST');
        // return $stat;
        return view('home', compact('now', 'stat', 'room', 'metatype', 'pageTitle', 'mnuHome', 'qlist', 'metas'));
    }    

    public function getListKiosk()
    {
        $cbn = Carbon::now('Asia/Jakarta');
        $date = $cbn->format('Y-m-d');

        $ulevel = Auth::user()->userlevel;
        if ($ulevel) {
            $qlist = Pendaftaran_Service::getQueueKiosk($date, $ulevel);
        } else {
            $qlist = Pendaftaran_Service::getAllQueueKiosk($date);
        }

        $datatable = Datatables::of($qlist);
        return $datatable->make(true);
    }

    public function listKiosk()
    {
        // $date = date('Y-m-d');
        // $now = Date_Service::getModedDate(date('N'), date('j'), date('m'), date('Y'));
        $cbn = Carbon::now('Asia/Jakarta');
        $date = $cbn->format('Y-m-d');
        $now = Date_Service::getModedDate($cbn->dayOfWeek, $cbn->day, $cbn->month, $cbn->year);

        $stat = $this->stat;
        $room = Sm_ruangan::get();
        $metatype = Meta_Service::metalist('FIRST');

        $qlist = Pendaftaran_Service::getAllQueueKiosk($date);
        if (isset($qlist)) {
            foreach ($qlist as $key => $value) {
                $disabled = '';
                if ($value->is_calling) {
                    $disabled = 'disabled = "disabled"';
                }

                $str = "<tr id='tr-" . $value->regid . "'>" .
                    "<td id='td-" . $value->regid . "' width='50%'>" .
                    "<strong>" . $value->noqueue . "</strong>" .
                    $stat[$value->status] .
                    "<span class='label label-default'>" .
                    $metatype[$value->metavalue]->description .
                    "</span><br/>" .
                    $value->timequeue .
                    "</td>" .
                    "<td align='right' width='50%'>" .
                    "<button" .
                    "type='button' " .
                    "value='Pendaftaran Kiosk' " .
                    "class='btn btn-primary btn-icon sq-24 btn-bullhorn' " .
                    $disabled .
                    "id='bl-" . $value->noqueue . "-" . $value->metavalue . "-" . $value->regid . "' " .
                    "title='Panggil Antrian'>" .
                    "<i class='icon icon-bullhorn'></i>" .
                    "</button>" .
                    "<button " .
                    "type='button' " .
                    "value='Pendaftaran Kiosk' " .
                    "class='btn btn-danger btn-icon sq-24 btn-cancel' " .
                    "id='cl-" . $value->noqueue . "-" . $value->metavalue . "-" . $value->regid . "' " .
                    "title='Batal Antrian'>" .
                    "<i class='icon icon-trash'></i>" .
                    "</button>" .
                    "<button " .
                    "type='button' " .
                    "value='Pendaftaran Kiosk' " .
                    "class='btn btn-warning btn-icon sq-24 btn-pending' " .
                    "id='pd-" . $value->noqueue . "-" . $value->metavalue . "-" . $value->regid . "' " .
                    "title='Ubah Status: Pending'>" .
                    "<i class='icon icon-bookmark-o'></i>" .
                    "</button>" .
                    "<button " .
                    "type='button' " .
                    "value='Pendaftaran Kiosk' " .
                    "class='btn btn-info btn-icon sq-24 btn-online' " .
                    "id='ol-" . $value->noqueue . "-" . $value->metavalue . "-" . $value->regid . "' " .
                    "title='Ubah Status: On Line'>" .
                    "<i class='icon icon-bookmark'></i>" .
                    "</button>" .
                        // "<button " .
                        // "type='button' " .
                        // "value='Pendaftaran Kiosk' " .
                        // "class='btn btn-default btn-icon sq-24 btn-scan' " .
                        // "id='sc-" . $value->noqueue . "-" . $value->metavalue . "-" . $value->regid . "' " .
                        // "title='Scan Barcode'>" .
                        // "<i class='icon icon-barcode'></i>" .
                        // "</button>" .
                        // "<button " .
                        // "type='button' " .
                        // "value='Pendaftaran Kiosk' " .
                        // "class='btn btn-default btn-icon sq-24 btn-register' " .
                        // "id='rg-" . $value->noqueue . "-" . $value->metavalue . "-" . $value->regid . "' " .
                        // "title='Pendaftaran Manual'>" .
                        // "<i class='icon icon-user-plus'></i>" .
                        // "</button>" .
                    "</td>" .
                    "</tr>";
            }
        } else {
            $str = "<tr><td colspan='2'>Tidak ada antrian...</td></tr>";
        }

        return $str;
    }

    //MERGING SOUND
    public function mergeSound1($noqueue, $metavalue, $desc)
    {
      try {
        //variable
        $link = Sm_meta::find($metavalue)->link;
        $noqueue = (int) $noqueue;
        $queueArray = str_split($noqueue);
        $ratusan = "";
        $puluhan = "";
        $satuan = "";

        //additional
        $soundLink = './assets/sounds/google/';
        $soundType = '.MP3';
        $soundQueue = file_get_contents($soundLink .'na' . $soundType);
        $soundQueue .= file_get_contents($soundLink .$link . $soundType);
      
        if ($noqueue >= 100) {
            $ratusan = $queueArray[0];
            $puluhan = $queueArray[1];
            $satuan = $queueArray[2];

            $soundQueue .= file_get_contents($soundLink . $ratusan . '00' . $soundType);
            if ($puluhan == 0) {
                if ($satuan != 0) {
                    $soundQueue .= file_get_contents($soundLink . $satuan  . $soundType);
                }
            } else if ($puluhan == 1) {
                $belasan = $puluhan.$satuan;
                $soundQueue .= file_get_contents($soundLink . $belasan . $soundType);
            } else {
                $soundQueue .= file_get_contents($soundLink . $puluhan . '0' . $soundType);
                if ($satuan != 0) {
                    $soundQueue .= file_get_contents($soundLink . $satuan  . $soundType);
                }
            }
        } else if ($noqueue >= 10 && $noqueue < 100) {
            $puluhan = $queueArray[0];
            $satuan = $queueArray[1];

            if ($puluhan == 1) {
                $belasan = $puluhan.$satuan;
                $soundQueue .= file_get_contents($soundLink . $belasan . $soundType);
            } else {
                $soundQueue .= file_get_contents($soundLink . $puluhan . '0' . $soundType);
                if ($satuan != 0) {
                    $soundQueue .= file_get_contents($soundLink . $satuan  . $soundType);
                }                
            }
        } else if ($noqueue > 0 && $noqueue < 10) {
            $satuan = $queueArray[0];
            $soundQueue .= file_get_contents($soundLink . $satuan . $soundType);
        }

        $soundQueue .= file_get_contents($soundLink . 'kl' . $soundType);    
        $loket_name = Auth::user()->loket_name;
        if (str_contains($loket_name, "Loket")) {
            $loket = explode(" ", $loket_name);
            if (str_contains($loket[1], ".")) {
                $loketNames = explode(".", $loket[1]);
            } else {
                $loketNames = str_split($loket[1]);
            }
            foreach ($loketNames as $key => $value) {
                $soundQueue .= file_get_contents($soundLink . $value . $soundType);   
            }
        }

        $newname = $noqueue . '_' . $metavalue . '.mp3';
        $name = $noqueue . '_' . $metavalue ;

        $url = './assets/sounds/temp/' . $newname;
        @unlink($url);
        clearstatcache(TRUE, $url);
        if (file_exists($url)) throw new Exception('file not deleted : ' . $url);

        file_put_contents('./assets/sounds/temp/' . $newname, $soundQueue);

        //update sm_meta voice
        // $update = array('description' => $newname);
        // Sm_meta::where('parameter', '=', 'VOICE')->update($update);
        $update = array('voice' => $newname);
        Sm_meta::where('value', $metavalue)->update($update);

        //update sm_meta voice
        $update_play_sound = array('is_calling' => true);
        Sm_pendaftaran::where('metavalue', '=', $metavalue)->update($update_play_sound);

        $this->redis->publish('kiosk-update', $metavalue);

        $data = array('sound' => $name);
      } catch (\Exception $e) {
            $update = array('voice' => '');
            Sm_meta::where('value', $metavalue)->update($update);

            //update sm_meta voice
            $update_play_sound = array('is_calling' => false);
            Sm_pendaftaran::where('metavalue', '=', $metavalue)->update($update_play_sound);

            $this->redis->publish('kiosk-update', $metavalue);

            $data = ['sound' => '', 'error' => $e->getMessage()];
      }

        return response()->json($data);
    }

    public function playSound1($regid, $metavalue)
    {
        $v = Sm_meta::where('value', $metavalue)->first();
        if (!$v) {
            return '-';
        }

        $url = './assets/sounds/temp/' . $v->voice;
        if (!file_exists($url)) {
            return '-';
        }

        $data = array('sound' => $v->voice);

        $sm_pendaftaran = Sm_pendaftaran::where('regid', '=', $regid)->first();
        $link = Sm_meta::find($metavalue)->link;
        $this->sendRedisFront($link, $sm_pendaftaran->noqueue, 'single');

        return response()->json($data);
    }    

    public function deleteSound1($file, $metavalue, $regid)
    {
        //update sm_meta voice
        $update_play_sound = array('is_calling' => false);
        Sm_pendaftaran::where('metavalue', '=', $metavalue)->update($update_play_sound);

        $data['status'] = 'PR';
        $data['updated_by'] = Auth::user()->id;
        Sm_pendaftaran::where('regid', '=', $regid)->update($data);            

        $files = glob('./assets/sounds/temp/' . $file); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
            $update = array('voice' => '');
            Sm_meta::where('value', '=', $metavalue)->update($update);
            // $this->mainmodel->edit('parameter', 'VOICE', $update, 'sm_meta');
        }

        $this->redis->publish('kiosk-update', $metavalue);
        $this->redis->publish('message', 'btn-yes');
    }

    public function deleteSoundAll1($metavalue)
    {
        $files = glob('./assets/sounds/temp/*'); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
            $update = array('voice' => '');
            Sm_meta::where('value', '=', $metavalue)->update($update);
            // $this->mainmodel->edit('parameter', 'VOICE', $update, 'sm_meta');
        }
    }    

    public function updateStatusKiosk($id, $status)
    {
        $data = array('status' => $status);
        try {
            if ($status == "PR") {
                $data['updated_by'] = Auth::user()->id;
            }

            Sm_pendaftaran::where('regid', '=', $id)->update($data);
            $metavalue = Sm_pendaftaran::where('regid', '=', $id)->first();
            
            if ($status == "PR") {
                $noqueue = $metavalue->noqueue;
                $link = Sm_meta::find($metavalue->metavalue)->link;
                $type = 'single';
            } else {
                $noqueue = '-';
                $link = '';
                $type = 'hide';
            }
            $this->sendRedisFront($link, $noqueue, $type);
            $this->redis->publish('kiosk-update', $metavalue->metavalue);
            $success = true;
            $msg = "Data updated";
        } catch (\Exception $e) {
            $success = false;  
            $msg = "Error updating data";
        }

        return response()->json(['success' => $success, 'msg' => $msg]);
    }

    public function sendRedisFront($link, $noqueue, $type) 
    {
        $this->redis->publish('front', 
            json_encode([
                    'link' => $link, 
                    'queue' => $noqueue, 
                    'type' => $type, 
                    'loket_name' => Auth::user()->loket_name, 
                    'id' => Auth::user()->id
                    ], true));
    }

    public function resetIsCalling() {
        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        $data = array('is_calling' => false);
        Sm_pendaftaran::whereDate('datequeue', $now)->update($data);
    }
}
