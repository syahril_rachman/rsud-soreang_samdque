<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;
use File;
use LRedis;

class UserController extends Controller
{
    private $redis;

    public function __construct(LRedis $lredis)
    {
        $this->redis = $lredis::connection();
    }

    public function index()
    {
        return view('user.index');
    }

    public function listUser()
    {
        $users = User::with('roles')->get();
        $datatable = Datatables::of($users);
        return $datatable->make(true);
    }

    public function getCreate()
    {
        $user = null;
        $roles = Role::get();
        return view('user.manage', compact('roles', 'user'));
    }

    public function getEdit($id)
    {
        $user = User::with(['roles'])->find($id);
        $roles = Role::get();
        return view('user.manage', compact('roles', 'user'));
    }

    public function postManage(Request $request)
    {
        $input = $request->all();

        $path = 'assets/img/pict';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        if (isset($input['userfile'])) {
            $filename = str_replace(' ', '_', $input['userfile']->getClientOriginalName());
            $input['userfile']->move($path, $filename);
            $input['upload'] = $path . '/' . $filename;
        }


        if (!$input['user_id']) {
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $user->roles()->attach($input['userlevel']);
        } else {
            if (!$input['password']) {
                unset($input['password']);
            } else {
                $input['password'] = bcrypt($input['password']);
            }
            $user = User::find($input['user_id']);
            if (isset($input['upload'])) {
                File::delete($user->upload);
            }

            $user->roles()->detach();
            $user->roles()->attach($input['userlevel']);
            $user->update($input);
        }

        $this->redis->publish('message', 'user');

        return redirect('/user')->with('status', 'Data berhasil disimpan ke dalam aplikasi');
    }

    public function deleteUser(Request $request)
    {
        $input = $request->all();
        $user = User::find($input['id']);
        File::delete($user->upload);
        $user->delete();
        $this->redis->publish('message', 'user');
        return redirect('/user')->with('status', 'Data berhasil dihapus dari aplikasi');
    }

    public function getProfile(){
        $user = User::with(['roles'])->find(Auth::user()->id);
        $roles = Role::get();
        return view('user.profile', compact(['user', 'roles']));
    }
}
