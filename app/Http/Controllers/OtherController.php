<?php

namespace App\Http\Controllers;

use App\Sm_meta;
use App\Sm_setting;
use App\Sm_pendaftaran;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;
use File;
use LRedis;


class OtherController extends Controller
{
    private $redis;

    public function __construct(LRedis $lredis)
    {
        $this->redis = $lredis::connection();
    }

    private $settingType = [
        'RT',
        'BN',
        'ADV'
    ];

    public function runningText(){
        return view('other.running_text');
    }
    public function runningTextList(){
        $runningtext = Sm_setting::where('settingtype', 'RT')->get();
        $datatable = Datatables::of($runningtext);
        return $datatable->make(true);
    }

    public function runningTextGetCreate(){
        $rt = null;
        return view('other.running_text_manage', compact('rt'));
    }

    public function runningTextGetEdit($id){
        $rt = Sm_setting::where('settingid', $id)->first();
        return view('other.running_text_manage', compact('rt'));
    }

    public function runningTextPostCreate(Request $request){

        $input = $request->all();
        if(!isset($input['settingstat'])){
            $input['settingstat'] = 'NA';
        }
        $input['settingtype'] = $this->settingType[0];
        $input['setting'] = json_encode(['font' => $input['setting_font']], true);

        if(!$input['rt_id']){
            Sm_setting::create($input);
        }else{
            $rt = Sm_setting::where('settingid', $input['rt_id'])->first();
            $rt->update($input);
        }

        $this->redis->publish('message', 'runningtext');
        return redirect('/other/runningtext/list')->with('status', 'Data berhasil disimpan ke dalam aplikasi');
    }

    public function runningTextDelete(Request $request){
        $input = $request->all();
        $rt = Sm_setting::where('settingid', $input['id'])->first();
        $rt->delete();
        $this->redis->publish('message', 'runningtext');
        return redirect('/other/runningtext/list')->with('status', 'Data berhasil dihapus dari aplikasi');
    }

    public function banner(){
        return view('other.banner');
    }

    public function bannerList(){
        $banner = Sm_setting::where('settingtype', 'BN')->get();
        $datatable = Datatables::of($banner);
        return $datatable->make(true);
    }

    public function bannerGetCreate(){
        $bn = null;
        return view('other.banner_manage', compact('bn'));
    }

    public function bannerGetEdit($id){
        $bn = Sm_setting::where('settingid', $id)->first();
        return view('other.banner_manage', compact('bn'));
    }

    public function bannerPostCreate(Request $request){
        $input = $request->all();

        if(!isset($input['settingstat'])){
            $input['settingstat'] = 'NA';
        }
        $input['settingtype'] = $this->settingType[1];


        $path = 'assets/img/banner';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        if (isset($input['userfile'])) {
            $filename = str_replace(' ', '_', $input['userfile']->getClientOriginalName());
            $input['userfile']->move($path, $filename);
            $input['settingvalue'] = $path . '/' . $filename;
        }


        if(!$input['bn_id']){
            Sm_setting::create($input);
        }else{
            $rt = Sm_setting::where('settingid', $input['bn_id'])->first();
            if (isset($input['settingvalue'])) {
                File::delete($rt->settingvalue);
            }
            $rt->update($input);
        }

        $this->redis->publish('message', 'banner');
        return redirect('/other/banner/list')->with('status', 'Data berhasil disimpan ke dalam aplikasi');
    }

    public function bannerDelete(Request $request){
        $input = $request->all();
        $bn = Sm_setting::where('settingid', $input['id'])->first();
        File::delete($bn->settingvalue);
        $bn->delete();
        $this->redis->publish('message', 'banner');
        return redirect('/other/banner/list')->with('status', 'Data berhasil dihapus dari aplikasi');
    }


    public function adv(){
        return view('other.adv');
    }

    public function advList(){
        $adv = Sm_setting::where('settingtype', 'ADV')->get();
        $datatable = Datatables::of($adv);
        return $datatable->make(true);
    }

    public function advGetCreate(){
        $adv = null;
        return view('other.adv_manage', compact('adv'));
    }

    public function advGetEdit($id){
        $adv = Sm_setting::where('settingid', $id)->first();
        return view('other.adv_manage', compact('adv'));
    }

    public function advPostCreate(Request $request){
        $input = $request->all();

        if(!isset($input['settingstat'])){
            $input['settingstat'] = 'NA';
        }
        $input['settingtype'] = $this->settingType[2];


        $path = 'assets/img/adv';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        if (isset($input['userfile'])) {
            $filename = str_replace(' ', '_', $input['userfile']->getClientOriginalName());
            $input['userfile']->move($path, $filename);
            $input['settingvalue'] = $path . '/' . $filename;
        }


        if(!$input['adv_id']){
            Sm_setting::create($input);
        }else{
            $rt = Sm_setting::where('settingid', $input['adv_id'])->first();
            if (isset($input['settingvalue'])) {
                File::delete($rt->settingvalue);
            }
            $rt->update($input);
        }
        $this->redis->publish('message', 'adv');
        return redirect('/other/adv/list')->with('status', 'Data berhasil disimpan ke dalam aplikasi');
    }

    public function advDelete(Request $request){
        $input = $request->all();
        $adv = Sm_setting::where('settingid', $input['id'])->first();
        File::delete($adv->settingvalue);
        $adv->delete();
        $this->redis->publish('message', 'adv');
        return redirect('/other/banner/list')->with('status', 'Data berhasil dihapus dari aplikasi');
    }
    
    public function resetQData(Request $request) {
        $input = $request->all();
        $metavalue = $input['meta'];
        $startfrom = $input['startfrom'];
        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d');

        $meta = '';
        if ($metavalue == 'all') {
            $queues = Sm_pendaftaran::where('noqueue', '>=', $startfrom)
                                    ->whereDate('datequeue', $now)
                                    ->get();
            foreach ($queues as $key => $queue) {
                $queue->delete();
            }

            $metas = Sm_meta::where('parameter', 'FIRST')->get();
            foreach ($metas as $key => $meta) {
                $record_total = Sm_pendaftaran::where('metavalue', $meta->value)
                                                ->whereDate('datequeue', $now)
                                                ->count();                

                $this->redis->publish('kiosk-update', $meta->value);
                $this->redis->publish('front', json_encode(['new_total' => $record_total, 'metavalue' => $meta->value, 'type' => 'list'], true));
            }
        } else {
            $queues = Sm_pendaftaran::where('metavalue', $metavalue)
                                    ->where('noqueue', '>=', $startfrom)
                                    ->whereDate('datequeue', $now)
                                    ->get();
            foreach ($queues as $key => $queue) {
                $queue->delete();
            }

            $record_total = Sm_pendaftaran::where('metavalue', $metavalue)
                                                ->whereDate('datequeue', $now)
                                                ->count(); 

            $this->redis->publish('kiosk-update', $metavalue);
            $this->redis->publish('front', json_encode(['new_total' => $record_total, 'metavalue' => $metavalue, 'type' => 'list'], true));
        }        
                
        return redirect('/home');
    }
}
