<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use \App\Service\Setting_Service;
use \App\Service\Meta_Service;
use \App\Service\Pendaftaran_Service;
use \App\Service\Date_Service;
use \App\Sm_pendaftaran;
use \App\Sm_meta;
use LRedis;

class FrontController extends Controller
{
    private $redis;

    public function __construct(LRedis $lredis)
    {
        $this->redis = $lredis::connection();
    }

    public function kiosk(){    
        $adv = Setting_Service::srvdetail('ADV');
        $banner = Setting_Service::srvdetail('BN');
        $runningtext = Setting_Service::srvdetail('RT');
        $pageTitle = 'Pendaftaran Kiosk';

        $metas = Meta_Service::metalist('FIRST');

        return view('front.kiosk', compact('adv', 'banner', 'runningtext', 'pageTitle', 'metas'));
    }

    public function inputKiosk($id){
        $cbn = Carbon::now('Asia/Jakarta');
        $date = $cbn->format('Y-m-d');
        $count = 1;
        $chexist = Pendaftaran_Service::countQueueKiosk($id, $date);
        if($chexist > 0){
            /* modul no antrian berdasarkan menu meta*/
          $det = Pendaftaran_Service::detailKioskMaxQueue($id, $date);
          $count = $count + $det[0]['noqueue'];
            /* modul no antrian berdasarkan menu meta*/

            /* modul no antrian gak liat menu meta*/
            // $count = $count + $chexist; 
        }
        $time = $cbn->format('h:i:s');
        
        $regid = $id.$cbn->format('dmy').str_pad($count, 3, "0", STR_PAD_LEFT);
        $input = array(
          'regid' => $regid,
          'noqueue' => $count,
          'datequeue' => $date,
          'timequeue' => $time,
          'status' => 'WT',
          'metavalue' => $id
        );
        Sm_pendaftaran::create($input);

        $sm_pendaftaran = Sm_pendaftaran::where('metavalue', $id)->whereDate('datequeue', $date)->get();
        $record_total = count($sm_pendaftaran);

        $this->redis->publish('kiosk-update', $id);
        $this->redis->publish('front', json_encode(['new_total' => $record_total, 'metavalue' => $id, 'type' => 'list'], true));
        return response()->json(['noqueue' => $count, 'metavalue' => $id]);
    }

    //PRINT FORMAT
    public function printStruk($noqueue, $metavalue){
        // $now = Date_Service::getModedDate(date('N'), date('j'), date('m'), date('Y'));
        // $datetime_now = Carbon::now()->format('Y-m-d H:i:s');
        // $now = Carbon::createFromFormat('H:i:s', $datetime_now, 'Asia/Jakarta');

        $cbn = Carbon::now('Asia/Jakarta');
        $now = Date_Service::getModedDate($cbn->dayOfWeek, $cbn->day, $cbn->month, $cbn->year);
        $time = $cbn->format('H:i:s');

        $metavalue = Meta_Service::metadetail('FIRST', $metavalue);
        $oldnoqueue = str_pad($noqueue, 3, '0', STR_PAD_LEFT);
        $noqueue = $metavalue->link.$oldnoqueue;
        // return $now.' '.$metavalue.' '.$noqueue;
        return view('front.page_print', compact('now', 'metavalue', 'noqueue', 'time'));
    }

    public function register(){
        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        $metavalue_2 = count(Sm_pendaftaran::where('metavalue', 2)->whereDate('datequeue', $now)->get());
        $metavalue_3 = count(Sm_pendaftaran::where('metavalue', 3)->whereDate('datequeue', $now)->get());
        $metavalue_4 = count(Sm_pendaftaran::where('metavalue', 4)->whereDate('datequeue', $now)->get());
        $metavalue_5 = count(Sm_pendaftaran::where('metavalue', 5)->whereDate('datequeue', $now)->get());
        $metavalue_6 = count(Sm_pendaftaran::where('metavalue', 6)->whereDate('datequeue', $now)->get());

        $opr_2 = Sm_pendaftaran::where('metavalue', 2)->where('status', 'PR')->whereDate('datequeue', $now)->orderBy('noqueue', 'desc')->first();
        $opr_3 = Sm_pendaftaran::where('metavalue', 3)->where('status', 'PR')->whereDate('datequeue', $now)->orderBy('noqueue', 'desc')->first();
        $opr_4 = Sm_pendaftaran::where('metavalue', 4)->where('status', 'PR')->whereDate('datequeue', $now)->orderBy('noqueue', 'desc')->first();
        $opr_5 = Sm_pendaftaran::where('metavalue', 5)->where('status', 'PR')->whereDate('datequeue', $now)->orderBy('noqueue', 'desc')->first();
        $opr_6 = Sm_pendaftaran::where('metavalue', 6)->where('status', 'PR')->whereDate('datequeue', $now)->orderBy('noqueue', 'desc')->first();

        if ($opr_2) {
            $opr_2 = $opr_2->noqueue;
        }
        if ($opr_3) {
            $opr_3 = $opr_3->noqueue;
        }
        if ($opr_4) {
            $opr_4 = $opr_4->noqueue;
        }
        if ($opr_5) {
            $opr_5 = $opr_5->noqueue;
        }
        if ($opr_6) {
            $opr_6 = $opr_6->noqueue;
        }

        return view(
                'front.register',
                compact([
                    'metavalue_2',
                    'metavalue_3',
                    'metavalue_4',
                    'metavalue_5',
                    'metavalue_6',
                    'opr_2',
                    'opr_3',
                    'opr_4',
                    'opr_5',
                    'opr_6'
                ])
            );
    }
    public function oneRegister($param){
        $param = strtolower($param);
        $user = User::where('loket_name', 'like', '%' .$param. '%')->first();

        $metavalue = null;
        $link = null;
        if($user){
            $metavalue = Pendaftaran_Service::getOnProcessQueuebyLoket($user->id);
            if ($metavalue) {
                $link = Sm_meta::find($metavalue->metavalue)->link;
            }
        }
        
        return view('front.register_1_loket', compact(['metavalue', 'user', 'link']));
    }

    public function twoRegister($param1, $param2){
        $param1 = strtolower($param1);
        $user1 = User::where('loket_name', 'like', '%' .$param1. '%')->first();
        $param2 = strtolower($param2);
        $user2 = User::where('loket_name', 'like', '%' .$param2. '%')->first();        

        $metavalue1 = null;
        $link1 = null;
        if($user1){
            $metavalue1 = Pendaftaran_Service::getOnProcessQueuebyLoket($user1->id);
            if ($metavalue1) {
                $link1 = Sm_meta::find($metavalue1->metavalue)->link;
            }
        }

        $metavalue2 = null;
        $link2 = null;
        if($user2){
            $metavalue2 = Pendaftaran_Service::getOnProcessQueuebyLoket($user2->id);
            if ($metavalue2) {
                $link2 = Sm_meta::find($metavalue2->metavalue)->link;
            }
        }

        return view('front.register_2_loket', compact(['link1', 'link2', 'metavalue1', 'metavalue2', 'user1', 'user2']));
    }

    public function examine(){
        return view('front.examine');
    }
    public function result(){
        return view('front.result');
    }    

    public function apiDspList($type) {
        $result = [];
        try{
            $datas = Setting_Service::dspdetail($type);
            $result = ['datas' => $datas, 'isSuccess' => true, 'message' => 'Sucess berhasil'];
        }catch(\Exception $e){
            $result = ['isSuccess' => false, 'message' => $e->getMessage()];
        }

        return response()->json($result);
    }

    public function getOnProcessQueue($metavalue) {
        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        $opr = Sm_pendaftaran::where('status', 'PR')
                                ->where('metavalue', $metavalue)
                                ->whereDate('datequeue', $now)
                                ->orderBy('noqueue', 'desc')
                                ->first();                                

        $res = null;
        if ($opr) {
            $res = $opr->noqueue;
        }

        return response()->json(['opr' => $res]);
    }

    public function apiCountQueueAllMeta() {
        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        $metavalue_2 = count(Sm_pendaftaran::where('metavalue', 2)->whereDate('datequeue', $now)->get());
        $metavalue_3 = count(Sm_pendaftaran::where('metavalue', 3)->whereDate('datequeue', $now)->get());
        $metavalue_4 = count(Sm_pendaftaran::where('metavalue', 4)->whereDate('datequeue', $now)->get());
        $metavalue_5 = count(Sm_pendaftaran::where('metavalue', 5)->whereDate('datequeue', $now)->get());
        $metavalue_6 = count(Sm_pendaftaran::where('metavalue', 6)->whereDate('datequeue', $now)->get());
    }
}
