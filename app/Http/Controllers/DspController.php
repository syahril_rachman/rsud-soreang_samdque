<?php

namespace App\Http\Controllers;

use App\Dsp_setting;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use File;
use LRedis;
use \App\Service\Setting_Service;

class DspController extends Controller
{
    private $redis;

    public function __construct(LRedis $lredis)
    {
        $this->redis = $lredis::connection();
    }

    private $type = [
        'RT',
        'BN',
        'ADV',
        'VID'
    ];

    public function runningText(){
        return view('dsp.running_text');
    }

    public function runningTextList(){
        $runningtext = Dsp_setting::where('type', 'RT')->get();
        $datatable = Datatables::of($runningtext);
        return $datatable->make(true);
    }

    public function runningTextGetCreate(){
        $rt = null;
        return view('dsp.running_text_manage', compact('rt'));
    }

    public function runningTextGetEdit($id){
        $rt = Dsp_setting::where('id', $id)->first();
        return view('dsp.running_text_manage', compact('rt'));
    }

    public function runningTextPostCreate(Request $request){

        $input = $request->all();        
        if(!isset($input['status'])){
            $input['status'] = 'NA';
        }
        $input['type'] = $this->type[0];
        $input['title'] = 'new runningtext';
        $input['setting'] = json_encode(['font' => $input['setting_font']], true);

        if(!$input['rt_id']){
            Dsp_setting::create($input);
        }else{
            $rt = Dsp_setting::where('id', $input['rt_id'])->first();
            $rt->update($input);
        }

        $this->redis->publish('message', 'dsp_runningtext');
        return redirect('/dsp/runningtext/list')->with('status', 'Data berhasil disimpan ke dalam aplikasi');
    }    

    public function runningTextDelete(Request $request){
        $input = $request->all();
        $rt = Dsp_setting::where('id', $input['id'])->first();
        $rt->delete();
        $this->redis->publish('message', 'dsp_runningtext');
        return redirect('/dsp/runningtext/list')->with('status', 'Data berhasil dihapus dari aplikasi');
    }

    public function banner(){
        return view('dsp.banner');
    }

    public function bannerList(){
        $banner = Dsp_setting::where('type', 'BN')->get();
        $datatable = Datatables::of($banner);
        return $datatable->make(true);
    }

    public function bannerGetCreate(){
        $bn = null;
        return view('dsp.banner_manage', compact('bn'));
    }

    public function bannerGetEdit($id){
        $bn = Dsp_setting::where('id', $id)->first();
        return view('dsp.banner_manage', compact('bn'));
    }

    public function bannerPostCreate(Request $request){
        $input = $request->all();
        
        if(!isset($input['status'])){
            $input['status'] = 'NA';
        }
        $input['type'] = $this->type[1];
        $input['title'] = 'new banner';

        $path = 'assets/img/banner';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        if (isset($input['userfile'])) {
            $filename = str_replace(' ', '_', $input['userfile']->getClientOriginalName());
            $input['userfile']->move($path, $filename);
            $input['file'] = $path . '/' . $filename;
        }


        if(!$input['bn_id']){
            Dsp_setting::create($input);
        }else{
            $rt = Dsp_setting::where('id', $input['bn_id'])->first();
            if (isset($input['file'])) {
                File::delete($rt->file);
            }
            $rt->update($input);
        }

        $this->redis->publish('message', 'dsp_banner');
        return redirect('/dsp/banner/list')->with('status', 'Data berhasil disimpan ke dalam aplikasi');
    }

    public function bannerDelete(Request $request){
        $input = $request->all();
        $bn = Dsp_setting::where('id', $input['id'])->first();
        File::delete($bn->file);
        $bn->delete();
        $this->redis->publish('message', 'dsp_banner');
        return redirect('/dsp/banner/list')->with('status', 'Data berhasil dihapus dari aplikasi');
    }

    public function adv(){
        return view('dsp.adv');
    }

    public function advList(){
        $adv = Dsp_setting::where('type', 'ADV')->get();
        $datatable = Datatables::of($adv);
        return $datatable->make(true);
    }

    public function advGetCreate(){
        $adv = null;
        return view('dsp.adv_manage', compact('adv'));
    }

    public function advGetEdit($id){
        $adv = Dsp_setting::where('id', $id)->first();
        return view('dsp.adv_manage', compact('adv'));
    }

    public function advUploader(Request $request) {
        $path = 'assets/img/adv';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $res = '';
        if ($request->hasFile("upload")) {
            $filename = str_replace(' ', '_', $request->upload->getClientOriginalName());
            $request->upload->move($path, $filename);
            $res = $path . '/' . $filename;
        }
        $url = url($res);
        $message = 'Upload Success';
        $funcNum = $_GET['CKEditorFuncNum'] ;

        return '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' . $funcNum . '", "' . $url . '", "' . $message . '")</script>';        
    }

    public function advPostCreate(Request $request){
        $input = $request->all();
        // return $input;
        if(!isset($input['status'])){
            $input['status'] = 'NA';
        }

        $input['type'] = $this->type[2];
		$input['title'] = 'new advertisement';

        $path = 'assets/img/adv';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        if (isset($input['userfile'])) {
            $filename = str_replace(' ', '_', $input['userfile']->getClientOriginalName());
            $input['userfile']->move($path, $filename);
            $input['file'] = $path . '/' . $filename;
        }


        if(!$input['adv_id']){
            Dsp_setting::create($input);
        }else{
            $rt = Dsp_setting::where('id', $input['adv_id'])->first();
            if (isset($input['file'])) {
                File::delete($rt->file);
            }
            $rt->update($input);
        }
        $this->redis->publish('message', 'dsp_adv');
        return redirect('/dsp/adv/list')->with('status', 'Data berhasil disimpan ke dalam aplikasi');
    }

    public function advDelete(Request $request){
        $input = $request->all();
        $adv = Dsp_setting::where('id', $input['id'])->first();
        File::delete($adv->file);
        $adv->delete();
        $this->redis->publish('message', 'dsp_adv');
        return redirect('/dsp/adv/list')->with('status', 'Data berhasil dihapus dari aplikasi');
    }

    public function vid(){
        return view('dsp.vid');
    }

    public function vidList(){
        $adv = Dsp_setting::where('type', 'VID')->get();
        $datatable = Datatables::of($adv);
        return $datatable->make(true);
    }

    public function vidGetCreate(){
        $vid = null;
        return view('dsp.vid_manage', compact('vid'));
    }

    public function vidGetEdit($id){
        $vid = Dsp_setting::where('id', $id)->first();
        return view('dsp.vid_manage', compact('vid'));
    }

    public function vidPostCreate(Request $request){
        $input = $request->all();        

        if(!isset($input['status'])){
            $input['status'] = 'NA';
        }
        $input['type'] = $this->type[3];		

        $path = 'assets/vid';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
                
        if (isset($input['userfile'])) {
            $filename = str_replace(' ', '_', $input['userfile']->getClientOriginalName());
            $input['userfile']->move($path, $filename);
            $input['file'] = $path . '/' . $filename;
        } else {
            $filename = 'new video';
        }
        $input['title'] = $filename;

        if(!$input['vid_id']){
            Dsp_setting::create($input);
        }else{
            $rt = Dsp_setting::where('id', $input['vid_id'])->first();
            if (isset($input['file'])) {
                File::delete($rt->file);
            }
            $rt->update($input);
        }
        $this->redis->publish('message', 'dsp_vid');
        return redirect('/dsp/vid/list')->with('status', 'Data berhasil disimpan ke dalam aplikasi');
    }

    public function vidDelete(Request $request){
        $input = $request->all();
        $vid = Dsp_setting::where('id', $input['id'])->first();
        File::delete($vid->file);
        $vid->delete();
        $this->redis->publish('message', 'dsp_vid');
        return redirect('/dsp/vid/list')->with('status', 'Data berhasil dihapus dari aplikasi');
    }    

    public function toggleStatus(Request $request) {
        $input = $request->all();

        $type = $input['type'];
        $id = $input['id'];

        $adv = Dsp_setting::find($id);

        if ($adv->status=="A") {
            $adv->status == "NA";
        } else {
            $adv->status == "A";
        }

        $adv->update();

                
    }
}
