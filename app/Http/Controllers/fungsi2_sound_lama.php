public function mergeSound($noqueue, $metavalue, $desc)
    {
        $daftarsound = array(
            '1' => 'c1.mp3',
            '2' => 'c2.mp3',
            '3' => 'c3.mp3',
            '4' => 'c4.mp3',
            '5' => 'c5.mp3'
        );
        $periksasound = array(
            '1' => 'd1.mp3',
            '2' => 'd2.mp3',
            '3' => 'd3.mp3',
            '4' => 'd4.mp3',
            '5' => 'd5.mp3'
        );
        $hasilsound = array(
            '1' => 'e1.mp3',
            '2' => 'e2.mp3',
            '3' => 'e3.mp3',
            '4' => 'e4.mp3',
            '5' => 'e5.mp3'
        );
        $no = str_split($noqueue);
        $no_antrian = file_get_contents('./assets/sounds/new/no-antrian.mp3');

        $sound = $no_antrian;
        for ($i = 0; $i < count($no); $i++) {
            $sound .= file_get_contents('./assets/sounds/new/' . $no[$i] . '.mp3');
        }

        switch ($desc) {
            case 'ca-utk-daftar':
                $sound .= file_get_contents('./assets/sounds/new/' . $daftarsound[$metavalue]);
                $moredesc = $daftarsound[$metavalue];
                break;

            case 'ca-utk-periksa':
                $sound .= file_get_contents('./assets/sounds/new/' . $daftarsound[$metavalue]);
                $moredesc = $daftarsound[$metavalue];
                break;

            case 'ca-utk-hasil':
                $sound .= file_get_contents('./assets/sounds/new/' . $hasilsound[$metavalue]);
                $moredesc = $hasilsound[$metavalue];
                break;

        }

        $newname = $noqueue . '_' . $metavalue . '_' . $moredesc . '.mp3';
        $name = $noqueue . '_' . $metavalue . '_' . $moredesc;

        $url = './assets/sounds/temp/' . $newname;
        @unlink($url);
        clearstatcache(TRUE, $url);
        if (file_exists($url)) throw new Exception('file not deleted : ' . $url);

        file_put_contents('./assets/sounds/temp/' . $newname, $sound);

        //update sm_meta voice
        $update = array('description' => $newname);
        Sm_meta::where('parameter', '=', 'VOICE')->update($update);

        //update sm_meta voice
        $update_play_sound = array('is_calling' => true);
        Sm_pendaftaran::where('metavalue', '=', $metavalue)->update($update_play_sound);

        $this->redis->publish('message', 'kiosk');

        $data = array('sound' => $name);
        /*$datas = array(
                    'action' => 'playsound',
                    'noqueue' => $noqueue, 
                    'metavalue' => $metavalue,
                    'desc' => $desc
                );
        $this->redis->publish('message', json_encode($datas, true));*/
        return response()->json($data);
    }


public function playSound($regid)
    {
        $v = Meta_Service::metalist('VOICE');
        if (!isset($v)) {
            return '-';
        }

        $url = './assets/sounds/temp/' . $v[0]['description'];
        if (!file_exists($url)) {
            return '-';
        }

        $data = array('sound' => $v[0]['description']);

        $sm_pendaftaran = Sm_pendaftaran::where('regid', '=', $regid)->first();
        $link = Sm_meta::find($sm_pendaftaran->metavalue)->link;
        $this->sendRedisFront($link, $sm_pendaftaran->noqueue, 'single');

        return response()->json($data);
    }

public function deleteSound($file, $metavalue, $regid)
    {
        //update sm_meta voice
        $update_play_sound = array('is_calling' => false);
        Sm_pendaftaran::where('metavalue', '=', $metavalue)->update($update_play_sound);

        $data['status'] = 'PR';
        $data['updated_by'] = Auth::user()->id;
        Sm_pendaftaran::where('regid', '=', $regid)->update($data);            

        $files = glob('./assets/sounds/temp/' . $file); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
            $update = array('description' => '');
            Sm_meta::where('parameter', '=', 'VOICE')->update($update);
            // $this->mainmodel->edit('parameter', 'VOICE', $update, 'sm_meta');
        }

        $this->redis->publish('kiosk-update', $metavalue);
        $this->redis->publish('message', 'btn-yes');
    }

public function deleteSoundAll($metavalue)
    {
        $files = glob('./assets/sounds/temp/*'); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
            $update = array('description' => '');
            Sm_meta::where('parameter', '=', 'VOICE')->update($update);
            // $this->mainmodel->edit('parameter', 'VOICE', $update, 'sm_meta');
        }
    }