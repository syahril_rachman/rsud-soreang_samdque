<?php

namespace App\Http\Controllers;

use App\Sm_pasien;
use App\Sm_pendaftaran;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use LRedis;
use App\Service\Meta_Service;

class MainController extends Controller
{
    private $redis;

    private $months = [
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    ];

    public function __construct(LRedis $lredis)
    {
        $this->redis = $lredis::connection();
    }

    public function listPatient(){
        return view('main.patient_list');
    }

    public function getListPatient(){
        $patients = Sm_pasien::get();
        $datatable = Datatables::of($patients);
        return $datatable->make(true);
    }

    public function getDetailPatient($idbank){
        $patient = Sm_pasien::where('IdBankNomor', $idbank)->first();
        return view('main.patient_detail', compact('patient'));
    }

    public function deletePatient(Request $request){
        $input = $request->all();
        $patient = Sm_pasien::find($input['id']);
        $patient->delete();

        $this->redis->publish('message', 'patient');
        return redirect('/main/patient/list')->with('status', 'Data berhasil dihapus dari aplikasi');
    }

    public function visitPatient(){
        $metalist = Meta_Service::metalist('FIRST');
        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        return view('main.visit_list', compact(['metalist', 'now']));
    }

    public function getVisitListPatient(Request $request){
        $param = $request->query();
        if(isset($param['date'])){
            $visitor = Sm_pendaftaran::where('metavalue', $param['value'])->whereDate('created_at', $param['date'])->get();
        }else{
            $now = Carbon::now('Asia/Jakarta')->format('Y-m-d');
            $visitor = Sm_pendaftaran::where('metavalue', $param['value'])->whereDate('created_at', $now)->get();
        }
        $datatable = Datatables::of($visitor);
        return $datatable->make(true);
    }

    public function reportMonth(Request $request){
        $metalist = Meta_Service::metalist('FIRST');
        $now = '';

        if($request->query('month') && $request->query('year')){
            $now = [
                0 => $request->query('year'),
                1 => $request->query('month'),
            ];
        }else{
            $now = Carbon::now('Asia/Jakarta')->format('Y-m');
            $now = explode('-', $now);

        }

        $days = cal_days_in_month(CAL_GREGORIAN, $now['1'], $now['0']);
        $indo_month = '';
        switch ($now[1]){
            case '1':
                $indo_month = 'Januari';
                break;
            case '2':
                $indo_month = 'Februari';
                break;
            case '3':
                $indo_month = 'Maret';
                break;
            case '4':
                $indo_month = 'April';
                break;
            case '5':
                $indo_month = 'Mei';
                break;
            case '6':
                $indo_month = 'Juni';
                break;
            case '7':
                $indo_month = 'Juli';
                break;
            case '8':
                $indo_month = 'Agustus';
                break;
            case '9':
                $indo_month = 'September';
                break;
            case '10':
                $indo_month = 'Oktober';
                break;
            case '11':
                $indo_month = 'November';
                break;
            case '12':
                $indo_month = 'Desember';
                break;
        }
        $months = $this->months;

        return view('main.report_month', compact(['metalist', 'days', 'now', 'indo_month', 'months']));
    }
    public function reportYear(Request $request){
        $year = '';
        if($request->query('year')){
            $year = $request->query('year');
        }else{
            $year = Carbon::now('Asia/Jakarta')->format('Y');
        }

        $metalist = Meta_Service::metalist('FIRST');
        $months = $this->months;
        return view('main.report_year', compact(['metalist', 'months', 'year']));
    }
}
