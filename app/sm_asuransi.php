<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sm_asuransi extends Model
{
    protected  $table = "sm_asuransis";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [   
        'asuransiidx', 'IdAsuransi', 'NamaAsuransi', 'keterangan', 'metavalue'
    ];
}
