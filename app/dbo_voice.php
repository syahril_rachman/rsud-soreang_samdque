<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dbo_voice extends Model
{
    protected  $table = "dbo_voices";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number', 'name'
    ];
}
