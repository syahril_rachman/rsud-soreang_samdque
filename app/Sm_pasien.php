<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sm_pasien extends Model
{
    protected  $table = "sm_pasiens";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'IdBankNomor', 'IdPasien', 'NamaPasien', 'IdJenisKelamin', 'TglLahir'
    ];
}
