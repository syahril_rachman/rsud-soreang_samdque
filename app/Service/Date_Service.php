<?php

namespace App\Service;

class Date_Service
{  
    private static $day = array(
      '0' => 'Minggu',
      '1' => 'Senin',
      '2' => 'Selasa',
      '3' => 'Rabu',
      '4' => 'Kamis',
      '5' => 'Jumat',
      '6' => 'Sabtu',
      '7' => 'Minggu',
    );
    private static $month = array(
      '1' => 'Januari',
      '2' => 'Februari',
      '3' => 'Maret',
      '4' => 'April',
      '5' => 'Mei',
      '6' => 'Juni',
      '7' => 'Juli',
      '8' => 'Agustus',
      '9' => 'September',
      '10' => 'Oktober',
      '11' => 'November',
      '12' => 'Desember',
    );  

    public static function getModedDate($n, $j, $m, $y)
    {    
        $date = self::$day[$n].', '.$j.' '.self::$month[$m].' '.$y;
        
        return $date;
    }
}
