<?php

namespace App\Service;

use Illuminate\Support\Facades\Auth;
use App\Sm_setting;
use App\Dsp_setting;

class Setting_Service
{    
    public static function srvdetail($type)
    {    

        $sm_settings = Sm_setting::where('settingtype', '=', $type)
                                ->where('settingstat', '=', 'A')
                                ->get();
        return $sm_settings;
    }

    public static function dspdetail($type)
    {    

        $dsp_settings = Dsp_setting::where('type', '=', $type)
                                ->where('status', '=', 'A')
                                ->get();
        return $dsp_settings;
    }
}
