<?php

namespace App\Service;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Sm_pendaftaran;
use DB;

class Pendaftaran_Service
{
    // public function add($data, $table){
    //     return $this->db->insert($table, $data);
    // }
    // public function edit($key, $val, $data, $table){
    //     $this->db->where($key, $val);
    //     return $this->db->update($table, $data);
    // }
    // public function view($table){
    //     return $this->db->get($table)->result_array();
    // }
    // public function detail($key, $val, $table){
    //     $this->db->where($key, $val);
    //     return $this->db->get($table)->result_array();
    // }
    // public function delete($key, $val, $table){
    //     return $this->db->delete($table, array($key=>$val));
    // }

    public static function findOne($regid) {
        return Sm_pendaftaran::where('regid', $regid)->first();
    }

    public static function findSome() {

    }

    public static function updatePendaftaran($regid, $data) {
        return Sm_pendaftaran::where('regid', $regid)->update($data);
    }

    public static function countQueueKiosk($id, $date){
        $res = Sm_pendaftaran::
        whereDate('datequeue', '=', $date)
            ->where('metavalue', '=', $id) /* modul no antrian berdasarkan menu meta*/
            ->count();
        return $res;
    }

    public static function detailKioskMaxQueue($id, $date){
        $res = Sm_pendaftaran::
        whereDate('datequeue', '=', $date)
            ->where('metavalue', '=', $id) /* modul no antrian berdasarkan menu meta*/
            ->orderBy('noqueue', 'desc') /* modul no antrian berdasarkan menu meta*/
            ->get();
        return $res;
    }

    public static function getAllQueueKiosk($date) {
        $status = ['WT','PD', 'PR'];

        $res = Sm_pendaftaran::whereIn('status', $status)
            ->whereDate('datequeue', '=', $date)
            ->orderBy('noqueue', 'asc')
            ->get();
        return $res;
    }

    public static function getQueueKiosk($date, $metavalue) {
        $status = ['WT','PD', 'PR'];

        $res = Sm_pendaftaran::whereIn('status', $status)
            ->where('metavalue', '=', $metavalue)
            ->whereDate('datequeue', '=', $date)
            ->orderBy('noqueue', 'asc')
            ->get();
        return $res;
    }

    public static function getOnProcessQueuebyLoket($loketID) {
        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        $res = Sm_pendaftaran::where('status', 'PR')->where('updated_by', $loketID)->whereDate('datequeue', $now)->first();

        return $res;
    }

    public static function getNextQueue($metavalue) {
        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        $res = Sm_pendaftaran::where('status', 'WT')
            ->where('metavalue', $metavalue)
            ->whereDate('datequeue', $now)
            ->orderBy('noqueue', 'asc')
            ->first();

        return $res;
    }

    public static function getRestQueueCount($metavalue) {
        $now = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        $res = Sm_pendaftaran::where('status', 'WT')
            ->where('metavalue', $metavalue)
            ->whereDate('datequeue', $now)
            ->count();

        return $res;
    }
}
