<?php

namespace App\Service;

use Illuminate\Support\Facades\Auth;
use App\Sm_meta;

class Meta_Service
{    
    /**
     * Get Meta Lists
     */
    public static function metalist($parameter)
    {
        $sm_metas = Sm_meta::where('parameter', '=', $parameter)->get();
        return $sm_metas;
    }

    public static function metalistpluck($parameter)
    {
        $sm_metas = Sm_meta::where('parameter', '=', $parameter)->pluck('value', 'description');
        return $sm_metas;
    }

    public static function metadetail($parameter, $value)
    {
        $sm_metas = Sm_meta::where('parameter', '=', $parameter)
                                ->where('value', '=', $value)
                                ->first();
        return $sm_metas;
    }
}
