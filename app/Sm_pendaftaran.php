<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sm_pendaftaran extends Model
{
    protected  $table = "sm_pendaftarans";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'regid', 'noqueue', 'datequeue', 'timequeue', 'status', 'metavalue',
    ];


    public static function countNumber($date, $status, $metavalue)
    {
        $register = Sm_pendaftaran::whereDate('datequeue', $date)->where('status', $status)->where('metavalue', $metavalue)->get();
        return count($register);
    }

    public static function countNumberYearly($year, $month, $status ,$metavalue){
        $register = Sm_pendaftaran::whereYear('datequeue', $year)->whereMonth('created_at', $month)->where('status', $status)->where('metavalue', $metavalue)->get();
        return count($register);
    }
}
