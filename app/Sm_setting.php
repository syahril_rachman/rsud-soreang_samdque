<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sm_setting extends Model
{
    protected  $table = "sm_settings";
    protected $primaryKey = 'settingid';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'settingtype', 'settingvalue', 'settingstat', 'setting'
    ];

    public function getSettingAttribute($value)
    {
            return json_decode($value, true);
    }
}
