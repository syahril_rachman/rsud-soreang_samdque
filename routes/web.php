<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'display/'], function () {
    Route::get('kiosk', 'FrontController@kiosk');
    Route::get('register', 'FrontController@register');
    Route::get('register/get_opr/{metavalue}', 'FrontController@getOnProcessQueue');
    Route::get('register/{param1}', 'FrontController@oneRegister');
    Route::get('register/{param1}/{param2}', 'FrontController@twoRegister');
    Route::get('examine', 'FrontController@examine');
    Route::get('result', 'FrontController@result');    
    Route::post('inputKiosk/{id}', 'FrontController@inputKiosk');
    Route::get('printStruk/{noqueue}/{metavalue}', 'FrontController@printStruk');
    Route::get('api/dsp-list/{type}', 'FrontController@apiDspList');

    Route::post('/adv_uploader', 'DspController@advUploader');
});

Route::group(['middleware' => ['auth']], function(){
    Route::group(['prefix' => 'user', ], function (){

        Route::get('/profile', 'UserController@getProfile');        

        Route::group(['middleware' => ['admin.check']], function (){
            Route::get('/', 'UserController@index');
            Route::post('/list', 'UserController@listUser');
            Route::get('/manage', 'UserController@getCreate');
            Route::get('/manage/{id}', 'UserController@getEdit');
            Route::post('/manage', 'UserController@postManage');
            Route::post('/delete', 'UserController@deleteUser');
        });
    });

    Route::group(['prefix' => 'main'], function (){
        Route::get('/kiosk/list', 'HomeController@listKiosk');
        Route::post('/kiosk/get-list', 'HomeController@getListKiosk');
        Route::post('/updateStatusKiosk/{regid}/{CL}', 'HomeController@updateStatusKiosk');
        Route::post('/resetIsCalling', 'HomeController@resetIsCalling');
        
        Route::get('/mergeSound/{queue}/{metavalue}/{desc}', 'HomeController@mergeSound1');
        // Route::get('playSound/{regid}', 'HomeController@playSound');
        Route::get('playSound/{regid}/{metavalue}', 'HomeController@playSound1');
        Route::post('/deleteSoundAll', 'HomeController@deleteSoundAll1');
        // Route::post('/deleteSound/{file}/{metavalue}/{regid}', 'HomeController@deleteSound');
        Route::post('/deleteSound/{file}/{metavalue}/{regid}', 'HomeController@deleteSound1');
        
        Route::get('/patient/list', 'MainController@listPatient');
        Route::post('/patient/get-list', 'MainController@getListPatient');
        Route::get('/patient/detail/{idbank}', 'MainController@getDetailPatient');
        Route::post('/patient/delete', 'MainController@deletePatient');

        Route::get('/visit/list', 'MainController@visitPatient');
        Route::post('/visit/get-list', 'MainController@getVisitListPatient');
        Route::get('/report/list-month', 'MainController@reportMonth');
        Route::get('/report/list-year', 'MainController@reportYear');
    });

    Route::group(['prefix' => 'other', 'middleware' => ['admin.check']], function (){
        Route::post('/resetQData', 'OtherController@resetQData');
        
        Route::group(['prefix' => 'runningtext'], function (){
            Route::get('/list', 'OtherController@runningText');
            Route::post('/get-list', 'OtherController@runningTextList');
            Route::get('/manage', 'OtherController@runningTextGetCreate');
            Route::get('/manage/{id}', 'OtherController@runningTextGetEdit');
            Route::post('/manage', 'OtherController@runningTextPostCreate');
            Route::post('/delete', 'OtherController@runningTextDelete');            
        });


        Route::group(['prefix' => 'banner'], function (){
            Route::get('/list', 'OtherController@banner');
            Route::post('/get-list', 'OtherController@bannerList');
            Route::get('/manage', 'OtherController@bannerGetCreate');
            Route::get('/manage/{id}', 'OtherController@bannerGetEdit');
            Route::post('/manage', 'OtherController@bannerPostCreate');
            Route::post('/delete', 'OtherController@bannerDelete');
        });


        Route::group(['prefix' => 'adv'], function (){
            Route::get('/list', 'OtherController@adv');
            Route::post('/get-list', 'OtherController@advList');
            Route::get('/manage', 'OtherController@advGetCreate');
            Route::get('/manage/{id}', 'OtherController@advGetEdit');
            Route::post('/manage', 'OtherController@advPostCreate');
            Route::post('/delete', 'OtherController@advDelete');
        });        
    });

    Route::group(['prefix' => 'dsp', 'middleware' => ['admin.check']], function (){
        Route::post('/toggleStatus', 'DspController@toggleStatus');
        
        Route::group(['prefix' => 'runningtext'], function (){
            Route::get('/list', 'DspController@runningText');
            Route::post('/get-list', 'DspController@runningTextList');
            Route::get('/manage', 'DspController@runningTextGetCreate');
            Route::get('/manage/{id}', 'DspController@runningTextGetEdit');
            Route::post('/manage', 'DspController@runningTextPostCreate');
            Route::post('/delete', 'DspController@runningTextDelete');
        });


        Route::group(['prefix' => 'banner'], function (){
            Route::get('/list', 'DspController@banner');
            Route::post('/get-list', 'DspController@bannerList');
            Route::get('/manage', 'DspController@bannerGetCreate');
            Route::get('/manage/{id}', 'DspController@bannerGetEdit');
            Route::post('/manage', 'DspController@bannerPostCreate');
            Route::post('/delete', 'DspController@bannerDelete');
        });


        Route::group(['prefix' => 'adv'], function (){            
            Route::get('/list', 'DspController@adv');
            Route::post('/get-list', 'DspController@advList');
            Route::get('/manage', 'DspController@advGetCreate');
            Route::get('/manage/{id}', 'DspController@advGetEdit');
            Route::post('/manage', 'DspController@advPostCreate');
            Route::post('/delete', 'DspController@advDelete');
        });

        Route::group(['prefix' => 'vid'], function (){
            Route::get('/list', 'DspController@vid');
            Route::post('/get-list', 'DspController@vidList');
            Route::get('/manage', 'DspController@vidGetCreate');
            Route::get('/manage/{id}', 'DspController@vidGetEdit');
            Route::post('/manage', 'DspController@vidPostCreate');
            Route::post('/delete', 'DspController@vidDelete');            
        });
    });

    Route::group(['prefix' => 'miniPanel'], function() {
        Route::get('/index', 'MiniPanelController@miniPanel');
        Route::get('/getRestq/{metavalue}', 'MiniPanelController@getRestq');
        Route::post('/next_queue/', 'MiniPanelController@nextQueue');
        Route::post('/queue_detail', 'MiniPanelController@queue_detail');
    });
});
