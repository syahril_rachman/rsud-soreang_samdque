@extends('layouts.app')

@section('mnuHome')
  active
@endsection

@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <button type="button" class="btn btn-default" onclick="openMiniPanel()">
                        <i class="icon icon-barcode icon-fw"></i> Mini Panel
                    </button>
                    <!-- <button type="button" class="btn btn-info" id="result-notaken">
                        <i class="icon icon-clipboard icon-fw"></i> Pengambilan Hasil
                    </button> -->
                    @if(Auth::user()->hasRole('administrator'))
                        <button type="button" class="btn btn-danger" onclick="resetData()">
                            <i class="icon icon-refresh icon-fw"></i> Reset Data Antrian
                        </button>
                    @endif
                    <button type="button" class="btn btn-primary" onclick="refreshHome()">
                        <i class="icon icon-refresh icon-fw"></i> Refresh
                    </button>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Beranda Hari ini ({{ $now }})</span>                    
                </h1>
                <p class="title-bar-description">
                    <!-- <small>Daftar antrian pendaftaran, pemeriksaan dan hasil per jenis nasabah. Klik tombol <strong>Pengambilan
                            Hasil</strong> untuk antrian hari-hari sebelumnya.
                    </small> -->
                </p>
            </div>

            <div class="row gutter-xs">
                <div class="col-md-12">
                    <h3>Antrian Pendaftaran 
                        @if(Auth::user()->hasRole('administrator'))
                            
                        @else
                            {{ $metatype[Auth::user()->userlevel-2]->description }}
                        @endif
                        <br/>
                        <small>Anda login sebagai {{ Auth::user()->username }} ({{ Auth::user()->roles[0]->display_name }})</small>
                    </h3>
                    <hr/>
                    <div class="row gutter-xs">
                        <div class="row gutter-xs">
                            <div class="col-xs-12">
                              <div class="panel panel-default">
                                <div class="panel-body panel-collapse">
                                  <table id="kiosk_table" class="table table-bordered table-striped dataTable userlevel-{{Auth::user()->userlevel}}" cellspacing="0" width="100%">
                                    <tbody>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal Reset -->
    <div class="modal fade" id="modal-reset" tabindex="-1" role="dialog" aria-labelledby="labelCancel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="labelDelete"><i class="icon icon-warning icon-fw"></i> Peringatan!</h4>
                </div>
                <div class="modal-body">
                    <form class="form form-horizontal" data-toggle="validator" id="formResetData" 
                          method="post" action="{{url('/other/resetQData')}}" style="margin-bottom: -56px;">
                          <div class="demo-form-wrapper">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Jenis Antrian</label>
                                <div class="col-sm-9">
                                    <select id="meta" name="meta" class="form-control" required>
                                        <option value="">--Pilih--</option>
                                        <option value="all">Pilih Semua</option>
                                        @foreach($metas as $meta)
                                            <option value="{{$meta->value}}">{{$meta->description}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mulai dari nomor</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="number" id="startfrom" name="startfrom" required>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-danger" id="btn-reset-yes">Lanjutkan
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Cancel -->
    <div class="modal fade" id="modal-cancel" tabindex="-1" role="dialog" aria-labelledby="labelCancel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="labelDelete"><i class="icon icon-warning icon-fw"></i> Peringatan!</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id-cancel" value=""/>
                    <input type="hidden" id="id-queue" value=""/>
                    <span id="text-cancel"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-danger" id="btn-cancel-yes" data-dismiss="modal">Lanjutkan
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Pending -->
    <div class="modal fade" id="modal-pending" tabindex="-1" role="dialog" aria-labelledby="labelPending">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="labelPending"><i class="icon icon-warning icon-fw"></i> Peringatan!</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id-pending" value=""/>
                    <input type="hidden" id="id-pqueue" value=""/>
                    <span id="text-pending"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-danger" id="btn-pending-yes" data-dismiss="modal">Lanjutkan
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Online -->
    <div class="modal fade" id="modal-online" tabindex="-1" role="dialog" aria-labelledby="labelOnline">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header modal-header-danger">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="labelOnline"><i class="icon icon-warning icon-fw"></i> Peringatan!</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" id="id-online" value=""/>
            <input type="hidden" id="id-oqueue" value=""/>
            <span id="text-online"></span>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
            <button type="button" class="btn btn-danger" id="btn-online-yes" data-dismiss="modal">Lanjutkan</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Registrasi -->
    <div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="labelRegister">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="labelRegister"><i class="icon icon-clipboard icon-fw"></i> Form
                        Pendaftaran Manual</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id-register" value=""/>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="demo-form-wrapper">
                                    <div class="form-group">
                                        <label class="control-label">No. Antrian</label>
                                        <input class="form-control" type="hidden" name="noqueue" id="noqueue">
                                        <input class="form-control" type="text" id="noqueuedisp" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">No. Pasien</label>
                                        <input class="form-control" type="text" name="IdBankNomor" id="IdBankNomor"
                                               maxlength="6" minlength="6">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Nama Pasien</label>
                                        <input class="form-control" type="text" name="NamaPasien" id="NamaPasien">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Jenis Kelamin</label>
                                        <select name="IdJenisKelamin" id="IdJenisKelamin" class="form-control">
                                            <option value="">--Pilih--</option>
                                            <option value="L">Laki-laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Tanggal Lahir</label>
                                        <input class="form-control" type="date" name="TglLahir" id="TglLahir">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="demo-form-wrapper">
                                    <div class="form-group">
                                        <label class="control-label">Ruang Perujuk</label>
                                        <select name="IdRuangPerujuk" id="IdRuangPerujuk" class="form-control">
                                            <option value="">--Pilih--</option>                                       
                                            @if (isset($room))
                                            <?php foreach($room as $row):?>
                                            <option value="<?php echo $row['idRuangPelayanan'];?>"><?php echo $row['aliasRuangPelayanan'] . ' - ' . $row['namaRuangPelayanan'];?></option>
                                            <?php endforeach?>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Dokter Perujuk</label>
                                        <input class="form-control" type="text" name="NamaDokterPerujuk"
                                               id="NamaDokterPerujuk">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Jenis Pasien</label>
                                        <select name="type" id="type" class="form-control">
                                            <option value="">--Pilih--</option>
                                            @if (isset($metatype))
                                            <?php foreach($metatype as $row):?>
                                            <option value="<?php echo $row['value'];?>"><?php echo $row['description'];?></option>
                                            <?php endforeach?>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Tanggal Pesan</label>
                                        <input class="form-control" type="date" name="TglPesan" id="TglPesan"
                                               value="<?php echo date('Y-m-d');?>">
                                        <small>Tanggal dapat diganti untuk menjadwal pemeriksaan laboratorium di hari
                                            lain.
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-danger" id="btn-register-yes" data-dismiss="modal">Lanjutkan
                    </button>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Scan Barcode -->
    <div class="modal fade" id="modal-scan" tabindex="-1" role="dialog" aria-labelledby="labelScan">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="labelScan"><i class="icon icon-clipboard icon-fw"></i> Scan Barcode</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id-register-sc" value=""/>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="demo-form-wrapper">
                                    <div class="form-group">
                                        <label class="control-label">No. Antrian</label>
                                        <input class="form-control" type="hidden" name="noqueue" id="noqueue-sc">
                                        <input class="form-control" type="text" id="noqueuedisp-sc" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Scan No. Pasien</label>
                                        <input class="form-control" type="text" name="IdPasien" id="IdPasien-sc"
                                               placeholder="Aktifkan kursor Anda di sini">
                                        <small>Aktifkan kursor Anda pada isian di atas sebelum melakukan scan</small>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Nama Pasien</label>
                                        <input class="form-control" type="text" name="NamaPasien" id="NamaPasien-sc"
                                               readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Jenis Kelamin</label>
                                        <input type="hidden" id="IdJenisKelamin-sc" value=""/>
                                        <input class="form-control" type="text" name="NamaJenisKelamin"
                                               id="NamaJenisKelamin-sc" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Tanggal Lahir</label>
                                        <input class="form-control" type="date" name="TglLahir" id="TglLahir-sc"
                                               readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="demo-form-wrapper">
                                    <!--
                                    <div class="form-group">
                                      <label class="control-label">Unique ID</label>
                                      <input class="form-control" type="text" name="IdBankNomor" id="IdBankNomor-sc" readonly="readonly">
                                    </div>
                                    -->
                                    <div class="form-group">
                                        <label class="control-label">Ruang Perujuk</label>
                                        <input type="hidden" id="IdRuangPerujuk-sc" value=""/>
                                        <input class="form-control" type="text" name="AliasNamaRuangPelayanan"
                                               id="AliasNamaRuangPelayanan-sc" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Dokter Perujuk</label>
                                        <input class="form-control" type="text" name="NamaDokterPerujuk"
                                               id="NamaDokterPerujuk-sc" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Jenis Pasien</label>
                                        <input type="hidden" id="type-sc" value=""/>
                                        <input class="form-control" type="text" name="type" id="assurance-sc"
                                               readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Tanggal Pesan</label>
                                        <input class="form-control" type="date" name="TglPesan" id="TglPesan-sc"
                                               value="<?php echo date('Y-m-d');?>">
                                        <small>Tanggal dapat diganti untuk menjadwal pemeriksaan laboratorium di hari
                                            lain.
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" id="btn-cancel-sc" data-dismiss="modal">
                        Tutup
                    </button>
                    <button type="button" class="btn btn-danger" id="btn-register-yes-sc" data-dismiss="modal">
                        Lanjutkan
                    </button>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Pengambilan Hasil Antrian Kemaren2 -->
    <div class="modal fade" id="modal-result" tabindex="-1" role="dialog" aria-labelledby="labelResult">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="labelResult"><i class="icon icon-clipboard icon-fw"></i> Daftar Antrian
                        Pengambilan Hasil Pemeriksaan</h4>
                </div>
                <div class="modal-body">
                    <div class="well">Berikut adalah daftar antrian tanggal sebelumnya untuk pengambilan hasil
                        pemeriksaan laboratorium.
                    </div>
                    <table id="dtable" class="table dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tgl Antrian</th>
                            <th>No. Pasien</th>
                            <th>Nama Pasien</th>
                            <th>(L/P)</th>
                            <th>Jenis</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Sound -->
    <div class="modal fade" id="modal-sound" tabindex="-1" role="dialog" aria-labelledby="labelSound"
         style="z-index: 1080">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close btn-sound-no" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="labelSound"><i class="icon icon-bullhorn icon-fw"></i> Panggilan Antrian
                    </h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id-queue-s" value=""/>
                    <input type="hidden" id="id-desc-s" value=""/>
                    <input type="hidden" id="id-metavalue-s" value=""/>
                    <input type="hidden" id="id-regid" value=""/>
                    <span id="text-sound-s"></span><br/><br/>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary btn-sound-no" data-dismiss="modal">Tutup
                    </button>
                    <button type="button" class="btn btn-danger" id="btn-sound-yes"><i
                                class="icon icon-play icon-fw"></i> Panggil Antrian
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Process -->
    <div class="modal fade" id="modal-process" tabindex="-1" role="dialog" aria-labelledby="labelPending">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="labelPending"><i class="icon icon-warning icon-fw"></i> Peringatan!</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id-process" value=""/>
                    <input type="hidden" id="id-pqueue" value=""/>
                    <span id="text-process"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-danger" id="btn-process-yes" data-dismiss="modal">Lanjutkan
                    </button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="dateall" value="<?php echo date('Y-m-d');?>">
    <input type="hidden" id="dateall2" value="<?php echo date('Ymd');?>">

@endsection

@section('scripts')
    <script src="{{asset('js/validator.js')}}"></script>
    <script>
        var base_url = "{{ url('/') }}";
        var stat = [@foreach($stat as $k => $value)
                       '{{ $value }}',
                    @endforeach ];
        var metad = [@foreach($metatype as $k => $value)
                           '{{ $value->description }}',
                        @endforeach ];
        var metal = [@foreach($metatype as $k => $value)
                           '{{ $value->link }}',
                        @endforeach ];

        var metavalue = 0;

        function openMiniPanel() {
            var win = window.open("{{ url('/miniPanel/index') }}", 'index', 'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=358,height=150');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }
        }

        function refreshHome() {
            $.post(base_url+'/main/resetIsCalling', {_token: "{{ csrf_token() }}"});
            location.reload()
        }

        //reset Data
        function resetData() {
            $('#meta option:eq("")').prop('selected', true);
            $('#startfrom').val('');

            $('#modal-reset').modal({
                backdrop: 'static'
            });
        }        
        $(document).on('click', '#btn-reset-yes', function(){
            // var meta = $('#meta option:selected').val();
            // var resetFrom = $('#startfrom').val();            
            var form = $('#formResetData');

            if (form.validator('validate')) {
                form.submit();
            }            
        }); 

        $(document).ready(function(){

            //Pengambilan hasil antrian kemaren2 ----------------------------------------
            var datenow = $('#dateall').val();
            $('#dtable').DataTable({
                "responsive":!0,
                "dom":"<'row'<'col-sm-6 col-sm-6'i><'col-sm-6 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-6'l><'col-sm-6'p>>",
                "language":{paginate:{previous:"&laquo;",next:"&raquo;"},
                    "search":"_INPUT_",
                    "searchPlaceholder":"Search…"},

                "processing": true,
                "serverSide": true,
                "ajax": base_url+"main/viewJsonResult/result/"+datenow
            });

            $('#result-notaken').click(function(){
                $('#modal-result').modal({
                    backdrop: 'static'
                });
            });

            /* -------------------------------------------------------------------------*/

            function strpad(str, max) {
                str = str.toString();
                return str.length < max ? strpad("0" + str, max) : str;
            }            

            //ACTION            
            var assurance = [@foreach($metatype as $k => $value)
                               '{{ $value->description }}',
                            @endforeach ];            

            //btn-cancel
            $(document).on('click', '.btn-cancel', function(){
                var str = $(this).attr('id').split('-');
                var desc = $(this).val();
                var noqueue = str[1];
                var metavalue = str[2];
                var regid = str[3];

                $('#id-cancel').val(regid);
                $('#text-cancel').empty().append('<h4>'+desc+'</h4><br/>Batalkan antrian nomor <strong>#'+strpad(noqueue,3)+'</strong> untuk jenis nasabah <strong>'+assurance[metavalue-2]+'</strong>');
                $('#modal-cancel').modal({
                    backdrop: 'static'
                });
            });
            $(document).on('click', '#btn-cancel-yes', function(){
                var regid = $('#id-cancel').val();
                
                $.post(base_url+'/main/updateStatusKiosk/'+regid+'/CL', {_token: "{{ csrf_token() }}"});
                $('#tr-'+regid).remove();
            });            

            //btn-pending
            $(document).on('click', '.btn-pending', function(){
                var str = $(this).attr('id').split('-');
                var desc = $(this).val();
                var noqueue = str[1];
                var metavalue = str[2];
                var regid = str[3];

                $('#id-pending').val(regid);
                $('#id-pqueue').val(noqueue);
                $('#text-pending').empty().append('<h4>'+desc+'</h4><br/>Ubah status antrian nomor <strong>#'+strpad(noqueue,3)+'</strong> untuk jenis nasabah <strong>'+assurance[metavalue-2]+'</strong> menjadi <span class="label label-warning">Pending</span>');
                $('#modal-pending').modal({
                    backdrop: 'static'
                });
            });
            $(document).on('click', '#btn-pending-yes', function(){
                var regid = $('#id-pending').val();
                var noqueue = $('#id-pqueue').val();
                $.post(base_url+'/main/updateStatusKiosk/'+regid+'/PD', {_token: "{{ csrf_token() }}"});
                $('#status-'+regid).empty().append(' <span class="label label-warning">Pending</span>');
            });

            //btn-online
            $(document).on('click', '.btn-online', function(){
                var str = $(this).attr('id').split('-');
                var desc = $(this).val();
                var noqueue = str[1];
                var metavalue = str[2];
                var regid = str[3];

                $('#id-online').val(regid);
                $('#id-oqueue').val(noqueue);
                $('#text-online').empty().append('<h4>'+desc+'</h4><br/>Ubah status antrian nomor <strong>#'+strpad(noqueue,3)+'</strong> untuk jenis nasabah <strong>'+assurance[metavalue-2]+'</strong> menjadi <span class="label label-default">Online</span>');
                $('#modal-online').modal({
                    backdrop: 'static'
                });
            });
            $(document).on('click', '#btn-online-yes', function(){
                var regid = $('#id-online').val();
                var noqueue = $('#id-oqueue').val();
                $.post(base_url+'/main/updateStatusKiosk/'+regid+'/WT', {_token: "{{ csrf_token() }}"});
                $('#status-'+regid).empty().append(' <span class="label label-default">Online</span>');
            });
            
            //btn-register
            $(document).on('click', '.btn-register', function(){
                var str = $(this).attr('id').split('-');
                var desc = $(this).val();
                var noqueue = str[1];
                var metavalue = str[2];
                var regid = str[3];
                var tanggal = $('#dateall').val();

                // $('#id-register').val(regid);
                // $('#noqueue').val(noqueue);
                // $('#noqueuedisp').val(strpad(noqueue, 3));
                // $('#type').val(metavalue);
                // $('#TglPesan').val(tanggal);
                // $('#modal-register').modal({
                //     backdrop: 'static'
                // });

                var win = window.open("http://10.10.10.2/SIMRS/simrs_pendaftaran/index.php/pendaftaran_pasien_int/pendaftaran_pasien_add_page", '_blank');
                if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                }
            });
            $(document).on('click', '#btn-register-yes', function(){
                var regid = $('#id-register').val();
                var noqueue = $('#noqueue').val();
                var IdBankNomor = $('#IdBankNomor').val();
                var NamaPasien = $('#NamaPasien').val();
                var IdJenisKelamin = $('#IdJenisKelamin').val();
                var IdRuangPerujuk = $('#IdRuangPerujuk').val();
                var NamaRuangPerujuk = $('#IdRuangPerujuk').children(':selected').text();
                var NamaDokterPerujuk = $('#NamaDokterPerujuk').val();
                var type = $('#type').val();
                var TglPesan = $('#TglPesan').val();
                var TglLahir = $('#TglLahir').val();

                var datainput = {
                    '_token': "{{ csrf_token() }}",
                    'regid': regid,
                    'noqueue': noqueue,
                    'IdBankNomor': IdBankNomor,
                    'NamaPasien': NamaPasien,
                    'IdJenisKelamin': IdJenisKelamin,
                    'IdRuangPerujuk': IdRuangPerujuk,
                    'NamaRuangPerujuk': NamaRuangPerujuk,
                    'NamaDokterPerujuk': NamaDokterPerujuk,
                    'type': type,
                    'TglPesan': TglPesan,
                    'TglLahir': TglLahir,
                };

                $.post(base_url+'/main/inputPendaftaran', datainput, function(result){
                    $('#tr-'+regid).remove();
                    $('#examine-'+type).append(result.data);
                }, 'json');

                $('#id-register').val("");
                $('#noqueue').val("");
                $('#IdBankNomor').val("");
                $('#NamaPasien').val("");
                $('#IdJenisKelamin').val("");
                $('#IdRuangPerujuk').val("");
                $('#NamaDokterPerujuk').val("");
                $('#type').val("");
                $('#TglPesan').val("");
                $('#TglLahir').val("");
            });

            //btn-scan
            $(document).on('click', '.btn-scan', function(){
                var str = $(this).attr('id').split('-');
                var desc = $(this).val();
                var noqueue = str[1];
                var metavalue = str[2];
                var regid = str[3];
                var tanggal = $('#dateall').val();

                $('#id-register-sc').val(regid);
                $('#noqueue-sc').val(noqueue);
                $('#noqueuedisp-sc').val(strpad(noqueue, 3));
                $('#type-sc').val(metavalue);
                $('#assurance-sc').val(assurance[metavalue-2]);
                $('#TglPesan-sc').val(tanggal);
                $('#modal-scan').modal({
                    backdrop: 'static'
                });
            });

            //SOUND
            $(document).on('click', '.btn-bullhorn', function(){
                var str = $(this).attr('id').split('-');
                var noqueue = str[1];
                var metavalue = str[2];
                var regid = str[3];
                var desc = 'ca-utk-daftar';

                $('#id-regid').val(regid);
                $('#id-queue-s').val(strpad(noqueue, 3));
                $('#id-metavalue-s').val(metavalue);
                $('#id-desc-s').val(desc);
                $('#modal-sound').modal({backdrop:'static'});
                $('#text-sound-s').empty().append('<h4>Panggilan Pendaftaran</h4> Panggil antrian nasabah <strong>'+assurance[metavalue-2]+'</strong> dengan nomor antrian <strong>#'+strpad(noqueue, 3)+'</strong>');

            });

            $(document).on('click', '#btn-sound-yes', function() {
                $(this).prop('disabled', true);

                var queue = $('#id-queue-s').val();
                var metavalue = $('#id-metavalue-s').val();
                var desc = $('#id-desc-s').val();
                var loket = $('#id-loket').val();
                var regid = $('#id-regid').val();

                console.log(queue+" "+metavalue+" "+desc+" "+loket+" "+regid);
                $.get(base_url+'/main/mergeSound/'+queue+'/'+metavalue+'/'+desc, function(result){                    
                    //var audio = new Audio(base_url+'/assets/sounds/temp/'+result.sound+'.mp3');
                    //audio.play();
                    //$.playSound(base_url+'/assets/sounds/temp/'+result.sound);
                    playsound(metavalue, regid);
                    //console.log(result);
                }, 'json');
            });

            //PROCESS
            $(document).on('click', '.btn-process', function(){
                var str = $(this).attr('id').split('-');
                var desc = $(this).val();
                var noqueue = str[1];
                var metavalue = str[2];
                var regid = str[3];

                $('#id-process').val(regid);
                $('#id-pqueue').val(noqueue);
                $('#text-process').empty().append('<h4>'+desc+'</h4><br/>Ubah status antrian nomor <strong>#'+strpad(noqueue,3)+'</strong> untuk jenis nasabah <strong>'+assurance[metavalue-2]+'</strong> menjadi <span class="label label-danger">On Process</span>');
                $('#modal-process').modal({
                    backdrop: 'static'
                });
            });
            $(document).on('click', '#btn-process-yes', function(){
                var regid = $('#id-process').val();
                var noqueue = $('#id-pqueue').val();
                $.post(base_url+'/main/updateStatusKiosk/'+regid+'/PR', {_token: "{{ csrf_token() }}"});
                $('#status-'+regid).empty().append(' <span class="label label-success">On Process</span>');
            });            

            //SCANNING
            var datenow2 = '20161129';//$('#dateall2').val();
            $(document).on('change keyup input', 'input#IdPasien-sc', function(){
                var code = $('#IdPasien-sc').val();
                var count = $(this).val().length;
                if(count >= 3){
                    $.get(base_url+'/display/jsonPasien/'+$(this).val(), function(result){
                        console.log(result.totarray);
                        $('#NamaPasien-sc').val(result.namaPasien);
                        $('#IdJenisKelamin-sc').val(result.idJenisKelamin);
                        $('#NamaJenisKelamin-sc').val(result.namaJenisKelamin);
                        $('#TglLahir-sc').val(result.tglLahir);
                        $('#IdRuangPerujuk-sc').val(result.idRuangPelayanan);
                        $('#AliasNamaRuangPelayanan-sc').val(result.aliasRuangPelayanan);
                        $('#NamaDokterPerujuk-sc').val(result.namaDokterPerujuk);
                    });
                }
            });

            $(document).on('click', '#btn-cancel-sc', function(){
                $('#NamaPasien-sc').val("");
                $('#IdJenisKelamin-sc').val("");
                $('#NamaJenisKelamin-sc').val("");
                $('#TglLahir-sc').val("");
                $('#IdRuangPerujuk-sc').val("");
                $('#AliasNamaRuangPelayanan-sc').val("");
                $('#NamaDokterPerujuk-sc').val("");
                $('#IdPasien-sc').val("");
            });

            $(document).on('click', '#btn-register-yes-sc', function(){
                var regid = $('#id-register-sc').val();
                var noqueue = $('#noqueue-sc').val();
                var IdBankNomor = $('#IdPasien-sc').val();
                var NamaPasien = $('#NamaPasien-sc').val();
                var IdJenisKelamin = $('#IdJenisKelamin-sc').val();
                var IdRuangPerujuk = $('#IdRuangPerujuk-sc').val();
                var NamaRuangPerujuk = $('#AliasNamaRuangPelayanan-sc').val();
                var NamaDokterPerujuk = $('#NamaDokterPerujuk-sc').val();
                var type = $('#type-sc').val();
                var TglPesan = $('#TglPesan-sc').val();
                var TglLahir = $('#TglLahir-sc').val();

                var datainput = {
                    '_token': "{{ csrf_token() }}",
                    'regid': regid,
                    'noqueue': noqueue,
                    'IdBankNomor': IdBankNomor,
                    'NamaPasien': NamaPasien,
                    'IdJenisKelamin': IdJenisKelamin,
                    'IdRuangPerujuk': IdRuangPerujuk,
                    'NamaRuangPerujuk': NamaRuangPerujuk,
                    'NamaDokterPerujuk': NamaDokterPerujuk,
                    'type': type,
                    'TglPesan': TglPesan,
                    'TglLahir': TglLahir,
                };

                $.post(base_url+'/main/inputPendaftaran', datainput, function(result){
                    $('#tr-'+regid).remove();
                    $('#examine-'+type).append(result.data);
                }, 'json');

                $('#NamaPasien-sc').val("");
                $('#IdJenisKelamin-sc').val("");
                $('#NamaJenisKelamin-sc').val("");
                $('#TglLahir-sc').val("");
                $('#IdRuangPerujuk-sc').val("");
                $('#AliasNamaRuangPelayanan-sc').val("");
                $('#NamaDokterPerujuk-sc').val("");
                $('#IdPasien-sc').val("");
            });


            /*check if sound exists*/
            function playsound(metavalue, regid){                
                $.get(base_url+'/main/playSound/'+regid+'/'+metavalue, function(result){
                    if(result.sound){
                        fileAndPlay(result.sound, metavalue, regid);
                    } 
                    // else {
                    //     setTimeout(playsound, 2000);
                    // }
                }, 'json');
            };

            function fileAndPlay(fileAudio, metavalue, regid) {
                var audio = new Audio(base_url+'/assets/sounds/temp/'+fileAudio);
                if (audio) {
                    if (!audio.currentTime) {
                        audio.playbackRate = 1.2;
                        audio.play();
                        audio.addEventListener("ended", function(){
                            audio.currentTime = 0;
                            $.post(base_url+'/main/deleteSound/'+fileAudio+'/'+metavalue+'/'+regid, {_token: "{{ csrf_token() }}"});
                            //setTimeout(playsound, 1000);
                        });
                    } else {
                        console.log("audio is paused||is playing");
                    }
                }
            }

            /*socket io*/
            /*var socket = io.connect('http://localhost:8890');
            socket.on('message', function (response) {
                var data = JSON.parse(response);
                if (data && data.action == 'playsound') {
                    //playsound(data.metavalue);
                }

                console.log('socket message -> ', data);
            });*/
        });      
    </script>

@endsection
