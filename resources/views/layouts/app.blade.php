<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo/favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo/favicon-16x16.png')}}" sizes="16x16">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="{{asset('assets/css/vendor.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/elephant.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/application.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/demo.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    <script src="{{asset('assets/js/vendor.min.js?v=1.1')}}"></script>
    <script src="{{asset('assets/js/playsound.js')}}"></script>
@yield('css')
</head>
<body class="layout layout-header-fixed layout-sidebar-fixed">
<div class="layout-header">
    <div class="navbar navbar-default">
        <div class="navbar-header">
            <a class="navbar-brand navbar-brand-center" href="index.html">
                <img class="navbar-brand-logo" src="{{asset('assets/img/logo/logo3.png')}}" alt="SIMRS Assistance">
            </a>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse"
                    data-target="#sidenav">
                <span class="sr-only">Toggle navigation</span>
                <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
                <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
            </button>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse"
                    data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="arrow-up"></span>
                <span class="ellipsis ellipsis-vertical">
              <img class="ellipsis-object" width="32" height="32" src="">
            </span>
            </button>
        </div>
        <div class="navbar-toggleable">
            <nav id="navbar" class="navbar-collapse collapse">
                <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true"
                        type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
                </button>
                <ul class="nav navbar-nav navbar-right">
                    <li class="visible-xs-block">
                        <h4 class="navbar-text text-center">Hi, NAMA HERE</h4>
                    </li>
                    <li class="hidden-xs hidden-sm">
                        <form class="navbar-search navbar-search-collapsed">
                            <div class="navbar-search-group">
                                <input class="navbar-search-input" type="text"
                                       placeholder="Search for people, companies, and more&hellip;">
                                <button class="navbar-search-toggler" title="Expand search form ( S )"
                                        aria-expanded="false" type="submit">
                                    <span class="icon icon-search icon-lg"></span>
                                </button>
                                <button class="navbar-search-adv-btn" type="button">Advanced</button>
                            </div>
                        </form>
                    </li>

                    <li class="dropdown hidden-xs">
                        <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
                            <img class="rounded" width="36" height="36"
                                 src="{{Auth::user()->upload ? asset(Auth::user()->upload) : asset('/assets/img/uicon.png')}}"
                                 alt="">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="navbar-upgrade-version">Version: 1.0.0</li>
                            <li class="divider"></li>
                            <li><a href="{{url('user/profile')}}">Profil</a></li>
                            <li><a href="javascript:;" onclick="logout()">Log out</a></li>
                        </ul>
                    </li>
                    <li class="visible-xs-block">
                        <a href="{{asset('')}}">
                            <span class="icon icon-user icon-lg icon-fw"></span>
                            Profil
                        </a>
                    </li>
                    <li class="visible-xs-block">
                        <a href="{{asset('')}}">
                            <span class="icon icon-power-off icon-lg icon-fw"></span>
                            Log out
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<div class="layout-main">
    <div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
            <div class="custom-scrollbar">
                <nav id="sidenav" class="sidenav-collapse collapse">
                    <ul class="sidenav">
                        <li class="sidenav-search hidden-md hidden-lg">
                            <form class="sidenav-form" action="/">
                                <div class="form-group form-group-sm">
                                    <div class="input-with-icon">
                                        <input class="form-control" type="text" placeholder="Search…">
                                        <span class="icon icon-search input-icon"></span>
                                    </div>
                                </div>
                            </form>
                        </li>
                        <li class="sidenav-heading">Navigasi</li>
                        <li class="sidenav-item @yield('mnuHome')">
                            <a href="{{url('/home')}}">
                                <span class="sidenav-icon icon icon-home"></span>
                                <span class="sidenav-label">Beranda</span>
                            </a>
                        </li>
                        <!-- <li class="sidenav-item @yield('mnuPatient')">
                            <a href="{{url('/main/patient/list')}}">
                                <span class="sidenav-icon icon icon-hospital-o"></span>
                                <span class="sidenav-label">Pasien</span>
                            </a>
                        </li>
                        <li class="sidenav-item @yield('mnuVisit')">
                            <a href="{{url('/main/visit/list')}}">
                                <span class="sidenav-icon icon icon-ticket"></span>
                                <span class="sidenav-label">Kunjungan</span>
                            </a>
                        </li> -->
                        <li class="sidenav-item has-subnav @yield('mnuReport')">
                            <a href="#" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-clipboard"></span>
                                <span class="sidenav-label">Laporan</span>
                            </a>
                            <ul class="sidenav-subnav collapse">
                                <li><a href="{{url('/main/report/list-month')}}">Rekapitulasi Bulanan</a></li>
                                <li><a href="{{url('/main/report/list-year')}}">Rekapitulasi Tahunan</a></li>
                            </ul>
                        </li>

                        @if(Auth::user()->hasRole('administrator'))
                            <li class="sidenav-heading">Pengaturan</li>
                            <li class="sidenav-item @yield('mnuUser')">
                                <a href="{{url('/user')}}">
                                    <span class="sidenav-icon icon icon-user"></span>
                                    <span class="sidenav-label">User</span>
                                </a>
                            </li>
                            </li>                            

                            <li class="sidenav-heading">Lain-Lain</li>

                            <li class="sidenav-item has-subnav @yield('mnuKiosk')">
                                <a href="#" aria-haspopup="true">
                                    <span class="sidenav-icon icon icon-gear"></span>
                                    <span class="sidenav-label">Display Kiosk</span>
                                </a>
                                <ul class="sidenav-subnav collapse">
                                    <li><a href="{{url('/other/runningtext/list')}}">Running Text</a></li>
                                    <!-- <li><a href="{{url('/other/banner/list')}}">Banner</a></li>
                                    <li><a href="{{url('/other/adv/list')}}">Iklan / Pengumuman</a></li> -->
                                </ul>
                            </li>

                            <!-- <li class="sidenav-item @yield('mnuRT')">
                                <a href="{{url('/other/runningtext/list')}}">
                                    <span class="sidenav-icon icon icon-indent"></span>
                                    <span class="sidenav-label">Running Text</span>
                                </a>
                            </li>
                            <li class="sidenav-item @yield('mnuBN')">
                                <a href="{{url('/other/banner/list')}}">
                                    <span class="sidenav-icon icon icon-th-large"></span>
                                    <span class="sidenav-label">Banner</span>
                                </a>
                            </li>
                            <li class="sidenav-item @yield('mnuADV')">
                                <a href="{{url('/other/adv/list')}}">
                                    <span class="sidenav-icon icon icon-th"></span>
                                    <span class="sidenav-label">Iklan</span>
                                </a>
                            </li> -->

                            <li class="sidenav-item has-subnav @yield('mnuKiosk')">
                                <a href="#" aria-haspopup="true">
                                    <span class="sidenav-icon icon icon-gear"></span>
                                    <span class="sidenav-label">Display Antrian</span>
                                </a>
                                <ul class="sidenav-subnav collapse">
                                    <li><a href="{{url('/dsp/runningtext/list')}}">Running Text</a></li>
                                    <!-- <li><a href="{{url('/dsp/banner/list')}}">Banner</a></li>
                                    <li><a href="{{url('/dsp/adv/list')}}">Iklan / Pengumuman</a></li> -->
                                    <li><a href="{{url('/dsp/vid/list')}}">Video</a></li>
                                </ul>
                            </li>

                            <!-- <li class="sidenav-item @yield('mnuDspSetting')">
                                <a href="{{url('/other/dsp/list')}}">
                                    <span class="sidenav-icon icon icon-clipboard"></span>
                                    <span class="sidenav-label">Display Setting</span>
                                </a>
                            </li> -->
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    @yield('content')


    <div class="layout-footer">
        <div class="layout-footer-body">
            <small class="version">Version 1.1</small>
            <small class="copyright">2016 &copy; RSUD Soreang</small>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{asset('js/socket.io.js')}}"></script>
<script type="text/javascript">
	var b_url = 'http://10.10.10.3';
	var full_url = document.location.protocol+'//'+document.location.hostname;
    function logout() {
        var $form = $('<form />');
        $form.attr('action', '/logout');
        $form.attr('method', 'post');
        $form.css({
            'display': 'none'
        });
        var csrf = $('<input />');
        csrf.attr('type', 'hidden');
        csrf.attr('name', '_token');
        csrf.val($('meta[name="csrf-token"]').attr('content'));
        $form.append(csrf);
        $('body').append($form);
        $form.submit();
    }
</script>

<script src="{{asset('assets/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('js/table.js')}}"></script>
<script src="{{asset('assets/js/elephant.min.js?v=1.1')}}"></script>
<script src="{{asset('assets/js/application.min.js?v=1.1')}}"></script>
{{--<script src="{{asset('assets/js/demo.min.js?v=1.1')}}"></script>--}}
@yield('scripts')
</body>
</html>
