<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('pg_title')</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo/favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo/favicon-16x16.png')}}" sizes="16x16">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="{{asset('assets/css/vendor.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/elephant.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/application.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/demo.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <style>
        body{
        }
    </style>

    <script src="{{asset('assets/js/vendor.min.js?v=1.1')}}"></script>
     <script src="{{asset('js/moment.js')}}"></script>
    <script>
        var base_url = '{{asset('')}}';
    </script>
</head>
<body>
<div>
@yield('content')
</div>


<div class="layout-footer col-md-12" style="margin-left: 0px; position: fixed; z-index: 999">
    <div class="layout-footer-body">
        <small class="version">Version 1.1</small>
        <small class="copyright">2016 &copy; RSUD Soreang</small>
    </div>
</div>

<script src="{{asset('assets/js/elephant.min.js?v=1.1')}}"></script>
<script src="{{asset('assets/js/application.min.js?v=1.1')}}"></script>
<script src="{{asset('assets/js/demo.min.js?v=1.1')}}"></script>
<script src="{{asset('js/socket.io.js')}}"></script>

<script type="text/javascript">
    var full_url = document.location.protocol+'//'+document.location.hostname;
    var socket = io.connect(full_url+':8890');
</script>
@yield('scripts')
</body>
</html>
