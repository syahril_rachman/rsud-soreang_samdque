@extends('layouts.app')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <a href="{{url('dsp/vid/manage')}}" class="btn btn-primary"><i class="icon icon-plus icon-fw"></i>
                        Video Baru</a>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Video Antrian</span>
                </h1>
                <p class="title-bar-description">
                    <small>Kelola <i>Video Antrian</i> pada display TV. Klik tombol tambah di sisi kanan atas aplikasi
                        untuk menambah video baru.
                    </small>
                </p>
            </div>

            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body panel-collapse">
                            <table id="dsp_vid_table" class="table table-bordered table-striped dataTable" cellspacing="0"
                                   width="100%" data-url="{{asset('/')}}">
                                <thead>
                                <tr>
                                    <th width="7%">No.</th>
                                    <th>Video Title</th>
                                    <th>Deskripsi</th>
                                    <th>Status</th>
                                    <th width="7%">#</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Delete -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="labelDelete">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="labelDelete"><i class="icon icon-warning icon-fw"></i> Peringatan!</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id-delete" value=""/>
                    Apakah Anda yakin akan menghapus data ini secara permanen?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-danger" id="btn-delete-yes"><i
                                class="icon icon-trash icon-fw"></i> Hapus
                    </button>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
    <script src="{{asset('js/table_dsp.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var info = '{{ session('status') }}';
            if(info){
                toastr.success(info, 'Berhasil!');
            }

            $(document).on('click', '.btn-delete', function(){
                var id = $(this).data('id');
                $('#id-delete').val(id);
                $('#modal-delete').modal({
                    backdrop: 'static'
                });
            });
            $(document).on('click', '#btn-delete-yes', function(){
                var $id = $('#id-delete').val();

                var $form = $('<form />');
                $form.attr('action', '/dsp/vid/delete');
                $form.attr('method', 'post');
                $form.css({
                    'display': 'none'
                });
                //csrf
                var csrf = $('<input />');
                csrf.attr('type', 'hidden');
                csrf.attr('name', '_token');
                csrf.val($('meta[name="csrf-token"]').attr('content'));
                $form.append(csrf);
                //id
                var id = $('<input />');
                id.attr('type', 'hidden');
                id.attr('name', 'id');
                id.val($id);
                $form.append(id);
                $('body').append($form);
                $form.submit();
            });
        });
    </script>
@endsection