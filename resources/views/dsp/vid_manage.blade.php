@extends('layouts.app')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <a href="{{url('dsp/vid/list')}}" class="btn btn-primary"><i class="icon icon-arrow-left icon-fw"></i> Kembali</a>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Video Antrian</span>
                </h1>
                <p class="title-bar-description">
                    <small> <i>Video Antrian</i> dengan mengisi form dibawah ini.</small>
                </p>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <form class="form form-horizontal" data-toggle="validator" enctype="multipart/form-data" method="post" action="{{url('dsp/vid/manage')}}">
                        <div class="demo-form-wrapper">
                            {{csrf_field()}}
                            <input type="hidden" name="vid_id" id="id" value="{{$vid ? $vid->id : ''}}">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Video</label>
                                <div class="col-sm-9">
                                    <div class="col-sm-9">
                                        <div class="contact-avatar">
                                            <video width="400" controls>
                                              <source 
                                                src="{{$vid && $vid->file ? asset($vid->file) : 'anchor.mp4'}}" 
                                                id="video_here">
                                                Your browser does not support HTML5 video.
                                            </video>

                                            <input 
                                                class="file_multi_video" 
                                                type="file" 
                                                name="userfile" 
                                                id="files"
                                                accept="video/*">
                                        </div>
                                        <small class="help-block" id="warning">Jenis file video: .mp4, .avi, .mpeg, .wmv, dsb</small>
                                        <input type="hidden" name="oldfile" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Deskripsi</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="3" name="description" required>{{$vid ? $vid->description : ''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status Aktif</label>
                                <div class="col-sm-9">
                                    <div class="custom-controls-stacked m-t">
                                        <label class="switch switch-primary">
                                            <input class="switch-input" type="checkbox" name="status" value="A" {{$vid && ($vid->status == 'A') ? 'checked="checked"' : ''}}>
                                            <span class="switch-track"></span>
                                            <span class="switch-thumb"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" class="btn btn-primary"><i class="icon icon-save icon-fw"></i> Simpan</button>
                                    <a href="{{url('dsp/vid/list')}}" class="btn btn-outline-primary">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            document.getElementById("files").onchange = function (evt) {
                var $source = $('#video_here');
                $source[0].src = URL.createObjectURL(this.files[0]);
                $source.parent()[0].load();
            };

        });
    </script>
@endsection