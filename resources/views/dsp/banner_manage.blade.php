@extends('layouts.app')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <a href="{{url('/dsp/banner/list')}}" class="btn btn-primary"><i class="icon icon-arrow-left icon-fw"></i> Kembali</a>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Banner Image</span>
                </h1>
                <p class="title-bar-description">
                    <small> <i>Banner Image</i> dengan mengisi form dibawah ini.</small>
                </p>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <form class="form form-horizontal" data-toggle="validator" enctype="multipart/form-data" method="post" action="{{url('dsp/banner/manage')}}">
                       {{csrf_field()}}
                        <div class="demo-form-wrapper">
                            <input type="hidden" name="bn_id" id="id" value="{{$bn ? $bn->id : ''}}">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Banner Image</label>
                                <div class="col-sm-9">
                                    <div class="col-sm-9">
                                        <div class="contact-avatar">
                                            <label class="contact-avatar-btn">
                                                <span class="icon icon-camera"></span>
                                                <input class="file-upload-input" type="file" name="userfile" id="files">
                                            </label>
                                            <img class="img-rounded" width="256" height="256" id="image" src="{{$bn && $bn->file ? asset($bn->file) : asset('assets/img/uimage.png')}}">
                                        </div>
                                        <small class="help-block" id="warning">Jenis file image: .gif, .png, .jpg, .jpeg</small>
                                        <input type="hidden" name="oldfile" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Deskripsi</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="3" name="description" required>{{$bn ? $bn->description : ''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status Aktif</label>
                                <div class="col-sm-9">
                                    <div class="custom-controls-stacked m-t">
                                        <label class="switch switch-primary">
                                            <input class="switch-input" type="checkbox" name="status" value="A" {{$bn && ($bn->status == 'A') ? 'checked="checked"' : ''}}>
                                            <span class="switch-track"></span>
                                            <span class="switch-thumb"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" class="btn btn-primary"><i class="icon icon-save icon-fw"></i> Simpan</button>
                                    <a href="{{url('dsp/banner/list')}}" class="btn btn-outline-primary">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            document.getElementById("files").onchange = function () {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // get loaded data and render thumbnail.
                    document.getElementById("image").src = e.target.result;
                };

                var file = this.files[0];
                var fileType = file["type"];
                var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(fileType, ValidImageTypes) < 0) {
                    // invalid file type code goes here.
                    $('#image').attr('src', base_url+'assets/img/uicon.png');
                    $('#files').val("");
                    $('#warning').addClass('has-error');
                    $('#warning').empty().text('Tipe file salah! Silakan ulangi');
                }else{
                    // read the image file as a data URL.
                    reader.readAsDataURL(this.files[0]);
                    $('#warning').removeClass('has-error');
                    $('#warning').empty().text('Jenis file image: .gif, .png, .jpg, .jpeg');
                }

            };

        });
    </script>
@endsection