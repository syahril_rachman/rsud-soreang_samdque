@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
@endsection
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <form class="form-inline">
                        <div class="form-group">
                            <input type="text" id="datepicker" class="form-control" name="date"
                                   value="{{$now}}"/>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-default" id="btn-datenow" type="button"><i
                                        class="icon icon-search icon-fw"></i> Cari
                            </button>
                        </div>
                    </form>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Daftar Kunjungan HARI INI</span>
                </h1>
                <!-- <p class="title-bar-description">
                    <small>Daftar kunjungan nasabah pemeriksaan laboratorium.</small>
                </p> -->
            </div>

            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#register" aria-controls="register"
                                                                      role="tab" data-toggle="tab">Pendaftaran</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="register">
                                <br/>
                                <div class="well">Daftar antrian pendaftaran melalui mesin kiosk pada hari <?php echo @
                                    $now;?>.<br/>Pilih tanggal pada isian di pojok kanan atas untuk melakukan pencarian
                                    antrian.
                                </div>
                                <div class="row gutter-xs">
                                    @foreach($metalist as $meta)
                                        <div class="col-xs-6" style="margin-bottom: 40px">
                                            <div class="panel panel-default">
                                                <div class="panel-body panel-collapse" style="height: 550px">
                                                    <table id="table_{{$meta->value}}"
                                                           class="table table-striped dataTable" cellspacing="0"
                                                           width="100%"
                                                           data-link="{{$meta->link}}">
                                                        <thead>
                                                        <tr>
                                                            <th>Antrian {{$meta->description}}</th>
                                                            <th>Status</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('/js/jquery-ui.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $( "#datepicker" ).datepicker({
                "dateFormat":'yy-mm-dd'
                }
            );
        });
    </script>
@endsection