@extends('layouts.app')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <form class="form-inline">
                        <div class="form-group">
                            <input type="text" id="year" name="year" class="form-control" value="{{$year}}"/>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-default" id="btn-search" type="button"><i
                                        class="icon icon-search icon-fw"></i> Cari
                            </button>
                            <!--<button class="btn btn-success btn-icon sq-32" type="button"><i class="icon icon-file-excel-o icon-fw"></i></button>-->
                            <button class="btn btn-primary btn-icon sq-32" type="button" id="btn-search"><i
                                        class="icon icon-refresh icon-fw"></i></button>
                        </div>
                    </form>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Rekapitulasi Tahun </span>
                </h1>
                <p class="title-bar-description">
                    <small>Rekapitulasi antrian nasabah.</small>
                </p>
            </div>

            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body panel-collapse">
                            <div class="well">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4>Keterangan Huruf</h4>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                C = CANCEL - Batal<br/>
                                                P = PENDING - Tunda<br/>
                                                W = WAITING - Menunggu dalam antrian<br/>
                                                D = DONE - Selesai
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive m-b-0">
                                <table class="table table-hover table-nowrap table-bordered m-b-0" width="100%">
                                    <thead>
                                    <tr>
                                        <th rowspan="2">Bulan</th>
                                        @foreach($metalist as $meta)
                                            <th colspan="4">{{$meta->description}}</th>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        @foreach($metalist as $meta)
                                            <th>C</th>
                                            <th>P</th>
                                            <th class="warning">W</th>
                                            <th>D</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($months as $index_month => $month)
                                        <tr>
                                            <td>{{$month}}</td>
                                            @for($i=2; $i<=5; $i++)
                                                <td>{{\App\Sm_pendaftaran::countNumberYearly($year, $index_month, "CL" ,$i) == 0 ? '' : \App\Sm_pendaftaran::countNumberYearly($year, $index_month, "CL" ,$i)}}</td>
                                                <td>{{\App\Sm_pendaftaran::countNumberYearly($year, $index_month, "PD" ,$i) == 0 ? '' : \App\Sm_pendaftaran::countNumberYearly($year, $index_month, "PD" ,$i)}}</td>
                                                <td class="warning">{{\App\Sm_pendaftaran::countNumberYearly($year, $index_month, "WT" ,$i) == 0 ? '' : \App\Sm_pendaftaran::countNumberYearly($year, $index_month, "WT" ,$i)}}</td>
                                                <td>{{\App\Sm_pendaftaran::countNumberYearly($year, $index_month, "DN" ,$i) == 0 ? '' : \App\Sm_pendaftaran::countNumberYearly($year, $index_month, "DN" ,$i)}}</td>
                                            @endfor
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        var url = "{{url('/main/report/list-year?')}}";
        $(document).ready(function () {
            $('#btn-search').on('click', function () {
                $this = $(this);
                var form = $this.closest('form');
                var data = form.serialize();
                var new_url = url + data;
                console.log(new_url);
                window.location.replace(new_url);
            })
        });
    </script>
@endsection