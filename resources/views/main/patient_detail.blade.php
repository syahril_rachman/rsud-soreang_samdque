@extends('layouts.app')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <a href="{{url('main/patient/list')}}" class="btn btn-primary"><i class="icon icon-arrow-left icon-fw"></i> Kembali</a>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Detail Pasien</span>
                </h1>
                <p class="title-bar-description">
                    <small>Detail nasabah yang pernah menggunakan fasilitas laboratorium.</small>
                </p>
            </div>

            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body panel-collapse">
                            <div class="panel panel-primary">
                                <!-- Default panel contents -->
                                <div class="panel-heading">Detail Histori Pasien</div>

                                <table class="table table-bordered">
                                    <tr>
                                        <td class="info" width="20%"><strong>No. Pasien</strong></td>
                                        <td>{{$patient->IdBankNomor}}</td>
                                    </tr>
                                    <tr>
                                        <td class="info"><strong>Nama Pasien</strong></td>
                                        <td>{{$patient->NamaPasien}}</td>
                                    </tr>
                                    <tr>
                                        <td class="info"><strong>Jenis Kelamin</strong></td>
                                        <td>{{$patient->IdJenisKelamin == 'L' ? 'Laki-laki' : 'Perempuan'}}</td>
                                    </tr>
                                    <tr>
                                        <td class="info"><strong>Tanggal Lahir</strong></td>
                                        <td>{{$patient->TglLahir}}</td>
                                    </tr>
                                </table>
                                <div class="panel-body" style="padding: 0px"></div>
                                <!-- Table -->
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="7%">#</th>
                                        <th>Jenis</th>
                                        <th>Ruang Perujuk</th>
                                        <th>Dokter Perujuk</th>
                                        <th>Tanggal Pemeriksaan</th>
                                        <th>Tanggal Pengambilan Hasil</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection