@extends('layouts.app')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <form class="form-inline">
                        <div class="form-group">
                            <select class="form-control" id="month" name="month">
                                @foreach($months as $index => $month)
                                    <option value="{{$index}}" {{$index == $now[1] ? 'selected' : ''}}>{{$month}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" id="year" name="year" class="form-control" value="{{$now[0]}}"/>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-default" id="btn-search" type="button"><i
                                        class="icon icon-search icon-fw"></i> Cari
                            </button>
                        </div>
                    </form>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Rekapitulasi Bulan </span>
                </h1>
                <p class="title-bar-description">
                    <small>Rekapitulasi antrian nasabah.</small>
                </p>
            </div>

            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body panel-collapse">
                            <div class="well">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4>Keterangan Huruf</h4>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                C = CANCEL - Batal<br/>
                                                P = PENDING - Tunda<br/>
                                                W = WAITING - Menunggu dalam antrian<br/>
                                                D = DONE - Selesai
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive m-b-0">
                                <table class="table table-hover table-nowrap table-bordered m-b-0" width="100%">
                                    <thead>
                                    <tr>
                                        <th rowspan="2">Hari & Tanggal</th>
                                        @foreach($metalist as $meta)
                                            <th colspan="4">{{$meta->description}}</th>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        @foreach($metalist as $meta)
                                            <th>C</th>
                                            <th>P</th>
                                            <th class="warning">W</th>
                                            <th>D</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i = 1; $i<= $days; $i++)
                                        @php
                                            $yearMonth = implode("-", $now);
                                            $date = $yearMonth.'-'.$i;
                                            $day = date('D', strtotime($date));
                                            $indo_day = '';
                                            switch ($day){
                                                case "Sun":
                                                $indo_day = 'Minggu';
                                                    break;
                                                case "Mon":
                                                $indo_day = 'Senin';
                                                    break;
                                                case "Tue":
                                                $indo_day = 'Selasa';
                                                    break;
                                                case "Wed":
                                                $indo_day = 'Rabu';
                                                    break;
                                                case "Thu":
                                                $indo_day = 'Kamis';
                                                    break;
                                                case "Fri":
                                                $indo_day = 'Jumat';
                                                    break;
                                                case "Sat":
                                                $indo_day = 'Sabtu';
                                                    break;
                                            }
                                        @endphp
                                        <tr>
                                            <td>{{$indo_day.', '.$i.' '.$indo_month.' '.$now[0]}}</td>
                                            @for($j=2; $j<=5; $j++)
                                                <td>{{\App\Sm_pendaftaran::countNumber($date, 'CL', $j) == 0 ? '' : \App\Sm_pendaftaran::countNumber($date, 'CL', $j)}}</td>
                                                <td>{{\App\Sm_pendaftaran::countNumber($date, 'PD', $j) == 0 ? '' : \App\Sm_pendaftaran::countNumber($date, 'PD', $j)}}</td>
                                                <td class="warning">{{\App\Sm_pendaftaran::countNumber($date, 'WT', $j) == 0 ? '' : \App\Sm_pendaftaran::countNumber($date, 'WT', $j)}}</td>
                                                <td>{{\App\Sm_pendaftaran::countNumber($date, 'DN', $j) == 0 ? '' : \App\Sm_pendaftaran::countNumber($date, 'DN', $j)}}</td>
                                            @endfor
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        var url = "{{url('/main/report/list-month?')}}";
        $(document).ready(function () {
            $('#btn-search').on('click', function () {
                $this = $(this);
                var form = $this.closest('form');
                var data = form.serialize();
                var new_url = url + data;
                window.location.replace(new_url);
            })
        });
    </script>
@endsection