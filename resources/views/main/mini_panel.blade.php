@extends('layouts.front')

@section('pg_title')
    Mini Panel
@endsection

@section('content')
    <div class="col-xs-12" style="background: #2c3e50;">
        <b style="margin-top: 90px;text-align: left;font-size: 17px;color: white;text-transform: uppercase;">
            {{$user ? $user->loket_name : ''}}
        </b>
        <button type="button" class="btn btn-primary pull-right" onclick="location.reload()">
            <i class="icon icon-refresh icon-fw"></i> Refresh
        </button>
    </div>

    <div class="layout-main">
        <div class="row col-xs-12">
            <div class="col-xs-6 text-left">
                <div class="contact-avatar">
                    <label class="contact-avatar-btn btn-bullhorn" style="right: 0;" title="Panggil Ulang">
                        <span class="icon icon-bullhorn"></span>
                    </label>

                    <h1 style="font-size: 50px;color: red;margin-top: 15px;margin-left: 5px;"
                        id="metavalue-{{$user ? $user->id : ''}}">
                        {{ $link }}{{$queue ? sprintf("%03d", $queue->noqueue) : '-'}}
                    </h1>
                    <input type="hidden" id="no-queue" value="{{$queue ? $queue->noqueue : null}}"/>
                    <input type="hidden" id="id-metavalue" value="{{Auth::user()->userlevel}}"/>
                    <input type="hidden" id="id-regid" value="{{$queue ? $queue->regid : null}}"/>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="row col-xs-12">
                    <h4 style="font-size: 16px;">
                        <small id="restq" style="font-weight: 700; color: black;">
                            Sisa Antrian : {{ $restq }}
                        </small>
                    </h4>
                </div>
                <div class="row col-xs-12">
                    <!-- <button type="button" value="Pendaftaran Kiosk" class="btn btn-primary btn-bullhorn" title="Panggil Antrian">
                        <i class="icon icon-bullhorn"></i>
                    </button> -->
                    <button type="button" class="btn btn-primary btn-next-queue" title="Antrian Selanjutnya">
                        <i class="icon icon-share">  Antrian Selanjutnya</i>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('assets/js/playsound.js')}}"></script>
    <script type="text/javascript">
        var base_url = "{{ url('/') }}";
        var init = "{{ $init }}";

        if (init) {
            var queue = $('#no-queue').val();
            var metavalue = $('#id-metavalue').val();
            var desc = "panggil_ulang";
            var regid = $('#id-regid').val();

            $.get(base_url+'/main/mergeSound/'+queue+'/'+metavalue+'/'+desc, function(result){
                playsound(metavalue, regid);
                // $('.btn-bullhorn').text("Memanggil...");
            }, 'json');
        }

        $(document).ready(function () {
            $(document).on('click', '.btn-next-queue', function() {
                $this = $(this);
                $this.prop('disabled', true);
                $('.btn-bullhorn').css("pointer-events", "none");
                $('.btn-pending').prop('disabled', true);

                var no_queue = $('#no-queue');
                var id_metavalue = $('#id-metavalue');
                var id_reqid = $('#id-regid');

                console.log("pressed");
                var queue = $('#no-queue').val();
                var metavalue = $('#id-metavalue').val();
                var desc = "panggil_ulang";
                var regid = $('#id-regid').val();

                var datainput = {
                    '_token': "{{ csrf_token() }}",
                    'metavalue': metavalue,
                    'prevqueue': queue,
                    'regid': regid,
                };

                $.post(base_url+'/miniPanel/next_queue', datainput, function(result){
                    if (!result.queue) {
                        $this.prop('disabled', false);
                        $('.btn-bullhorn').css("pointer-events", "visible");
                        $('.btn-pending').prop('disabled', false);
                        return
                    }

                    no_queue.val(result.queue.noqueue);
                    id_metavalue.val(result.queue.metavalue);
                    id_reqid.val(result.queue.regid);

                    var queue = no_queue.val();
                    var metavalue = id_metavalue.val();
                    var desc = "panggil_ulang";
                    var regid = id_reqid.val();

                    $.get(base_url+'/main/mergeSound/'+queue+'/'+metavalue+'/'+desc, function(result){
                        playsound(metavalue, regid);
                        // $('.btn-bullhorn').text("Memanggil...");
                    }, 'json');

                }, 'json');
            });

            $(document).on('click', '.btn-bullhorn', function() {
                var queue = $('#no-queue').val();
                var metavalue = $('#id-metavalue').val();
                var desc = "panggil_ulang";
                var regid = $('#id-regid').val();

                $.get(base_url+'/main/mergeSound/'+queue+'/'+metavalue+'/'+desc, function(result){
                    playsound(metavalue, regid);
                    // $('.btn-bullhorn').text("Memanggil...");
                }, 'json');
            });

            function playsound(metavalue, regid){
                $.get(base_url+'/main/playSound/'+regid+'/'+metavalue, function(result){
                    if(result.sound){
                        fileAndPlay(result.sound, metavalue, regid);
                    }
                }, 'json');
            }

            function fileAndPlay(fileAudio, metavalue, regid) {
                var audio = new Audio(base_url+'/assets/sounds/temp/'+fileAudio);
                if (audio) {
                    if (!audio.currentTime) {
                        audio.playbackRate = 1.2;
                        audio.play();
                        audio.addEventListener("ended", function(){
                            audio.currentTime = 0;
                            $.post(base_url+'/main/deleteSound/'+fileAudio+'/'+metavalue+'/'+regid, {_token: "{{ csrf_token() }}"});

                            // $('.btn-bullhorn').text("Panggil Ulang");
                        });
                    } else {
                        console.log("audio is paused||is playing");
                    }
                }
            }

            function pad (str, max) {
                str = str.toString();
                return str.length < max ? pad("0" + str, max) : str;
            }

            socket.on('front', function (data) {
                var data_array = JSON.parse(data);
                if(data_array['type'] == 'single'){
                    var number_queue = pad(data_array['queue'], 3);
                    $('#loket-'+data_array['id']).text(data_array['loket_name']);
                    $('#metavalue-'+data_array['id']).text(data_array['link']+number_queue);
                } else if (data_array['type'] == 'hide') {
                    $('#loket-'+data_array['id']).text(data_array['loket_name']);
                    $('#metavalue-'+data_array['id']).text('-');
                }
            });

            socket.on('kiosk-update', function(data) {
                var metavalue = $('#id-metavalue').val();
                if (data==metavalue) {
                    $.post(base_url+'/miniPanel/queue_detail', {_token: "{{ csrf_token() }}"}, function(result){
                        if (result.is_calling) {
                            // $('.btn-bullhorn').prop('disabled', true);
                            $('.btn-bullhorn').css("pointer-events", "none");
                            $('.btn-pending').prop('disabled', true);
                            $('.btn-next-queue').prop('disabled', true);                            
                        } else {
                            // $('.btn-bullhorn').prop('disabled', false);
                            $('.btn-bullhorn').css("pointer-events", "visible");
                            $('.btn-pending').prop('disabled', false);
                            $('.btn-next-queue').prop('disabled', false);
                        }
                    }, 'json');

                    updateRestq();
                }
            });

            function updateRestq() {

                var metavalue = $('#id-metavalue').val();
                $.get(base_url+'/miniPanel/getRestq/'+metavalue, function(result){
                    $('#restq').text("Sisa Antrian : "+result.restq);
                }, 'json');
            }
        });
    </script>
@endsection