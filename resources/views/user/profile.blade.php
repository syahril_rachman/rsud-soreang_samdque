@extends('layouts.app')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <a href="{{url('/user')}}" class="btn btn-primary"><i
                                class="icon icon-arrow-left icon-fw"></i> Kembali</a>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Pengaturan User</span>
                </h1>
                <p class="title-bar-description">
                    <small> data user. Isi form dibawah ini.</small>
                </p>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <form class="form form-horizontal" data-toggle="validator" enctype="multipart/form-data"
                          method="post" action="{{url('/user/manage')}}">
                        {{csrf_field()}}
                        <div class="demo-form-wrapper">
                            <input type="hidden" name="user_id" value="{{$user ? $user->id : ''}}">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Upload Foto</label>
                                <div class="col-sm-9">
                                    <div class="contact-avatar">
                                        <label class="contact-avatar-btn">
                                            <span class="icon icon-camera"></span>
                                            <input class="file-upload-input" type="file" name="userfile" id="files">
                                        </label>
                                        <img class="img-rounded" width="128" height="128" id="image"
                                             src="{{$user && $user->upload ? asset($user->upload) : asset('assets/img/uicon.png')}}"
                                             accept="image/*" data-type='image'>
                                    </div>
                                    <small class="help-block" id="warning">Jenis file image: .gif, .png, .jpg, .jpeg
                                    </small>
                                    <input type="hidden" name="oldfile" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">No. Induk Pegawai</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" name="empid"
                                           value="{{$user ? $user->empid : ''}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama Pegawai</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" name="empname"
                                           value="{{$user ? $user->empname : ''}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama Loket</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" name="loket_name"
                                           value="{{$user ? $user->loket_name : ''}}">
                                </div>
                            </div>
                        </div>

                        <div class="demo-form-wrapper">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Username</label>
                                <div class="col-sm-9">
                                    <div class="input-with-icon">
                                        <input class="form-control" type="text" name="username" id="user"
                                               value="{{$user ? $user->username : ''}}" required>
                                        <span class="icon icon-user input-icon"></span>
                                    </div>
                                    <small id="userwarning" style="display: none">Username sudah digunakan!</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{$user ? 'Ubah Password' : 'Password'}}</label>
                                <div class="col-sm-9">
                                    @if($user)
                                        <div class="custom-controls-stacked m-t">
                                            <label class="switch switch-primary" id="chbut">
                                                <input class="switch-input" type="checkbox" id="chbox">
                                                <span class="switch-track"></span>
                                                <span class="switch-thumb"></span>
                                            </label>
                                        </div>
                                        <br/>
                                        <input class="form-control" type="password" name="password" value=""
                                               id="newpass"
                                               style="display: none" placeholder="Ketik password baru disini...">
                                        <input class="form-control" type="hidden" name="oldpass"
                                               value="">
                                    @else
                                        <input class="form-control" type="password" name="password" value="" required>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Akses User</label>
                                <div class="col-sm-9">
                                    <select name="userlevel" class="form-control" required>
                                        <option value="">--Pilih--</option>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}" {{$user && ($user->roles[0]->id == $role->id) ? 'selected' : '' }}>{{$role->display_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" class="btn btn-primary"><i
                                                class="icon icon-save icon-fw"></i> Simpan
                                    </button>
                                    <a href="{{url('/user')}}" class="btn btn-outline-primary">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            document.getElementById("files").onchange = function () {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // get loaded data and render thumbnail.
                    document.getElementById("image").src = e.target.result;
                };

                var file = this.files[0];
                var fileType = file["type"];
                var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if ($.inArray(fileType, ValidImageTypes) < 0) {
                    // invalid file type code goes here.
                    $('#image').attr('src', base_url + 'assets/img/uicon.png');
                    $('#files').val("");
                    $('#warning').addClass('has-error');
                    $('#warning').empty().text('Tipe file salah! Silakan ulangi');
                } else {
                    // read the image file as a data URL.
                    reader.readAsDataURL(this.files[0]);
                    $('#warning').removeClass('has-error');
                    $('#warning').empty().text('Jenis file image: .gif, .png, .jpg, .jpeg');
                }

            };

            $('#chbut').click(function () {
                var ch = $('#chbox').prop('checked');
                if (ch == true) {
                    $('#newpass').show();
                    $('#newpass').attr('required', 'required');
                } else {
                    $('#newpass').hide();
                    $('#newpass').removeAttr('required');
                }
            });

            //checking username
            $('#user').keyup(function () {
                var idx = $('#id').val();
                var value = $(this).val();

                $.getJSON(base_url + 'user/json_username/' + value + '/' + idx, function (result) {
                    console.log(result.status);
                    if (result.status == 'YES') {
                        $('#userwarning').show();
                        $('#user').val("");
                    } else {
                        $('#userwarning').hide();
                    }


                });
            });
        });
    </script>

@endsection