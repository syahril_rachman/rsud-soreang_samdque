<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Print</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link rel="stylesheet" href="{{asset('assets/fonts/roboto.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vendor.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/elephant.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/application.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/demo.min.css?v=1.1')}}">
    <style>
      body{
        background: none;
        color: #000;
      }
      .content-print{
        text-align: center;
        background: none;
        width: 80mm;
        padding-bottom: 20px;

      }
      h3 small{
        color: #000;
      }
      h1{
        font-size: 60px;
      }
      @page{
        margin: 0;
      }
      @media print {
        @page{
          margin: 0;
          padding: 0;
          text-align: center;
        }
        .content-print{

          background: none;
          width: 80mm;
          padding-bottom: 20px;
          border-bottom: 1px solid #000;
        }
        h3 small{
          color: #000;
        }
        body{
          font-size: 12px;
        }
        h1{
          font-size: 60px;
        }
        div.page-break {
          display: block; page-break-before: always;
        }
      }
    </style>
  <style>
    #section-to-print{
      margin-top: 10px;
    }

    @media print {
      body * {
      visibility: hidden;
      }
      #section-to-print, #section-to-print * {
      visibility: visible;
      }
      #section-to-print {
      position: absolute;
      left: 0;
      top: 0;
      }
    }
  </style>
  </head>
  <body onload="window.print()">

    <!-- <div class="content-print" id="section-to-print" style="margin-top: 0px;">
      <small>RSUD Soreang <br>Kabupaten Bandung</small>
      <h3 style="margin-top: 10px;"><small>Nomor Antrian</small><br>PASIEN {{ $metavalue->description }}</h3>
      <hr style="border-top: 1px dashed #000;margin-top: 10px;margin-bottom: 10px;">
      <h1 style="margin-top: 10px;">{{ $noqueue }}</h1>
      <hr style="border-top: 1px dashed #000;margin-top: 10px;margin-bottom: 10px;">
      <small>Antrian untuk <br>Hari {{ $now }} {{$time}}</small>
      <br>
      <div style="margin-top: 10px;">
        
      </div>
      <b style="font-size: 12px;">Penting!</b><br>
      <small style="font-size: 11px">Simpan struk antrian <br>sampai proses pendaftaran selesai</small>
    </div> -->

    <div class="content-print" id="section-to-print" style="margin-top: 0px;">
      <table width="100%" style="font-size: 90%;">
        <tr>
          <td></td>
          <td style="text-align: left; line-height: 125%;"><small>RSUD Soreang</small></td>
          <td style="text-align: right;"><small>{{ $now }} {{$time}}</small></td>
        </tr>
      </table>      
      <h3 style="margin-top: 10px;"><small>Nomor Antrian</small><br>{{ $metavalue->description }}</h3>
      <hr style="border-top: 1px dashed #000;margin-top: 10px;margin-bottom: 10px;">
      <h1 style="margin-top: 10px;">{{ $noqueue }}</h1>
      <hr style="border-top: 1px dashed #000;margin-top: 10px;margin-bottom: 10px;">
      <small style="font-size: 11px">
        Jika terlewat lebih dari 10 antrian<br>
        silahkan ambil no antrian baru
      </small>      
      <br>
      <b style="font-size: 12px;">Terima Kasih</b><br>
    </div>

    <!--<img src="{{asset('assets/css/barcode3.png')}}"/>-->
    <div class="page-break"></div>
  </body>
</html>
