@extends('layouts.front')

@section('pg_title')
    Display Antrian Loket
@endsection

@section('content')
    <div class="container-fluid" style="  overflow: hidden !important;">
        <div class="row">
            <div class="col-md-6" style="padding:0px;height: 100%; border-right: 2px solid; height: 930px !important;">
                <div class="main-title">
                    DAFTAR ANTRIAN
                </div>

                <div class="layout-main" style="padding: 10px 20px">
                    <div class="row">
                        <div class="col-md-12">

                            <h1 id="loket-{{$user1 ? $user1->id : ''}}"
                                class="text-center"
                                style="text-transform: uppercase; font-size: 150px">{{$user1 ? $user1->loket_name : ''}}</h1>
                            <br>
                            <h1 style="text-align: center; font-size: 300px; color: red"
                                id="metavalue-{{$user1 ? $user1->id : ''}}">{{ $link1 }}{{$metavalue1 ? sprintf("%03d", $metavalue1->noqueue) : '-'}}</h1>

                        </div>
                    </div>


                </div>
            </div>
            <div class="col-md-6" style="padding:0px; border-left: 2px solid; height: 930px !important;">
                <div class="main-title">
                    DAFTAR ANTRIAN
                </div>

                <div class="layout-main" style="padding: 10px 20px">
                    <div class="row">
                        <div class="col-md-12">

                            <h1 id="loket-{{$user2 ? $user2->id : ''}}"
                                class="text-center"
                                style="text-transform: uppercase; font-size: 150px">{{$user2 ? $user2->loket_name : ''}}</h1>
                            <br>
                            <h1 style="text-align: center; font-size: 300px; color: red"
                                id="metavalue-{{$user2 ? $user2->id : ''}}">{{ $link2 }}{{$metavalue2 ? sprintf("%03d", $metavalue2->noqueue) : '-'}}</h1>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            function pad(str, max) {
                str = str.toString();
                return str.length < max ? pad("0" + str, max) : str;
            }

            function blink(selector) {
                selector.fadeOut(800).fadeIn(500).fadeOut(800).fadeIn(500).fadeOut(800).fadeIn(500);
            }

            socket.on('front', function (data) {
                var data_array = JSON.parse(data);
                if (data_array['type'] == 'single') {
                    var number_queue = pad(data_array['queue'], 3);
                    var selector = $('#metavalue-' + data_array['id']);
                    selector.text(data_array['link'] + number_queue);
                    blink(selector);
                } else if (data_array['type'] == 'hide') {
                    $('#loket-' + data_array['id']).text(data_array['loket_name']);
                    $('#metavalue-' + data_array['id']).text('-');
                }
            });
        });
    </script>
@endsection