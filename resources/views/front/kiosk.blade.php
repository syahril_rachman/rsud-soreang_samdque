<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $pageTitle }}</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo/favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo/favicon-16x16.png')}}" sizes="16x16">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{asset('assets/fonts/roboto.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vendor.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/elephant.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/application.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/demo.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <style>
        html,
        body {
            height: 100%;
            width: 100%;
            margin: 0;
        }

        .menu-title-0 {
            background-color: #0b7505;
        }
        .menu-title-1 {
            background-color: #0b7505;
        }
        .menu-title-2 {
            background-color: #093969;
        }
        .menu-title-3 {
            background-color: #2c3e50;
        }
        .menu-title-4 {
            background-color: #800303;
        }

        .btn:hover {
            color: #000;
            text-decoration: none;
        }
        .btn-outline-primary-0:hover{
            background-color: #0b7505 !important;
            border-color:#0b7505 !important;
            color:#fff
        }
        .btn-outline-primary-1:hover{
            background-color: #0b7505 !important;
            border-color:#0b7505 !important;
            color:#fff
        }
        .btn-outline-primary-2:hover{
            background-color: #093969 !important;
            border-color: #093969 !important;
            color:#fff
        }
        .btn-outline-primary-3:hover{
            background-color: #2c3e50 !important;
            border-color: #2c3e50 !important;
            color:#fff
        }
        .btn-outline-primary-4:hover{
            background-color: #800303 !important;
            border-color: #800303 !important;
            color:#fff
        }
        /*.menu-title:hover {
            background-color: #fff;
        }        
        .menu-title:hover span {
            font-size: 50px;
            color: #2c3e50;
        }*/

    </style>

    <script src="{{asset('assets/js/vendor.min.js?v=1.1')}}"></script>
    <script>
        var base_url = '{{asset('')}}';
    </script>

    <!-- PRINT -->
    <script type="text/javascript">
        function closePrint () {
            document.body.removeChild(this.__container__);
        }

        function setPrint () {
            this.contentWindow.__container__ = this;
            this.contentWindow.onbeforeunload = closePrint;
            this.contentWindow.onafterprint = closePrint;
            this.contentWindow.focus(); // Required for IE
            this.contentWindow.print();
        }

        function printPage (sURL) {
            var oHiddFrame = document.createElement("iframe");
            oHiddFrame.onload = setPrint;
            oHiddFrame.style.visibility = "hidden";
            oHiddFrame.style.position = "fixed";
            oHiddFrame.style.right = "0";
            oHiddFrame.style.bottom = "0";
            oHiddFrame.src = sURL;
            document.body.appendChild(oHiddFrame);
        }

        function loadOtherPage(sURL) {
            $("<iframe>").hide().attr("src", sURL).appendTo("body");
        }
    </script>
</head>
<body>
<div class="layout-main">
    <div class="row-fluid kiosk">
        <div class="col-md-12" data-toggle="match-height">
            <div class="layout-content" style="margin-left: 0px">
                <div class="layout-content-body">
                    <div class="title-bar">
                        <!-- <h1 class="title-bar-title">
                            <span class="d-ib">Selamat Datang</span>
                        </h1>
                        <p class="title-bar-description">
                            <small>Terima kasih telah menggunakan fasilitas kiosk untuk melakukan pendaftaran pelayanan rumah sakit.</small>
                        </p> -->
                    </div>

                    <div class="row gutter-xs">
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-body panel-collapse">
                                    <h2 align="center" style="font-size:40px;">
                                        SILAHKAN AMBIL NO ANTRIAN PENDAFTARAN<br>
                                        SESUAI DENGAN TUJUAN PELAYANAN
                                    </h3>
                                    <br/>

                                    <div class="row">
                                        <div class="col-xs-6" style="margin-top:5px; margin-bottom: 5px; border-radius: 6px;padding-left:;padding-right:3%">
                                            <div id="div_lt_1" class="btn btn-lg btn-block btn-thick">
                                                <div id="menu_title_lt_1" class="col-md-12 menu-title-0" style="padding: 20px 0px 12px 0px; margin-top: 0px">
                                                    <span style="font-size: 50px;color: #fff;">BPJS Lantai 1</span>
                                                </div>
                                                <br><br>
                                                <div
                                                    class="col-md-6 btn-outline-primary-0"
                                                    id="pr-2"
                                                    value="2"
                                                    onclick="printStruk(2)">
                                                    <div class="col-md-12 text-left" style="font-size:20px;margin-top: 0px">
                                                        Tujuan Klinik
                                                    </div>
                                                    <br><br>
                                                    <div class="row" style="font-weight: 12px; font-size: 15px; margin-top: 18px;">
                                                        <div class="col-md-12 col-md-offset-1"> 
                                                            <div class="text-left">
                                                                <div class="col-md-6">
                                                                    <ul>
                                                                        <li style="font-size:20px;">Klinik Dalam</li>
                                                                        <li style="font-size:20px;">Klinik Jantung</li>
                                                                    </ul>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="col-md-6 btn-outline-primary-0"
                                                    id="pr-3"
                                                    value="3"
                                                    onclick="printStruk(3)">
                                                    <div class="col-md-12 text-left" style="font-size:20px;margin-top: 0px">
                                                        Tujuan Klinik
                                                    </div>
                                                    <br><br>
                                                    <div class="row" style="font-weight: 12px; font-size: 15px; margin-top: 18px;">
                                                        <div class="col-md-12 col-md-offset-1"> 
                                                            <div class="text-left">
                                                                <div class="col-md-6">
                                                                    <ul>
                                                                        <li style="font-size:20px;">Klinik Bedah</li>
                                                                        <li style="font-size:20px;">Klinik Kebidanan</li>
                                                                        <li style="font-size:20px;">Klinik Rehab Medis</li>
                                                                        <li style="font-size:20px;">Klinik Saraf</li>
                                                                    </ul>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php foreach($metas as $index => $row):?>
                                            @if ($index >= 2)
                                                <div 
                                                    id="div_{{$index}}"
                                                    class="col-xs-6" style="margin-top:{{ $index==3 || $index==4 ? '35px': '5px' }}; margin-bottom: 5px; border-radius: 6px;padding-left:{{ $index==2 || $index==4 ? '3%': '' }};padding-right:{{ $index==3 ? '3%': '' }}">
                                                    <button 
                                                        type="button"
                                                        id="pr-<?php echo $row['value'];?>" 
                                                        value="<?php echo $row['value'];?>" 
                                                        class="btn btn-outline-primary btn-outline-primary-{{$index}} btn-lg btn-block btn-thick btn-print" 
                                                        style="min-height: 280px; padding: 0 0 0;border-radius: 6px;">
                                                        <div class="col-md-12 menu-title-{{$index}}" style="padding: {{$row['description']!='SKTM' ? "20px 0px 12px 0px" : "0px"}}; margin-top: {{ $index==3 || $index==4 ? '-65px': '0px' }}">
                                                            @if ($row['description']=='SKTM')
                                                                <span style="font-size: 50px;color: #fff;">
                                                                    <?php echo $row['description'];?>
                                                                </span>
                                                                <br>
                                                                <span style="font-size: 25px;color: #fff;">
                                                                    BPJS Ketenagakerjaan, Jamkesda, Taspen
                                                                </span>
                                                            @else
                                                                <span style="font-size: 50px;color: #fff;">
                                                                    <?php echo $row['description'];?>
                                                                </span>
                                                            @endif
                                                        </div>
                                                        <br/><br/>
                                                        @if ($row['description']=='SKTM')
                                                            <div class="col-md-12 text-left" style="font-size:20px;">
                                                        @else
                                                            <div class="col-md-12 text-left" style="font-size:20px;margin-top: {{ $index==3 ? '-45px': '0px' }}">
                                                        @endif
                                                            Tujuan Klinik
                                                        </div>
                                                        <br/><br/>
                                                        <div class="row" style="
                                                            font-weight: 12px; font-size: 15px; margin-top: 18px;">
                                                            @if (strpos($row['description'], 'Lantai 1')  !== false)
                                                                <div class="col-md-12 col-md-offset-1">
                                                                    <div class="text-left">
                                                                        <div class="col-md-6">
                                                                            <ul>
                                                                                <li style="font-size:20px;">Klinik Rehab Medis</li>
                                                                                <li style="font-size:20px;">Klinik Dalam</li>
                                                                                <li style="font-size:20px;">Klinik Bedah</li>
                                                                                <li style="font-size:20px;">Klinik Kebidanan</li>
                                                                                <li style="font-size:20px;">Klinik Jantung</li>
                                                                                <li style="font-size:20px;">Klinik Saraf</li>
                                                                            </ul>
                                                                        </div>
                                                                    </div> 
                                                                </div>
                                                            @elseif (strpos($row['description'], 'Lantai 2') !== false)
                                                                <div class="col-md-12 col-md-offset-1">
                                                                    <div class="text-left">
                                                                        <div class="col-md-6">
                                                                            <ul>
                                                                                <li style="font-size:20px;">Klinik Kemuning</li>
                                                                                <li style="font-size:20px;">Klinik Kulit dan Kelamin</li>
                                                                                <li style="font-size:20px;">Klinik Bedah Mulut</li>
                                                                                <li style="font-size:20px;">Klinik Gizi</li>
                                                                                <li style="font-size:20px;">Klinik THT</li>
                                                                                <li style="font-size:20px;">Klinik Mata</li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <ul>            
                                                                                <li style="font-size:20px;">Klinik Gigi</li>
                                                                                <li style="font-size:20px;">Klinik Orthopedi</li>
                                                                                <li style="font-size:20px;">Klinik Anak</li>
                                                                                <li style="font-size:20px;">Klinik Dot</li>
                                                                                <li style="font-size:20px;">Klinik Jiwa</li>                                                                        
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @else                                                        
                                                                <span class="col-md-12" style="font-size: 26px;margin-top: -10px;">
                                                                    Semua Klinik
                                                                </span>
                                                                <br/><br/>
                                                            @endif
                                                        </div>
                                                    </button>
                                                </div>
                                            @endif
                                        <?php endforeach?>
                                    </div>
                                    <br/>

                                    <div class="well">
                                        <div class="text-center">
                                            Struk antrian akan dicetak secara otomatis
                                        </div>
                                        <!-- <div class="text-left">
                                            <h4>Keterangan : </h4>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <h5>BPJS Lt 1 untuk:</h5>
                                                    <ul>
                                                        <li>Klinik Rehab Medis</li>
                                                        <li>Klinik Dalam</li>
                                                        <li>Klinik Bedah</li>
                                                        <li>Klinik Kebidanan</li>
                                                        <li>Klinik Jantung</li>
                                                        <li>Klinik Saraf</li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-5">
                                                    <h5>BPJS Lt 2 untuk:</h5>
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li>Klinik Kemuning</li>
                                                            <li>Klinik Orthopedi</li>
                                                            <li>Klinik Bedah Mulut</li>
                                                            <li>Klinik Gizi</li>
                                                            <li>Klinik THT</li>
                                                            <li>Klinik Mata</li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <ul>
                                                            <li>Klinik Gigi</li>
                                                            <li>Klinik Kulit dan Kelamin</li>
                                                            <li>Klinik Anak</li>
                                                            <li>Klinik Dot</li>
                                                            <li>Klinik Jiwa</li>
                                                            <li>Klinik Umum</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 widget">
            <!-- RUNNING TEXT -->
            <div class="panel panel-default" style="display: block;">
                <div class="panel-body panel-collapse">
                    <div class="marquee">
                        <?php if(@$runningtext):?>
                        <marquee>
                            <?php foreach($runningtext as $row):?>
                            <span><?php echo $row['settingvalue'];?></span>
                            <?php endforeach?>
                        </marquee>
                        <?php endif?>
                    </div>
                    <br/>

                    <!-- BANNER -->
                <!--
              <?php if(@$banner):?>
                <div class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner" role="listbox">
                    <?php $i=1;?>
                <?php foreach($banner as $row):?>
                        <div class="item <?php echo $i == 1 ? 'active' : '';?>">
                        <img src="" width="100%"/>
                      </div>
                      <?php $i++;?>
                <?php endforeach?>
                        </div>
                      </div>
                     <?php endif?>
                        <br/>
                        -->
                    <!-- ADV -->
                <!--
              <?php if(@$adv):?>
                <div class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner" role="listbox">
                    <?php $i=1;?>
                <?php foreach($adv as $row):?>
                        <div class="item <?php echo $i == 1 ? 'active' : '';?>">
                        <img src="" width="100%"/>
                      </div>
                      <?php $i++;?>
                <?php endforeach?>
                        </div>
                      </div>
                    <?php endif?>
                        -->
                </div>
            </div>


        </div>
    </div>
</div>

<div class="layout-footer" style="margin-left: 0px; position: fixed;">
    <div class="layout-footer-body">
        <small class="version">Version 1.1</small>
        <small class="copyright">2016 &copy; RSUD Soreang</small>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="modal-notif" tabindex="-1" role="dialog" aria-labelledby="labelNotif">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h3>Terima Kasih <i class="icon icon-thumbs-up icon-fw"></i><br/><small>Silakan ambil struk antrian Anda</small></h3>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('assets/js/elephant.min.js?v=1.1')}}"></script>
<script src="{{asset('assets/js/application.min.js?v=1.1')}}"></script>
<script src="{{asset('assets/js/demo.min.js?v=1.1')}}"></script>

<script>
    var base_url = "{{ url('/') }}";    

    $(document).ready(function(){
        $(document).on('click', '.btn-print', function(){
            var ch = $(this).val();
            
            printStruk(ch);
        });

        var mtitleHeight = $('#menu_title_lt_1').height();
        var mHeight = mtitleHeight + 35;
        var divHeight = $('#div_2').height();

        $('#div_lt_1').attr('style','height: '+divHeight+'px;padding: 0;border-radius: 6px;border-color:#2c3e50 !important;');
        $('#pr-2').attr('style','height: '+(divHeight - mHeight)+'px;border-right:1px solid  #2c3e50;');
        $('#pr-3').attr('style','height: '+(divHeight - mHeight)+'px;border-left:1px solid  #2c3e50;');
    });

    function printStruk(ch) {
        /* SAVE DATABASE */
        $.post(base_url+'/display/inputKiosk/'+ch, {_token: "{{ csrf_token() }}"}, function(result){
            /* PRINTING KIOSK EXECUTION */
            /*printPage(base_url+'/display/printStruk/'+result.noqueue+'/'+result.metavalue);*/
            loadOtherPage(base_url+'/display/printStruk/'+result.noqueue+'/'+result.metavalue);
        }, 'json');

        $('#modal-notif').modal({
            backdrop: 'static'
        });


        setTimeout(function(){ $('#modal-notif').modal('hide') }, 1000);
    }

    $(function(){
        $('.kiosk').slimScroll({
            height: '100%'
        });
    });


</script>
</body>
</html>