@extends('layouts.front')

@section('pg_title')
Display Utama
@endsection

@section('content')
    <div id="headerArea" class="main-title" style="font-size: 50px;">
        DAFTAR ANTRIAN
        <p class="pull-right"><span id='ct'></span></p>
    </div>

    <div id="bodyArea" class="layout-main container">
        <div class="row">
            <div id="antrianArea" class="col-md-12">
                <div class="col-md-3">
                    <h2 style="font-size: 32px;">BPJS Lt1 (B1) <br/>
                        <small style="color: black;">Total Antrian: <span id='metavalue-2'>{{$metavalue_2}}</span></small><br/>
                        <small style="color: black;">Sudah diproses: <span id='opr-2'>{{$opr_2}}</span></small>
                    </h2>
                    <hr/>
                    <ul>

                    </ul>
                </div>
                <div class="col-md-3">
                    <h2 style="font-size: 32px;">BPJS Lt1 (B2)<br/>
                        <small style="color: black;">Total Antrian: <span id='metavalue-3'>{{$metavalue_3}}</span></small><br/>
                        <small style="color: black;">Sudah diproses: <span id='opr-3'>{{$opr_3}}</span></small>
                    </h2>
                    <hr/>
                    <ul>

                    </ul>
                </div>
                <div class="col-md-2">
                    <h2 style="font-size: 32px;">BPJS Lt2 (A)<br/>
                        <small style="color: black;">Total Antrian: <span id='metavalue-4'>{{$metavalue_4}}</span></small><br/>
                        <small style="color: black;">Sudah diproses: <span id='opr-4'>{{$opr_4}}</span></small>
                    </h2>
                    <hr/>
                    <ul>

                    </ul>
                </div>
                <div class="col-md-2">
                    <h2 style="font-size: 32px;">SKTM (C)<br/>
                        <small style="color: black;">Total Antrian: <span id='metavalue-5'>{{$metavalue_5}}</span></small><br/>
                        <small style="color: black;">Sudah diproses: <span id='opr-5'>{{$opr_5}}</span></small>
                    </h2>
                    <hr/>
                    <ul>

                    </ul>
                </div>
                <div class="col-md-2">
                    <h2 style="font-size: 32px;">Umum (D)<br/>
                        <small style="color: black;">Total Antrian: <span id='metavalue-6'>{{$metavalue_6}}</span></small><br/>
                        <small style="color: black;">Sudah diproses: <span id='opr-6'>{{$opr_6}}</span></small>
                    </h2>
                    <hr/>
                    <ul>

                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <video id="myVideo" width="auto" controls muted autoplay onended="run();">
                    <source id="ss">                    
                </video>
            </div>
            <div class="col-md-6">
                <div class="slideShowContainer">
                  
                </div>
                
                <!-- <div id="slideShowArea" class="slideshow-container"></div> -->
            </div>
        </div>
        <div id="footerArea" class="row col-md-12" style="position: fixed; bottom: 20px;">
            <div class="col-md-12 widget">
                <!-- RUNNING TEXT -->
                <div class="panel panel-default" style="display: block;">
                    <div class="panel-collapse">
                        <div class="marquee row" style="padding:8px;">
                            <div id="rts">
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
@endsection

@section('scripts')
    <script src="{{asset('js/moment.js')}}"></script>
    <script src="{{asset('assets/js/image-scale.min.js')}}"></script>
    <script type="text/javascript">
        videoPlayer = document.getElementById("ss");
        video=document.getElementById("myVideo");

        video_count =1;
        videos = [];

        function run(){            
            if (video_count == videos.length) {
                video_count = 0;                
            }
            
            videoPlayer.setAttribute("src", videos[video_count]);
            video.load();
            video.play();

            video_count++;
       }       

       var slideIndex = 0;
       function carousel() {
            var i;
            var x = document.getElementsByClassName("mySlides");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none"; 
            }
            slideIndex++;
            if (slideIndex > x.length) {slideIndex = 1} 
            x[slideIndex-1].style.display = "block";

            // $(".slideShowContainer img")
            //     .imageScale();

            setTimeout(carousel, 60000); 
       }

       function showSlides() {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            //var dots = document.getElementsByClassName("dot");
            for (i = 0; i < slides.length; i++) {
               slides[i].style.display = "none";  
            }
            slideIndex++;
            if (slideIndex> slides.length) {slideIndex = 1}    
            /*for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }*/
            slides[slideIndex-1].style.display = "block";  
            //dots[slideIndex-1].className += " active";
            setTimeout(showSlides, 2000); // Change image every 2 seconds
        }

        function update() {
            $('#ct').html(moment().format('H:mm:ss'));
        }

        setInterval(update, 1000);

        var base_asset = "{{ asset('/') }}";

        function reSize () {
            var hWindow = $(window).height();
            var hHeaderArea = $('#headerArea').height();
            var hAntrianArea = $('#antrianArea').height();
            var hFooterArea = $('#footerArea').height();

            var hBody = hWindow - (hHeaderArea + 20);
            $('#bodyArea').attr('style','height: '+hBody+'px');

            var hVideo = hWindow - (hHeaderArea + hFooterArea + hAntrianArea + 80);
            
            $('#slideShowArea').attr('style','height: '+hVideo+'px; max-width: 100%; overflow: hidden;');
            $('#myVideo').attr('style','height: '+hVideo+'px');
            $('#myVideo').addClass('hide');
            $('#myVideo').removeClass('hide');
        }

        $(document).ready(function () {            
            socket.on('front', function (data) {
                var data_array = JSON.parse(data);
                console.log(data, data_array);
                if(data_array['type'] == 'list'){
                    $('#metavalue-'+data_array['metavalue']).html(data_array['new_total']);
                }
            });

            socket.on('message', function (data) {
                if (data == 'dsp_runningtext') {
                    $("#rts").empty();                    
                    getRTs();
                }
                if (data == 'dsp_banner') {
                    getBNs();
                }
                if (data == 'dsp_adv') {
                    $(".slideshow-container").empty();
                    // getAdvs();

                    $(".slideShowContainer").empty();
                    getAdvs1();
                }
                if (data == 'dsp_vid') {
                    videos = [];
                    getVids();
                }                
            });

            socket.on('kiosk-update', function(data) {
                $.get(base_url+'display/register/get_opr/'+data, function(result){                    
                    $('#opr-'+data).text(result.opr);
                }, 'json');
            });

            function getVids () {
                $.get( "/display/api/dsp-list/VID", function( data ) {
                    // console.log('getting videos');
                    $.each(data.datas, function( index, value ) {
                        videos[index] = base_asset+value.file
                    });

                    video_count = 0;
                    videoPlayer.setAttribute("src", videos[video_count]);
                    video.load();
                    video.play();
                    video_count++;
                });
            };

            function getRTs () {
                $.get("/display/api/dsp-list/RT", function (data) {
                    // console.log('getting runningtexts');

                    var rts = '';
                    $.each(data.datas, function( index, value ) {
                        var font = value.setting['font'];
                        rts += '<span style="font-size: '+ font +'px;">'+value.description+'</span>';
                    });

                    $("#rts").append(
                            "<marquee>"+
                                rts+
                            "</marquee>"
                        );
                });
            };

            function getAdvs () {
                $.get("/display/api/dsp-list/ADV", function (data) {
                    var adv = '';
                    $.each(data.datas, function( index, value ) {
                        adv += '<div class="mySlides" style="height: inherit;">'+
                                  '<img src='+base_asset+value.file+' style="height: inherit;">'+
                               '</div>';
                    });

                    $(".slideshow-container").append(adv);
                    showSlides();     
                });
            };

            function getAdvs1 () {
                $.get("/display/api/dsp-list/ADV", function (data) {
                    var adv = '';
                    $.each(data.datas, function( index, value ) {
                        adv += '<div class="mySlides" style="width: auto;max-height: 100%;">'+
                                    value.description+
                               '</div>';                        
                    });

                    $(".slideShowContainer").append(adv);
                    carousel();
                });
            };

            function getBNs () {
                $.get("/display/api/dsp-list/BN", function (data) {
                    console.log('todo == getting banners');
                });
            };

            getRTs();
            // getAdvs();
            getAdvs1();
            getVids();
            reSize();
        });

        $(function() {
            $(window).bind('resize', function() {
                reSize();
            });
        });
    </script>
@endsection