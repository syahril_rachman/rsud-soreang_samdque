<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RSUD Soreang</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">

    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo/favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo/favicon-16x16.png')}}" sizes="16x16">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="{{asset('assets/css/vendor.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/elephant.min.css?v=1.1')}}">
    <link rel="stylesheet" href="{{asset('assets/css/login-2.min.css?v=1.1')}}">
</head>
<body>
<div class="login">
    <div class="login-body">
        <a class="login-brand" href="{{asset('')}}">
            <img class="img-responsive" src="{{asset('assets/img/logo/logo4.png')}}" alt="SIMRS Assistance">
        </a>
        <div class="login-form">
            <form role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="username">Username</label>
                    <input id="username" class="form-control" type="text" name="username" required>
                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input id="password" class="form-control" type="password" name="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <button class="btn btn-primary btn-block" type="submit" name="submit" value="submit">Login</button>
            </form>
        </div>
    </div>
    <div class="login-footer">
        Hubungi Administrator untuk mendapatkan akun baru.<br/>

        2016 &copy; RSUD Soreang<br/><br/>
        <small>
            <a href="{{asset('display/kiosk')}}" target="_blank" style="margin: 2px 5px">Kiosk</a>
            <a href="{{asset('display/register')}}" target="_blank" style="margin: 2px 5px">Display Antrian</a>
            <!-- <a href="{{asset('display/examine')}}" target="_blank" style="margin: 2px 5px">Pemeriksaan</a>
            <a href="{{asset('display/result')}}" target="_blank" style="margin: 2px 5px">Pengambilan Hasil</a> -->
        </small>
    </div>
</div>
<script src="{{asset('assets/js/vendor.min.js?v=1.1')}}"></script>
<script src="{{asset('assets/js/elephant.min.js?v=1.1')}}"></script>
</body>
</html>
