@extends('layouts.app')
@section('content')
    <div class="layout-content">
        <div class="layout-content-body">
            <div class="title-bar">
                <div class="title-bar-actions">
                    <a href="{{url('/other/runningtext/list')}}" class="btn btn-primary"><i class="icon icon-arrow-left icon-fw"></i> Kembali</a>
                </div>
                <h1 class="title-bar-title">
                    <span class="d-ib">Running Text</span>
                </h1>
                <p class="title-bar-description">
                    <small> <i>Running Text</i> dengan mengisi form dibawah ini.</small>
                </p>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <form class="form form-horizontal" data-toggle="validator" enctype="multipart/form-data" method="post" action="{{url('/other/runningtext/manage')}}">
                        {{csrf_field()}}
                        <div class="demo-form-wrapper">
                            <input type="hidden" name="rt_id" id="id" value="{{$rt ? $rt->settingid : ''}}">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Running Text</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="3" name="settingvalue" required>{{$rt ? $rt->settingvalue : ''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Font Size</label>
                                <div class="col-sm-9">
                                    <input type="number" min="1" class="form-control" name="setting_font" value="{{$rt && isset($rt['setting']['font']) ? $rt['setting']['font'] : ''}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status Aktif</label>
                                <div class="col-sm-9">
                                    <div class="custom-controls-stacked m-t">
                                        <label class="switch switch-primary">
                                            <input class="switch-input" type="checkbox" name="settingstat" value="A" {{$rt && ($rt->settingstat == 'A') ? 'checked="checked"' : ''}}>
                                            <span class="switch-track"></span>
                                            <span class="switch-thumb"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" class="btn btn-primary"><i class="icon icon-save icon-fw"></i> Simpan</button>
                                    <a href="{{url('/other/runningtext/list')}}" class="btn btn-outline-primary">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection