/*
Navicat MySQL Data Transfer

Source Server         : SYS
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : samdque_sor

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2017-07-11 06:16:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dbo_voices`
-- ----------------------------
DROP TABLE IF EXISTS `dbo_voices`;
CREATE TABLE `dbo_voices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dbo_voices
-- ----------------------------

-- ----------------------------
-- Table structure for `dsp_settings`
-- ----------------------------
DROP TABLE IF EXISTS `dsp_settings`;
CREATE TABLE `dsp_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` text COLLATE utf8mb4_unicode_ci,
  `setting` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of dsp_settings
-- ----------------------------

-- ----------------------------
-- Table structure for `grup`
-- ----------------------------
DROP TABLE IF EXISTS `grup`;
CREATE TABLE `grup` (
  `ID_GRUP` int(11) NOT NULL AUTO_INCREMENT,
  `GRUP` varchar(50) NOT NULL DEFAULT '',
  `KATEGORI` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_GRUP`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of grup
-- ----------------------------
INSERT INTO `grup` VALUES ('1', 'STAF REKAM MEDIK', '1');
INSERT INTO `grup` VALUES ('2', 'PELAKSANA LAB', '6');
INSERT INTO `grup` VALUES ('3', 'PERAWAT POLI', '3');
INSERT INTO `grup` VALUES ('4', 'FARMASI', '2');
INSERT INTO `grup` VALUES ('5', 'KA. UNIT FARMASI', '2');
INSERT INTO `grup` VALUES ('6', 'KA. BID. KEU', '10');
INSERT INTO `grup` VALUES ('7', 'PENAGIHAN', '10');
INSERT INTO `grup` VALUES ('8', 'PROMOSI', '17');
INSERT INTO `grup` VALUES ('9', 'BANK DARAH', '8');
INSERT INTO `grup` VALUES ('10', 'KA.SUB.MOBILISASI', '16');
INSERT INTO `grup` VALUES ('11', 'PENGENDALI KAS', '10');
INSERT INTO `grup` VALUES ('12', 'KASIR', '11');
INSERT INTO `grup` VALUES ('13', 'PENDAFTARAN', '18');
INSERT INTO `grup` VALUES ('14', 'ADM RUANGAN', '4');
INSERT INTO `grup` VALUES ('15', 'PENGENDALI PIUTANG', '10');
INSERT INTO `grup` VALUES ('16', 'DOKTER', '15');
INSERT INTO `grup` VALUES ('17', 'ADM RADIOLOGI', '5');
INSERT INTO `grup` VALUES ('18', 'DIREKTUR', '9');
INSERT INTO `grup` VALUES ('19', 'ADMIN', '14');
INSERT INTO `grup` VALUES ('20', 'KEHUMASAN', '17');
INSERT INTO `grup` VALUES ('21', 'STAF GAKINDA', '19');
INSERT INTO `grup` VALUES ('22', 'KA.SUB.REKAM MEDIK', '1');
INSERT INTO `grup` VALUES ('23', 'PELAKSANA RADIOLOGI', '5');

-- ----------------------------
-- Table structure for `kategori`
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `ID_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID_kategori`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kategori
-- ----------------------------
INSERT INTO `kategori` VALUES ('1', 'REKAM MEDIS');
INSERT INTO `kategori` VALUES ('2', 'FARMASI');
INSERT INTO `kategori` VALUES ('3', 'RAWAT JALAN');
INSERT INTO `kategori` VALUES ('4', 'RAWAT INAP');
INSERT INTO `kategori` VALUES ('5', 'RADIOLOGI');
INSERT INTO `kategori` VALUES ('6', 'LABORATORIUM');
INSERT INTO `kategori` VALUES ('7', 'IGD');
INSERT INTO `kategori` VALUES ('8', 'BANK DARAH');
INSERT INTO `kategori` VALUES ('9', 'DIREKTUR');
INSERT INTO `kategori` VALUES ('10', 'KEUANGAN');
INSERT INTO `kategori` VALUES ('11', 'KASIR');
INSERT INTO `kategori` VALUES ('12', 'OK');
INSERT INTO `kategori` VALUES ('13', 'VK');
INSERT INTO `kategori` VALUES ('14', 'ADMIN');
INSERT INTO `kategori` VALUES ('15', 'DOKTER');
INSERT INTO `kategori` VALUES ('16', 'MOBILISASI');
INSERT INTO `kategori` VALUES ('17', 'HUMAS');
INSERT INTO `kategori` VALUES ('18', 'PENDAFTARAN');
INSERT INTO `kategori` VALUES ('19', 'GAKINDA');
INSERT INTO `kategori` VALUES ('20', 'BPJS');
INSERT INTO `kategori` VALUES ('21', 'ICU');

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('106', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('107', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('108', '2017_06_12_102916_entrust_setup_tables', '1');
INSERT INTO `migrations` VALUES ('109', '2017_06_12_140056_create_dbo_voices_table', '1');
INSERT INTO `migrations` VALUES ('110', '2017_06_12_140057_sm_metas', '1');
INSERT INTO `migrations` VALUES ('111', '2017_06_12_140058_sm_ruangans', '1');
INSERT INTO `migrations` VALUES ('112', '2017_06_12_140059_sm_settings', '1');
INSERT INTO `migrations` VALUES ('113', '2017_06_12_140750_create_sm_asuransis_table', '1');
INSERT INTO `migrations` VALUES ('114', '2017_06_12_141704_sm_kunjungans', '1');
INSERT INTO `migrations` VALUES ('115', '2017_06_12_143929_sm_pasiens', '1');
INSERT INTO `migrations` VALUES ('116', '2017_06_12_144851_sm_pendaftarans', '1');
INSERT INTO `migrations` VALUES ('117', '2017_06_13_133218_create_sessions_table', '1');
INSERT INTO `migrations` VALUES ('118', '2017_06_14_082011_add_field_sm_pendaftarans', '1');
INSERT INTO `migrations` VALUES ('119', '2017_06_14_170552_add_field1_sm_pendaftaran', '1');
INSERT INTO `migrations` VALUES ('120', '2017_06_14_170908_create_table_dsp_setting', '1');

-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for `pegawai`
-- ----------------------------
DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE `pegawai` (
  `NPRS` char(20) NOT NULL DEFAULT '0',
  `NIP` char(20) DEFAULT NULL,
  `Gelar_Depan` char(10) DEFAULT NULL,
  `Nama` char(50) DEFAULT '',
  `Gelar_Belakang` char(20) DEFAULT NULL,
  `Kode_JnsKelamin` char(1) DEFAULT 'L',
  `Tempat_Lahir` char(15) DEFAULT '',
  `Tgl_Lahir` date DEFAULT '0000-00-00',
  `Kode_Agama` char(1) DEFAULT '',
  `Kode_StsKawin` char(2) DEFAULT NULL,
  `Alamat` char(60) DEFAULT NULL,
  `RT` char(2) DEFAULT '',
  `RW` char(2) DEFAULT '',
  `Kode_Kelurahan` char(5) DEFAULT '',
  `Kode_Kecamatan` char(4) DEFAULT NULL,
  `Kode_Kabupaten` char(3) DEFAULT NULL,
  `Warga_Negara` char(3) DEFAULT '',
  `TMT` date DEFAULT NULL,
  `Kode_Pendidikan` char(2) DEFAULT NULL,
  `Kode_StsPeg` char(2) DEFAULT NULL,
  `Kode_SubJnsPeg` char(5) DEFAULT NULL,
  `No_Karpeg` char(10) DEFAULT '',
  `Golongan` char(10) DEFAULT NULL,
  `TMT_PG` date DEFAULT NULL,
  `Kode_WKerja` char(1) DEFAULT '2',
  `Kode_PPDS` char(1) DEFAULT '1',
  `Kode_Jabatan` char(5) DEFAULT NULL,
  PRIMARY KEY (`NPRS`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pegawai
-- ----------------------------
INSERT INTO `pegawai` VALUES ('0316', '', '', 'ELON AMINUDIN', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2007-08-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0314', '', 'Dr', 'Dr. YAYAN GINANJAR', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0313', '', 'Dr', 'Dr. FENNY MULJANI PRIYATNA', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0312', '', 'Dr', 'Dr. ARI KURNIAWAN', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0311', '', '', 'ENJANG SAEFUL', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0310', '', '', 'WARLIA', '', 'P', 'Bandung', '1979-12-17', '0', '2', 'Kp. Cikupa', '01', '02', '', '0022', '008', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0309', '', '', 'IMAS ENIN', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0308', '', '', 'NURWENDAH', '', '', '', '0001-01-01', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0307', '', '', 'NENENG LESTARI', '', '', '', '0001-01-01', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0306', '', '', 'TETI NURHASANAH, AMK', '', '', '', '0001-01-01', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0305', '', '', 'HIDAYAT, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0304', '', '', 'TATI ROSMIATI, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0303', '', '', 'HERU HERIYANTO, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0302', '', '', 'DEDI SUDIRMAN, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0301', '', '', 'YUNINGSIH SONJAYA, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0300', '', '', 'SONHAJI, AMR', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0298', '', '', 'ANI MULYANI, AMD.PK', '', '', '', '0001-01-01', '0', '', '', '', '', '', '', '', '', '2006-12-02', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0297', '', '', 'DETI HAPITRI, AMG', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0296', '', '', 'SUSI LUSYATI, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0295', '', '', 'ELIN HERLIATIN, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0294', '', '', 'ISWANTORO, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0293', '', '', 'CECEP NURZAMAN, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0292', '', '', 'RAMDAN EDI SETIADI, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0291', '', '', 'NARDI, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0290', '', '', 'HALIMATUS SA\'DIYYAH, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0289', '', '', 'TINI ROSTIANINGSIH, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0288', '', '', 'HERA RATNANINGSIH, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0287', '', '', 'BUDI SISWOKO, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0286', '', '', 'RINA RISNAWATI, AMK', '', '', '', '0000-00-00', '0', '', '', '', '', '', '', '', '', '2006-12-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0284', '', '', 'ASEP ROHMAN, S.IP', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-10-05', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0283', '', '', 'TITIN SUPARTINI, AM.Keb', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-08-04', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0282', '', '', 'LASTIAR KRISTINA, AM.Keb', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-08-04', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0281', '', '', 'INGGRIT CHODIJAH', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-08-04', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0280', '', '', 'HADRIYANA', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-08-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0279', '', '', 'NENI SUPARTINI, AMD', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-08-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0278', '', 'Dr', 'Dr. M FAISAL AKBAR THAMRIN', '', '', '', '1989-10-30', '', '', '', '', '', '', '', '', '', '2014-10-01', '', '', '', '', '', '2014-10-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0277', '', '', 'MOHAMAD AGUS', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-06-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0276', '', '', 'Iwan Kurniawan', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-03-23', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0275', '', '', 'Asep Hermansyah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0274', '', '', 'ABDULLAH, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0273', '', 'Dr', 'Dr. IMAN S.F. WIRAYAT, Sp.OG', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0272', '', '', 'Nendi', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-01-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0271', '', '', 'Budiman', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-01-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0270', '', '', 'Roni Nurdiansyah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-01-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0269', '', '', 'Dadang Wahyudin', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-01-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0268', '', '', 'Iip Sanaipah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2006-01-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0267', '', '', 'JUMHUR', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-12-05', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0266', '', 'Dr', 'Dr. Yesi Syafril', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-11-22', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0265', '', 'Dr', 'Dr. Bertha Saulina P., Sp.S', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-11-22', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0264', '', '', 'Dede Sri Megawati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-09-12', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0263', '', '', 'Siti Maryam', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-08-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0262', '', '', 'Masnur Tarjono', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-08-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0260', '', '', 'DEWI AMBAWATI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0259', '', '', 'UTIS SUTISNA', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-05-04', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0258', '', 'Dr', 'Dr. SENO M. KAMIL, Sp.PD', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-05-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0257', '', '', 'Yuyun Yulianti, SP', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-14', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0256', '', '', 'Usman Agustian', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0255', '', '', 'Machmud Mansoor', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0254', '', '', 'Nunung Nurjanah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0252', '', '', 'Utami Purliasari', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0251', '', '', 'Isnaeni Tutisah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0250', '', '', 'Heni Nur Septiani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0249', '', '', 'Rose Yulianti, SE', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0248', '', '', 'PIPIT SUMINARTI, AMG', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0247', '', 'AMKL', 'EWIT PUTRIANA, AMKL', '', 'P', 'GARUT', '1982-01-08', '0', '1', 'SALUYU SAYATI', '', '', 'SAYAT', '0024', '008', 'INA', '2005-04-01', '5', '1', '', '', 'II/C', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0246', '', '', 'IMAS MASRUROH', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0245', '', '', 'SOPITA, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0244', '', '', 'WENI TRESNAWATI, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0243', '', '', 'SITI MARIA GUNAWAN', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0242', '', '', 'INDRI WINDIASARI, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0241', '', '', 'IRA RAMILAH, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0240', '', '', 'YULIA NURHAYATI, Amd.PK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0239', '', '', 'ALFI ZUBAEDAH, AMR', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0238', '', '', 'Tina Andriani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-03-15', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0237', '', '', 'Siti Nurjanah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-03-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0236', '', '', 'Neng Dewi Nurhayati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-03-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0235', '', '', 'Suyati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-03-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0233', '', '', 'Diani Hidayanti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-03-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0232', '', '', 'Sumi Lestari', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0231', '', '', 'Neni Mulyani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0230', '', '', 'Moh. Taupan Ramdhan Hadi', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0229', '', '', 'Kusmana', '', '', '', '0001-01-01', '0', '', '', '', '', '', '', '', '', '2005-02-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0228', '', '', 'Andries Darajat Syaban', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0227', '', '', 'Ade Mulyana', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2005-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0226', '', '', 'Rina Karlina', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-08-03', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0225', '', '', 'Irma Hermayanti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-06-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0224', '', '', 'Sumina', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-06-09', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0223', '', '', 'Saima Yulianti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-05-28', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0222', '', '', 'Juju Juariah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-05-19', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0221', '', '', 'Novi Trian Pahlina', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-05-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0220', '', '', 'Emi Muharami', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-05-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0218', '', '', 'Nuni', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0217', '', '', 'Yogi Rudianto W.', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0216', '', '', 'Windi Hardini', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0215', '', '', 'Widaningsih', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0214', '', '', 'Titi Susanti Anggraeni', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0213', '', '', 'Sugiyanti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0212', '', '', 'Sri Mulyani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0211', '', '', 'Novi Nurcholis Majid', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0210', '', '', 'Neng Wulan Yulianti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0209', '', '', 'Lilis Susilawati Utami', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0208', '', '', 'Jamhur Sihabudin', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0207', '', '', 'Ingge Ivana', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0206', '', '', 'Heri Diana', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0205', '', '', 'Fitri Ilhami', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0204', '', '', 'Elis Lisnawati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0203', '', '', 'Elin Waliana', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0202', '', '', 'Een Sukaenah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0201', '', '', 'Asriyudin', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0200', '', '', 'Ajid Sajidin', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0199', '', '', 'Yusup Muslih', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-19', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0198', '', '', 'Dadang Jamiat Rosid', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-19', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0197', '', '', 'Wiwin Winarni', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-04-19', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0195', '', '', 'Rezky Widyastuty', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-03-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0194', '', '', 'Christine Tri Astuti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-03-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0193', '', '', 'Yulia Aviany, S.Pd', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-03-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0192', '', '', 'Yuliani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-02-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0191', '', '', 'Welly Hendrawan', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-02-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0190', '', '', 'Uum Mulyati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-02-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0189', '', '', 'Sri Mulyani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-02-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0187', '', '', 'Dani Teguh Ramdhani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-02-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0186', '', '', 'Ade Santi', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-02-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0185', '', '', 'Dede Ilyas, S sos', '', '', '', '1977-05-15', '', '', '', '', '', '', '', '', '', '2004-02-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0184', '', '', 'Nenden Nurbani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-01-26', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0183', '', '', 'SYULVI, AMG', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2004-01-14', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0182', '', '', 'Mochamad Sholahudin', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-12-31', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0181', '', '', 'Epi Kusmayawati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-12-09', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0180', '', '', 'Euis Vevi Ismayawati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-09-25', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0179', '', '', 'Siti Nurlela', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-09-19', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0178', '', '', 'Nenden Susana', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-09-19', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0177', '', '', 'Titin Patimah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-09-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0176', '', '', 'Dadi Rustandi', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-09-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0175', '', '', 'Asep Budiman', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-09-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0174', '', '', 'Yuli Handayani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-09-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0173', '', '', 'Eulis Cica Susilawati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-08-25', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0172', '', '', 'Tresnawati, AMK', '', '', '', '0001-01-01', '', '', '', '', '', '', '', '', '', '2003-07-09', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0170', '', 'Dr', 'Dr. IRVAN AGUSTA', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-07-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0168', '', '', 'Cipto', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-06-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0167', '', '', 'AAN KARTINAH', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-06-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0166', '', '', 'MUJI HARTI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-06-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0165', '', '', 'Wina Kusmayanti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-04-17', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0164', '', '', 'Muhtar Hasbi', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-04-17', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0163', '', '', 'TAUFIK FAHRIZA', '', 'L', 'BANDUNG', '1983-05-23', '0', '1', 'PERUM BPK BLOK G4 NO. 18', '2', '4', 'PANAN', '0030', '008', 'INA', '2003-04-01', '4', '9', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0162', '', '', 'Nurul Fitriana Rahman', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0159', '', '', 'Riseu Lestari', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-03-24', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0157', '', '', 'Rini Nuraeni', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-03-24', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0156', '', '', 'Lia Amalia Hidayah', 'AMK', 'P', 'Bandung', '1981-05-14', '0', '2', 'Babakan Gombong RT', '03', '13', '', '', '008', 'INA', '2003-03-24', '8', '3', 'A2.05', '', 'II/A', '2015-05-15', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0155', '', '', 'Devia Nurwenda Eliyana', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-03-24', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0154', '', '', 'Ai Yuyun Yuliani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-03-24', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0153', '', '', 'Yani Kartinah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-01-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0152', '', '', 'Iim Melani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-01-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0151', '', '', 'Geget Dwi Nirmala', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2003-01-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0150', '', '', 'Yully Yanny', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-10-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0149', '', '', 'Sri Hanifah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-10-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0148', '', '', 'Dody Wardhiman', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-10-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0146', '', '', 'Eni Rohaeni', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-08-10', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0145', '', '', 'Ujang Ajat Sudrajat', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-08-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0144', '', '', 'Iwan Suwandi', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-08-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0143', '', '', 'Imas Mulyani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-05-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0142', '', '', 'Ratih Suhartini', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-05-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0141', '', '', 'Santi Yunia, A.Md', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-05-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0140', '', '', 'Nora Haryanti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-05-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0139', '', '', 'WAHYU WAHDANA, Amd. An', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-05-16', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0138', '', 'Dr', 'Dr.NURVITA SUSANTO, Sp.A', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0137', '', 'Drg', 'Drg. CAHYA KUSTIAWAN', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-02-13', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0136', '', '', 'Syamsul Azhar', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-01-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0135', '', '', 'Mulyati Khoiriyah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-01-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0134', '', '', 'Listiany Pramanik', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-01-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0133', '', '', 'Ahmad Gilang Riswanto', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2002-01-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0132', '', '', 'Irma Nurbaiti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-12-18', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0131', '', '', 'Nuryuliani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-12-07', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0130', '', 'Dr', 'Dr.HENNY K. KOESNA, Sp.PD', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-11-26', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0129', '', '', 'Sumiati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-11-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0128', '', '', 'Asep Yuyun, S.Sos', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-09-27', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0126', '', '', 'NURATRY AMBARWATI, AMG', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-06-12', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0125', '', '', 'JAJAT SUDRAJAT, AMK.AN', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-04-23', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0124', '', '', 'Fila Noviantina,A.Md', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-04-10', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0123', '', '', 'Rany Rahmawati, A.Md', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-04-10', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0122', '', '', 'PUJI SUWANDHI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-04-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0121', '', 'Dr', 'Dr.H. ZIRMACATRA, Sp.THT', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-03-12', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0119', '', '', 'Ima Ulfa Daniah Y., A.Md', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0118', '', '', 'Dian Rosydiani, A.KS', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0117', '', '', 'Ade Siti Mulyani, SE', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0116', '', '', 'Tini Rostini, SE', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0115', '', '', 'Jeni Herdiana, S.Sos', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0114', '', '', 'Cucu Kurniasih, S.Sos', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0113', '', '', 'Dede Risnandar S., S.Sos', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0112', '', '', 'Nia Aisyah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-01-29', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0111', '', '', 'Nani Sumartini', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-01-29', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0110', '', '', 'Hani Widiani Syaeful', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-01-29', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0109', '', '', 'Neva Valia Sopa', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-01-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0108', '', '', 'Kurniawati Suwarsa Putri', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-01-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0107', '', '', 'Iwan Sukandar', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2001-01-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0106', '', '', 'DIAH NOWO RETNO', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-09-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0105', '', '', 'Dadang', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-09-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0104', '', '', 'Aan Suhanda', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-09-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0101', '', '', 'SITI PATIMAH', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-08-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0100', '', 'Dr', 'Dr. JENNY SW, Sp.PK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-07-03', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0099', '', 'Dr', 'Dr. DIANTINIA, Sp.M', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-07-03', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0098', '', '', 'Taufik Rahmat', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-06-12', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0097', '', '', 'RINA FATIMAH', '', 'P', 'BANDUNG', '1976-11-04', '0', '2', '', '02', '02', '', '0003', '008', 'INA', '2000-06-06', '4', '3', 'B5.00', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0096', '', 'Dr', 'Dr. RIRI RISANTI, Sp.An', '', '', '', '1960-12-01', '', '', '', '', '', '', '', '', '', '2009-12-01', '', '', '', '', '', '2009-12-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0095', '', '', 'Yan Sofyan Ramdhan', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-06-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0094', '', '', 'Nandang Mulyana', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-05-15', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0093', '', '', 'Dina Rosyana', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-05-15', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0092', '', '', 'EKA KOMALASARI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-05-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0091', '', '', 'Diki Rusaman', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-04-27', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0089', '', '', 'Karmana', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-04-20', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0088', '', '', 'Suganda', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-04-15', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0087', '', '', 'Damayanti Supriatna', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-04-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0086', '', 'Dr', 'Dr. YEPPY A.N., Sp.B, MM', '', '', '', '0001-01-01', '', '', '', '', '', '', '', '', '', '2000-04-01', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0085', '', '', 'KOSWARA, BE', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '2000-03-17', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0083', '', '', 'Nurbainah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-12-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0082', '', '', 'Novi Susanti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0081', '', '', 'Epa Martiani, A.Md', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0080', '', '', 'Iim Wartikah, SE', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-12-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0079', '', '', 'Lia Marliani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-11-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0078', '', '', 'Tatan Suryahata', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-09-30', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0077', '', '', 'Rohmat', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-09-30', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0076', '', '', 'Nursari Pujiati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-09-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0075', '', '', 'Ismawati Sobariyah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-09-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0073', '', '', 'Asep Supriyadi', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-07-29', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0072', '', '', 'YANI SUDARYANI, AM.KEB', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-07-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0071', '', '', 'Aep Saepudin', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-07-03', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0070', '', '', 'SITI WAHIDAH NURAENI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-21', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0069', '', '', 'Sutarlin', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0068', '', '', 'Gandi', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0067', '', '', 'Nina Saripah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0066', '', '', 'Yanti Yulianti', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0065', '', '', 'Kurniasari', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0064', '', '', 'Dori Mustofa', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0063', '', '', 'Deti Rosdiyani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0062', '', '', 'Lidya Megandari', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0061', '', '', 'Lilis Rohaeti Rahayu, S.Sos', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0060', '', '', 'Rini Rostikawati, SE', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0059', '', '', 'Sri Betari, SE', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0058', '', '', 'Tria Hasanah', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-05-11', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0057', '', '', 'IKA SARTIKA, B.Sc', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0054', '', '', 'RUKANTI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-03-15', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0318', '198311062009011003', 'Dr', 'Dr. AHMAD HIDAYATULLAH', '', 'L', 'BANDUNG', '1983-11-07', '0', '1', 'KBSI BLOK O2 NO 23', '00', '00', '', '0016', '008', 'INA', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0052', '', '', 'EUIS SUSILAWATI, SKM.M.Si', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-03-15', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0525', '', '', 'HENDRIK AGUSTINA', '', 'L', 'Jakarta', '1985-08-21', '0', '1', 'kp.lebak cihideung', '00', '01', '', '0007', '010', 'INA', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0049', '', '', 'SOFYAN SETIAWAN', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-03-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0048', '', '', 'SRI SUHARTINI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-03-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0047', '', '', 'SUHARNO', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-03-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0046', '', '', 'MAHENDRAWAN ISMONO', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-03-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0045', '', '', 'Santi Noorcahyantie', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-02-09', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0043', '', '', 'CECE RACHMAT MULYADIN, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1999-01-13', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0042', '', '', 'Rohayati', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1998-09-06', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0041', '', '', 'EULIS NURAENI, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1998-07-20', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0040', '', '', 'KANA MULYANA', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1998-07-20', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0039', '', '', 'Melin Melani', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1998-06-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0038', '', '', 'EMI SUHAEMI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1998-04-27', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0036', '', '', 'NUH ALI AZKIA, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1997-10-13', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0035', '', '', 'INDRIYANI NEFANINGTIJAS', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1997-08-18', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0033', '', '', 'EUIS TEJANINGRUM, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1997-08-04', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0032', '', '', 'DUDI EKA PRASETIANA', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1997-08-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0031', '', '', 'ADE SRI TITA, AMG', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1997-07-16', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0030', '', '', 'ATIS SUTISNA', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1997-05-02', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0029', '', '', 'AI CAESAREA UTAMI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1997-01-26', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0028', '', '', 'DIAN VERDIANI, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1997-01-03', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0027', '', '', 'Iis Rosita', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1996-12-24', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0026', '', '', 'CUCU MARIAM, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1996-11-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0025', '', '', 'EEN KARMINI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1996-11-08', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0024', '', '', 'SUKIRWAN, SE.AK', '', '', '', '0001-01-01', '', '', '', '', '', '', '', '', '', '1996-09-16', '', '', '', '', '', '0001-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0023', '', '', 'DANI SUMARYA', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1996-06-09', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0022', '', 'Dra', 'Dra. UTARI KAMAWATI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1996-05-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0021', '', '', 'TETI NURHAYATI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1996-04-17', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0020', '', '', 'LINDASARI, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1996-04-15', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0016', '', '', 'NINA ROSTIKA', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1996-02-13', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0015', '', '', 'Kosasih', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1996-01-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0014', '', '', 'HARNIDAH', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1995-12-22', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0012', '', '', 'ELIS SUPARTIKA', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1995-10-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0011', '', '', 'ADAO BELLO', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1995-09-28', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0010', '', '', 'UTE DADANG, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1994-11-25', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0009', '', '', 'UTANG TAYIB', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1993-04-07', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0008', '', '', 'SUTIAH, AMK', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1993-02-09', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0007', '', '', 'RAHMAT RIADI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1991-03-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0006', '', '', 'SULARSIH', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1991-03-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0005', '', '', 'MASITOH', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1990-11-25', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0004', '', '', 'NURHAYATI', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1987-03-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0003', '', '', 'ENGKOR', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1985-02-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('0002', '', '', 'ANA', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '1984-04-01', '', '', '', '', '', '0000-00-00', '', '', '');
INSERT INTO `pegawai` VALUES ('9901', '', '', 'Tim Psikiatri RSHS', '', '', '', '2007-09-25', '', '', 'RSHS Bandung', '', '', '', '', '', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('9903', '', '', 'Tim Kulit Kelamin RSHS', '', '', '', '2007-09-01', '', '', 'RSHS Bandung', '', '', '', '', '', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0909', '', '', 'SURNI SURYANTO', '', 'L', '', '2001-01-18', '', '', '', '', '', '', '', '', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0326', '', 'Dr', 'Dr. Yulia Halim, Sp.RAD', '', 'P', 'BANDUNG', '1975-01-24', '2', '2', 'RSD SOREANG', '', '', '', '0022', '008', 'INA', null, '9', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0317', '', 'Dr', 'Dr. H. BUDI, SpA., M.Kes', '', 'L', 'BANDUNG', '1960-03-14', '', '', 'SOREANG', '', '', '', '', '', '', '2008-01-01', '', '', '', '', '', '2004-01-01', '1', '1', '');
INSERT INTO `pegawai` VALUES ('0320', '', 'Dr', 'Dr. HEDI HENDRAWAN R.SpKK.,M.Kes', '', 'L', '', '1979-01-01', '0', '1', 'RSD SOREANG', '', '', '', '', '', 'INA', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0999', '', '', 'SETYO WITJAKSONO', 'AMD', 'L', 'BANDUNG', '1970-05-26', '0', '2', 'JL. RAYA SOREANG NO. 133', '02', '02', 'SOREA', '0022', '008', 'INA', null, '8', '9', 'B4.09', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0333', '', '', 'DODI HADIAT', 'AMD', 'L', 'BANDUNG', '1978-07-09', '0', '2', 'CIREUNGIT', '1', '2', '', 'CANG', '008', 'INA', null, '8', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0330', '', '', 'DEDEN MUSTAFA', 'A Md', 'L', 'BANDUNG', '1976-01-10', '0', '2', 'NAMBO DS BATU KARUT', '4', '2', '', '0018', '008', 'INA', null, '8', '9', 'B4.09', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0340', '', 'Dr', 'Dr. Luh Putu Yunita Anggreni', '', 'P', 'BALI', '1970-01-01', '0', '2', 'RSD SOREANG', '', '', '', 'SORE', '008', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0378', '', 'Dr', 'Dr. DINI, Sp.PD', '', 'P', 'BANDUNG', '1960-01-01', '0', '2', 'RSUD SOREANG', '', '', '', '', '', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0342', '1983100620090122002', '', 'HERA RATNA FATRIA', '', 'P', 'BANDUNG', '1983-10-06', '0', '1', 'KP.CIKUNDUL NO.85', '02', '09', '', 'KUTA', '008', 'INA', null, '4', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0348', '', '', 'RESTY RESMAWATI', '', 'P', 'BANDUNG', '1984-12-06', '0', '2', 'Jl. RAYA PAMEUNGPEUK NO. 569', '1', '2', 'BOJON', 'PAME', '008', 'INA', null, '', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0344', '', '', 'NURWANTI NINGSIH, AMD.FT', '', 'P', 'BANDUNG', '1986-04-20', '0', '1', 'GRIYA ALAM CIBEBER BLOK B NO.5', '05', '01', '', '0003', '007', 'INA', '2009-11-02', '8', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0321', '', 'Dr', 'Dr. Susanti Dharmmika', 'SpRm', 'P', 'Bandung', '1969-03-06', '0', '2', 'Jl. Atlas IV No V', '4', '13', '', '', '009', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0345', '', 'Dr', 'Dr. H. MARSUDI, SpKJ., M.Kes', '', 'L', 'BANDUNG', '1900-01-01', '0', '2', 'RSUD SOREANG', '', '', '', '', '', '', null, '', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0346', '198210042010011002', '', 'ADITYA RIYADI, AMD', '', 'L', 'JAKARTA', '1982-10-04', '0', '2', 'JL. KALIDAM UTARA NO. 115', '3', '8', '', '0002', '007', 'INA', null, '8', '10', '', '', 'II/C', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0347', '', '', 'ARIF AHMAD RIFAI', 'AMD,Ft', 'L', 'BANDUNG', '1982-07-16', '0', '1', 'BATUJAJAR NO.19', '03', '08', '', 'BATU', '010', 'INA', null, '8', '10', '', '', 'II/C', '2010-01-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0349', '', '', 'PITRANG HARSANTI', '', 'P', 'BANDUNG', '1987-01-16', '0', '1', 'JL. NUSANTARA V CIBABAT CIMAHI UTARA', '2', '25', '', '', '007', 'INA', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0351', '', '', 'ISYANA SRI KURNIAWATI', '', 'P', 'BANDUNG', '1984-10-25', '0', '2', 'SOREANG INDAH JL. CEMPAKA C8 SOREANG', '01', '15', '', '', '008', 'INA', '2009-01-01', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0601', '196902320200812006', '', 'SITI SUNDARI SUBARDIENNY', '', 'P', 'Sumedang', '1969-03-20', '0', '2', 'Jl. Raya Banjaran No. Lt. 1 No. 1', '', '', '', '0019', '008', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('9999', '', '', 'OZIEN', '', '', '', '1976-04-15', '', '', '', '', '', '', '', '', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0352', '', 'Dr', 'KomPol Dr. NURHADI WIJAYANTA, Sp.AN', '', 'L', '', '1960-01-01', '0', '2', 'RSUD SOREANG', '', '', '', '', '', 'INA', '2010-04-10', '9', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0353', '', '', 'GILANG RAMADHAN', 'AMK', 'L', 'BANDUNG', '1987-05-19', '0', '1', 'KP. BARAANTA DS. PARUNG SERAB', '1', '9', '', 'SORE', '008', '', '2010-01-01', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0354', '', '', 'REZA SENJAYA HUSEN', 'AMD.KEP', 'L', 'Garut', '1985-08-02', '0', '2', 'Perum Cingcin Permata Indah Blok D 154', '01', '01', 'cingc', 'sore', '008', 'INA', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0355', '', 'Dr', 'Dr. Adityo Januarjie, Sp.OG,M.Kes', '', 'L', 'Bandung', '1978-01-16', '0', '2', 'RSUD Soreang', '', '', '', 'Sore', '008', '', '2010-08-01', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0357', '', '', 'Yuliani', '', 'P', 'Bandung', '1976-09-01', '0', '2', 'Jl. Bp. Suhaya Nyengseret', '2', '5', '', 'Asta', '009', 'INA', null, '', '3', '', '', 'II/A', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0358', '', '', 'DEWI HARYATI PUSPITASARI', 'S.Sos', 'P', 'BANDUNG', '1981-07-29', '0', '1', 'KP. GAMBLOK DS. PASIRHUNI', '1', '4', '', 'CIMA', '008', 'INA', '2010-12-13', '9', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0359', '', '', 'WISNU PRAKOSO EKO PUTRO', 'SE', 'L', 'INDRAMAYU', '1976-03-09', '0', '4', 'JLN. TMN. DIRGANTARA NO.15 ARCAMANIK', '', '', '', '', '009', 'INA', '2010-12-13', '9', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0360', '198208092011012004', 'Dr', 'Dr.RR.DEVY AGUS SUSYANI', '', 'P', 'LEBAK', '1982-08-09', '0', '2', 'Lio Utara', '04', '09', 'Cipad', 'CIBI', '008', 'INA', null, '', '', '', '', 'III/B', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0361', '', '', 'ILHAM AKBAR', '', 'L', 'BANDUNG', '1984-10-18', '0', '1', 'Kp. Mandala', '01', '05', 'Sukaj', 'sore', '008', 'INA', null, '8', '', '', '', 'II/C', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0362', '', '', 'EKA RATNAWATI', '', 'P', 'Jakarta', '1977-04-14', '0', '2', 'Jl. Kenari F4 No. 15 RT.01/12', '', '', 'bojon', '0017', '008', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0363', '', '', 'Dewi Haryati Puspitasari, S.Sos', '', 'P', 'Bandung', '1981-07-08', '0', '2', 'Gamblok Pasir Huni', '1', '4', '', 'Cima', '008', 'INA', null, '9', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0364', '198510312010012004', '', 'KRISTINE OCTAVIANTY', 'SKM', 'P', 'BEKASI', '1985-10-31', '2', '1', 'Jl. Parasdi No.4 Kelurahan Situsaeur', '01', '06', '', 'Bojo', '009', 'INA', null, '9', '3', '', '', 'III/A', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0365', '', '', 'Irma Solihat S', '', 'P', 'BANDUNG', '1987-08-11', '0', '2', 'KP. BABAKAN SONDARI', '3', '7', '', 'KATA', '008', '', null, '', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0366', '', '', 'Fitrianingsih', '', 'P', 'Bandung', '1990-07-08', '0', '2', 'Kp. Tarigu Ds. Margahurip', '03', '01', '', 'banj', '008', 'INA', null, '4', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0367', '', '', 'Ifan Wardani Yusron', '', 'L', 'Bandung', '1977-08-31', '0', '2', 'Kp. Ciapis Ds. Ciapus', '15', '', '', 'BANJ', '008', 'INA', null, '', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0368', '', '', 'DIMAN SUDIRMAN', '', 'L', 'TASIKMALAYA', '1981-10-17', '0', '1', 'JL WARUNG JAMBU 56 KIARACONDONG', '05', '02', 'KEBON', '0019', '009', '', null, '8', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0369', '', '', 'ADI KRISTIYADI', '', 'L', 'BANDUNG', '1987-06-07', '0', '2', 'KP PASANTREN  , DS SUKAMUKTI', '1', '6', '', 'KATA', '008', 'WNI', null, '13', '9', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0370', '', '', 'DEDI KUSWANDI', '', 'L', 'BANDUNG', '1981-08-20', '0', '2', 'KP GAMBUNG ,DS MEKARSARI', '01', '07', '', 'PASI', '008', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0371', '', '', 'AZHARY D N', '', 'L', 'BANDUNG', '1987-11-25', '0', '1', 'GADING TUTUKA1 BLOK B3 NO 4', '03', '12', 'CINGC', 'SORE', '008', 'INA', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0372', '', '', 'INSANIYAH, S.Si', '', 'P', 'CIREBON', '1969-10-05', '0', '2', 'RSUD SOREANG', '', '', '', 'SORE', '008', '', '2011-07-01', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0373', '', '', 'kartika', '', 'P', 'bandung', '1991-10-12', '0', '1', 'komp marken blok c10 no 18', '12', '14', '', '', '008', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0374', '', '', 'marela wislilian', '', '', 'gunung dempo', '1981-08-11', '0', '2', 'gading tutuka 1 C2 no.41', '03', '12', 'cingc', 'sore', '008', 'INA', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0375', '', '', 'AMAN SUPARMAN', '', 'L', 'BANDUNG', '1974-08-01', '0', '2', 'KP. PASIR PANJANG', '04', '10', '', 'CIMA', '008', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0376', '', 'Dr', 'Dr.Henry Moesfairil, Sp.B', '', 'L', 'Bandung', '1961-03-13', '0', '2', 'TKI II Blok D', '', '', '', '', '008', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0377', '', 'Dr', 'Dr. Dik Adi Nugraha, Sp.B', '', 'L', 'Jakarta', '1980-09-21', '0', '2', 'Cibaduyut BLR No. 54', '', '', '', '', '009', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0379', '', 'Dr', 'Dr. MIRA RELYYTAN, SP.An', '', 'P', 'BONTANG', '1978-08-14', '0', '2', 'JL. SARIJADI RAYA NO. 58', '', '', '', '', '', '', '2012-11-12', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0380', '', '', 'LETIN MULYAWATI', '', 'P', '', '1976-08-22', '0', '2', 'RSUD SOREANG', '', '', '', '', '', '', '2012-09-01', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0513', '19601507199103001', 'Drg', 'Drg. IWAN MULYAWAN', '', 'L', 'PALEMBANG', '1960-07-15', '0', '2', 'Jl. Suryalaya XVII No. 15  A', '01', '00', '', 'lenk', '009', 'INA', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0514', '197508252006042018', 'Drg', 'Drg. SWANTARI', '', 'P', 'BANDUNG', '1975-08-25', '0', '2', 'RSUD SOREANG', '', '', '', '', '', '', '2013-11-20', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0515', '', '', 'RONI INDRA SETIAWAN', '', 'L', 'BANDUNG', '1976-04-24', '0', '2', 'RSUD SOREANG', '', '', '', '', '', '', '2014-01-02', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0516', '', '', 'SONNI SURYA NINGRAT', '', 'L', 'BANDUNG', '1980-07-31', '0', '2', 'rsud soreang', '', '', '', '', '', '', '2014-01-02', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0517', '', '', 'LILIS TARSIAH', '', 'P', 'BANDUNG', '1977-03-16', '0', '2', 'RSUD SOREANG', '', '', '', '', '', '', '2014-01-02', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0518', '', '', 'ANWARI RUSNANDAR', '', 'L', 'BANDUNG', '1971-01-19', '0', '2', 'RSUD SOREANG', '', '', '', '', '', '', '2014-01-02', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0519', '', '', 'LUKI YOGASWARA', '', 'L', 'BANDUNG', '1980-07-30', '0', '2', 'TAMAN KOPO INDAH I G 90', '', '', '', '0024', '008', 'INA', null, '9', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0520', '', '', 'VENNI MUJITIANTO', '', 'L', 'BANDUNG', '1987-03-28', '0', '1', 'RSUD SOREANG', '', '', '', '', '', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0521', '', '', 'ADITYA RAHMAN', '', 'L', 'BANDUNG', '1987-01-01', '0', '1', 'RSUD SOREANG', '', '', '', '', '', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0343', null, null, 'ESSA ANAROGA', null, 'L', 'BANDUNG', '1987-03-28', '0', '1', 'RSUD SOREANG', '', '', '', null, null, '', null, null, null, null, '', null, null, '2', '1', null);
INSERT INTO `pegawai` VALUES ('0522', '', 'AMD', 'BAMBANG TRIYADI', '', 'L', 'BANDUNG', '1974-05-14', '0', '2', 'CIBAYONDAH 61', '02', '05', 'PAMEK', 'SORE', '008', 'INA', '2014-01-01', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0523', '', '', 'RIKI IRAWAN', '', 'L', 'BANDUNG', '1981-04-19', '0', '2', 'KP LANGONSARI', '06', '03', 'PAMEU', 'PAME', '008', 'INA', '2014-01-01', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0524', '', '', 'SYAEFUL RAKHMAN', '', 'L', 'BANDUNG', '1975-03-10', '0', '2', 'JL. CILOA', '4', '10', 'PAMEK', 'SORE', '008', '', '2014-01-01', '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0000', null, null, 'RSUD SOREANG', null, 'L', 'SOREANG', '0000-00-00', '0', '2', 'RSUD SOREANG', null, null, null, null, null, null, '2014-01-02', null, null, null, null, null, '0000-00-00', null, null, null);
INSERT INTO `pegawai` VALUES ('0526', '', 'Dr', 'Dr. DENI JAENI', '', '', 'BANDUNG', '1966-01-25', '0', '', 'RSUD SOREANG', '', '', '', '', '', '', null, '', '', '', '', '', null, '', '', '');
INSERT INTO `pegawai` VALUES ('0800', '', '', 'Irma Nurbaiti, AM.keb', '', 'P', 'Subang', '1975-08-30', '0', '2', 'Gading Tutuka I Blok G3 No.7', '01', '13', '', '', '', '', '2014-11-01', '', '', '', '', '', '2014-11-01', '', '', '');
INSERT INTO `pegawai` VALUES ('0527', '', 'Dr', 'Dr. YULIANA Sp. OT, M. HKes', 'Sp. OT, M. HKes', 'P', 'BANDUNG', '1981-10-25', '1', '2', 'JL. SAWO NO.14 CIHAPIT BANDUNG WETAN', '00', '00', '', '', '', '', '2015-02-10', '', '', '', '', '', '2015-02-10', '', '', '');
INSERT INTO `pegawai` VALUES ('0528', '', '', 'SANDY KAMALUDDIN', '', 'L', 'BANDUNG', '1989-02-05', '0', '2', 'KP BOJONG BUAH', '01', '04', '', '', '009', 'INA', '2015-02-12', '', '', '', '', '', '2015-02-12', '', '', '');
INSERT INTO `pegawai` VALUES ('0998', '', '', 'RIDWAN ILYAS', 'ST., MT.', 'L', 'SUMEDNAG', '1990-01-27', '0', '1', 'KP NARINGGUL', '03', '06', '', '', '011', 'INA', '2015-02-12', '10', '9', 'B2.09', '', '', '2015-02-12', '', '', '');
INSERT INTO `pegawai` VALUES ('0529', '197010122000121004', 'Drg', 'Drg. ANANG PRASETIYONO, SP.BM', 'SP.BM', 'L', 'BANDUNG', '1970-01-01', '0', '2', 'antapani', '01', '01', '', '', '', '', '2015-03-24', '', '', '', '', '', '2015-03-24', '', '', '');
INSERT INTO `pegawai` VALUES ('0530', '', '', 'ERWIN SUHERWANTO', 'Amd., RMIK', 'L', 'BANDUNG', '1991-04-12', '0', '1', 'KP. BURUJUL  NO 33', '06', '17', 'mek', '0023', '008', 'INA', '2015-06-26', '8', '9', 'A7.07', '', '', '2015-06-26', '2', '', '');
INSERT INTO `pegawai` VALUES ('0531', '', '', 'EPA SITI NURJANAH', 'AMD', 'P', 'MAJALENGKA', '1987-04-17', '0', '2', 'JL. KAMANDILAN NO 6C', '01', '01', 'LIN', 'LENG', '009', 'INA', '2015-06-26', '8', '9', 'A7.07', '', '', '2015-06-26', '2', '', '');
INSERT INTO `pegawai` VALUES ('0532', '', '', 'YUSTINA DAMAYANTI', 'AMD.,RMIK', 'P', 'BANDUNG', '1993-12-18', '0', '1', 'KP.CUKANGHAUR', '03', '03', 'SUK', '0022', '008', 'INA', '2015-06-26', '8', '9', 'A7.07', '', '', '2015-06-26', '2', '', '');
INSERT INTO `pegawai` VALUES ('0533', '', '', 'MAESYAROH FUSFITA NINGRUM', 'A.md', 'P', 'BANDUNG', '1993-12-13', '0', '1', 'KP.LEBAK WANGI', '03', '01', '', '', '008', 'INA', '2015-06-27', '', '', '', '', '', '2015-06-27', '', '', '');
INSERT INTO `pegawai` VALUES ('1000', '', '', 'Rendra Piscela', 'S.komp', 'L', 'Bandung', '1989-03-15', '0', '1', 'Margahurip indah no.711, banjaran.', '02', '10', '', 'banj', '', 'INA', '2015-06-25', '10', '', '', '', '', '2015-06-29', '', '', '');
INSERT INTO `pegawai` VALUES ('0534', null, null, 'Yuningsih Sondjaya', 'Amk', 'P', 'Ujungpandang', '1975-10-27', '0', '2', 'Komp. Soreang Residence', '', '', '', null, null, '', null, null, null, null, '', null, null, '2', '1', null);
INSERT INTO `pegawai` VALUES ('0535', null, null, 'Firman Wahyudi', 'Amk', 'L', 'Cianjur', '1992-03-07', '0', '1', 'Kampung babakan sondari', '03', '06', '', null, null, 'INA', null, null, null, null, '', null, null, '2', '1', null);
INSERT INTO `pegawai` VALUES ('0967', '196702121989012002', null, 'Oneng Nurhikmah', 'Amkg', 'P', 'Bandung', '1967-08-02', '0', '1', 'Kampung pamujatan desa arjasari', '02', '02', '', null, null, '', '2015-07-10', null, null, null, '', null, '2015-07-10', '2', '1', null);
INSERT INTO `pegawai` VALUES ('0536', '', '', 'santi nuryantini', '', 'P', 'bandung', '1983-01-10', '0', '2', 'sadu', '00', '00', '03', '0022', '008', 'INA', '2014-01-01', '13', '9', 'A7.07', '', 'IV/E', '2014-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0537', '', '', 'rina', '', 'P', 'bandung', '1984-08-10', '0', '2', 'kp.cikancung', '03', '07', '04', '0030', '008', 'INA', '2014-01-01', '8', '9', 'A7.07', '', 'I/A', '2014-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0102', '', '', 'Tessa Anissa Galuh Pasiwi', 'S.Farm., Apt', 'P', 'BANDUNG', '1986-02-22', '0', '1', 'JL. LEUWIPANJANG GG BAKTI 1 NO 126 B RT 003/007 BANDUNG', '00', '00', '05', '0017', '', 'INA', '2015-11-02', '9', '9', 'A3.02', '', 'III/B', '2015-11-02', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0103', '', '', 'ADIYAH', 'S.Farm., Apt.', 'P', 'BANDUNG', '1985-08-23', '0', '2', 'JALAN TIRTAWANGI IV CIGANITRI', '00', '00', '06', '028', '008', 'INA', '2015-11-02', '9', '9', 'A3.02', '', 'III/B', '2015-11-02', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0612', '', '', 'LARAS GUMILAR', 'S.Si., Apt.', 'P', 'SUBANG', '1981-06-12', '0', '1', 'KEBONJAYANTI NO. 130', '00', '00', '07', '0019', '008', 'INA', '2015-11-02', '9', '9', 'A3.02', '', 'III/B', '2015-11-02', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0983', '', 'Dr', 'Dr.Fahmi Attaufany', '', 'L', 'Bandung', '1983-10-06', '0', '2', 'Jl. Kopo sayati hilir No. 226', '01', '08', '08', '0024', '009', 'INA', '2015-11-01', '9', '9', 'A1.11', '', 'II/A', '2015-11-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0610', '196510162009011002', 'Drs', 'Enjang Suparman', '', 'L', 'Garut', '1965-10-16', '0', '2', 'Komp. Margahurip Asih RT 01 RW 07, Desa Margahurip, Banjaran', '01', '07', '09', '0019', '008', 'INA', '2009-03-01', '9', '3', 'A7.88', '', 'II/B', '2009-03-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0712', '', '', 'Lilis Tarsiah,Amd.', '', 'P', 'Bandung', '1977-03-16', '0', '2', 'Komp. soreang indah Blok P no.23', '08', '15', '010', '0022', '008', 'INA', '2012-03-01', '8', '9', 'B4.03', '', 'I/A', '2012-03-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0614', '', '', 'Heni Hendayani', '', 'P', 'Bandung', '1970-03-10', '0', '1', 'Kampung kamasan, banjaran.', '02', '09', '010', '0019', '008', 'INA', '2012-03-01', '6', '9', 'B4.88', '', 'I/A', '2012-03-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0401', '', 'Dr', 'Dr.Adila Nurhadiya', '', 'P', 'Bandung', '1991-10-20', '0', '1', 'Jln. Kembar Timur VII No.29', '06', '06', '010', '0002', '009', 'INA', '2015-01-01', '9', '9', 'A1.00', '3217065904', 'III/A', '2015-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0402', '', 'Dr', 'Dr. Nanda Putri Ramadhani', '', 'P', 'Jakarta', '1989-04-19', '0', '1', 'Perum the awani residen blok C-01', '02', '05', '010', '0010', '008', 'INA', '2015-01-01', '9', '9', 'A1.00', '3217065904', 'III/A', '2015-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0403', '', 'Dr', 'Dr.Dinda Andini', '', 'P', 'Bandung', '1984-06-05', '0', '2', 'Jl. Agronomi no.10', '01', '07', '010', '0004', '009', 'INA', '2015-01-01', '9', '9', 'A1.00', '3273184506', 'III/A', '2015-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0404', '', 'Dr', 'Dr.Andiyang Rosseliene', '', 'P', 'Bandung', '2010-10-17', '0', '1', 'Jl. BBK Ciamis No.21 / 1 - B', '04', '07', '010', '0022', '009', 'INA', '2015-01-01', '9', '9', 'A1.00', '3273195710', 'III/A', '2015-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0992', '', '', 'Witri Rahma Nurliana', '', 'P', 'Bandung', '1992-04-09', '0', '1', 'Kp. Cipedung Desa Jatisari', '02', '01', '010', '0010', '008', 'INA', '2009-01-01', '5', '9', '', '', 'III/B', '2009-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0993', '', '', 'Alisa Yusuf', '', 'P', 'Bandung', '1991-09-25', '0', '1', 'Kp. Babakan Sari Suka no.15', '01', '07', '010', '0031', '008', 'INA', '2009-01-01', '5', '9', 'A3.02', '', 'III/C', '2009-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0994', '', '', 'Yayah Rokayah', '', 'P', 'Bandung', '1971-04-12', '0', '2', 'Lebak Wangi RT 02/01 DS Cingcin', '02', '01', '010', '0022', '008', 'INA', '2016-01-11', '13', '9', 'B5.00', '', 'I/A', '2016-01-11', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0995', '', '', 'Ulia Utammima', '', 'P', 'Tasikmalaya', '1979-02-14', '0', '2', 'Soreang Indah Blok B no 31', '02', '02', '010', '0022', '008', 'INA', '2016-01-11', '13', '9', '', '', 'I/A', '2016-01-11', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0996', '', '', 'Noneng Nurhayati', '', 'P', 'Bandung', '1975-08-13', '0', '2', 'Gading Tutuka 2 Blok K2A no 3', '01', '01', '010', '0030', '008', 'INA', '2016-01-11', '13', '9', '', '', 'I/A', '2016-01-11', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0997', '', '', 'Desi Saritilawati', '', 'P', 'Bandung', '2016-01-11', '0', '1', 'Kp. Bojong Buah No. 580 B', '02', '08', '010', '029', '008', 'INA', '2016-01-11', '13', '9', '', '', 'I/A', '2016-01-11', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0898', '', '', 'Lilis Anita Ningsih', '', 'P', 'Bandung', '1980-01-19', '0', '2', 'Kp. Cikurip', '03', '01', '010', '0019', '008', 'INA', '2016-01-11', '13', '9', '', '', 'I/A', '2016-01-11', '2', '1', '');
INSERT INTO `pegawai` VALUES ('1234', '', '', 'Adi Suwardani', '', 'L', 'Cimahi', '1986-08-14', '0', '2', 'Kp, Cijagra no. 13', '02', '12', '010', '', '', null, '2016-02-01', '13', '9', '', '', 'I/A', '2016-02-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('1111', '000000', 'Dr', 'NOName', null, 'L', '', '0000-00-00', '', null, null, '', '', '', null, null, '', null, null, null, null, '', null, null, '2', '1', null);
INSERT INTO `pegawai` VALUES ('0405', '', '', 'ENDEN RIKI M F', '', 'L', 'BANDUNG', '1989-05-21', '0', '1', 'CANGKUANG', '3', '2', '010', '0030', '008', 'INA', '2016-01-01', '13', '9', 'B5.00', '', 'I/A', '2016-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0801', '', '', 'Roni rahmat saputra', '', 'L', 'Bandung', '2016-10-10', '0', '2', 'kp.cikiara Ds. bandasari Kec. cangkuang Kab Bandung', '01', '01', '010', '0030', '008', 'INA', '2015-01-01', '9', '9', 'B3.03', '', 'I/A', '2015-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0391', '0391', 'Dr', 'Dr. Ade Erni, M.Gizi,Sp.GK', 'DR', 'L', 'Medan', '1972-02-04', '0', '2', 'JL. Margahayu Raya Blok H2/27C', '00', '01', '08', '0030', '009', 'INA', '2015-11-01', '10', '9', 'A5.01', '1111', 'III/B', '2015-11-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0802', '', '', 'lisda apriani', '', 'P', 'bandung', '1990-04-18', '0', '2', 'cikambuy girang', '05', '06', '010', '0021', '008', 'INA', '2014-01-01', '13', '9', 'A7.88', '', 'I/A', '2014-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0789', '', '', 'Suci Rianti', '', 'L', 'Bandung', '1991-10-02', '0', '2', 'Babakan stasion no.51, Banjaran.', '02', '03', '010', '0019', '008', 'INA', '2016-01-01', '4', '9', '', '', 'I/A', '2016-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0788', '', '', 'Rike Anggraeni', '', 'P', 'Jakarta', '1995-06-16', '0', '1', 'Gang mesjid desa kamasan', '04', '04', '010', '0019', '008', 'INA', '2015-01-01', '4', '9', '', '', 'I/A', '2015-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0981', '', '', 'Himawan Agung Susanto', 'Amd. Kes', 'L', 'Purwokerto', '1994-08-02', '0', '1', 'Komplek Soreang Residence Blok B5 no 18', '1', '16', '010', '029', '008', 'INA', '0000-00-00', '8', '9', 'A7.07', '', 'I/A', '2016-03-11', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0982', '', '', 'Muhamad Alim Robbani', 'Amd.kes S.Ter Kes', 'L', 'Bandung', '1993-03-12', '0', '1', 'Kp. Juntihilir rt.05/03 Desa sangkanhurip Kecamatan katapang', '', '', '010', '', '', null, '0000-00-00', '9', '9', 'A7.07', '', 'I/A', '2016-03-18', '2', '1', '');
INSERT INTO `pegawai` VALUES ('1335', null, null, 'Nandang Priatna', null, 'L', '', '1968-10-05', '', null, null, '', '', '', null, null, '', null, null, null, null, '', null, null, '2', '1', null);
INSERT INTO `pegawai` VALUES ('0406', '198503012010012012', '', 'SITI JUWITA', '', 'P', 'Bandung', '1985-03-01', '0', '2', 'KP. Gempol RT 05/02 Ds. Batukarut Kec. Arjasari Kab. Bandung', '05', '02', '010', '0018', '009', 'INA', '2010-01-01', '8', '1', 'A7.09', '0941/kep.I', 'II/D', '2010-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0407', '', '', 'DINI ANGGRAENI', '', 'P', 'BANDUNG', '1990-03-13', '0', '1', 'GADING JUNTI ASRI 21 No. 25', '2', '5', '010', '0021', '008', null, '2015-03-01', '8', '9', 'A7.09', '', 'II/A', '0000-00-00', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0408', '', '', 'Ika Nurbayanti', '', 'P', 'Bandung', '1989-08-10', '0', '2', 'Jl.Pesantren Barat No.22 RT.03/02 Ds. Pamekaran Kec. Soreang', '03', '02', '010', '0022', '008', 'INA', '2011-04-01', '8', '9', 'A7.09', '', 'I/A', '2011-04-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0409', '', '', 'INA SURYA DIANAWATI', '', 'P', 'BANDUNG', '1988-11-14', '0', '1', 'Kp. Kaum Kaler Desa Pamekaran', '01', '01', '010', '0022', '008', 'INA', '2010-04-01', '8', '9', 'A7.09', '', 'II/A', '2010-04-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0410', '197808142009012001', '', 'SITI SAADAH NS', '', 'P', 'BANDUNG', '1978-08-14', '0', '2', 'Kr ANYAR Desa CINGCIN', '01', '14', '010', '0022', '008', 'INA', '2009-01-01', '8', '3', 'A7.09', '', 'I/D', '2009-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0411', '', '', 'KARIZA FIRDIA MULYANI', '', 'L', 'BANDUNG', '1993-07-27', '0', '1', 'JL. KARANG TENGAH', '01', '07', '010', '0005', '008', 'INA', '2015-03-01', '8', '9', 'A7.09', '', 'I/D', '2015-03-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0412', '', '', 'GISTIYANINGSIH SHOLIHAT', '', 'P', 'BANDUNG', '1992-01-07', '0', '1', 'KP. LEUWENG KALENG', '03', '01', '010', '0021', '008', 'INA', '2015-03-01', '8', '9', 'A7.09', '', 'I/A', '2015-03-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0413', '', '', 'WENI LAELATUL ISMAH', '', 'P', 'BANDUNG', '1993-11-17', '0', '1', 'JLN. PUMA No. 6 BLOK A LANUD SULAEMAN', '2', '1', '010', '0024', '008', 'INA', '2015-03-01', '8', '9', 'A7.09', '', 'II/A', '2015-03-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0473', '00000', '', 'AGHNIA KHOERUNNISA', '', 'P', 'Bandung', '1990-03-31', '0', '2', 'KP. Sirnagalih no. 54', '01', '11', '010', '0022', '008', 'INA', '2016-01-01', '9', '9', '', '', 'I/A', '2016-01-01', '2', '1', '');
INSERT INTO `pegawai` VALUES ('0831', null, null, 'Yuli Pujiawati', null, 'P', 'Bandung', '1982-07-01', '', '', 'Tegalcaang', '', '', '', null, null, '', '2015-10-01', null, null, null, '', null, '2015-10-01', '2', '1', null);
INSERT INTO `pegawai` VALUES ('0392', '0392', 'Dr', 'Dr. Muhammad Aldito Rivaldi', 'DR', 'L', 'Bandung', '1991-09-24', '0', '1', 'Jl. Suryalaya XVII No. 15 A', '01', '00', '', null, null, 'INA', '2016-10-01', '9', '9', 'A1.00', '', null, null, '2', '1', null);

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `permission_role`
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'administrator', 'Administrator', 'Role Administrator', null, null);
INSERT INTO `roles` VALUES ('2', 'bpjs_lt_1', 'BPJS Lt 1', 'Role BPJS Lt 1', null, null);
INSERT INTO `roles` VALUES ('3', 'bpjs_lt_2', 'BPJS Lt 2', 'Role BPJS Lt 2', null, null);
INSERT INTO `roles` VALUES ('4', 'sktm', 'SKTM', 'Role SKTM', null, null);
INSERT INTO `roles` VALUES ('5', 'umum', 'Umum', 'Role Umum', null, null);

-- ----------------------------
-- Table structure for `role_user`
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES ('1', '1');
INSERT INTO `role_user` VALUES ('2', '2');
INSERT INTO `role_user` VALUES ('3', '2');
INSERT INTO `role_user` VALUES ('4', '2');
INSERT INTO `role_user` VALUES ('5', '2');
INSERT INTO `role_user` VALUES ('6', '3');
INSERT INTO `role_user` VALUES ('7', '3');
INSERT INTO `role_user` VALUES ('8', '4');
INSERT INTO `role_user` VALUES ('9', '5');

-- ----------------------------
-- Table structure for `sessions`
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sessions
-- ----------------------------
INSERT INTO `sessions` VALUES ('EycRrymuMExtqQZw3m3XMEFS288upSgdnCDyKoSt', '1', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiWUpBYTFxeVJKcWd6dElCdDlLeVZNeUFwb3RRRXhMYTkwRW5TZDR1dCI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjM3OiJodHRwOi8vbG9jYWxob3N0OjgwODAvbWluaVBhbmVsL2luZGV4Ijt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTt9', '1499725492');
INSERT INTO `sessions` VALUES ('h8zrxtGidCRPU3EHp4wF6CrKEiA8RaOxEeouHIZ7', null, '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoic2tNcVBYbG9uVDZtQURObXZqTThVR1hQem1tb1QxWlozODBzVkNpQiI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyMToiaHR0cDovL2xvY2FsaG9zdDo4MDgwIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', '1499724694');
INSERT INTO `sessions` VALUES ('qZxwqt4AWy6wb7WsOxj8wpCYb8oCygIrJUH8J735', null, '192.168.10.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3151.0 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoieUhLUThuUFZZSjM3VVZ4THpPc1FhWU5FOTRzaDFBSFBqVTh2OGx1ZyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly8xMC4xMC4xMC4zOjgwODAvZGlzcGxheS9yZWdpc3Rlci9iMS9iMiI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', '1499728554');
INSERT INTO `sessions` VALUES ('vBlLgWxLTVXziEXvdQlw9eaxDy4Qlk4KjmSndEax', null, '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoidUp1Y1JrVXNjWGdxMHFjTXFIVlp1T1lTSU5jZ1gybGdmSTZYbjdDdSI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyMToiaHR0cDovL2xvY2FsaG9zdDo4MDgwIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', '1499724694');
INSERT INTO `sessions` VALUES ('X7orv3hEWgHvVwT6JAQuS28eoY0L03H7068aYbQk', null, '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiem0wc25GbXM1eXlTa2V4RXBlWDVVcFlCeWN0cnVNS01Wbm9VWVdMWCI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyMToiaHR0cDovL2xvY2FsaG9zdDo4MDgwIjt9czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', '1499724694');

-- ----------------------------
-- Table structure for `sm_asuransis`
-- ----------------------------
DROP TABLE IF EXISTS `sm_asuransis`;
CREATE TABLE `sm_asuransis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asuransiidx` int(11) NOT NULL,
  `IdAsuransi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NamaAsuransi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `metavalue` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sm_asuransis
-- ----------------------------

-- ----------------------------
-- Table structure for `sm_kunjungans`
-- ----------------------------
DROP TABLE IF EXISTS `sm_kunjungans`;
CREATE TABLE `sm_kunjungans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visitidx` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdBankNomor` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdRuangPerujuk` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NamaRuangPerujuk` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AliasNamaRuangPerujuk` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdDokterPerujuk` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NamaDokterPerujuk` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TglPesan` timestamp NULL DEFAULT NULL,
  `TglAntrian` timestamp NULL DEFAULT NULL,
  `TglAmbilHasil` timestamp NULL DEFAULT NULL,
  `position` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `noqueue` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sm_kunjungans
-- ----------------------------

-- ----------------------------
-- Table structure for `sm_metas`
-- ----------------------------
DROP TABLE IF EXISTS `sm_metas`;
CREATE TABLE `sm_metas` (
  `value` int(11) NOT NULL AUTO_INCREMENT,
  `parameter` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `link` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sm_metas
-- ----------------------------
INSERT INTO `sm_metas` VALUES ('1', 'VOICE', '', '', '2017-07-07 23:58:39', '2017-07-10 05:37:24');
INSERT INTO `sm_metas` VALUES ('2', 'FIRST', 'BPJS Lantai 1', 'A', '2017-07-07 23:58:40', '2017-07-07 23:58:40');
INSERT INTO `sm_metas` VALUES ('3', 'FIRST', 'BPJS Lantai 2', 'B', '2017-07-07 23:58:40', '2017-07-07 23:58:40');
INSERT INTO `sm_metas` VALUES ('4', 'FIRST', 'SKTM', 'C', '2017-07-07 23:58:40', '2017-07-07 23:58:40');
INSERT INTO `sm_metas` VALUES ('5', 'FIRST', 'UMUM', 'D', '2017-07-07 23:58:40', '2017-07-07 23:58:40');

-- ----------------------------
-- Table structure for `sm_pasiens`
-- ----------------------------
DROP TABLE IF EXISTS `sm_pasiens`;
CREATE TABLE `sm_pasiens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdBankNomor` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdPasien` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NamaPasien` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IdJenisKelamin` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TglLahir` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sm_pasiens
-- ----------------------------

-- ----------------------------
-- Table structure for `sm_pendaftarans`
-- ----------------------------
DROP TABLE IF EXISTS `sm_pendaftarans`;
CREATE TABLE `sm_pendaftarans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `regid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noqueue` int(11) DEFAULT NULL,
  `datequeue` timestamp NULL DEFAULT NULL,
  `timequeue` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metavalue` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_calling` tinyint(1) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=871 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sm_pendaftarans
-- ----------------------------
INSERT INTO `sm_pendaftarans` VALUES ('1', '2070717001', '1', '2017-07-07 00:00:00', '11:59:35', 'WT', '2', '2017-07-07 23:59:35', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('2', '2070717002', '2', '2017-07-07 00:00:00', '11:59:47', 'WT', '2', '2017-07-07 23:59:47', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('3', '2080717001', '1', '2017-07-08 00:00:00', '12:00:29', 'DN', '2', '2017-07-08 00:00:29', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('4', '2080717002', '2', '2017-07-08 00:00:00', '12:00:35', 'DN', '2', '2017-07-08 00:00:35', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('5', '2080717003', '3', '2017-07-08 00:00:00', '12:00:39', 'DN', '2', '2017-07-08 00:00:39', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('6', '2080717004', '4', '2017-07-08 00:00:00', '12:00:54', 'DN', '2', '2017-07-08 00:00:54', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('7', '2080717005', '5', '2017-07-08 00:00:00', '12:01:01', 'DN', '2', '2017-07-08 00:01:01', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('8', '2080717006', '6', '2017-07-08 00:00:00', '12:01:08', 'DN', '2', '2017-07-08 00:01:08', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('9', '2080717007', '7', '2017-07-08 00:00:00', '12:01:14', 'DN', '2', '2017-07-08 00:01:14', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('10', '2080717008', '8', '2017-07-08 00:00:00', '12:01:19', 'DN', '2', '2017-07-08 00:01:19', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('11', '2080717009', '9', '2017-07-08 00:00:00', '12:01:26', 'DN', '2', '2017-07-08 00:01:26', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('12', '2080717010', '10', '2017-07-08 00:00:00', '12:01:33', 'DN', '2', '2017-07-08 00:01:33', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('13', '2080717011', '11', '2017-07-08 00:00:00', '12:01:40', 'DN', '2', '2017-07-08 00:01:40', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('14', '2080717012', '12', '2017-07-08 00:00:00', '12:01:46', 'CL', '2', '2017-07-08 00:01:46', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('15', '2080717013', '13', '2017-07-08 00:00:00', '12:01:52', 'CL', '2', '2017-07-08 00:01:52', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('16', '2080717014', '14', '2017-07-08 00:00:00', '12:01:59', 'CL', '2', '2017-07-08 00:01:59', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('17', '2080717015', '15', '2017-07-08 00:00:00', '12:02:06', 'CL', '2', '2017-07-08 00:02:06', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('18', '2080717016', '16', '2017-07-08 00:00:00', '12:02:13', 'CL', '2', '2017-07-08 00:02:13', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('19', '2080717017', '17', '2017-07-08 00:00:00', '12:02:21', 'CL', '2', '2017-07-08 00:02:21', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('20', '2080717018', '18', '2017-07-08 00:00:00', '12:02:29', 'CL', '2', '2017-07-08 00:02:29', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('21', '2080717019', '19', '2017-07-08 00:00:00', '12:02:33', 'CL', '2', '2017-07-08 00:02:33', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('22', '2080717020', '20', '2017-07-08 00:00:00', '12:02:39', 'CL', '2', '2017-07-08 00:02:39', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('23', '2080717021', '21', '2017-07-08 00:00:00', '12:02:47', 'CL', '2', '2017-07-08 00:02:47', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('24', '2080717022', '22', '2017-07-08 00:00:00', '12:02:54', 'CL', '2', '2017-07-08 00:02:54', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('25', '2080717023', '23', '2017-07-08 00:00:00', '12:03:02', 'CL', '2', '2017-07-08 00:03:02', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('26', '2080717024', '24', '2017-07-08 00:00:00', '12:03:08', 'CL', '2', '2017-07-08 00:03:08', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('27', '2080717025', '25', '2017-07-08 00:00:00', '12:03:15', 'CL', '2', '2017-07-08 00:03:15', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('28', '2080717026', '26', '2017-07-08 00:00:00', '12:03:21', 'DN', '2', '2017-07-08 00:03:21', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('29', '2080717027', '27', '2017-07-08 00:00:00', '12:03:28', 'DN', '2', '2017-07-08 00:03:28', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('30', '2080717028', '28', '2017-07-08 00:00:00', '12:03:35', 'DN', '2', '2017-07-08 00:03:35', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('31', '2080717029', '29', '2017-07-08 00:00:00', '12:03:41', 'DN', '2', '2017-07-08 00:03:41', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('32', '2080717030', '30', '2017-07-08 00:00:00', '12:03:47', 'DN', '2', '2017-07-08 00:03:47', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('33', '2080717031', '31', '2017-07-08 00:00:00', '12:03:54', 'DN', '2', '2017-07-08 00:03:54', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('34', '2080717032', '32', '2017-07-08 00:00:00', '12:04:02', 'DN', '2', '2017-07-08 00:04:02', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('35', '2080717033', '33', '2017-07-08 00:00:00', '12:04:09', 'DN', '2', '2017-07-08 00:04:09', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('36', '2080717034', '34', '2017-07-08 00:00:00', '12:04:17', 'DN', '2', '2017-07-08 00:04:17', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('37', '2080717035', '35', '2017-07-08 00:00:00', '12:04:24', 'DN', '2', '2017-07-08 00:04:24', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('38', '2080717036', '36', '2017-07-08 00:00:00', '12:04:31', 'DN', '2', '2017-07-08 00:04:31', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('39', '2080717037', '37', '2017-07-08 00:00:00', '12:04:38', 'DN', '2', '2017-07-08 00:04:38', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('40', '2080717038', '38', '2017-07-08 00:00:00', '12:04:44', 'DN', '2', '2017-07-08 00:04:44', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('41', '2080717039', '39', '2017-07-08 00:00:00', '12:04:51', 'DN', '2', '2017-07-08 00:04:51', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('42', '2080717040', '40', '2017-07-08 00:00:00', '12:04:58', 'DN', '2', '2017-07-08 00:04:58', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('43', '3080717001', '1', '2017-07-08 00:00:00', '12:05:04', 'DN', '3', '2017-07-08 00:05:04', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('44', '3080717002', '2', '2017-07-08 00:00:00', '12:05:10', 'DN', '3', '2017-07-08 00:05:10', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('45', '3080717003', '3', '2017-07-08 00:00:00', '12:05:17', 'DN', '3', '2017-07-08 00:05:17', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('46', '3080717004', '4', '2017-07-08 00:00:00', '12:05:23', 'DN', '3', '2017-07-08 00:05:23', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('47', '3080717005', '5', '2017-07-08 00:00:00', '12:05:29', 'DN', '3', '2017-07-08 00:05:29', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('48', '3080717006', '6', '2017-07-08 00:00:00', '12:05:35', 'DN', '3', '2017-07-08 00:05:35', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('49', '3080717007', '7', '2017-07-08 00:00:00', '12:05:41', 'DN', '3', '2017-07-08 00:05:41', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('50', '3080717008', '8', '2017-07-08 00:00:00', '12:05:46', 'DN', '3', '2017-07-08 00:05:46', '2017-07-10 05:37:24', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('51', '3080717009', '9', '2017-07-08 00:00:00', '12:05:52', 'DN', '3', '2017-07-08 00:05:52', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('52', '3080717010', '10', '2017-07-08 00:00:00', '12:05:58', 'DN', '3', '2017-07-08 00:05:58', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('53', '5080717001', '1', '2017-07-08 00:00:00', '12:06:07', 'DN', '5', '2017-07-08 00:06:07', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('54', '5080717002', '2', '2017-07-08 00:00:00', '12:06:13', 'DN', '5', '2017-07-08 00:06:13', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('55', '5080717003', '3', '2017-07-08 00:00:00', '12:06:19', 'DN', '5', '2017-07-08 00:06:19', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('56', '5080717004', '4', '2017-07-08 00:00:00', '12:06:23', 'DN', '5', '2017-07-08 00:06:23', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('57', '5080717005', '5', '2017-07-08 00:00:00', '12:06:28', 'DN', '5', '2017-07-08 00:06:28', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('58', '5080717006', '6', '2017-07-08 00:00:00', '12:06:32', 'DN', '5', '2017-07-08 00:06:32', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('59', '5080717007', '7', '2017-07-08 00:00:00', '12:06:36', 'DN', '5', '2017-07-08 00:06:36', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('60', '5080717008', '8', '2017-07-08 00:00:00', '12:06:42', 'DN', '5', '2017-07-08 00:06:42', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('61', '5080717009', '9', '2017-07-08 00:00:00', '12:06:45', 'DN', '5', '2017-07-08 00:06:45', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('62', '5080717010', '10', '2017-07-08 00:00:00', '12:06:49', 'DN', '5', '2017-07-08 00:06:49', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('63', '5080717011', '11', '2017-07-08 00:00:00', '12:06:55', 'DN', '5', '2017-07-08 00:06:55', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('64', '5080717012', '12', '2017-07-08 00:00:00', '12:07:00', 'DN', '5', '2017-07-08 00:07:00', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('65', '5080717013', '13', '2017-07-08 00:00:00', '12:07:04', 'DN', '5', '2017-07-08 00:07:04', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('66', '5080717014', '14', '2017-07-08 00:00:00', '12:07:08', 'DN', '5', '2017-07-08 00:07:08', '2017-07-10 04:21:18', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('67', '5080717015', '15', '2017-07-08 00:00:00', '12:07:14', 'DN', '5', '2017-07-08 00:07:14', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('68', '4080717001', '1', '2017-07-08 00:00:00', '12:07:19', 'DN', '4', '2017-07-08 00:07:19', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('69', '4080717002', '2', '2017-07-08 00:00:00', '12:07:23', 'DN', '4', '2017-07-08 00:07:23', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('70', '4080717003', '3', '2017-07-08 00:00:00', '12:07:26', 'DN', '4', '2017-07-08 00:07:26', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('71', '4080717004', '4', '2017-07-08 00:00:00', '12:07:31', 'DN', '4', '2017-07-08 00:07:31', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('72', '4080717005', '5', '2017-07-08 00:00:00', '12:07:36', 'DN', '4', '2017-07-08 00:07:36', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('73', '3080717011', '11', '2017-07-08 00:00:00', '12:07:46', 'DN', '3', '2017-07-08 00:07:46', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('74', '3080717012', '12', '2017-07-08 00:00:00', '12:07:52', 'DN', '3', '2017-07-08 00:07:52', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('75', '3080717013', '13', '2017-07-08 00:00:00', '12:07:56', 'DN', '3', '2017-07-08 00:07:56', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('76', '3080717014', '14', '2017-07-08 00:00:00', '12:08:00', 'DN', '3', '2017-07-08 00:08:00', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('77', '3080717015', '15', '2017-07-08 00:00:00', '12:08:04', 'DN', '3', '2017-07-08 00:08:04', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('78', '3080717016', '16', '2017-07-08 00:00:00', '12:08:09', 'DN', '3', '2017-07-08 00:08:09', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('79', '3080717017', '17', '2017-07-08 00:00:00', '12:08:14', 'DN', '3', '2017-07-08 00:08:14', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('80', '3080717018', '18', '2017-07-08 00:00:00', '12:08:19', 'DN', '3', '2017-07-08 00:08:19', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('81', '3080717019', '19', '2017-07-08 00:00:00', '12:08:23', 'DN', '3', '2017-07-08 00:08:23', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('82', '3080717020', '20', '2017-07-08 00:00:00', '12:08:28', 'DN', '3', '2017-07-08 00:08:28', '2017-07-10 05:37:24', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('83', '2080717041', '41', '2017-07-08 00:00:00', '12:08:36', 'DN', '2', '2017-07-08 00:08:36', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('84', '2080717042', '42', '2017-07-08 00:00:00', '12:08:40', 'DN', '2', '2017-07-08 00:08:40', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('85', '2080717043', '43', '2017-07-08 00:00:00', '12:08:43', 'DN', '2', '2017-07-08 00:08:43', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('86', '2080717044', '44', '2017-07-08 00:00:00', '12:08:48', 'DN', '2', '2017-07-08 00:08:48', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('87', '2080717045', '45', '2017-07-08 00:00:00', '12:08:52', 'DN', '2', '2017-07-08 00:08:52', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('88', '2080717046', '46', '2017-07-08 00:00:00', '12:08:57', 'DN', '2', '2017-07-08 00:08:57', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('89', '2080717047', '47', '2017-07-08 00:00:00', '12:09:02', 'DN', '2', '2017-07-08 00:09:02', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('90', '2080717048', '48', '2017-07-08 00:00:00', '12:09:07', 'DN', '2', '2017-07-08 00:09:07', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('91', '2080717049', '49', '2017-07-08 00:00:00', '12:09:12', 'DN', '2', '2017-07-08 00:09:12', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('92', '2080717050', '50', '2017-07-08 00:00:00', '12:09:19', 'DN', '2', '2017-07-08 00:09:19', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('93', '2080717051', '51', '2017-07-08 00:00:00', '12:09:23', 'DN', '2', '2017-07-08 00:09:23', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('94', '2080717052', '52', '2017-07-08 00:00:00', '12:09:28', 'DN', '2', '2017-07-08 00:09:28', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('95', '2080717053', '53', '2017-07-08 00:00:00', '12:09:32', 'DN', '2', '2017-07-08 00:09:32', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('96', '2080717054', '54', '2017-07-08 00:00:00', '12:09:38', 'DN', '2', '2017-07-08 00:09:38', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('97', '2080717055', '55', '2017-07-08 00:00:00', '12:09:44', 'DN', '2', '2017-07-08 00:09:44', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('98', '2080717056', '56', '2017-07-08 00:00:00', '12:09:49', 'DN', '2', '2017-07-08 00:09:49', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('99', '2080717057', '57', '2017-07-08 00:00:00', '12:09:55', 'DN', '2', '2017-07-08 00:09:55', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('100', '2080717058', '58', '2017-07-08 00:00:00', '12:10:00', 'DN', '2', '2017-07-08 00:10:00', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('101', '2080717059', '59', '2017-07-08 00:00:00', '12:10:05', 'DN', '2', '2017-07-08 00:10:05', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('102', '2080717060', '60', '2017-07-08 00:00:00', '12:10:10', 'DN', '2', '2017-07-08 00:10:10', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('103', '2080717061', '61', '2017-07-08 00:00:00', '12:10:15', 'DN', '2', '2017-07-08 00:10:15', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('104', '2080717062', '62', '2017-07-08 00:00:00', '12:10:20', 'DN', '2', '2017-07-08 00:10:20', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('105', '2080717063', '63', '2017-07-08 00:00:00', '12:10:25', 'DN', '2', '2017-07-08 00:10:25', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('106', '2080717064', '64', '2017-07-08 00:00:00', '12:10:28', 'DN', '2', '2017-07-08 00:10:28', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('107', '2080717065', '65', '2017-07-08 00:00:00', '12:10:33', 'DN', '2', '2017-07-08 00:10:33', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('108', '2080717066', '66', '2017-07-08 00:00:00', '12:10:38', 'DN', '2', '2017-07-08 00:10:38', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('109', '2080717067', '67', '2017-07-08 00:00:00', '12:10:42', 'DN', '2', '2017-07-08 00:10:42', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('110', '2080717068', '68', '2017-07-08 00:00:00', '12:10:46', 'DN', '2', '2017-07-08 00:10:46', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('111', '2080717069', '69', '2017-07-08 00:00:00', '12:10:51', 'DN', '2', '2017-07-08 00:10:51', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('112', '2080717070', '70', '2017-07-08 00:00:00', '12:10:56', 'DN', '2', '2017-07-08 00:10:56', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('113', '2080717071', '71', '2017-07-08 00:00:00', '12:10:59', 'DN', '2', '2017-07-08 00:10:59', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('114', '2080717072', '72', '2017-07-08 00:00:00', '12:11:03', 'DN', '2', '2017-07-08 00:11:03', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('115', '2080717073', '73', '2017-07-08 00:00:00', '12:11:06', 'DN', '2', '2017-07-08 00:11:06', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('116', '2080717074', '74', '2017-07-08 00:00:00', '12:11:13', 'DN', '2', '2017-07-08 00:11:13', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('117', '2080717075', '75', '2017-07-08 00:00:00', '12:11:17', 'DN', '2', '2017-07-08 00:11:17', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('118', '2080717076', '76', '2017-07-08 00:00:00', '12:11:21', 'DN', '2', '2017-07-08 00:11:21', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('119', '2080717077', '77', '2017-07-08 00:00:00', '12:11:24', 'DN', '2', '2017-07-08 00:11:24', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('120', '2080717078', '78', '2017-07-08 00:00:00', '12:11:29', 'DN', '2', '2017-07-08 00:11:29', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('121', '2080717079', '79', '2017-07-08 00:00:00', '12:11:34', 'DN', '2', '2017-07-08 00:11:34', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('122', '2080717080', '80', '2017-07-08 00:00:00', '12:11:39', 'DN', '2', '2017-07-08 00:11:39', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('123', '2080717081', '81', '2017-07-08 00:00:00', '12:11:42', 'DN', '2', '2017-07-08 00:11:42', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('124', '2080717082', '82', '2017-07-08 00:00:00', '12:11:45', 'DN', '2', '2017-07-08 00:11:45', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('125', '2080717083', '83', '2017-07-08 00:00:00', '12:11:51', 'DN', '2', '2017-07-08 00:11:51', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('126', '2080717084', '84', '2017-07-08 00:00:00', '12:11:55', 'DN', '2', '2017-07-08 00:11:55', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('127', '2080717085', '85', '2017-07-08 00:00:00', '12:11:59', 'DN', '2', '2017-07-08 00:11:59', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('128', '2080717086', '86', '2017-07-08 00:00:00', '12:12:03', 'DN', '2', '2017-07-08 00:12:03', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('129', '2080717087', '87', '2017-07-08 00:00:00', '12:12:08', 'DN', '2', '2017-07-08 00:12:08', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('130', '2080717088', '88', '2017-07-08 00:00:00', '12:12:12', 'DN', '2', '2017-07-08 00:12:12', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('131', '2080717089', '89', '2017-07-08 00:00:00', '12:12:15', 'DN', '2', '2017-07-08 00:12:15', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('132', '2080717090', '90', '2017-07-08 00:00:00', '12:12:21', 'DN', '2', '2017-07-08 00:12:21', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('133', '2080717091', '91', '2017-07-08 00:00:00', '12:12:26', 'DN', '2', '2017-07-08 00:12:26', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('134', '2080717092', '92', '2017-07-08 00:00:00', '12:12:30', 'DN', '2', '2017-07-08 00:12:30', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('135', '2080717093', '93', '2017-07-08 00:00:00', '12:12:35', 'DN', '2', '2017-07-08 00:12:35', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('136', '2080717094', '94', '2017-07-08 00:00:00', '12:12:41', 'DN', '2', '2017-07-08 00:12:41', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('137', '2080717095', '95', '2017-07-08 00:00:00', '12:12:46', 'DN', '2', '2017-07-08 00:12:46', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('138', '2080717096', '96', '2017-07-08 00:00:00', '12:12:50', 'DN', '2', '2017-07-08 00:12:50', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('139', '2080717097', '97', '2017-07-08 00:00:00', '12:12:56', 'DN', '2', '2017-07-08 00:12:56', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('140', '2080717098', '98', '2017-07-08 00:00:00', '12:13:01', 'DN', '2', '2017-07-08 00:13:01', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('141', '2080717099', '99', '2017-07-08 00:00:00', '12:13:05', 'DN', '2', '2017-07-08 00:13:05', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('142', '2080717100', '100', '2017-07-08 00:00:00', '12:13:09', 'DN', '2', '2017-07-08 00:13:09', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('143', '2080717101', '101', '2017-07-08 00:00:00', '12:13:13', 'DN', '2', '2017-07-08 00:13:13', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('144', '2080717102', '102', '2017-07-08 00:00:00', '12:13:18', 'DN', '2', '2017-07-08 00:13:18', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('145', '2080717103', '103', '2017-07-08 00:00:00', '12:13:24', 'DN', '2', '2017-07-08 00:13:24', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('146', '2080717104', '104', '2017-07-08 00:00:00', '12:13:28', 'DN', '2', '2017-07-08 00:13:28', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('147', '2080717105', '105', '2017-07-08 00:00:00', '12:13:33', 'DN', '2', '2017-07-08 00:13:33', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('148', '2080717106', '106', '2017-07-08 00:00:00', '12:13:37', 'DN', '2', '2017-07-08 00:13:37', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('149', '2080717107', '107', '2017-07-08 00:00:00', '12:13:40', 'CL', '2', '2017-07-08 00:13:40', '2017-07-10 04:38:30', '0', '1');
INSERT INTO `sm_pendaftarans` VALUES ('150', '2080717108', '108', '2017-07-08 00:00:00', '12:13:44', 'CL', '2', '2017-07-08 00:13:44', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('151', '2080717109', '109', '2017-07-08 00:00:00', '12:13:49', 'CL', '2', '2017-07-08 00:13:49', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('152', '2080717110', '110', '2017-07-08 00:00:00', '12:13:54', 'CL', '2', '2017-07-08 00:13:54', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('153', '2080717111', '111', '2017-07-08 00:00:00', '12:13:59', 'CL', '2', '2017-07-08 00:13:59', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('154', '2080717112', '112', '2017-07-08 00:00:00', '12:14:02', 'CL', '2', '2017-07-08 00:14:02', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('155', '2080717113', '113', '2017-07-08 00:00:00', '12:14:07', 'CL', '2', '2017-07-08 00:14:07', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('156', '2080717114', '114', '2017-07-08 00:00:00', '12:14:12', 'CL', '2', '2017-07-08 00:14:12', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('157', '2080717115', '115', '2017-07-08 00:00:00', '12:14:15', 'CL', '2', '2017-07-08 00:14:15', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('158', '2080717116', '116', '2017-07-08 00:00:00', '12:14:19', 'DN', '2', '2017-07-08 00:14:19', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('159', '2080717117', '117', '2017-07-08 00:00:00', '12:14:24', 'DN', '2', '2017-07-08 00:14:24', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('160', '2080717118', '118', '2017-07-08 00:00:00', '12:14:28', 'DN', '2', '2017-07-08 00:14:28', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('161', '2080717119', '119', '2017-07-08 00:00:00', '12:14:33', 'DN', '2', '2017-07-08 00:14:33', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('162', '2080717120', '120', '2017-07-08 00:00:00', '12:14:40', 'DN', '2', '2017-07-08 00:14:40', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('163', '2080717121', '121', '2017-07-08 00:00:00', '12:14:45', 'DN', '2', '2017-07-08 00:14:45', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('164', '2080717122', '122', '2017-07-08 00:00:00', '12:14:51', 'DN', '2', '2017-07-08 00:14:51', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('165', '2080717123', '123', '2017-07-08 00:00:00', '12:14:56', 'DN', '2', '2017-07-08 00:14:56', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('166', '2080717124', '124', '2017-07-08 00:00:00', '12:15:02', 'DN', '2', '2017-07-08 00:15:02', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('167', '2080717125', '125', '2017-07-08 00:00:00', '12:15:07', 'DN', '2', '2017-07-08 00:15:07', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('168', '2080717126', '126', '2017-07-08 00:00:00', '12:15:11', 'DN', '2', '2017-07-08 00:15:11', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('169', '2080717127', '127', '2017-07-08 00:00:00', '12:15:16', 'DN', '2', '2017-07-08 00:15:16', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('170', '2080717128', '128', '2017-07-08 00:00:00', '12:15:20', 'DN', '2', '2017-07-08 00:15:20', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('171', '2080717129', '129', '2017-07-08 00:00:00', '12:15:25', 'DN', '2', '2017-07-08 00:15:25', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('172', '2080717130', '130', '2017-07-08 00:00:00', '12:15:28', 'DN', '2', '2017-07-08 00:15:28', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('173', '2080717131', '131', '2017-07-08 00:00:00', '12:15:33', 'DN', '2', '2017-07-08 00:15:33', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('174', '2080717132', '132', '2017-07-08 00:00:00', '12:15:37', 'DN', '2', '2017-07-08 00:15:37', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('175', '2080717133', '133', '2017-07-08 00:00:00', '12:15:41', 'DN', '2', '2017-07-08 00:15:41', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('176', '3080717021', '21', '2017-07-08 00:00:00', '12:15:50', 'DN', '3', '2017-07-08 00:15:50', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('177', '2080717134', '134', '2017-07-08 00:00:00', '12:15:56', 'DN', '2', '2017-07-08 00:15:56', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('178', '3080717022', '22', '2017-07-08 00:00:00', '12:16:00', 'DN', '3', '2017-07-08 00:16:00', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('179', '2080717135', '135', '2017-07-08 00:00:00', '12:16:07', 'DN', '2', '2017-07-08 00:16:07', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('180', '2080717136', '136', '2017-07-08 00:00:00', '12:16:13', 'DN', '2', '2017-07-08 00:16:13', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('181', '2080717137', '137', '2017-07-08 00:00:00', '12:16:18', 'DN', '2', '2017-07-08 00:16:18', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('182', '2080717138', '138', '2017-07-08 00:00:00', '12:16:24', 'DN', '2', '2017-07-08 00:16:24', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('183', '2080717139', '139', '2017-07-08 00:00:00', '12:16:29', 'DN', '2', '2017-07-08 00:16:29', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('184', '3080717023', '23', '2017-07-08 00:00:00', '12:16:36', 'DN', '3', '2017-07-08 00:16:36', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('185', '3080717024', '24', '2017-07-08 00:00:00', '12:16:42', 'DN', '3', '2017-07-08 00:16:42', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('186', '2080717140', '140', '2017-07-08 00:00:00', '12:16:48', 'DN', '2', '2017-07-08 00:16:48', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('187', '2080717141', '141', '2017-07-08 00:00:00', '12:16:53', 'DN', '2', '2017-07-08 00:16:53', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('188', '2080717142', '142', '2017-07-08 00:00:00', '12:16:56', 'DN', '2', '2017-07-08 00:16:56', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('189', '2080717143', '143', '2017-07-08 00:00:00', '12:17:00', 'DN', '2', '2017-07-08 00:17:00', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('190', '2080717144', '144', '2017-07-08 00:00:00', '12:17:04', 'DN', '2', '2017-07-08 00:17:04', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('191', '2080717145', '145', '2017-07-08 00:00:00', '12:17:09', 'PR', '2', '2017-07-08 00:17:09', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('192', '2080717146', '146', '2017-07-08 00:00:00', '12:17:12', 'DN', '2', '2017-07-08 00:17:12', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('193', '2080717147', '147', '2017-07-08 00:00:00', '12:17:17', 'DN', '2', '2017-07-08 00:17:17', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('194', '2080717148', '148', '2017-07-08 00:00:00', '12:17:22', 'DN', '2', '2017-07-08 00:17:22', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('195', '2080717149', '149', '2017-07-08 00:00:00', '12:17:26', 'DN', '2', '2017-07-08 00:17:26', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('196', '2080717150', '150', '2017-07-08 00:00:00', '12:17:30', 'DN', '2', '2017-07-08 00:17:30', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('197', '2080717151', '151', '2017-07-08 00:00:00', '12:17:37', 'DN', '2', '2017-07-08 00:17:37', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('198', '2080717152', '152', '2017-07-08 00:00:00', '12:17:44', 'DN', '2', '2017-07-08 00:17:44', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('199', '2080717153', '153', '2017-07-08 00:00:00', '12:17:48', 'DN', '2', '2017-07-08 00:17:48', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('200', '2080717154', '154', '2017-07-08 00:00:00', '12:17:52', 'DN', '2', '2017-07-08 00:17:52', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('201', '2080717155', '155', '2017-07-08 00:00:00', '12:17:56', 'DN', '2', '2017-07-08 00:17:56', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('202', '2080717156', '156', '2017-07-08 00:00:00', '12:18:02', 'DN', '2', '2017-07-08 00:18:02', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('203', '2080717157', '157', '2017-07-08 00:00:00', '12:18:07', 'DN', '2', '2017-07-08 00:18:07', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('204', '2080717158', '158', '2017-07-08 00:00:00', '12:18:13', 'DN', '2', '2017-07-08 00:18:13', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('205', '2080717159', '159', '2017-07-08 00:00:00', '12:18:16', 'DN', '2', '2017-07-08 00:18:16', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('206', '2080717160', '160', '2017-07-08 00:00:00', '12:18:23', 'DN', '2', '2017-07-08 00:18:23', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('207', '2080717161', '161', '2017-07-08 00:00:00', '12:18:28', 'DN', '2', '2017-07-08 00:18:28', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('208', '2080717162', '162', '2017-07-08 00:00:00', '12:18:32', 'DN', '2', '2017-07-08 00:18:32', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('209', '2080717163', '163', '2017-07-08 00:00:00', '12:18:35', 'DN', '2', '2017-07-08 00:18:35', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('210', '2080717164', '164', '2017-07-08 00:00:00', '12:18:39', 'DN', '2', '2017-07-08 00:18:39', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('211', '2080717165', '165', '2017-07-08 00:00:00', '12:18:44', 'DN', '2', '2017-07-08 00:18:44', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('212', '2080717166', '166', '2017-07-08 00:00:00', '12:18:49', 'DN', '2', '2017-07-08 00:18:49', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('213', '3080717025', '25', '2017-07-08 00:00:00', '12:19:05', 'DN', '3', '2017-07-08 00:19:05', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('214', '3080717026', '26', '2017-07-08 00:00:00', '12:20:02', 'DN', '3', '2017-07-08 00:20:02', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('215', '3080717027', '27', '2017-07-08 00:00:00', '12:20:48', 'DN', '3', '2017-07-08 00:20:48', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('216', '2080717167', '167', '2017-07-08 00:00:00', '12:21:56', 'DN', '2', '2017-07-08 00:21:56', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('217', '2080717168', '168', '2017-07-08 00:00:00', '12:22:27', 'DN', '2', '2017-07-08 00:22:27', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('218', '2080717169', '169', '2017-07-08 00:00:00', '12:22:35', 'DN', '2', '2017-07-08 00:22:35', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('219', '3080717028', '28', '2017-07-08 00:00:00', '12:22:57', 'DN', '3', '2017-07-08 00:22:57', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('220', '3080717029', '29', '2017-07-08 00:00:00', '12:23:11', 'DN', '3', '2017-07-08 00:23:11', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('221', '3080717030', '30', '2017-07-08 00:00:00', '12:23:17', 'DN', '3', '2017-07-08 00:23:17', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('222', '3080717031', '31', '2017-07-08 00:00:00', '12:24:10', 'DN', '3', '2017-07-08 00:24:10', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('223', '2080717170', '170', '2017-07-08 00:00:00', '12:24:38', 'DN', '2', '2017-07-08 00:24:38', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('224', '2080717171', '171', '2017-07-08 00:00:00', '12:24:45', 'DN', '2', '2017-07-08 00:24:45', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('225', '2080717172', '172', '2017-07-08 00:00:00', '12:25:00', 'DN', '2', '2017-07-08 00:25:00', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('226', '2080717173', '173', '2017-07-08 00:00:00', '12:25:14', 'DN', '2', '2017-07-08 00:25:14', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('227', '2080717174', '174', '2017-07-08 00:00:00', '12:25:30', 'DN', '2', '2017-07-08 00:25:30', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('228', '2080717175', '175', '2017-07-08 00:00:00', '12:26:25', 'DN', '2', '2017-07-08 00:26:25', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('229', '2080717176', '176', '2017-07-08 00:00:00', '12:26:56', 'DN', '2', '2017-07-08 00:26:56', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('230', '2080717177', '177', '2017-07-08 00:00:00', '12:28:22', 'DN', '2', '2017-07-08 00:28:22', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('231', '2080717178', '178', '2017-07-08 00:00:00', '12:28:55', 'DN', '2', '2017-07-08 00:28:55', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('232', '2080717179', '179', '2017-07-08 00:00:00', '12:29:55', 'DN', '2', '2017-07-08 00:29:55', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('233', '2080717180', '180', '2017-07-08 00:00:00', '12:31:08', 'DN', '2', '2017-07-08 00:31:08', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('234', '3080717032', '32', '2017-07-08 00:00:00', '12:32:00', 'PR', '3', '2017-07-08 00:32:00', '2017-07-10 05:37:24', '0', '1');
INSERT INTO `sm_pendaftarans` VALUES ('235', '2080717181', '181', '2017-07-08 00:00:00', '12:32:43', 'DN', '2', '2017-07-08 00:32:43', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('236', '2080717182', '182', '2017-07-08 00:00:00', '12:33:13', 'DN', '2', '2017-07-08 00:33:13', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('237', '2080717183', '183', '2017-07-08 00:00:00', '12:34:33', 'DN', '2', '2017-07-08 00:34:33', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('238', '3080717033', '33', '2017-07-08 00:00:00', '12:37:37', 'DN', '3', '2017-07-08 00:37:37', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('239', '2080717184', '184', '2017-07-08 00:00:00', '12:38:36', 'DN', '2', '2017-07-08 00:38:36', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('240', '2080717185', '185', '2017-07-08 00:00:00', '12:38:45', 'DN', '2', '2017-07-08 00:38:45', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('241', '2080717186', '186', '2017-07-08 00:00:00', '12:38:52', 'DN', '2', '2017-07-08 00:38:52', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('242', '3080717034', '34', '2017-07-08 00:00:00', '12:39:50', 'DN', '3', '2017-07-08 00:39:50', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('243', '2080717187', '187', '2017-07-08 00:00:00', '12:45:53', 'DN', '2', '2017-07-08 00:45:53', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('244', '2080717188', '188', '2017-07-08 00:00:00', '12:48:44', 'DN', '2', '2017-07-08 00:48:44', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('245', '5080717016', '16', '2017-07-08 00:00:00', '12:51:44', 'DN', '5', '2017-07-08 00:51:44', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('246', '2080717189', '189', '2017-07-08 00:00:00', '12:51:56', 'DN', '2', '2017-07-08 00:51:56', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('247', '2080717190', '190', '2017-07-08 00:00:00', '12:52:26', 'DN', '2', '2017-07-08 00:52:26', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('248', '3080717035', '35', '2017-07-08 00:00:00', '12:52:41', 'DN', '3', '2017-07-08 00:52:41', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('249', '5080717017', '17', '2017-07-08 00:00:00', '12:52:57', 'DN', '5', '2017-07-08 00:52:57', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('250', '2080717191', '191', '2017-07-08 00:00:00', '12:54:48', 'DN', '2', '2017-07-08 00:54:48', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('251', '2080717192', '192', '2017-07-08 00:00:00', '12:54:55', 'DN', '2', '2017-07-08 00:54:55', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('252', '2080717193', '193', '2017-07-08 00:00:00', '12:56:45', 'DN', '2', '2017-07-08 00:56:45', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('253', '2080717194', '194', '2017-07-08 00:00:00', '01:00:08', 'DN', '2', '2017-07-08 01:00:08', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('254', '2080717195', '195', '2017-07-08 00:00:00', '01:00:53', 'DN', '2', '2017-07-08 01:00:53', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('255', '3080717036', '36', '2017-07-08 00:00:00', '01:02:54', 'DN', '3', '2017-07-08 01:02:54', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('256', '2080717196', '196', '2017-07-08 00:00:00', '01:03:29', 'DN', '2', '2017-07-08 01:03:29', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('257', '2080717197', '197', '2017-07-08 00:00:00', '01:04:25', 'DN', '2', '2017-07-08 01:04:25', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('258', '5080717018', '18', '2017-07-08 00:00:00', '01:04:43', 'DN', '5', '2017-07-08 01:04:43', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('259', '2080717198', '198', '2017-07-08 00:00:00', '01:05:25', 'DN', '2', '2017-07-08 01:05:25', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('260', '3080717037', '37', '2017-07-08 00:00:00', '01:05:33', 'DN', '3', '2017-07-08 01:05:33', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('261', '2080717199', '199', '2017-07-08 00:00:00', '01:05:43', 'DN', '2', '2017-07-08 01:05:43', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('262', '2080717200', '200', '2017-07-08 00:00:00', '01:05:51', 'DN', '2', '2017-07-08 01:05:51', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('263', '2080717201', '201', '2017-07-08 00:00:00', '01:07:22', 'DN', '2', '2017-07-08 01:07:22', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('264', '3080717038', '38', '2017-07-08 00:00:00', '01:07:30', 'DN', '3', '2017-07-08 01:07:30', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('265', '2080717202', '202', '2017-07-08 00:00:00', '01:07:45', 'DN', '2', '2017-07-08 01:07:45', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('266', '2080717203', '203', '2017-07-08 00:00:00', '01:09:37', 'DN', '2', '2017-07-08 01:09:37', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('267', '2080717204', '204', '2017-07-08 00:00:00', '01:09:43', 'DN', '2', '2017-07-08 01:09:43', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('268', '2080717205', '205', '2017-07-08 00:00:00', '01:10:58', 'DN', '2', '2017-07-08 01:10:58', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('269', '2080717206', '206', '2017-07-08 00:00:00', '01:13:13', 'DN', '2', '2017-07-08 01:13:13', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('270', '5080717019', '19', '2017-07-08 00:00:00', '01:13:39', 'DN', '5', '2017-07-08 01:13:39', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('271', '3080717039', '39', '2017-07-08 00:00:00', '01:15:20', 'DN', '3', '2017-07-08 01:15:20', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('272', '5080717020', '20', '2017-07-08 00:00:00', '01:15:43', 'DN', '5', '2017-07-08 01:15:43', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('273', '2080717207', '207', '2017-07-08 00:00:00', '01:17:01', 'DN', '2', '2017-07-08 01:17:01', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('274', '2080717208', '208', '2017-07-08 00:00:00', '01:17:22', 'DN', '2', '2017-07-08 01:17:22', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('275', '2080717209', '209', '2017-07-08 00:00:00', '01:17:47', 'DN', '2', '2017-07-08 01:17:47', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('276', '2080717210', '210', '2017-07-08 00:00:00', '01:19:41', 'DN', '2', '2017-07-08 01:19:41', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('277', '5080717021', '21', '2017-07-08 00:00:00', '01:20:00', 'DN', '5', '2017-07-08 01:20:00', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('278', '2080717211', '211', '2017-07-08 00:00:00', '01:20:29', 'DN', '2', '2017-07-08 01:20:29', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('279', '5080717022', '22', '2017-07-08 00:00:00', '01:20:59', 'DN', '5', '2017-07-08 01:20:59', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('280', '2080717212', '212', '2017-07-08 00:00:00', '01:21:29', 'DN', '2', '2017-07-08 01:21:29', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('281', '2080717213', '213', '2017-07-08 00:00:00', '01:21:44', 'DN', '2', '2017-07-08 01:21:44', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('282', '2080717214', '214', '2017-07-08 00:00:00', '01:28:42', 'DN', '2', '2017-07-08 01:28:42', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('283', '5080717023', '23', '2017-07-08 00:00:00', '01:28:53', 'DN', '5', '2017-07-08 01:28:53', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('284', '2080717215', '215', '2017-07-08 00:00:00', '01:29:21', 'DN', '2', '2017-07-08 01:29:21', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('285', '2080717216', '216', '2017-07-08 00:00:00', '01:29:57', 'DN', '2', '2017-07-08 01:29:57', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('286', '5080717024', '24', '2017-07-08 00:00:00', '01:30:05', 'DN', '5', '2017-07-08 01:30:05', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('287', '3080717040', '40', '2017-07-08 00:00:00', '01:30:12', 'DN', '3', '2017-07-08 01:30:12', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('288', '3080717041', '41', '2017-07-08 00:00:00', '01:31:01', 'DN', '3', '2017-07-08 01:31:01', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('289', '2080717217', '217', '2017-07-08 00:00:00', '01:33:14', 'DN', '2', '2017-07-08 01:33:14', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('290', '3080717042', '42', '2017-07-08 00:00:00', '01:35:17', 'DN', '3', '2017-07-08 01:35:17', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('291', '5080717025', '25', '2017-07-08 00:00:00', '01:35:41', 'DN', '5', '2017-07-08 01:35:41', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('292', '2080717218', '218', '2017-07-08 00:00:00', '01:36:29', 'DN', '2', '2017-07-08 01:36:29', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('293', '2080717219', '219', '2017-07-08 00:00:00', '01:38:42', 'DN', '2', '2017-07-08 01:38:42', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('294', '3080717043', '43', '2017-07-08 00:00:00', '01:39:39', 'DN', '3', '2017-07-08 01:39:39', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('295', '5080717026', '26', '2017-07-08 00:00:00', '01:45:23', 'DN', '5', '2017-07-08 01:45:23', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('296', '3080717044', '44', '2017-07-08 00:00:00', '01:45:54', 'DN', '3', '2017-07-08 01:45:54', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('297', '5080717027', '27', '2017-07-08 00:00:00', '01:46:14', 'DN', '5', '2017-07-08 01:46:14', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('298', '5080717028', '28', '2017-07-08 00:00:00', '01:49:38', 'DN', '5', '2017-07-08 01:49:38', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('299', '5080717029', '29', '2017-07-08 00:00:00', '01:51:16', 'DN', '5', '2017-07-08 01:51:16', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('300', '2080717220', '220', '2017-07-08 00:00:00', '01:52:03', 'DN', '2', '2017-07-08 01:52:03', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('301', '2080717221', '221', '2017-07-08 00:00:00', '01:53:20', 'DN', '2', '2017-07-08 01:53:20', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('302', '5080717030', '30', '2017-07-08 00:00:00', '01:57:29', 'DN', '5', '2017-07-08 01:57:29', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('303', '5080717031', '31', '2017-07-08 00:00:00', '01:58:32', 'DN', '5', '2017-07-08 01:58:32', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('304', '3080717045', '45', '2017-07-08 00:00:00', '02:00:59', 'DN', '3', '2017-07-08 02:00:59', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('305', '3080717046', '46', '2017-07-08 00:00:00', '02:01:08', 'DN', '3', '2017-07-08 02:01:08', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('306', '3080717047', '47', '2017-07-08 00:00:00', '02:02:19', 'DN', '3', '2017-07-08 02:02:19', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('307', '2080717222', '222', '2017-07-08 00:00:00', '02:03:16', 'DN', '2', '2017-07-08 02:03:16', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('308', '3080717048', '48', '2017-07-08 00:00:00', '02:03:34', 'PR', '3', '2017-07-08 02:03:34', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('309', '5080717032', '32', '2017-07-08 00:00:00', '02:06:31', 'DN', '5', '2017-07-08 02:06:31', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('310', '3080717049', '49', '2017-07-08 00:00:00', '02:06:42', 'DN', '3', '2017-07-08 02:06:42', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('311', '4080717006', '6', '2017-07-08 00:00:00', '02:07:32', 'DN', '4', '2017-07-08 02:07:32', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('312', '2080717223', '223', '2017-07-08 00:00:00', '02:07:42', 'DN', '2', '2017-07-08 02:07:42', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('313', '2080717224', '224', '2017-07-08 00:00:00', '02:11:24', 'DN', '2', '2017-07-08 02:11:24', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('314', '4080717007', '7', '2017-07-08 00:00:00', '02:11:38', 'DN', '4', '2017-07-08 02:11:38', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('315', '2080717225', '225', '2017-07-08 00:00:00', '02:16:45', 'DN', '2', '2017-07-08 02:16:45', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('316', '2080717226', '226', '2017-07-08 00:00:00', '02:17:12', 'DN', '2', '2017-07-08 02:17:12', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('317', '5080717033', '33', '2017-07-08 00:00:00', '02:22:48', 'DN', '5', '2017-07-08 02:22:48', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('318', '3080717050', '50', '2017-07-08 00:00:00', '02:22:53', 'PR', '3', '2017-07-08 02:22:53', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('319', '2080717227', '227', '2017-07-08 00:00:00', '02:23:05', 'DN', '2', '2017-07-08 02:23:05', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('320', '4080717008', '8', '2017-07-08 00:00:00', '02:23:29', 'DN', '4', '2017-07-08 02:23:29', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('321', '2080717228', '228', '2017-07-08 00:00:00', '02:24:41', 'DN', '2', '2017-07-08 02:24:41', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('322', '5080717034', '34', '2017-07-08 00:00:00', '02:26:52', 'DN', '5', '2017-07-08 02:26:52', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('323', '5080717035', '35', '2017-07-08 00:00:00', '02:27:03', 'DN', '5', '2017-07-08 02:27:03', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('324', '2080717229', '229', '2017-07-08 00:00:00', '02:27:16', 'DN', '2', '2017-07-08 02:27:16', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('325', '5080717036', '36', '2017-07-08 00:00:00', '02:27:26', 'DN', '5', '2017-07-08 02:27:26', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('326', '2080717230', '230', '2017-07-08 00:00:00', '02:28:45', 'DN', '2', '2017-07-08 02:28:45', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('327', '2080717231', '231', '2017-07-08 00:00:00', '02:28:58', 'DN', '2', '2017-07-08 02:28:58', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('328', '5080717037', '37', '2017-07-08 00:00:00', '02:29:47', 'DN', '5', '2017-07-08 02:29:47', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('329', '2080717232', '232', '2017-07-08 00:00:00', '02:33:42', 'PR', '2', '2017-07-08 02:33:42', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('330', '2080717233', '233', '2017-07-08 00:00:00', '02:38:26', 'DN', '2', '2017-07-08 02:38:26', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('331', '2080717234', '234', '2017-07-08 00:00:00', '02:42:04', 'DN', '2', '2017-07-08 02:42:04', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('332', '2080717235', '235', '2017-07-08 00:00:00', '02:43:05', 'DN', '2', '2017-07-08 02:43:05', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('333', '5080717038', '38', '2017-07-08 00:00:00', '02:44:38', 'DN', '5', '2017-07-08 02:44:38', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('334', '5080717039', '39', '2017-07-08 00:00:00', '02:48:32', 'DN', '5', '2017-07-08 02:48:32', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('335', '2080717236', '236', '2017-07-08 00:00:00', '02:48:45', 'DN', '2', '2017-07-08 02:48:45', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('336', '2080717237', '237', '2017-07-08 00:00:00', '02:50:50', 'PR', '2', '2017-07-08 02:50:50', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('337', '2080717238', '238', '2017-07-08 00:00:00', '02:51:13', 'DN', '2', '2017-07-08 02:51:13', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('338', '2080717239', '239', '2017-07-08 00:00:00', '02:53:39', 'PR', '2', '2017-07-08 02:53:39', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('339', '2080717240', '240', '2017-07-08 00:00:00', '02:54:25', 'DN', '2', '2017-07-08 02:54:25', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('340', '2080717241', '241', '2017-07-08 00:00:00', '02:54:41', 'PR', '2', '2017-07-08 02:54:41', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('341', '3080717051', '51', '2017-07-08 00:00:00', '02:54:49', 'WT', '3', '2017-07-08 02:54:49', '2017-07-10 05:37:24', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('342', '5080717040', '40', '2017-07-08 00:00:00', '02:54:57', 'PR', '5', '2017-07-08 02:54:57', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('343', '4080717009', '9', '2017-07-08 00:00:00', '02:55:07', 'PR', '4', '2017-07-08 02:55:07', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('361', '2100717001', '1', '2017-07-10 00:00:00', '07:02:12', 'DN', '2', '2017-07-10 00:02:12', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('362', '2100717002', '2', '2017-07-10 00:00:00', '07:02:21', 'DN', '2', '2017-07-10 00:02:21', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('363', '2100717003', '3', '2017-07-10 00:00:00', '07:02:27', 'DN', '2', '2017-07-10 00:02:27', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('364', '3100717001', '1', '2017-07-10 00:00:00', '07:02:40', 'DN', '3', '2017-07-10 00:02:40', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('365', '3100717002', '2', '2017-07-10 00:00:00', '07:02:45', 'DN', '3', '2017-07-10 00:02:45', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('366', '3100717003', '3', '2017-07-10 00:00:00', '07:02:51', 'DN', '3', '2017-07-10 00:02:51', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('367', '4100717001', '1', '2017-07-10 00:00:00', '07:02:59', 'CL', '4', '2017-07-10 00:02:59', '2017-07-10 03:57:04', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('368', '4100717002', '2', '2017-07-10 00:00:00', '07:03:04', 'DN', '4', '2017-07-10 00:03:04', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('369', '4100717003', '3', '2017-07-10 00:00:00', '07:03:09', 'DN', '4', '2017-07-10 00:03:09', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('370', '5100717001', '1', '2017-07-10 00:00:00', '07:03:17', 'DN', '5', '2017-07-10 00:03:17', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('371', '5100717002', '2', '2017-07-10 00:00:00', '07:03:23', 'DN', '5', '2017-07-10 00:03:23', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('372', '5100717003', '3', '2017-07-10 00:00:00', '07:03:29', 'DN', '5', '2017-07-10 00:03:29', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('373', '2100717004', '4', '2017-07-10 00:00:00', '07:03:43', 'DN', '2', '2017-07-10 00:03:43', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('374', '2100717005', '5', '2017-07-10 00:00:00', '07:03:49', 'DN', '2', '2017-07-10 00:03:49', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('375', '2100717006', '6', '2017-07-10 00:00:00', '07:03:55', 'DN', '2', '2017-07-10 00:03:55', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('376', '2100717007', '7', '2017-07-10 00:00:00', '07:04:01', 'DN', '2', '2017-07-10 00:04:01', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('377', '2100717008', '8', '2017-07-10 00:00:00', '07:04:06', 'DN', '2', '2017-07-10 00:04:06', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('378', '2100717009', '9', '2017-07-10 00:00:00', '07:04:12', 'DN', '2', '2017-07-10 00:04:12', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('379', '2100717010', '10', '2017-07-10 00:00:00', '07:04:17', 'DN', '2', '2017-07-10 00:04:17', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('380', '2100717011', '11', '2017-07-10 00:00:00', '07:04:23', 'DN', '2', '2017-07-10 00:04:23', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('381', '2100717012', '12', '2017-07-10 00:00:00', '07:04:28', 'DN', '2', '2017-07-10 00:04:28', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('382', '2100717013', '13', '2017-07-10 00:00:00', '07:04:33', 'DN', '2', '2017-07-10 00:04:33', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('383', '2100717014', '14', '2017-07-10 00:00:00', '07:04:39', 'DN', '2', '2017-07-10 00:04:39', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('384', '2100717015', '15', '2017-07-10 00:00:00', '07:04:44', 'DN', '2', '2017-07-10 00:04:44', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('385', '2100717016', '16', '2017-07-10 00:00:00', '07:04:50', 'CL', '2', '2017-07-10 00:04:50', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('386', '2100717017', '17', '2017-07-10 00:00:00', '07:04:55', 'DN', '2', '2017-07-10 00:04:55', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('387', '2100717018', '18', '2017-07-10 00:00:00', '07:05:00', 'DN', '2', '2017-07-10 00:05:00', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('388', '2100717019', '19', '2017-07-10 00:00:00', '07:05:05', 'CL', '2', '2017-07-10 00:05:06', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('389', '2100717020', '20', '2017-07-10 00:00:00', '07:05:12', 'DN', '2', '2017-07-10 00:05:12', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('390', '2100717021', '21', '2017-07-10 00:00:00', '07:05:18', 'DN', '2', '2017-07-10 00:05:18', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('391', '2100717022', '22', '2017-07-10 00:00:00', '07:05:24', 'DN', '2', '2017-07-10 00:05:24', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('392', '2100717023', '23', '2017-07-10 00:00:00', '07:05:31', 'DN', '2', '2017-07-10 00:05:31', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('393', '2100717024', '24', '2017-07-10 00:00:00', '07:05:36', 'DN', '2', '2017-07-10 00:05:36', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('394', '2100717025', '25', '2017-07-10 00:00:00', '07:05:41', 'CL', '2', '2017-07-10 00:05:41', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('395', '2100717026', '26', '2017-07-10 00:00:00', '07:05:47', 'CL', '2', '2017-07-10 00:05:47', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('396', '2100717027', '27', '2017-07-10 00:00:00', '07:05:53', 'CL', '2', '2017-07-10 00:05:53', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('397', '2100717028', '28', '2017-07-10 00:00:00', '07:06:01', 'CL', '2', '2017-07-10 00:06:01', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('398', '2100717029', '29', '2017-07-10 00:00:00', '07:06:07', 'CL', '2', '2017-07-10 00:06:07', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('399', '2100717030', '30', '2017-07-10 00:00:00', '07:06:12', 'CL', '2', '2017-07-10 00:06:12', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('400', '2100717031', '31', '2017-07-10 00:00:00', '07:06:17', 'CL', '2', '2017-07-10 00:06:18', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('401', '2100717032', '32', '2017-07-10 00:00:00', '07:06:23', 'CL', '2', '2017-07-10 00:06:23', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('402', '2100717033', '33', '2017-07-10 00:00:00', '07:06:29', 'CL', '2', '2017-07-10 00:06:29', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('403', '2100717034', '34', '2017-07-10 00:00:00', '07:06:36', 'CL', '2', '2017-07-10 00:06:36', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('404', '2100717035', '35', '2017-07-10 00:00:00', '07:06:42', 'CL', '2', '2017-07-10 00:06:42', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('405', '2100717036', '36', '2017-07-10 00:00:00', '07:06:48', 'DN', '2', '2017-07-10 00:06:48', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('406', '2100717037', '37', '2017-07-10 00:00:00', '07:06:54', 'DN', '2', '2017-07-10 00:06:54', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('407', '2100717038', '38', '2017-07-10 00:00:00', '07:07:01', 'DN', '2', '2017-07-10 00:07:01', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('408', '2100717039', '39', '2017-07-10 00:00:00', '07:07:09', 'DN', '2', '2017-07-10 00:07:09', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('409', '2100717040', '40', '2017-07-10 00:00:00', '07:07:22', 'DN', '2', '2017-07-10 00:07:22', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('410', '2100717041', '41', '2017-07-10 00:00:00', '07:07:29', 'DN', '2', '2017-07-10 00:07:29', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('411', '2100717042', '42', '2017-07-10 00:00:00', '07:07:40', 'DN', '2', '2017-07-10 00:07:40', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('412', '2100717043', '43', '2017-07-10 00:00:00', '07:07:46', 'DN', '2', '2017-07-10 00:07:46', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('413', '2100717044', '44', '2017-07-10 00:00:00', '07:07:52', 'DN', '2', '2017-07-10 00:07:52', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('414', '2100717045', '45', '2017-07-10 00:00:00', '07:07:58', 'DN', '2', '2017-07-10 00:07:58', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('415', '2100717046', '46', '2017-07-10 00:00:00', '07:08:03', 'DN', '2', '2017-07-10 00:08:03', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('416', '2100717047', '47', '2017-07-10 00:00:00', '07:08:08', 'DN', '2', '2017-07-10 00:08:08', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('417', '2100717048', '48', '2017-07-10 00:00:00', '07:08:18', 'DN', '2', '2017-07-10 00:08:18', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('418', '2100717049', '49', '2017-07-10 00:00:00', '07:08:23', 'DN', '2', '2017-07-10 00:08:23', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('419', '2100717050', '50', '2017-07-10 00:00:00', '07:08:29', 'DN', '2', '2017-07-10 00:08:29', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('420', '3100717004', '4', '2017-07-10 00:00:00', '07:08:41', 'DN', '3', '2017-07-10 00:08:41', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('421', '3100717005', '5', '2017-07-10 00:00:00', '07:08:50', 'DN', '3', '2017-07-10 00:08:50', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('422', '3100717006', '6', '2017-07-10 00:00:00', '07:08:54', 'DN', '3', '2017-07-10 00:08:54', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('423', '3100717007', '7', '2017-07-10 00:00:00', '07:08:58', 'DN', '3', '2017-07-10 00:08:58', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('424', '3100717008', '8', '2017-07-10 00:00:00', '07:09:05', 'DN', '3', '2017-07-10 00:09:05', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('425', '3100717009', '9', '2017-07-10 00:00:00', '07:09:11', 'DN', '3', '2017-07-10 00:09:11', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('426', '3100717010', '10', '2017-07-10 00:00:00', '07:09:17', 'DN', '3', '2017-07-10 00:09:17', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('427', '3100717011', '11', '2017-07-10 00:00:00', '07:09:20', 'DN', '3', '2017-07-10 00:09:20', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('428', '3100717012', '12', '2017-07-10 00:00:00', '07:09:24', 'DN', '3', '2017-07-10 00:09:24', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('429', '3100717013', '13', '2017-07-10 00:00:00', '07:09:28', 'DN', '3', '2017-07-10 00:09:28', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('430', '3100717014', '14', '2017-07-10 00:00:00', '07:09:32', 'DN', '3', '2017-07-10 00:09:32', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('431', '3100717015', '15', '2017-07-10 00:00:00', '07:09:35', 'DN', '3', '2017-07-10 00:09:35', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('432', '3100717016', '16', '2017-07-10 00:00:00', '07:09:41', 'DN', '3', '2017-07-10 00:09:41', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('433', '3100717017', '17', '2017-07-10 00:00:00', '07:09:44', 'DN', '3', '2017-07-10 00:09:44', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('434', '3100717018', '18', '2017-07-10 00:00:00', '07:09:48', 'DN', '3', '2017-07-10 00:09:48', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('435', '3100717019', '19', '2017-07-10 00:00:00', '07:09:52', 'DN', '3', '2017-07-10 00:09:52', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('436', '3100717020', '20', '2017-07-10 00:00:00', '07:09:55', 'DN', '3', '2017-07-10 00:09:55', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('437', '3100717021', '21', '2017-07-10 00:00:00', '07:10:01', 'DN', '3', '2017-07-10 00:10:01', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('438', '3100717022', '22', '2017-07-10 00:00:00', '07:10:05', 'DN', '3', '2017-07-10 00:10:05', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('439', '3100717023', '23', '2017-07-10 00:00:00', '07:10:09', 'DN', '3', '2017-07-10 00:10:09', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('440', '3100717024', '24', '2017-07-10 00:00:00', '07:10:16', 'DN', '3', '2017-07-10 00:10:16', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('441', '3100717025', '25', '2017-07-10 00:00:00', '07:10:23', 'DN', '3', '2017-07-10 00:10:23', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('442', '3100717026', '26', '2017-07-10 00:00:00', '07:10:29', 'DN', '3', '2017-07-10 00:10:29', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('443', '3100717027', '27', '2017-07-10 00:00:00', '07:10:33', 'DN', '3', '2017-07-10 00:10:33', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('444', '3100717028', '28', '2017-07-10 00:00:00', '07:10:38', 'DN', '3', '2017-07-10 00:10:38', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('445', '3100717029', '29', '2017-07-10 00:00:00', '07:10:44', 'DN', '3', '2017-07-10 00:10:45', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('446', '3100717030', '30', '2017-07-10 00:00:00', '07:10:49', 'DN', '3', '2017-07-10 00:10:49', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('447', '3100717031', '31', '2017-07-10 00:00:00', '07:10:54', 'DN', '3', '2017-07-10 00:10:54', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('448', '3100717032', '32', '2017-07-10 00:00:00', '07:10:57', 'DN', '3', '2017-07-10 00:10:57', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('449', '3100717033', '33', '2017-07-10 00:00:00', '07:11:01', 'DN', '3', '2017-07-10 00:11:01', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('450', '3100717034', '34', '2017-07-10 00:00:00', '07:11:10', 'DN', '3', '2017-07-10 00:11:10', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('451', '3100717035', '35', '2017-07-10 00:00:00', '07:11:14', 'DN', '3', '2017-07-10 00:11:14', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('452', '3100717036', '36', '2017-07-10 00:00:00', '07:11:17', 'DN', '3', '2017-07-10 00:11:17', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('453', '3100717037', '37', '2017-07-10 00:00:00', '07:11:20', 'DN', '3', '2017-07-10 00:11:20', '2017-07-10 05:37:24', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('454', '3100717038', '38', '2017-07-10 00:00:00', '07:11:24', 'DN', '3', '2017-07-10 00:11:24', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('455', '3100717039', '39', '2017-07-10 00:00:00', '07:11:27', 'DN', '3', '2017-07-10 00:11:27', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('456', '3100717040', '40', '2017-07-10 00:00:00', '07:11:32', 'DN', '3', '2017-07-10 00:11:32', '2017-07-10 05:37:24', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('457', '3100717041', '41', '2017-07-10 00:00:00', '07:11:36', 'DN', '3', '2017-07-10 00:11:36', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('458', '3100717042', '42', '2017-07-10 00:00:00', '07:11:39', 'DN', '3', '2017-07-10 00:11:39', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('459', '3100717043', '43', '2017-07-10 00:00:00', '07:11:43', 'DN', '3', '2017-07-10 00:11:43', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('460', '3100717044', '44', '2017-07-10 00:00:00', '07:11:46', 'DN', '3', '2017-07-10 00:11:46', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('461', '3100717045', '45', '2017-07-10 00:00:00', '07:11:50', 'DN', '3', '2017-07-10 00:11:50', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('462', '3100717046', '46', '2017-07-10 00:00:00', '07:11:53', 'DN', '3', '2017-07-10 00:11:53', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('463', '3100717047', '47', '2017-07-10 00:00:00', '07:11:57', 'DN', '3', '2017-07-10 00:11:57', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('464', '3100717048', '48', '2017-07-10 00:00:00', '07:12:00', 'DN', '3', '2017-07-10 00:12:00', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('465', '3100717049', '49', '2017-07-10 00:00:00', '07:12:04', 'DN', '3', '2017-07-10 00:12:04', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('466', '3100717050', '50', '2017-07-10 00:00:00', '07:12:07', 'DN', '3', '2017-07-10 00:12:07', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('467', '3100717051', '51', '2017-07-10 00:00:00', '07:12:11', 'DN', '3', '2017-07-10 00:12:11', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('468', '3100717052', '52', '2017-07-10 00:00:00', '07:12:16', 'DN', '3', '2017-07-10 00:12:16', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('469', '3100717053', '53', '2017-07-10 00:00:00', '07:12:20', 'DN', '3', '2017-07-10 00:12:20', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('470', '3100717054', '54', '2017-07-10 00:00:00', '07:12:24', 'DN', '3', '2017-07-10 00:12:24', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('471', '3100717055', '55', '2017-07-10 00:00:00', '07:12:29', 'DN', '3', '2017-07-10 00:12:29', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('472', '3100717056', '56', '2017-07-10 00:00:00', '07:12:33', 'DN', '3', '2017-07-10 00:12:33', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('473', '3100717057', '57', '2017-07-10 00:00:00', '07:12:36', 'DN', '3', '2017-07-10 00:12:36', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('474', '3100717058', '58', '2017-07-10 00:00:00', '07:12:40', 'DN', '3', '2017-07-10 00:12:40', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('475', '3100717059', '59', '2017-07-10 00:00:00', '07:12:43', 'DN', '3', '2017-07-10 00:12:43', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('476', '3100717060', '60', '2017-07-10 00:00:00', '07:12:47', 'DN', '3', '2017-07-10 00:12:47', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('477', '3100717061', '61', '2017-07-10 00:00:00', '07:12:51', 'DN', '3', '2017-07-10 00:12:51', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('478', '3100717062', '62', '2017-07-10 00:00:00', '07:12:54', 'DN', '3', '2017-07-10 00:12:54', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('479', '3100717063', '63', '2017-07-10 00:00:00', '07:12:58', 'DN', '3', '2017-07-10 00:12:58', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('480', '3100717064', '64', '2017-07-10 00:00:00', '07:13:01', 'DN', '3', '2017-07-10 00:13:01', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('481', '3100717065', '65', '2017-07-10 00:00:00', '07:13:10', 'DN', '3', '2017-07-10 00:13:10', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('482', '3100717066', '66', '2017-07-10 00:00:00', '07:13:13', 'DN', '3', '2017-07-10 00:13:13', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('483', '3100717067', '67', '2017-07-10 00:00:00', '07:13:19', 'DN', '3', '2017-07-10 00:13:19', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('484', '3100717068', '68', '2017-07-10 00:00:00', '07:13:24', 'DN', '3', '2017-07-10 00:13:24', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('485', '2100717051', '51', '2017-07-10 00:00:00', '07:13:28', 'DN', '2', '2017-07-10 00:13:28', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('486', '2100717052', '52', '2017-07-10 00:00:00', '07:13:31', 'DN', '2', '2017-07-10 00:13:31', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('487', '2100717053', '53', '2017-07-10 00:00:00', '07:13:42', 'DN', '2', '2017-07-10 00:13:42', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('488', '2100717054', '54', '2017-07-10 00:00:00', '07:13:50', 'DN', '2', '2017-07-10 00:13:50', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('489', '2100717055', '55', '2017-07-10 00:00:00', '07:13:53', 'DN', '2', '2017-07-10 00:13:53', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('490', '2100717056', '56', '2017-07-10 00:00:00', '07:13:57', 'DN', '2', '2017-07-10 00:13:57', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('491', '2100717057', '57', '2017-07-10 00:00:00', '07:14:01', 'DN', '2', '2017-07-10 00:14:01', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('492', '2100717058', '58', '2017-07-10 00:00:00', '07:14:05', 'DN', '2', '2017-07-10 00:14:05', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('493', '2100717059', '59', '2017-07-10 00:00:00', '07:14:09', 'DN', '2', '2017-07-10 00:14:09', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('494', '2100717060', '60', '2017-07-10 00:00:00', '07:14:13', 'DN', '2', '2017-07-10 00:14:13', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('495', '2100717061', '61', '2017-07-10 00:00:00', '07:14:16', 'DN', '2', '2017-07-10 00:14:16', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('496', '2100717062', '62', '2017-07-10 00:00:00', '07:14:19', 'DN', '2', '2017-07-10 00:14:19', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('497', '2100717063', '63', '2017-07-10 00:00:00', '07:14:22', 'DN', '2', '2017-07-10 00:14:22', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('498', '2100717064', '64', '2017-07-10 00:00:00', '07:14:25', 'DN', '2', '2017-07-10 00:14:25', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('499', '2100717065', '65', '2017-07-10 00:00:00', '07:14:33', 'DN', '2', '2017-07-10 00:14:33', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('500', '2100717066', '66', '2017-07-10 00:00:00', '07:14:36', 'DN', '2', '2017-07-10 00:14:36', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('501', '2100717067', '67', '2017-07-10 00:00:00', '07:14:39', 'DN', '2', '2017-07-10 00:14:39', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('502', '2100717068', '68', '2017-07-10 00:00:00', '07:14:43', 'DN', '2', '2017-07-10 00:14:43', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('503', '2100717069', '69', '2017-07-10 00:00:00', '07:14:47', 'DN', '2', '2017-07-10 00:14:47', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('504', '2100717070', '70', '2017-07-10 00:00:00', '07:14:50', 'DN', '2', '2017-07-10 00:14:50', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('505', '2100717071', '71', '2017-07-10 00:00:00', '07:14:54', 'DN', '2', '2017-07-10 00:14:54', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('506', '2100717072', '72', '2017-07-10 00:00:00', '07:14:57', 'DN', '2', '2017-07-10 00:14:57', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('507', '2100717073', '73', '2017-07-10 00:00:00', '07:15:02', 'DN', '2', '2017-07-10 00:15:02', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('508', '2100717074', '74', '2017-07-10 00:00:00', '07:15:06', 'DN', '2', '2017-07-10 00:15:06', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('509', '2100717075', '75', '2017-07-10 00:00:00', '07:15:09', 'DN', '2', '2017-07-10 00:15:09', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('510', '2100717076', '76', '2017-07-10 00:00:00', '07:15:13', 'DN', '2', '2017-07-10 00:15:13', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('511', '2100717077', '77', '2017-07-10 00:00:00', '07:15:16', 'DN', '2', '2017-07-10 00:15:16', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('512', '2100717078', '78', '2017-07-10 00:00:00', '07:15:19', 'DN', '2', '2017-07-10 00:15:19', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('513', '2100717079', '79', '2017-07-10 00:00:00', '07:15:23', 'DN', '2', '2017-07-10 00:15:23', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('514', '2100717080', '80', '2017-07-10 00:00:00', '07:15:26', 'DN', '2', '2017-07-10 00:15:26', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('515', '2100717081', '81', '2017-07-10 00:00:00', '07:15:29', 'DN', '2', '2017-07-10 00:15:29', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('516', '2100717082', '82', '2017-07-10 00:00:00', '07:15:33', 'DN', '2', '2017-07-10 00:15:33', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('517', '2100717083', '83', '2017-07-10 00:00:00', '07:15:37', 'DN', '2', '2017-07-10 00:15:37', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('518', '2100717084', '84', '2017-07-10 00:00:00', '07:15:40', 'DN', '2', '2017-07-10 00:15:40', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('519', '2100717085', '85', '2017-07-10 00:00:00', '07:15:44', 'DN', '2', '2017-07-10 00:15:44', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('520', '2100717086', '86', '2017-07-10 00:00:00', '07:15:47', 'DN', '2', '2017-07-10 00:15:47', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('521', '2100717087', '87', '2017-07-10 00:00:00', '07:15:50', 'DN', '2', '2017-07-10 00:15:50', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('522', '2100717088', '88', '2017-07-10 00:00:00', '07:15:53', 'DN', '2', '2017-07-10 00:15:53', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('523', '2100717089', '89', '2017-07-10 00:00:00', '07:15:57', 'DN', '2', '2017-07-10 00:15:57', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('524', '2100717090', '90', '2017-07-10 00:00:00', '07:16:00', 'DN', '2', '2017-07-10 00:16:00', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('525', '2100717091', '91', '2017-07-10 00:00:00', '07:16:04', 'DN', '2', '2017-07-10 00:16:04', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('526', '2100717092', '92', '2017-07-10 00:00:00', '07:16:07', 'DN', '2', '2017-07-10 00:16:07', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('527', '2100717093', '93', '2017-07-10 00:00:00', '07:16:10', 'DN', '2', '2017-07-10 00:16:10', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('528', '2100717094', '94', '2017-07-10 00:00:00', '07:16:14', 'DN', '2', '2017-07-10 00:16:14', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('529', '2100717095', '95', '2017-07-10 00:00:00', '07:16:17', 'DN', '2', '2017-07-10 00:16:17', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('530', '2100717096', '96', '2017-07-10 00:00:00', '07:16:21', 'DN', '2', '2017-07-10 00:16:21', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('531', '2100717097', '97', '2017-07-10 00:00:00', '07:16:24', 'DN', '2', '2017-07-10 00:16:24', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('532', '2100717098', '98', '2017-07-10 00:00:00', '07:16:28', 'DN', '2', '2017-07-10 00:16:28', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('533', '2100717099', '99', '2017-07-10 00:00:00', '07:16:31', 'DN', '2', '2017-07-10 00:16:31', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('534', '2100717100', '100', '2017-07-10 00:00:00', '07:16:34', 'DN', '2', '2017-07-10 00:16:34', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('535', '2100717101', '101', '2017-07-10 00:00:00', '07:16:38', 'DN', '2', '2017-07-10 00:16:38', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('536', '2100717102', '102', '2017-07-10 00:00:00', '07:16:41', 'DN', '2', '2017-07-10 00:16:41', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('537', '2100717103', '103', '2017-07-10 00:00:00', '07:16:45', 'DN', '2', '2017-07-10 00:16:45', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('538', '2100717104', '104', '2017-07-10 00:00:00', '07:16:49', 'DN', '2', '2017-07-10 00:16:49', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('539', '2100717105', '105', '2017-07-10 00:00:00', '07:16:52', 'DN', '2', '2017-07-10 00:16:52', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('540', '2100717106', '106', '2017-07-10 00:00:00', '07:16:56', 'DN', '2', '2017-07-10 00:16:56', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('541', '2100717107', '107', '2017-07-10 00:00:00', '07:16:59', 'DN', '2', '2017-07-10 00:16:59', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('542', '2100717108', '108', '2017-07-10 00:00:00', '07:17:03', 'DN', '2', '2017-07-10 00:17:03', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('543', '2100717109', '109', '2017-07-10 00:00:00', '07:17:07', 'DN', '2', '2017-07-10 00:17:07', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('544', '2100717110', '110', '2017-07-10 00:00:00', '07:17:10', 'DN', '2', '2017-07-10 00:17:10', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('545', '2100717111', '111', '2017-07-10 00:00:00', '07:17:14', 'DN', '2', '2017-07-10 00:17:14', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('546', '2100717112', '112', '2017-07-10 00:00:00', '07:17:18', 'DN', '2', '2017-07-10 00:17:18', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('547', '2100717113', '113', '2017-07-10 00:00:00', '07:17:21', 'DN', '2', '2017-07-10 00:17:21', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('548', '2100717114', '114', '2017-07-10 00:00:00', '07:17:25', 'DN', '2', '2017-07-10 00:17:25', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('549', '2100717115', '115', '2017-07-10 00:00:00', '07:17:28', 'DN', '2', '2017-07-10 00:17:28', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('550', '2100717116', '116', '2017-07-10 00:00:00', '07:17:32', 'DN', '2', '2017-07-10 00:17:32', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('551', '2100717117', '117', '2017-07-10 00:00:00', '07:17:36', 'DN', '2', '2017-07-10 00:17:36', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('552', '2100717118', '118', '2017-07-10 00:00:00', '07:17:39', 'DN', '2', '2017-07-10 00:17:39', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('553', '2100717119', '119', '2017-07-10 00:00:00', '07:17:43', 'DN', '2', '2017-07-10 00:17:43', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('554', '2100717120', '120', '2017-07-10 00:00:00', '07:17:46', 'DN', '2', '2017-07-10 00:17:46', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('555', '2100717121', '121', '2017-07-10 00:00:00', '07:17:49', 'DN', '2', '2017-07-10 00:17:49', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('556', '2100717122', '122', '2017-07-10 00:00:00', '07:17:53', 'DN', '2', '2017-07-10 00:17:53', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('557', '2100717123', '123', '2017-07-10 00:00:00', '07:18:00', 'DN', '2', '2017-07-10 00:18:00', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('558', '2100717124', '124', '2017-07-10 00:00:00', '07:18:06', 'DN', '2', '2017-07-10 00:18:06', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('559', '2100717125', '125', '2017-07-10 00:00:00', '07:18:09', 'DN', '2', '2017-07-10 00:18:09', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('560', '2100717126', '126', '2017-07-10 00:00:00', '07:18:26', 'DN', '2', '2017-07-10 00:18:26', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('561', '2100717127', '127', '2017-07-10 00:00:00', '07:18:30', 'DN', '2', '2017-07-10 00:18:30', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('562', '2100717128', '128', '2017-07-10 00:00:00', '07:18:33', 'DN', '2', '2017-07-10 00:18:33', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('563', '2100717129', '129', '2017-07-10 00:00:00', '07:18:37', 'DN', '2', '2017-07-10 00:18:37', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('564', '5100717004', '4', '2017-07-10 00:00:00', '07:18:41', 'CL', '5', '2017-07-10 00:18:41', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('565', '5100717005', '5', '2017-07-10 00:00:00', '07:19:03', 'CL', '5', '2017-07-10 00:19:03', '2017-07-10 04:21:18', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('566', '5100717006', '6', '2017-07-10 00:00:00', '07:19:07', 'DN', '5', '2017-07-10 00:19:07', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('567', '5100717007', '7', '2017-07-10 00:00:00', '07:19:10', 'DN', '5', '2017-07-10 00:19:10', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('568', '5100717008', '8', '2017-07-10 00:00:00', '07:19:14', 'DN', '5', '2017-07-10 00:19:14', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('569', '5100717009', '9', '2017-07-10 00:00:00', '07:19:17', 'DN', '5', '2017-07-10 00:19:17', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('570', '5100717010', '10', '2017-07-10 00:00:00', '07:19:20', 'DN', '5', '2017-07-10 00:19:20', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('571', '5100717011', '11', '2017-07-10 00:00:00', '07:19:24', 'DN', '5', '2017-07-10 00:19:24', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('572', '5100717012', '12', '2017-07-10 00:00:00', '07:19:28', 'DN', '5', '2017-07-10 00:19:28', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('573', '5100717013', '13', '2017-07-10 00:00:00', '07:19:31', 'DN', '5', '2017-07-10 00:19:31', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('574', '3100717069', '69', '2017-07-10 00:00:00', '07:19:41', 'DN', '3', '2017-07-10 00:19:41', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('575', '2100717130', '130', '2017-07-10 00:00:00', '07:19:51', 'DN', '2', '2017-07-10 00:19:51', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('576', '2100717131', '131', '2017-07-10 00:00:00', '07:20:00', 'DN', '2', '2017-07-10 00:20:00', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('577', '2100717132', '132', '2017-07-10 00:00:00', '07:20:07', 'DN', '2', '2017-07-10 00:20:07', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('578', '2100717133', '133', '2017-07-10 00:00:00', '07:20:11', 'DN', '2', '2017-07-10 00:20:11', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('579', '3100717070', '70', '2017-07-10 00:00:00', '07:20:18', 'DN', '3', '2017-07-10 00:20:18', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('580', '3100717071', '71', '2017-07-10 00:00:00', '07:20:23', 'DN', '3', '2017-07-10 00:20:23', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('581', '3100717072', '72', '2017-07-10 00:00:00', '07:20:27', 'DN', '3', '2017-07-10 00:20:27', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('582', '3100717073', '73', '2017-07-10 00:00:00', '07:20:30', 'DN', '3', '2017-07-10 00:20:30', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('583', '3100717074', '74', '2017-07-10 00:00:00', '07:20:35', 'DN', '3', '2017-07-10 00:20:35', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('584', '3100717075', '75', '2017-07-10 00:00:00', '07:20:41', 'DN', '3', '2017-07-10 00:20:41', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('585', '3100717076', '76', '2017-07-10 00:00:00', '07:20:46', 'DN', '3', '2017-07-10 00:20:46', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('586', '2100717134', '134', '2017-07-10 00:00:00', '07:20:55', 'DN', '2', '2017-07-10 00:20:55', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('587', '2100717135', '135', '2017-07-10 00:00:00', '07:20:58', 'DN', '2', '2017-07-10 00:20:58', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('588', '2100717136', '136', '2017-07-10 00:00:00', '07:21:01', 'DN', '2', '2017-07-10 00:21:01', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('589', '2100717137', '137', '2017-07-10 00:00:00', '07:21:06', 'DN', '2', '2017-07-10 00:21:06', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('590', '2100717138', '138', '2017-07-10 00:00:00', '07:21:09', 'DN', '2', '2017-07-10 00:21:10', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('591', '2100717139', '139', '2017-07-10 00:00:00', '07:21:13', 'DN', '2', '2017-07-10 00:21:13', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('592', '2100717140', '140', '2017-07-10 00:00:00', '07:21:17', 'DN', '2', '2017-07-10 00:21:17', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('593', '2100717141', '141', '2017-07-10 00:00:00', '07:21:20', 'DN', '2', '2017-07-10 00:21:21', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('594', '2100717142', '142', '2017-07-10 00:00:00', '07:21:24', 'DN', '2', '2017-07-10 00:21:24', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('595', '2100717143', '143', '2017-07-10 00:00:00', '07:21:28', 'DN', '2', '2017-07-10 00:21:28', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('596', '2100717144', '144', '2017-07-10 00:00:00', '07:21:33', 'DN', '2', '2017-07-10 00:21:33', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('597', '2100717145', '145', '2017-07-10 00:00:00', '07:21:36', 'DN', '2', '2017-07-10 00:21:36', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('598', '2100717146', '146', '2017-07-10 00:00:00', '07:21:39', 'DN', '2', '2017-07-10 00:21:39', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('599', '2100717147', '147', '2017-07-10 00:00:00', '07:21:45', 'DN', '2', '2017-07-10 00:21:45', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('600', '2100717148', '148', '2017-07-10 00:00:00', '07:21:49', 'DN', '2', '2017-07-10 00:21:49', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('601', '2100717149', '149', '2017-07-10 00:00:00', '07:21:53', 'DN', '2', '2017-07-10 00:21:53', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('602', '2100717150', '150', '2017-07-10 00:00:00', '07:21:56', 'DN', '2', '2017-07-10 00:21:56', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('603', '2100717151', '151', '2017-07-10 00:00:00', '07:21:59', 'DN', '2', '2017-07-10 00:21:59', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('604', '2100717152', '152', '2017-07-10 00:00:00', '07:22:03', 'DN', '2', '2017-07-10 00:22:03', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('605', '2100717153', '153', '2017-07-10 00:00:00', '07:22:10', 'DN', '2', '2017-07-10 00:22:10', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('606', '2100717154', '154', '2017-07-10 00:00:00', '07:22:15', 'DN', '2', '2017-07-10 00:22:15', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('607', '2100717155', '155', '2017-07-10 00:00:00', '07:22:22', 'DN', '2', '2017-07-10 00:22:22', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('608', '2100717156', '156', '2017-07-10 00:00:00', '07:22:27', 'DN', '2', '2017-07-10 00:22:27', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('609', '2100717157', '157', '2017-07-10 00:00:00', '07:22:30', 'DN', '2', '2017-07-10 00:22:30', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('610', '2100717158', '158', '2017-07-10 00:00:00', '07:22:34', 'DN', '2', '2017-07-10 00:22:34', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('611', '2100717159', '159', '2017-07-10 00:00:00', '07:22:37', 'DN', '2', '2017-07-10 00:22:37', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('612', '2100717160', '160', '2017-07-10 00:00:00', '07:22:41', 'DN', '2', '2017-07-10 00:22:41', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('613', '2100717161', '161', '2017-07-10 00:00:00', '07:22:45', 'DN', '2', '2017-07-10 00:22:45', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('614', '2100717162', '162', '2017-07-10 00:00:00', '07:22:48', 'DN', '2', '2017-07-10 00:22:48', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('615', '2100717163', '163', '2017-07-10 00:00:00', '07:22:51', 'DN', '2', '2017-07-10 00:22:51', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('616', '2100717164', '164', '2017-07-10 00:00:00', '07:22:55', 'DN', '2', '2017-07-10 00:22:55', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('617', '2100717165', '165', '2017-07-10 00:00:00', '07:22:59', 'DN', '2', '2017-07-10 00:22:59', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('618', '2100717166', '166', '2017-07-10 00:00:00', '07:23:03', 'DN', '2', '2017-07-10 00:23:03', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('619', '2100717167', '167', '2017-07-10 00:00:00', '07:23:13', 'DN', '2', '2017-07-10 00:23:13', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('620', '2100717168', '168', '2017-07-10 00:00:00', '07:23:20', 'DN', '2', '2017-07-10 00:23:20', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('621', '2100717169', '169', '2017-07-10 00:00:00', '07:23:29', 'DN', '2', '2017-07-10 00:23:29', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('622', '2100717170', '170', '2017-07-10 00:00:00', '07:23:36', 'DN', '2', '2017-07-10 00:23:36', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('623', '2100717171', '171', '2017-07-10 00:00:00', '07:23:43', 'DN', '2', '2017-07-10 00:23:43', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('624', '2100717172', '172', '2017-07-10 00:00:00', '07:23:50', 'DN', '2', '2017-07-10 00:23:50', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('625', '2100717173', '173', '2017-07-10 00:00:00', '07:23:56', 'DN', '2', '2017-07-10 00:23:56', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('626', '2100717174', '174', '2017-07-10 00:00:00', '07:24:02', 'DN', '2', '2017-07-10 00:24:02', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('627', '2100717175', '175', '2017-07-10 00:00:00', '07:24:06', 'DN', '2', '2017-07-10 00:24:06', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('628', '2100717176', '176', '2017-07-10 00:00:00', '07:24:10', 'DN', '2', '2017-07-10 00:24:10', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('629', '2100717177', '177', '2017-07-10 00:00:00', '07:24:14', 'DN', '2', '2017-07-10 00:24:14', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('630', '2100717178', '178', '2017-07-10 00:00:00', '07:24:18', 'DN', '2', '2017-07-10 00:24:18', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('631', '3100717077', '77', '2017-07-10 00:00:00', '07:24:31', 'DN', '3', '2017-07-10 00:24:31', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('632', '3100717078', '78', '2017-07-10 00:00:00', '07:24:35', 'DN', '3', '2017-07-10 00:24:35', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('633', '5100717014', '14', '2017-07-10 00:00:00', '07:24:47', 'DN', '5', '2017-07-10 00:24:47', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('634', '5100717015', '15', '2017-07-10 00:00:00', '07:24:51', 'DN', '5', '2017-07-10 00:24:51', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('635', '5100717016', '16', '2017-07-10 00:00:00', '07:24:55', 'DN', '5', '2017-07-10 00:24:55', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('636', '4100717004', '4', '2017-07-10 00:00:00', '07:25:02', 'DN', '4', '2017-07-10 00:25:02', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('637', '5100717017', '17', '2017-07-10 00:00:00', '07:25:15', 'DN', '5', '2017-07-10 00:25:15', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('638', '5100717018', '18', '2017-07-10 00:00:00', '07:25:21', 'DN', '5', '2017-07-10 00:25:21', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('639', '5100717019', '19', '2017-07-10 00:00:00', '07:25:25', 'DN', '5', '2017-07-10 00:25:25', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('640', '5100717020', '20', '2017-07-10 00:00:00', '07:25:29', 'DN', '5', '2017-07-10 00:25:29', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('641', '5100717021', '21', '2017-07-10 00:00:00', '07:25:32', 'DN', '5', '2017-07-10 00:25:32', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('642', '5100717022', '22', '2017-07-10 00:00:00', '07:25:36', 'DN', '5', '2017-07-10 00:25:36', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('643', '3100717079', '79', '2017-07-10 00:00:00', '07:27:34', 'DN', '3', '2017-07-10 00:27:34', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('644', '5100717023', '23', '2017-07-10 00:00:00', '07:28:20', 'DN', '5', '2017-07-10 00:28:20', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('645', '5100717024', '24', '2017-07-10 00:00:00', '07:28:50', 'DN', '5', '2017-07-10 00:28:50', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('646', '2100717179', '179', '2017-07-10 00:00:00', '07:29:17', 'DN', '2', '2017-07-10 00:29:17', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('647', '4100717005', '5', '2017-07-10 00:00:00', '07:29:50', 'DN', '4', '2017-07-10 00:29:50', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('648', '2100717180', '180', '2017-07-10 00:00:00', '07:30:11', 'DN', '2', '2017-07-10 00:30:11', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('649', '2100717181', '181', '2017-07-10 00:00:00', '07:30:20', 'DN', '2', '2017-07-10 00:30:20', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('650', '2100717182', '182', '2017-07-10 00:00:00', '07:30:36', 'DN', '2', '2017-07-10 00:30:36', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('651', '5100717025', '25', '2017-07-10 00:00:00', '07:31:28', 'DN', '5', '2017-07-10 00:31:28', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('652', '3100717080', '80', '2017-07-10 00:00:00', '07:31:53', 'DN', '3', '2017-07-10 00:31:53', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('653', '5100717026', '26', '2017-07-10 00:00:00', '07:32:12', 'DN', '5', '2017-07-10 00:32:12', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('654', '2100717183', '183', '2017-07-10 00:00:00', '07:32:25', 'DN', '2', '2017-07-10 00:32:25', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('655', '2100717184', '184', '2017-07-10 00:00:00', '07:32:34', 'DN', '2', '2017-07-10 00:32:34', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('656', '5100717027', '27', '2017-07-10 00:00:00', '07:32:46', 'DN', '5', '2017-07-10 00:32:46', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('657', '3100717081', '81', '2017-07-10 00:00:00', '07:32:57', 'DN', '3', '2017-07-10 00:32:57', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('658', '2100717185', '185', '2017-07-10 00:00:00', '07:33:07', 'DN', '2', '2017-07-10 00:33:07', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('659', '2100717186', '186', '2017-07-10 00:00:00', '07:33:16', 'DN', '2', '2017-07-10 00:33:16', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('660', '5100717028', '28', '2017-07-10 00:00:00', '07:33:24', 'DN', '5', '2017-07-10 00:33:24', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('661', '2100717187', '187', '2017-07-10 00:00:00', '07:34:01', 'DN', '2', '2017-07-10 00:34:01', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('662', '2100717188', '188', '2017-07-10 00:00:00', '07:34:14', 'DN', '2', '2017-07-10 00:34:14', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('663', '2100717189', '189', '2017-07-10 00:00:00', '07:34:23', 'DN', '2', '2017-07-10 00:34:23', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('664', '5100717029', '29', '2017-07-10 00:00:00', '07:34:42', 'DN', '5', '2017-07-10 00:34:42', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('665', '3100717082', '82', '2017-07-10 00:00:00', '07:34:55', 'DN', '3', '2017-07-10 00:34:55', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('666', '2100717190', '190', '2017-07-10 00:00:00', '07:35:04', 'DN', '2', '2017-07-10 00:35:04', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('667', '2100717191', '191', '2017-07-10 00:00:00', '07:35:11', 'DN', '2', '2017-07-10 00:35:11', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('668', '5100717030', '30', '2017-07-10 00:00:00', '07:35:22', 'DN', '5', '2017-07-10 00:35:22', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('669', '3100717083', '83', '2017-07-10 00:00:00', '07:35:31', 'DN', '3', '2017-07-10 00:35:31', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('670', '2100717192', '192', '2017-07-10 00:00:00', '07:35:39', 'DN', '2', '2017-07-10 00:35:39', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('671', '2100717193', '193', '2017-07-10 00:00:00', '07:35:47', 'DN', '2', '2017-07-10 00:35:47', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('672', '2100717194', '194', '2017-07-10 00:00:00', '07:35:54', 'DN', '2', '2017-07-10 00:35:54', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('673', '3100717084', '84', '2017-07-10 00:00:00', '07:36:02', 'DN', '3', '2017-07-10 00:36:02', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('674', '5100717031', '31', '2017-07-10 00:00:00', '07:36:51', 'DN', '5', '2017-07-10 00:36:51', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('675', '2100717195', '195', '2017-07-10 00:00:00', '07:37:03', 'DN', '2', '2017-07-10 00:37:03', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('676', '2100717196', '196', '2017-07-10 00:00:00', '07:37:44', 'DN', '2', '2017-07-10 00:37:44', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('677', '5100717032', '32', '2017-07-10 00:00:00', '07:38:10', 'DN', '5', '2017-07-10 00:38:10', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('678', '5100717033', '33', '2017-07-10 00:00:00', '07:38:20', 'DN', '5', '2017-07-10 00:38:20', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('679', '5100717034', '34', '2017-07-10 00:00:00', '07:38:32', 'DN', '5', '2017-07-10 00:38:32', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('680', '4100717006', '6', '2017-07-10 00:00:00', '07:38:46', 'DN', '4', '2017-07-10 00:38:46', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('681', '5100717035', '35', '2017-07-10 00:00:00', '07:39:02', 'DN', '5', '2017-07-10 00:39:02', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('682', '4100717007', '7', '2017-07-10 00:00:00', '07:39:12', 'DN', '4', '2017-07-10 00:39:12', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('683', '5100717036', '36', '2017-07-10 00:00:00', '07:39:21', 'DN', '5', '2017-07-10 00:39:21', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('684', '5100717037', '37', '2017-07-10 00:00:00', '07:41:15', 'DN', '5', '2017-07-10 00:41:15', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('685', '2100717197', '197', '2017-07-10 00:00:00', '07:41:22', 'DN', '2', '2017-07-10 00:41:22', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('686', '2100717198', '198', '2017-07-10 00:00:00', '07:41:33', 'DN', '2', '2017-07-10 00:41:33', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('687', '5100717038', '38', '2017-07-10 00:00:00', '07:41:42', 'DN', '5', '2017-07-10 00:41:42', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('688', '5100717039', '39', '2017-07-10 00:00:00', '07:41:55', 'DN', '5', '2017-07-10 00:41:55', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('689', '2100717199', '199', '2017-07-10 00:00:00', '07:43:56', 'DN', '2', '2017-07-10 00:43:56', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('690', '3100717085', '85', '2017-07-10 00:00:00', '07:44:04', 'DN', '3', '2017-07-10 00:44:04', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('691', '2100717200', '200', '2017-07-10 00:00:00', '07:44:14', 'DN', '2', '2017-07-10 00:44:14', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('692', '5100717040', '40', '2017-07-10 00:00:00', '07:45:10', 'DN', '5', '2017-07-10 00:45:10', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('693', '5100717041', '41', '2017-07-10 00:00:00', '07:45:42', 'DN', '5', '2017-07-10 00:45:42', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('694', '3100717086', '86', '2017-07-10 00:00:00', '07:46:19', 'DN', '3', '2017-07-10 00:46:19', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('695', '5100717042', '42', '2017-07-10 00:00:00', '07:46:28', 'DN', '5', '2017-07-10 00:46:28', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('696', '2100717201', '201', '2017-07-10 00:00:00', '07:47:08', 'DN', '2', '2017-07-10 00:47:08', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('697', '3100717087', '87', '2017-07-10 00:00:00', '07:48:15', 'DN', '3', '2017-07-10 00:48:15', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('698', '5100717043', '43', '2017-07-10 00:00:00', '07:48:49', 'DN', '5', '2017-07-10 00:48:49', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('699', '5100717044', '44', '2017-07-10 00:00:00', '07:49:44', 'DN', '5', '2017-07-10 00:49:44', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('700', '2100717202', '202', '2017-07-10 00:00:00', '07:49:53', 'DN', '2', '2017-07-10 00:49:53', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('701', '2100717203', '203', '2017-07-10 00:00:00', '07:49:59', 'DN', '2', '2017-07-10 00:49:59', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('702', '3100717088', '88', '2017-07-10 00:00:00', '07:51:02', 'DN', '3', '2017-07-10 00:51:02', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('703', '5100717045', '45', '2017-07-10 00:00:00', '07:51:24', 'DN', '5', '2017-07-10 00:51:24', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('704', '2100717204', '204', '2017-07-10 00:00:00', '07:51:57', 'DN', '2', '2017-07-10 00:51:57', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('705', '5100717046', '46', '2017-07-10 00:00:00', '07:52:05', 'DN', '5', '2017-07-10 00:52:05', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('706', '5100717047', '47', '2017-07-10 00:00:00', '07:52:15', 'DN', '5', '2017-07-10 00:52:15', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('707', '5100717048', '48', '2017-07-10 00:00:00', '07:52:45', 'DN', '5', '2017-07-10 00:52:45', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('708', '2100717205', '205', '2017-07-10 00:00:00', '07:52:58', 'DN', '2', '2017-07-10 00:52:58', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('709', '2100717206', '206', '2017-07-10 00:00:00', '07:54:54', 'DN', '2', '2017-07-10 00:54:54', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('710', '4100717008', '8', '2017-07-10 00:00:00', '07:55:57', 'DN', '4', '2017-07-10 00:55:57', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('711', '5100717049', '49', '2017-07-10 00:00:00', '07:56:24', 'DN', '5', '2017-07-10 00:56:24', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('712', '3100717089', '89', '2017-07-10 00:00:00', '07:56:39', 'DN', '3', '2017-07-10 00:56:39', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('713', '5100717050', '50', '2017-07-10 00:00:00', '07:57:21', 'DN', '5', '2017-07-10 00:57:21', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('714', '3100717090', '90', '2017-07-10 00:00:00', '07:57:55', 'DN', '3', '2017-07-10 00:57:55', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('715', '3100717091', '91', '2017-07-10 00:00:00', '07:58:37', 'DN', '3', '2017-07-10 00:58:37', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('716', '5100717051', '51', '2017-07-10 00:00:00', '07:59:19', 'DN', '5', '2017-07-10 00:59:19', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('717', '2100717207', '207', '2017-07-10 00:00:00', '08:02:04', 'DN', '2', '2017-07-10 01:02:04', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('718', '5100717052', '52', '2017-07-10 00:00:00', '08:02:26', 'DN', '5', '2017-07-10 01:02:26', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('719', '5100717053', '53', '2017-07-10 00:00:00', '08:02:44', 'DN', '5', '2017-07-10 01:02:44', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('720', '5100717054', '54', '2017-07-10 00:00:00', '08:02:54', 'DN', '5', '2017-07-10 01:02:54', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('721', '3100717092', '92', '2017-07-10 00:00:00', '08:05:17', 'DN', '3', '2017-07-10 01:05:17', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('722', '5100717055', '55', '2017-07-10 00:00:00', '08:05:48', 'DN', '5', '2017-07-10 01:05:48', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('723', '2100717208', '208', '2017-07-10 00:00:00', '08:06:31', 'DN', '2', '2017-07-10 01:06:31', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('724', '4100717009', '9', '2017-07-10 00:00:00', '08:06:42', 'DN', '4', '2017-07-10 01:06:42', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('725', '2100717209', '209', '2017-07-10 00:00:00', '08:07:17', 'DN', '2', '2017-07-10 01:07:17', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('726', '2100717210', '210', '2017-07-10 00:00:00', '08:07:37', 'DN', '2', '2017-07-10 01:07:37', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('727', '3100717093', '93', '2017-07-10 00:00:00', '08:07:53', 'DN', '3', '2017-07-10 01:07:53', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('728', '2100717211', '211', '2017-07-10 00:00:00', '08:09:48', 'DN', '2', '2017-07-10 01:09:48', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('729', '5100717056', '56', '2017-07-10 00:00:00', '08:10:03', 'DN', '5', '2017-07-10 01:10:03', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('730', '5100717057', '57', '2017-07-10 00:00:00', '08:10:12', 'DN', '5', '2017-07-10 01:10:12', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('731', '5100717058', '58', '2017-07-10 00:00:00', '08:11:13', 'DN', '5', '2017-07-10 01:11:13', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('732', '2100717212', '212', '2017-07-10 00:00:00', '08:13:36', 'DN', '2', '2017-07-10 01:13:36', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('733', '3100717094', '94', '2017-07-10 00:00:00', '08:13:45', 'DN', '3', '2017-07-10 01:13:45', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('734', '5100717059', '59', '2017-07-10 00:00:00', '08:13:54', 'DN', '5', '2017-07-10 01:13:54', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('735', '2100717213', '213', '2017-07-10 00:00:00', '08:14:04', 'DN', '2', '2017-07-10 01:14:04', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('736', '2100717214', '214', '2017-07-10 00:00:00', '08:16:14', 'DN', '2', '2017-07-10 01:16:14', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('737', '5100717060', '60', '2017-07-10 00:00:00', '08:16:37', 'DN', '5', '2017-07-10 01:16:37', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('738', '2100717215', '215', '2017-07-10 00:00:00', '08:16:53', 'DN', '2', '2017-07-10 01:16:53', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('739', '3100717095', '95', '2017-07-10 00:00:00', '08:17:01', 'DN', '3', '2017-07-10 01:17:01', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('740', '2100717216', '216', '2017-07-10 00:00:00', '08:17:19', 'DN', '2', '2017-07-10 01:17:19', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('741', '2100717217', '217', '2017-07-10 00:00:00', '08:18:55', 'DN', '2', '2017-07-10 01:18:55', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('742', '5100717061', '61', '2017-07-10 00:00:00', '08:19:02', 'DN', '5', '2017-07-10 01:19:02', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('743', '5100717062', '62', '2017-07-10 00:00:00', '08:19:19', 'DN', '5', '2017-07-10 01:19:19', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('744', '5100717063', '63', '2017-07-10 00:00:00', '08:20:25', 'DN', '5', '2017-07-10 01:20:25', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('745', '3100717096', '96', '2017-07-10 00:00:00', '08:20:39', 'DN', '3', '2017-07-10 01:20:39', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('746', '2100717218', '218', '2017-07-10 00:00:00', '08:21:24', 'DN', '2', '2017-07-10 01:21:24', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('747', '2100717219', '219', '2017-07-10 00:00:00', '08:24:39', 'DN', '2', '2017-07-10 01:24:39', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('748', '3100717097', '97', '2017-07-10 00:00:00', '08:24:52', 'DN', '3', '2017-07-10 01:24:52', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('749', '3100717098', '98', '2017-07-10 00:00:00', '08:25:05', 'DN', '3', '2017-07-10 01:25:05', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('750', '3100717099', '99', '2017-07-10 00:00:00', '08:25:12', 'DN', '3', '2017-07-10 01:25:12', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('751', '3100717100', '100', '2017-07-10 00:00:00', '08:25:23', 'DN', '3', '2017-07-10 01:25:23', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('752', '3100717101', '101', '2017-07-10 00:00:00', '08:25:30', 'DN', '3', '2017-07-10 01:25:30', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('753', '4100717010', '10', '2017-07-10 00:00:00', '08:25:35', 'DN', '4', '2017-07-10 01:25:35', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('754', '5100717064', '64', '2017-07-10 00:00:00', '08:25:43', 'DN', '5', '2017-07-10 01:25:43', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('755', '3100717102', '102', '2017-07-10 00:00:00', '08:29:31', 'DN', '3', '2017-07-10 01:29:31', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('756', '3100717103', '103', '2017-07-10 00:00:00', '08:29:53', 'DN', '3', '2017-07-10 01:29:53', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('757', '3100717104', '104', '2017-07-10 00:00:00', '08:29:59', 'DN', '3', '2017-07-10 01:29:59', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('758', '3100717105', '105', '2017-07-10 00:00:00', '08:30:13', 'DN', '3', '2017-07-10 01:30:13', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('759', '5100717065', '65', '2017-07-10 00:00:00', '08:31:36', 'DN', '5', '2017-07-10 01:31:36', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('760', '3100717106', '106', '2017-07-10 00:00:00', '08:31:51', 'DN', '3', '2017-07-10 01:31:51', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('761', '5100717066', '66', '2017-07-10 00:00:00', '08:33:34', 'DN', '5', '2017-07-10 01:33:34', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('762', '5100717067', '67', '2017-07-10 00:00:00', '08:36:05', 'DN', '5', '2017-07-10 01:36:05', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('763', '3100717107', '107', '2017-07-10 00:00:00', '08:39:08', 'DN', '3', '2017-07-10 01:39:08', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('764', '4100717011', '11', '2017-07-10 00:00:00', '08:40:08', 'DN', '4', '2017-07-10 01:40:08', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('765', '2100717220', '220', '2017-07-10 00:00:00', '08:41:46', 'DN', '2', '2017-07-10 01:41:46', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('766', '3100717108', '108', '2017-07-10 00:00:00', '08:42:07', 'DN', '3', '2017-07-10 01:42:07', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('767', '2100717221', '221', '2017-07-10 00:00:00', '08:42:15', 'DN', '2', '2017-07-10 01:42:15', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('768', '5100717068', '68', '2017-07-10 00:00:00', '08:42:31', 'DN', '5', '2017-07-10 01:42:31', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('769', '5100717069', '69', '2017-07-10 00:00:00', '08:42:38', 'DN', '5', '2017-07-10 01:42:38', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('770', '2100717222', '222', '2017-07-10 00:00:00', '08:43:03', 'DN', '2', '2017-07-10 01:43:03', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('771', '3100717109', '109', '2017-07-10 00:00:00', '08:44:25', 'DN', '3', '2017-07-10 01:44:25', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('772', '2100717223', '223', '2017-07-10 00:00:00', '08:47:01', 'DN', '2', '2017-07-10 01:47:02', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('773', '3100717110', '110', '2017-07-10 00:00:00', '08:47:19', 'DN', '3', '2017-07-10 01:47:19', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('774', '2100717224', '224', '2017-07-10 00:00:00', '08:48:15', 'DN', '2', '2017-07-10 01:48:15', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('775', '3100717111', '111', '2017-07-10 00:00:00', '08:48:37', 'DN', '3', '2017-07-10 01:48:37', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('776', '3100717112', '112', '2017-07-10 00:00:00', '08:48:59', 'DN', '3', '2017-07-10 01:48:59', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('777', '3100717113', '113', '2017-07-10 00:00:00', '08:50:58', 'DN', '3', '2017-07-10 01:50:58', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('778', '2100717225', '225', '2017-07-10 00:00:00', '08:51:21', 'DN', '2', '2017-07-10 01:51:21', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('779', '2100717226', '226', '2017-07-10 00:00:00', '08:51:34', 'DN', '2', '2017-07-10 01:51:34', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('780', '2100717227', '227', '2017-07-10 00:00:00', '08:54:51', 'DN', '2', '2017-07-10 01:54:51', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('781', '3100717114', '114', '2017-07-10 00:00:00', '08:55:36', 'DN', '3', '2017-07-10 01:55:36', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('782', '5100717070', '70', '2017-07-10 00:00:00', '08:56:28', 'DN', '5', '2017-07-10 01:56:28', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('783', '5100717071', '71', '2017-07-10 00:00:00', '08:56:49', 'DN', '5', '2017-07-10 01:56:49', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('784', '2100717228', '228', '2017-07-10 00:00:00', '08:56:57', 'DN', '2', '2017-07-10 01:56:57', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('785', '5100717072', '72', '2017-07-10 00:00:00', '08:57:06', 'DN', '5', '2017-07-10 01:57:06', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('786', '3100717115', '115', '2017-07-10 00:00:00', '08:57:21', 'DN', '3', '2017-07-10 01:57:21', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('787', '4100717012', '12', '2017-07-10 00:00:00', '08:58:21', 'DN', '4', '2017-07-10 01:58:21', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('788', '5100717073', '73', '2017-07-10 00:00:00', '09:03:05', 'DN', '5', '2017-07-10 02:03:05', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('789', '5100717074', '74', '2017-07-10 00:00:00', '09:10:08', 'DN', '5', '2017-07-10 02:10:08', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('790', '5100717075', '75', '2017-07-10 00:00:00', '09:10:29', 'DN', '5', '2017-07-10 02:10:29', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('791', '5100717076', '76', '2017-07-10 00:00:00', '09:13:04', 'DN', '5', '2017-07-10 02:13:04', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('792', '4100717013', '13', '2017-07-10 00:00:00', '09:15:53', 'DN', '4', '2017-07-10 02:15:53', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('793', '5100717077', '77', '2017-07-10 00:00:00', '09:16:58', 'DN', '5', '2017-07-10 02:16:58', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('794', '3100717116', '116', '2017-07-10 00:00:00', '09:17:22', 'DN', '3', '2017-07-10 02:17:22', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('795', '5100717078', '78', '2017-07-10 00:00:00', '09:19:18', 'DN', '5', '2017-07-10 02:19:18', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('796', '2100717229', '229', '2017-07-10 00:00:00', '09:19:29', 'DN', '2', '2017-07-10 02:19:29', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('797', '3100717117', '117', '2017-07-10 00:00:00', '09:19:49', 'DN', '3', '2017-07-10 02:19:49', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('798', '5100717079', '79', '2017-07-10 00:00:00', '09:20:56', 'DN', '5', '2017-07-10 02:20:56', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('799', '5100717080', '80', '2017-07-10 00:00:00', '09:22:30', 'DN', '5', '2017-07-10 02:22:30', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('800', '5100717081', '81', '2017-07-10 00:00:00', '09:24:18', 'DN', '5', '2017-07-10 02:24:18', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('801', '2100717230', '230', '2017-07-10 00:00:00', '09:24:42', 'DN', '2', '2017-07-10 02:24:42', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('802', '3100717118', '118', '2017-07-10 00:00:00', '09:25:21', 'DN', '3', '2017-07-10 02:25:21', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('803', '2100717231', '231', '2017-07-10 00:00:00', '09:28:03', 'DN', '2', '2017-07-10 02:28:04', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('804', '5100717082', '82', '2017-07-10 00:00:00', '09:28:39', 'DN', '5', '2017-07-10 02:28:39', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('805', '3100717119', '119', '2017-07-10 00:00:00', '09:30:16', 'DN', '3', '2017-07-10 02:30:16', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('806', '3100717120', '120', '2017-07-10 00:00:00', '09:32:16', 'DN', '3', '2017-07-10 02:32:16', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('807', '3100717121', '121', '2017-07-10 00:00:00', '09:35:14', 'DN', '3', '2017-07-10 02:35:14', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('808', '2100717232', '232', '2017-07-10 00:00:00', '09:36:00', 'DN', '2', '2017-07-10 02:36:00', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('809', '3100717122', '122', '2017-07-10 00:00:00', '09:36:18', 'DN', '3', '2017-07-10 02:36:18', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('810', '3100717123', '123', '2017-07-10 00:00:00', '09:36:34', 'DN', '3', '2017-07-10 02:36:34', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('811', '2100717233', '233', '2017-07-10 00:00:00', '09:37:05', 'DN', '2', '2017-07-10 02:37:05', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('812', '4100717014', '14', '2017-07-10 00:00:00', '09:37:18', 'DN', '4', '2017-07-10 02:37:18', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('813', '4100717015', '15', '2017-07-10 00:00:00', '09:37:46', 'DN', '4', '2017-07-10 02:37:46', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('814', '5100717083', '83', '2017-07-10 00:00:00', '09:38:15', 'DN', '5', '2017-07-10 02:38:15', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('815', '2100717234', '234', '2017-07-10 00:00:00', '09:38:34', 'DN', '2', '2017-07-10 02:38:34', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('816', '5100717084', '84', '2017-07-10 00:00:00', '09:39:31', 'DN', '5', '2017-07-10 02:39:31', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('817', '3100717124', '124', '2017-07-10 00:00:00', '09:41:48', 'DN', '3', '2017-07-10 02:41:48', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('818', '3100717125', '125', '2017-07-10 00:00:00', '09:43:40', 'DN', '3', '2017-07-10 02:43:40', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('819', '5100717085', '85', '2017-07-10 00:00:00', '09:45:32', 'DN', '5', '2017-07-10 02:45:32', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('820', '2100717235', '235', '2017-07-10 00:00:00', '09:45:41', 'DN', '2', '2017-07-10 02:45:41', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('821', '5100717086', '86', '2017-07-10 00:00:00', '09:47:49', 'DN', '5', '2017-07-10 02:47:49', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('822', '2100717236', '236', '2017-07-10 00:00:00', '09:48:33', 'DN', '2', '2017-07-10 02:48:33', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('823', '3100717126', '126', '2017-07-10 00:00:00', '09:50:08', 'PR', '3', '2017-07-10 02:50:08', '2017-07-10 05:37:24', '0', '7');
INSERT INTO `sm_pendaftarans` VALUES ('824', '3100717127', '127', '2017-07-10 00:00:00', '09:52:00', 'DN', '3', '2017-07-10 02:52:00', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('825', '5100717087', '87', '2017-07-10 00:00:00', '09:52:39', 'DN', '5', '2017-07-10 02:52:39', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('826', '2100717237', '237', '2017-07-10 00:00:00', '09:55:23', 'PR', '2', '2017-07-10 02:55:23', '2017-07-10 04:38:30', '0', '4');
INSERT INTO `sm_pendaftarans` VALUES ('827', '2100717238', '238', '2017-07-10 00:00:00', '09:55:34', 'DN', '2', '2017-07-10 02:55:34', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('828', '5100717088', '88', '2017-07-10 00:00:00', '09:55:55', 'DN', '5', '2017-07-10 02:55:55', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('829', '5100717089', '89', '2017-07-10 00:00:00', '10:02:24', 'DN', '5', '2017-07-10 03:02:24', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('830', '3100717128', '128', '2017-07-10 00:00:00', '10:03:38', 'DN', '3', '2017-07-10 03:03:38', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('831', '2100717239', '239', '2017-07-10 00:00:00', '10:04:54', 'DN', '2', '2017-07-10 03:04:54', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('832', '5100717090', '90', '2017-07-10 00:00:00', '10:05:16', 'DN', '5', '2017-07-10 03:05:16', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('833', '3100717129', '129', '2017-07-10 00:00:00', '10:08:40', 'DN', '3', '2017-07-10 03:08:40', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('834', '5100717091', '91', '2017-07-10 00:00:00', '10:09:27', 'DN', '5', '2017-07-10 03:09:27', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('835', '3100717130', '130', '2017-07-10 00:00:00', '10:09:47', 'DN', '3', '2017-07-10 03:09:47', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('836', '5100717092', '92', '2017-07-10 00:00:00', '10:10:13', 'DN', '5', '2017-07-10 03:10:13', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('837', '5100717093', '93', '2017-07-10 00:00:00', '10:10:21', 'DN', '5', '2017-07-10 03:10:21', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('838', '2100717240', '240', '2017-07-10 00:00:00', '10:10:38', 'DN', '2', '2017-07-10 03:10:38', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('839', '2100717241', '241', '2017-07-10 00:00:00', '10:10:51', 'DN', '2', '2017-07-10 03:10:51', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('840', '2100717242', '242', '2017-07-10 00:00:00', '10:11:49', 'DN', '2', '2017-07-10 03:11:49', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('841', '5100717094', '94', '2017-07-10 00:00:00', '10:12:18', 'DN', '5', '2017-07-10 03:12:18', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('842', '2100717243', '243', '2017-07-10 00:00:00', '10:14:22', 'DN', '2', '2017-07-10 03:14:22', '2017-07-10 04:38:30', '0', '0');
INSERT INTO `sm_pendaftarans` VALUES ('843', '5100717095', '95', '2017-07-10 00:00:00', '10:15:49', 'DN', '5', '2017-07-10 03:15:49', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('844', '2100717244', '244', '2017-07-10 00:00:00', '10:17:24', 'DN', '2', '2017-07-10 03:17:24', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('845', '2100717245', '245', '2017-07-10 00:00:00', '10:19:46', 'DN', '2', '2017-07-10 03:19:46', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('846', '5100717096', '96', '2017-07-10 00:00:00', '10:20:18', 'DN', '5', '2017-07-10 03:20:18', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('847', '4100717016', '16', '2017-07-10 00:00:00', '10:20:28', 'DN', '4', '2017-07-10 03:20:28', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('848', '2100717246', '246', '2017-07-10 00:00:00', '10:20:34', 'DN', '2', '2017-07-10 03:20:35', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('849', '2100717247', '247', '2017-07-10 00:00:00', '10:20:54', 'DN', '2', '2017-07-10 03:20:54', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('850', '2100717248', '248', '2017-07-10 00:00:00', '10:21:58', 'DN', '2', '2017-07-10 03:21:58', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('851', '2100717249', '249', '2017-07-10 00:00:00', '10:24:18', 'DN', '2', '2017-07-10 03:24:18', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('852', '2100717250', '250', '2017-07-10 00:00:00', '10:27:10', 'DN', '2', '2017-07-10 03:27:10', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('853', '5100717097', '97', '2017-07-10 00:00:00', '10:29:03', 'DN', '5', '2017-07-10 03:29:03', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('854', '2100717251', '251', '2017-07-10 00:00:00', '10:29:18', 'DN', '2', '2017-07-10 03:29:18', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('855', '5100717098', '98', '2017-07-10 00:00:00', '10:31:45', 'DN', '5', '2017-07-10 03:31:45', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('856', '2100717252', '252', '2017-07-10 00:00:00', '10:33:15', 'DN', '2', '2017-07-10 03:33:15', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('857', '3100717131', '131', '2017-07-10 00:00:00', '10:36:20', 'DN', '3', '2017-07-10 03:36:20', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('858', '2100717253', '253', '2017-07-10 00:00:00', '10:38:00', 'DN', '2', '2017-07-10 03:38:00', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('859', '5100717099', '99', '2017-07-10 00:00:00', '10:38:51', 'DN', '5', '2017-07-10 03:38:51', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('860', '2100717254', '254', '2017-07-10 00:00:00', '10:39:55', 'PR', '2', '2017-07-10 03:39:56', '2017-07-10 04:38:30', '0', '5');
INSERT INTO `sm_pendaftarans` VALUES ('861', '5100717100', '100', '2017-07-10 00:00:00', '10:40:07', 'DN', '5', '2017-07-10 03:40:07', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('862', '2100717255', '255', '2017-07-10 00:00:00', '10:40:23', 'DN', '2', '2017-07-10 03:40:23', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('863', '2100717256', '256', '2017-07-10 00:00:00', '10:43:36', 'PR', '2', '2017-07-10 03:43:36', '2017-07-10 04:38:30', '0', '2');
INSERT INTO `sm_pendaftarans` VALUES ('864', '2100717257', '257', '2017-07-10 00:00:00', '10:45:14', 'DN', '2', '2017-07-10 03:45:14', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('865', '5100717101', '101', '2017-07-10 00:00:00', '10:48:47', 'DN', '5', '2017-07-10 03:48:47', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('866', '2100717258', '258', '2017-07-10 00:00:00', '10:53:26', 'PR', '2', '2017-07-10 03:53:26', '2017-07-10 04:38:30', '0', '3');
INSERT INTO `sm_pendaftarans` VALUES ('867', '3100717132', '132', '2017-07-10 00:00:00', '10:53:34', 'PR', '3', '2017-07-10 03:53:34', '2017-07-10 05:37:24', '0', '6');
INSERT INTO `sm_pendaftarans` VALUES ('868', '4100717017', '17', '2017-07-10 00:00:00', '10:53:41', 'PR', '4', '2017-07-10 03:53:41', '2017-07-10 03:57:04', '0', '8');
INSERT INTO `sm_pendaftarans` VALUES ('869', '5100717102', '102', '2017-07-10 00:00:00', '10:53:48', 'PR', '5', '2017-07-10 03:53:48', '2017-07-10 04:21:18', '0', '9');
INSERT INTO `sm_pendaftarans` VALUES ('870', '2110717001', '1', '2017-07-11 00:00:00', '05:12:08', 'CL', '2', '2017-07-10 22:12:08', '2017-07-10 22:14:58', '0', '0');

-- ----------------------------
-- Table structure for `sm_ruangans`
-- ----------------------------
DROP TABLE IF EXISTS `sm_ruangans`;
CREATE TABLE `sm_ruangans` (
  `roomidx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idRuangPelayanan` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `namaRuangPelayanan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aliasRuangPelayanan` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`roomidx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sm_ruangans
-- ----------------------------

-- ----------------------------
-- Table structure for `sm_settings`
-- ----------------------------
DROP TABLE IF EXISTS `sm_settings`;
CREATE TABLE `sm_settings` (
  `settingid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `settingtype` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settingvalue` text COLLATE utf8mb4_unicode_ci,
  `settingstat` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setting` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`settingid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sm_settings
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empid` int(11) DEFAULT NULL,
  `empname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userlevel` int(11) DEFAULT NULL,
  `statdel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loket_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', null, 'admin', 'user1@teknolands.com', '$2y$10$QZ.NIvHGm3Auh6YFiAoryexR/o3Pd/5Y8J4EPKcfl4brare1V63Ku', '123456', 'admin', null, null, null, null, 'JWJSVXGMP5YAefXQ6apNL18st0uzDcgfUn1E73XtxbUlZBTOUWfA6MCRvCwG', null, null);
INSERT INTO `users` VALUES ('2', null, 'loketa1', null, '$2y$10$SAHcBB/xy2cCKMp20qj0Ru9.398K8C/QBqwdGvdsvNowoxgqJhhAa', '21', 'Loket A1', '2', null, null, 'Loket A1', 'pzh06I3UtjYdNJlREUrHtLDxsH4KNYWr7pJO2xMGZHVgOCtA6Q4NKI9haOdH', '2017-07-07 23:58:38', '2017-07-07 23:58:38');
INSERT INTO `users` VALUES ('3', null, 'loketa2', null, '$2y$10$tfqqi9HjosbVqEZw.4sm0O/Xx9c9AAdBR1C.3Wn0xTmeZrs.74HBi', '22', 'Loket A2', '2', null, null, 'Loket A2', null, '2017-07-07 23:58:38', '2017-07-07 23:58:38');
INSERT INTO `users` VALUES ('4', null, 'loketa3', null, '$2y$10$J5ArDSkyS/BA587DIRNgUeO.RRQy/trw0jJcgnypxnxmpDtSnRKCG', '23', 'Loket A3', '2', null, null, 'Loket A3', null, '2017-07-07 23:58:38', '2017-07-07 23:58:38');
INSERT INTO `users` VALUES ('5', null, 'loketa4', null, '$2y$10$i98FgyXJ3iaKQ.61Ne1yhON6bknnVqXGha0GYqVfw.nrfUyRWkEBa', '24', 'Loket A4', '2', null, null, 'Loket A4', 'He06tikaftYDSwF5mYgeX6FNNOvNHV2nYpnuxUc4l220PHrhCjM4KrYlbFAR', '2017-07-07 23:58:38', '2017-07-07 23:58:38');
INSERT INTO `users` VALUES ('6', null, 'loketb1', null, '$2y$10$ySieS1jB7Z9j6XsRUYNBVOGmtuOKecWXXOg/rJh/X/9FjrJzM2U12', '31', 'Loket B1', '3', null, null, 'Loket B1', 'X9gjR1a8jFjrrQYGSctb5bhqiN3RJTPeXfJVeIHGi3bukaxUCG3grrqwlgey', '2017-07-07 23:58:39', '2017-07-07 23:58:39');
INSERT INTO `users` VALUES ('7', null, 'loketb2', null, '$2y$10$3RQge/XvqY50gLbyk3Zs2.6g84C3CeCe8LxUz8VN/4rQJWMUFRH8a', '32', 'Loket B2', '3', null, null, 'Loket B2', 'SIA5mWq84vMQmOok3BBOSoZTn5JtvmDVIms4ZzuxPxSKQTdtGTCVCyAi045d', '2017-07-07 23:58:39', '2017-07-07 23:58:39');
INSERT INTO `users` VALUES ('8', null, 'loketc1', null, '$2y$10$MVxqQJ3RWIF.Ssm8tl5umejC5lvUgKBskUVgZbTnJL3OKPYUVpvSW', '41', 'Loket C1', '4', null, null, 'Loket C1', 'c3tuThidmYsEEwZYgqHg36pcnxEnD6kbLppGiKJ94EN07LUFIEiSHq7STwcq', '2017-07-07 23:58:39', '2017-07-07 23:58:39');
INSERT INTO `users` VALUES ('9', null, 'loketd1', null, '$2y$10$r.NwP2q.lTbdzNvfYpeY9uz69oQ96ZXYqWVbZVVgU2Q6FgTJ3WVX6', '51', 'Loket D1', '5', null, null, 'Loket D1', null, '2017-07-07 23:58:39', '2017-07-07 23:58:39');

-- ----------------------------
-- Table structure for `user_baru`
-- ----------------------------
DROP TABLE IF EXISTS `user_baru`;
CREATE TABLE `user_baru` (
  `NPRS` char(20) NOT NULL DEFAULT '',
  `Password` char(50) DEFAULT NULL,
  PRIMARY KEY (`NPRS`),
  KEY `NPRS` (`NPRS`,`Password`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_baru
-- ----------------------------
INSERT INTO `user_baru` VALUES ('0003', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0006', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0012', 'bb18a65fa53faf62c1e132166ed7fe2ce8724075');
INSERT INTO `user_baru` VALUES ('0021', '1878e69f8e52d656fce3d26ba59967167b999748');
INSERT INTO `user_baru` VALUES ('0022', 'b78b5836b84955e6951a8e5377c0e85b0cfa40e7');
INSERT INTO `user_baru` VALUES ('0024', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0028', 'c6defcc37551ee31ae8310064c7748077bac31c3');
INSERT INTO `user_baru` VALUES ('0029', 'f2ff2e6559cd6f253e0327d01966c3c0a6a5a501');
INSERT INTO `user_baru` VALUES ('0034', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0035', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0038', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0043', 'aa9c0d0930b8f648b52b044352932fffa088ae95');
INSERT INTO `user_baru` VALUES ('0046', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0054', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0057', 'dd807146a3a81793ecd1b8fb613e9ad5bb76c93e');
INSERT INTO `user_baru` VALUES ('0058', '7afbc219e31a640eee8fd9be368ed1e50d6be3f6');
INSERT INTO `user_baru` VALUES ('0059', '1855105db9c41c0e25b6131a4be9c2d400ce2345');
INSERT INTO `user_baru` VALUES ('0060', 'e30b2b02462ed68bf469c01e5bb2b6fc26567398');
INSERT INTO `user_baru` VALUES ('0061', '0dbba9b150e4e263b4cdd4af2605d1659443558a');
INSERT INTO `user_baru` VALUES ('0063', 'cca0b54151c4c6d7ad3c9f55eda4426856e3879d');
INSERT INTO `user_baru` VALUES ('0064', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0065', '857e44a10303711fe544f6983623eb202810c0cf');
INSERT INTO `user_baru` VALUES ('0066', 'd980513d995af029461ad580e4e11f377fab645d');
INSERT INTO `user_baru` VALUES ('0067', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0069', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0073', '87b30f58588cdfb76046dd28ba042bdabe68eb23');
INSERT INTO `user_baru` VALUES ('0077', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0078', '60296f6db115b0cb6d53e51108d96b97fe3cbd22');
INSERT INTO `user_baru` VALUES ('0080', '2c16b5024f0447c297a016ad74a4ae8d274eaa41');
INSERT INTO `user_baru` VALUES ('0081', '7fbfa35d53830319bf9b24c0a998c658f4635ede');
INSERT INTO `user_baru` VALUES ('0082', 'a7cc6220ee99002330090f013aca75a7ff0e4a93');
INSERT INTO `user_baru` VALUES ('0086', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0087', '8bfcd7fddc78c646625f45b08128ad903a26e20c');
INSERT INTO `user_baru` VALUES ('0091', 'e1e82689820516f9fdc412676cd2d333fbf5d888');
INSERT INTO `user_baru` VALUES ('0093', 'faaf1f12b9e348640031d1afd6ded11b8b0d718f');
INSERT INTO `user_baru` VALUES ('0094', '8e4b2c3b6acc429bc7f6dfc13142493f42d925fa');
INSERT INTO `user_baru` VALUES ('0097', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0100', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0102', 'eedf7019cc231349ca0e19bf955140e469188202');
INSERT INTO `user_baru` VALUES ('0103', '479b529b4a08c6765a6488d92faceda86406e091');
INSERT INTO `user_baru` VALUES ('0104', 'ec929aee78b6d4d3bd04f67e191bce252de16b31');
INSERT INTO `user_baru` VALUES ('0111', 'dfe8782289a27dbbab2f093f811699e713d329de');
INSERT INTO `user_baru` VALUES ('0113', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0115', 'c5bd1b05c4a1a4408903572fe14bfde45ca85113');
INSERT INTO `user_baru` VALUES ('0116', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0117', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0118', 'af76311517f6d4d9f6dd6714668eb672e5e0a6fd');
INSERT INTO `user_baru` VALUES ('0119', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0121', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0122', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0123', '90b9aa7e25f80cf4f64e990b78a9fc5ebd6cecad');
INSERT INTO `user_baru` VALUES ('0124', '3aa2a667f37bf1b9c40d3ab414a9486307b8d4ee');
INSERT INTO `user_baru` VALUES ('0127', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0131', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0136', '8776b49856a760fafd3b88095ee0e82f4151f969');
INSERT INTO `user_baru` VALUES ('0137', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0140', '911f1580a8bd820467e204e83a448cdcb14ba34a');
INSERT INTO `user_baru` VALUES ('0142', '07687db431f6b0af1e164adcce7c29cab8d13110');
INSERT INTO `user_baru` VALUES ('0143', '10470c3b4b1fed12c3baac014be15fac67c6e815');
INSERT INTO `user_baru` VALUES ('0149', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0150', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0152', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0155', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0156', 'f4282b7ca72d00615f0d9d4c6469663ff59098a8');
INSERT INTO `user_baru` VALUES ('0160', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0163', 'aaa06a514180825c83748d8cb4bad8bee3bb91e1');
INSERT INTO `user_baru` VALUES ('0166', '061e0d99f92938189f82fab70368b0f7b4c782c2');
INSERT INTO `user_baru` VALUES ('0167', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0169', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0172', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0173', '1f02d38cf7c91020d8478b873b18dac0895f7a7f');
INSERT INTO `user_baru` VALUES ('0174', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0175', 'af31253adb74a7c8d7b8f692d2692d0552edf8b7');
INSERT INTO `user_baru` VALUES ('0176', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0177', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0179', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0182', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0185', 'a369b000cec6d1b6a8f1a89531e09e80920de934');
INSERT INTO `user_baru` VALUES ('0189', '65bf83584692d0c1569017a2fe4097184b6fd09e');
INSERT INTO `user_baru` VALUES ('0193', '9426855baa237d81217878b35df61ddde2ebb24d');
INSERT INTO `user_baru` VALUES ('0194', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0195', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0202', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0233', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0239', 'd9ac43692179c4f63c003b4406f10d6e0231eb20');
INSERT INTO `user_baru` VALUES ('0240', '8900479bf4dd3f835f8c1292ce14c9ff6c7a92c7');
INSERT INTO `user_baru` VALUES ('0243', '6966837f4ea2b250790e954412e44d5813eb787c');
INSERT INTO `user_baru` VALUES ('0246', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0251', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0252', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0253', 'd67d513f2df37fdaf480bd60579df35cbff867dc');
INSERT INTO `user_baru` VALUES ('0256', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0257', 'cd561cc910c430516b846a5575a37bd44f8bb619');
INSERT INTO `user_baru` VALUES ('0259', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0260', 'ea0b1d564a75406551ef0b4b88722cd7419ffb87');
INSERT INTO `user_baru` VALUES ('0263', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0265', '199099cd11728a326aee31694069b9da95160b8c');
INSERT INTO `user_baru` VALUES ('0269', '9f839404f8993c4ed0837841b919bb686e6219a9');
INSERT INTO `user_baru` VALUES ('0271', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0276', '3e593a80ae14c5d54b9b92aae9210a1a32d4da57');
INSERT INTO `user_baru` VALUES ('0278', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0279', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0280', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0286', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0287', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0289', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0290', '467ba13471c09edbb20497cefe16cf80529e4864');
INSERT INTO `user_baru` VALUES ('0298', '582edc3bd58687a4617701c9674843a9a0656d54');
INSERT INTO `user_baru` VALUES ('0300', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0302', '808aeb8762d2ed02ff655e173d72c027aa6a4a13');
INSERT INTO `user_baru` VALUES ('0307', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0308', 'ad7f0ef8fe98606e49855580eb4805d4a352f95a');
INSERT INTO `user_baru` VALUES ('0309', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0310', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0311', 'd3386e9c19340e9b6540a66721b6713702a3f871');
INSERT INTO `user_baru` VALUES ('0314', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0326', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0330', '591e42e1419e29ab95033fff06fdfb6cb46b0996');
INSERT INTO `user_baru` VALUES ('0333', 'e77379cce3661a79baec034a00ece83a661cb9a7');
INSERT INTO `user_baru` VALUES ('0340', '740263257d8fcfc93e834fe88e3d503d6761d752');
INSERT INTO `user_baru` VALUES ('0342', '4e95e690d9ff77d23a22dc6c26f7241b5a09099e');
INSERT INTO `user_baru` VALUES ('0343', '8878b5b574960bad4f74d7a3dc07ecb27a8e15a7');
INSERT INTO `user_baru` VALUES ('0344', '4f7a78ab1aaa89dfa4e252fe1ca8a4e629522f9c');
INSERT INTO `user_baru` VALUES ('0346', '619f2548d0d7c866fa90c7d51ea4f8f383c14747');
INSERT INTO `user_baru` VALUES ('0347', '920e608ffa49923eec06fd917268d6533f8ac5f2');
INSERT INTO `user_baru` VALUES ('0348', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0349', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0351', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0354', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0357', '5628d12ec525b02444aff0f686358b338a966a06');
INSERT INTO `user_baru` VALUES ('0361', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0362', '4789b62c64b3741c0e536e30d15e1cf4ebbee5c9');
INSERT INTO `user_baru` VALUES ('0363', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0364', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0365', '229c0b8ec0d64bbfcceed613b738a113c7f2c72e');
INSERT INTO `user_baru` VALUES ('0366', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0367', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0368', 'e32aec7fa5184f5f0d3e5b7ce9cf4daa795997a0');
INSERT INTO `user_baru` VALUES ('0369', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0370', '116aaf16016af23f0af075da409d5a4392ef3b76');
INSERT INTO `user_baru` VALUES ('0371', '16868a60212b6b92441f5bbc1e68b9473a2ea553');
INSERT INTO `user_baru` VALUES ('0372', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0373', 'e21d94cdccc74ee72e7cb55bf23ebdcc1d583840');
INSERT INTO `user_baru` VALUES ('0374', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0375', '10470c3b4b1fed12c3baac014be15fac67c6e815');
INSERT INTO `user_baru` VALUES ('0380', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0405', 'd0040bb3c7390887cf3721d777f9fed391ff9152');
INSERT INTO `user_baru` VALUES ('0406', '00be103f5aaf01f39473501c4199694812087758');
INSERT INTO `user_baru` VALUES ('0407', 'f3bdd827afc53eb6cc18f27e14a2632ea6d2e309');
INSERT INTO `user_baru` VALUES ('0408', '8e07f1292972472ded53919269f63e8d6d37d7e6');
INSERT INTO `user_baru` VALUES ('0409', '0587767bf409c9c8a0cc76a2f7b8972da27de874');
INSERT INTO `user_baru` VALUES ('0410', 'e2a7e3e15c965abf1c95a0e5aa09d0ea2b51220f');
INSERT INTO `user_baru` VALUES ('0411', '6bb5687008bbc5bd5181318416513332778fcad5');
INSERT INTO `user_baru` VALUES ('0412', 'e8c079a0dc96ce278dd5d6afdae076a88a4943cf');
INSERT INTO `user_baru` VALUES ('0413', 'e6a7f2c7940a9db2eba8f3d1ea7ef731ff4370d0');
INSERT INTO `user_baru` VALUES ('0473', '67af572f74b50dc4de6e299d07bc8d54548ee3cc');
INSERT INTO `user_baru` VALUES ('0515', '1c13774af9e425400e9c8d5d6c42bc81a9932a5c');
INSERT INTO `user_baru` VALUES ('0516', 'a55d80ea7de723f7c1eb66f38858afe5ecfb6f5e');
INSERT INTO `user_baru` VALUES ('0520', '23f03e862685ceb9e545f12088ec3c025730a65d');
INSERT INTO `user_baru` VALUES ('0521', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0522', 'a1cb1996c1664eecc3216a65f8e7e2bc7db79fc8');
INSERT INTO `user_baru` VALUES ('0523', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0524', '5d2ec3ad350befec179d01c2fabba50e97bb784f');
INSERT INTO `user_baru` VALUES ('0525', '63f7f1ab5ae1da63e55f0edcb83c9ddb7ce76dbf');
INSERT INTO `user_baru` VALUES ('0527', 'fdf3a5cb1123d4770c0c840fa46e7afb64a5cf5d');
INSERT INTO `user_baru` VALUES ('0528', 'c5b8ff7accaa271d42b05cc757231252ed1d4141');
INSERT INTO `user_baru` VALUES ('0530', '3d2cdcb34504ee431e02a6fddd06f330fb8450fd');
INSERT INTO `user_baru` VALUES ('0531', '15b29bfa3da5cae347f6dd7d6271ec7dc78d654d');
INSERT INTO `user_baru` VALUES ('0532', '826ddb03c850cd0b45f0694f49d2a337d146b0b5');
INSERT INTO `user_baru` VALUES ('0533', '74dfb57afff8339c6a1873873724af6d2dbb6969');
INSERT INTO `user_baru` VALUES ('0534', '3a3ec5685a4493abfb2c9c6605a6057be4a66a88');
INSERT INTO `user_baru` VALUES ('0536', '8fe08b3e438e471b3abf165c8d946e8e1e51b093');
INSERT INTO `user_baru` VALUES ('0537', '01698d230e40707668de234c7e41a9ab1b0f51ac');
INSERT INTO `user_baru` VALUES ('0601', 'b34abca3fe22ca4eb4303d3238957e730fae9a8d');
INSERT INTO `user_baru` VALUES ('0612', 'fa822c358bd45eaadc4ba8ab7b327d6c04563b34');
INSERT INTO `user_baru` VALUES ('0614', '625800dc9d8628a5c10274c87a241e453cae0442');
INSERT INTO `user_baru` VALUES ('0712', 'a211747804c7d2fcd49365791cc44aed7116308c');
INSERT INTO `user_baru` VALUES ('0788', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('0789', '43af3636dd91b5913b7020c68b2fb1988edcc786');
INSERT INTO `user_baru` VALUES ('0801', '9582fa357a27d9b5b0667fab6ec1c0a25bfc9152');
INSERT INTO `user_baru` VALUES ('0802', 'b8878cac048df9c45ff4151327ab585c1323f236');
INSERT INTO `user_baru` VALUES ('0831', '64956a67293af76f0a45e6ee199ee48325223748');
INSERT INTO `user_baru` VALUES ('0898', 'ddb96d5a7a990d3a60de2590be8dc21cb1a9d519');
INSERT INTO `user_baru` VALUES ('0967', '08709e8803b25eddef69e37cc476b4a064f2023d');
INSERT INTO `user_baru` VALUES ('0981', '49577d94190c9de29b87f5c66bbd88d427b42cf1');
INSERT INTO `user_baru` VALUES ('0982', '909b29a84c6710434d9321287038f3ee3dadbb4b');
INSERT INTO `user_baru` VALUES ('0990', '96243b88fc351c24c8151e619be09da1ce5627bb');
INSERT INTO `user_baru` VALUES ('0992', '9a1b67e6c32838e644db7621c120281a3894b1eb');
INSERT INTO `user_baru` VALUES ('0993', 'aeed1fa04f8615164dc9881cd6feababe176f401');
INSERT INTO `user_baru` VALUES ('0994', '8575514c637d9f1b96abaed8b89989719c5ca608');
INSERT INTO `user_baru` VALUES ('0995', 'ce545f30f2d76c1f0904f1f54f500ab1c55674f4');
INSERT INTO `user_baru` VALUES ('0996', '352de067f88e7fc9a67e444265984671501856e3');
INSERT INTO `user_baru` VALUES ('0997	', 'cbbc01d35527f2390aa04263934e933c11aa5323');
INSERT INTO `user_baru` VALUES ('0997', 'cc7146827774f27b92e584e02cde6a5c39a6fa75');
INSERT INTO `user_baru` VALUES ('0998', 'f60bc5dcaa41aa8ef12a1517f1bbf735bf4ebcf3');
INSERT INTO `user_baru` VALUES ('0999', 'f60bc5dcaa41aa8ef12a1517f1bbf735bf4ebcf3');
INSERT INTO `user_baru` VALUES ('1000', 'dc05f0c4bb8d1d192f0b4bbd98aa67920f934e14');
INSERT INTO `user_baru` VALUES ('1234', 'f60bc5dcaa41aa8ef12a1517f1bbf735bf4ebcf3');
INSERT INTO `user_baru` VALUES ('12345', '55c3b5386c486feb662a0785f340938f518d547f');
INSERT INTO `user_baru` VALUES ('1335', '51cfb2f96229bd8bea24b18fb2fabcf247ceb4f4');

-- ----------------------------
-- Table structure for `user_grup`
-- ----------------------------
DROP TABLE IF EXISTS `user_grup`;
CREATE TABLE `user_grup` (
  `no` int(10) NOT NULL AUTO_INCREMENT,
  `nprs` char(20) NOT NULL DEFAULT '0',
  `id_grup` bigint(11) DEFAULT NULL,
  `id_kategori` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`no`)
) ENGINE=MyISAM AUTO_INCREMENT=999 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_grup
-- ----------------------------
INSERT INTO `user_grup` VALUES ('456', '0601', '11', '11');
INSERT INTO `user_grup` VALUES ('642', '0522', '1', '1');
INSERT INTO `user_grup` VALUES ('815', '0066', '13', '3');
INSERT INTO `user_grup` VALUES ('954', '0058', '12', '4');
INSERT INTO `user_grup` VALUES ('5', '0363', '12', '11');
INSERT INTO `user_grup` VALUES ('959', '0080', '11', '16');
INSERT INTO `user_grup` VALUES ('696', '0057', '10', '10');
INSERT INTO `user_grup` VALUES ('8', '0024', '6', '10');
INSERT INTO `user_grup` VALUES ('9', '0127', '18', '9');
INSERT INTO `user_grup` VALUES ('176', '0063', '1', '1');
INSERT INTO `user_grup` VALUES ('11', '0271', '1', '1');
INSERT INTO `user_grup` VALUES ('174', '0137', '19', '9');
INSERT INTO `user_grup` VALUES ('13', '0113', '1', '1');
INSERT INTO `user_grup` VALUES ('581', '0276', '1', '18');
INSERT INTO `user_grup` VALUES ('333', '0140', '1', '18');
INSERT INTO `user_grup` VALUES ('383', '0166', '21', '4');
INSERT INTO `user_grup` VALUES ('17', '0380', '4', '2');
INSERT INTO `user_grup` VALUES ('18', '0067', '15', '10');
INSERT INTO `user_grup` VALUES ('19', '0034', '7', '10');
INSERT INTO `user_grup` VALUES ('964', '0118', '7', '10');
INSERT INTO `user_grup` VALUES ('271', '0081', '14', '4');
INSERT INTO `user_grup` VALUES ('22', '0167', '21', '19');
INSERT INTO `user_grup` VALUES ('23', '0065', '14', '4');
INSERT INTO `user_grup` VALUES ('24', '0012', '3', '3');
INSERT INTO `user_grup` VALUES ('316', '0119', '14', '4');
INSERT INTO `user_grup` VALUES ('774', '0142', '13', '3');
INSERT INTO `user_grup` VALUES ('942', '0240', '1', '7');
INSERT INTO `user_grup` VALUES ('860', '0143', '1', '3');
INSERT INTO `user_grup` VALUES ('901', '0269', '13', '3');
INSERT INTO `user_grup` VALUES ('30', '0259', '1', '1');
INSERT INTO `user_grup` VALUES ('975', '0060', '21', '19');
INSERT INTO `user_grup` VALUES ('314', '0069', '13', '7');
INSERT INTO `user_grup` VALUES ('849', '0257', '13', '3');
INSERT INTO `user_grup` VALUES ('34', '0233', '3', '3');
INSERT INTO `user_grup` VALUES ('921', '0981', '1', '3');
INSERT INTO `user_grup` VALUES ('578', '0333', '12', '11');
INSERT INTO `user_grup` VALUES ('842', '0999', '19', '1');
INSERT INTO `user_grup` VALUES ('398', '0163', '19', '14');
INSERT INTO `user_grup` VALUES ('272', '0043', '3', '18');
INSERT INTO `user_grup` VALUES ('40', '0003', '1', '1');
INSERT INTO `user_grup` VALUES ('448', '0298', '1', '4');
INSERT INTO `user_grup` VALUES ('42', '0308', '14', '4');
INSERT INTO `user_grup` VALUES ('43', '0059', '12', '11');
INSERT INTO `user_grup` VALUES ('931', '0093', '12', '11');
INSERT INTO `user_grup` VALUES ('974', '1335', '21', '19');
INSERT INTO `user_grup` VALUES ('341', '0061', '14', '4');
INSERT INTO `user_grup` VALUES ('373', '0193', '14', '19');
INSERT INTO `user_grup` VALUES ('265', '0082', '14', '12');
INSERT INTO `user_grup` VALUES ('841', '0999', '19', '3');
INSERT INTO `user_grup` VALUES ('50', '0155', '3', '3');
INSERT INTO `user_grup` VALUES ('966', '0087', '3', '18');
INSERT INTO `user_grup` VALUES ('627', '0260', '3', '3');
INSERT INTO `user_grup` VALUES ('761', '0131', '3', '18');
INSERT INTO `user_grup` VALUES ('54', '0152', '3', '3');
INSERT INTO `user_grup` VALUES ('55', '0172', '1', '1');
INSERT INTO `user_grup` VALUES ('56', '0149', '3', '3');
INSERT INTO `user_grup` VALUES ('57', '0150', '3', '3');
INSERT INTO `user_grup` VALUES ('322', '0311', '3', '3');
INSERT INTO `user_grup` VALUES ('343', '0253', '21', '19');
INSERT INTO `user_grup` VALUES ('903', '0021', '4', '2');
INSERT INTO `user_grup` VALUES ('61', '0246', '4', '2');
INSERT INTO `user_grup` VALUES ('62', '0307', '4', '2');
INSERT INTO `user_grup` VALUES ('63', '0342', '4', '2');
INSERT INTO `user_grup` VALUES ('701', '0185', '12', '11');
INSERT INTO `user_grup` VALUES ('672', '0343', '13', '18');
INSERT INTO `user_grup` VALUES ('66', '0169', '22', '1');
INSERT INTO `user_grup` VALUES ('67', '0239', '17', '5');
INSERT INTO `user_grup` VALUES ('68', '0300', '17', '5');
INSERT INTO `user_grup` VALUES ('69', '0326', '23', '5');
INSERT INTO `user_grup` VALUES ('70', '0252', '2', '6');
INSERT INTO `user_grup` VALUES ('71', '0194', '2', '6');
INSERT INTO `user_grup` VALUES ('72', '0100', '2', '6');
INSERT INTO `user_grup` VALUES ('73', '0122', '2', '6');
INSERT INTO `user_grup` VALUES ('74', '0286', '3', '3');
INSERT INTO `user_grup` VALUES ('75', '0097', '17', '5');
INSERT INTO `user_grup` VALUES ('76', '0278', '17', '5');
INSERT INTO `user_grup` VALUES ('662', '0029', '3', '3');
INSERT INTO `user_grup` VALUES ('357', '0344', '3', '3');
INSERT INTO `user_grup` VALUES ('80', '0289', '14', '4');
INSERT INTO `user_grup` VALUES ('325', '0116', '14', '4');
INSERT INTO `user_grup` VALUES ('82', '0346', '23', '5');
INSERT INTO `user_grup` VALUES ('278', '0347', '3', '3');
INSERT INTO `user_grup` VALUES ('439', '0173', '3', '3');
INSERT INTO `user_grup` VALUES ('85', '0256', '12', '11');
INSERT INTO `user_grup` VALUES ('257', '0077', '13', '7');
INSERT INTO `user_grup` VALUES ('641', '0522', '1', '3');
INSERT INTO `user_grup` VALUES ('88', '0038', '2', '6');
INSERT INTO `user_grup` VALUES ('89', '0349', '2', '6');
INSERT INTO `user_grup` VALUES ('90', '0006', '2', '6');
INSERT INTO `user_grup` VALUES ('91', '0195', '2', '6');
INSERT INTO `user_grup` VALUES ('92', '0251', '2', '6');
INSERT INTO `user_grup` VALUES ('93', '0351', '2', '6');
INSERT INTO `user_grup` VALUES ('94', '0174', '2', '6');
INSERT INTO `user_grup` VALUES ('95', '0121', '16', '15');
INSERT INTO `user_grup` VALUES ('96', '0086', '16', '15');
INSERT INTO `user_grup` VALUES ('542', '0124', '14', '19');
INSERT INTO `user_grup` VALUES ('98', '0263', '3', '3');
INSERT INTO `user_grup` VALUES ('442', '0523', '4', '2');
INSERT INTO `user_grup` VALUES ('100', '0111', '3', '3');
INSERT INTO `user_grup` VALUES ('101', '0309', '3', '3');
INSERT INTO `user_grup` VALUES ('102', '0310', '3', '3');
INSERT INTO `user_grup` VALUES ('103', '0279', '21', '19');
INSERT INTO `user_grup` VALUES ('104', '0035', '3', '3');
INSERT INTO `user_grup` VALUES ('105', '0314', '16', '15');
INSERT INTO `user_grup` VALUES ('106', '0287', '3', '3');
INSERT INTO `user_grup` VALUES ('107', '0354', '1', '1');
INSERT INTO `user_grup` VALUES ('438', '0022', '5', '2');
INSERT INTO `user_grup` VALUES ('553', '0115', '14', '4');
INSERT INTO `user_grup` VALUES ('110', '0280', '4', '2');
INSERT INTO `user_grup` VALUES ('111', '0357', '4', '2');
INSERT INTO `user_grup` VALUES ('112', '0046', '8', '17');
INSERT INTO `user_grup` VALUES ('280', '0054', '9', '8');
INSERT INTO `user_grup` VALUES ('114', '0175', '9', '8');
INSERT INTO `user_grup` VALUES ('115', '0176', '9', '8');
INSERT INTO `user_grup` VALUES ('116', '0177', '9', '8');
INSERT INTO `user_grup` VALUES ('789', '0073', '12', '11');
INSERT INTO `user_grup` VALUES ('781', '0361', '1', '18');
INSERT INTO `user_grup` VALUES ('546', '0362', '12', '4');
INSERT INTO `user_grup` VALUES ('617', '0340', '22', '18');
INSERT INTO `user_grup` VALUES ('121', '0364', '1', '1');
INSERT INTO `user_grup` VALUES ('122', '0365', '4', '2');
INSERT INTO `user_grup` VALUES ('123', '0366', '4', '2');
INSERT INTO `user_grup` VALUES ('124', '0367', '4', '2');
INSERT INTO `user_grup` VALUES ('125', '0202', '3', '3');
INSERT INTO `user_grup` VALUES ('126', '0368', '12', '11');
INSERT INTO `user_grup` VALUES ('784', '0369', '13', '18');
INSERT INTO `user_grup` VALUES ('259', '0370', '13', '7');
INSERT INTO `user_grup` VALUES ('246', '0371', '12', '11');
INSERT INTO `user_grup` VALUES ('130', '0372', '4', '2');
INSERT INTO `user_grup` VALUES ('131', '0373', '14', '4');
INSERT INTO `user_grup` VALUES ('132', '0374', '3', '3');
INSERT INTO `user_grup` VALUES ('375', '0193', '14', '4');
INSERT INTO `user_grup` VALUES ('663', '0243', '3', '3');
INSERT INTO `user_grup` VALUES ('135', '012345', '19', '14');
INSERT INTO `user_grup` VALUES ('281', '0054', '9', '3');
INSERT INTO `user_grup` VALUES ('332', '0140', '1', '7');
INSERT INTO `user_grup` VALUES ('640', '0522', '1', '4');
INSERT INTO `user_grup` VALUES ('639', '0522', '1', '18');
INSERT INTO `user_grup` VALUES ('166', '12345', '19', '14');
INSERT INTO `user_grup` VALUES ('173', '0137', '19', '14');
INSERT INTO `user_grup` VALUES ('840', '0999', '19', '4');
INSERT INTO `user_grup` VALUES ('839', '0999', '19', '5');
INSERT INTO `user_grup` VALUES ('838', '0999', '19', '18');
INSERT INTO `user_grup` VALUES ('837', '0999', '19', '12');
INSERT INTO `user_grup` VALUES ('836', '0999', '19', '16');
INSERT INTO `user_grup` VALUES ('835', '0999', '19', '6');
INSERT INTO `user_grup` VALUES ('834', '0999', '19', '10');
INSERT INTO `user_grup` VALUES ('833', '0999', '19', '11');
INSERT INTO `user_grup` VALUES ('194', '6666', '0', '14');
INSERT INTO `user_grup` VALUES ('195', '6666', '0', '8');
INSERT INTO `user_grup` VALUES ('196', '6666', '0', '9');
INSERT INTO `user_grup` VALUES ('197', '6666', '0', '15');
INSERT INTO `user_grup` VALUES ('198', '6666', '0', '2');
INSERT INTO `user_grup` VALUES ('199', '6666', '0', '19');
INSERT INTO `user_grup` VALUES ('200', '6666', '0', '17');
INSERT INTO `user_grup` VALUES ('201', '6666', '0', '7');
INSERT INTO `user_grup` VALUES ('202', '6666', '0', '11');
INSERT INTO `user_grup` VALUES ('203', '6666', '0', '10');
INSERT INTO `user_grup` VALUES ('204', '6666', '0', '6');
INSERT INTO `user_grup` VALUES ('205', '6666', '0', '16');
INSERT INTO `user_grup` VALUES ('206', '6666', '0', '12');
INSERT INTO `user_grup` VALUES ('207', '6666', '0', '18');
INSERT INTO `user_grup` VALUES ('208', '6666', '0', '5');
INSERT INTO `user_grup` VALUES ('209', '6666', '0', '4');
INSERT INTO `user_grup` VALUES ('210', '6666', '0', '3');
INSERT INTO `user_grup` VALUES ('211', '6666', '0', '1');
INSERT INTO `user_grup` VALUES ('212', '6666', '0', '13');
INSERT INTO `user_grup` VALUES ('832', '0999', '19', '7');
INSERT INTO `user_grup` VALUES ('831', '0999', '19', '21');
INSERT INTO `user_grup` VALUES ('830', '0999', '19', '17');
INSERT INTO `user_grup` VALUES ('829', '0999', '19', '19');
INSERT INTO `user_grup` VALUES ('828', '0999', '19', '2');
INSERT INTO `user_grup` VALUES ('827', '0999', '19', '15');
INSERT INTO `user_grup` VALUES ('826', '0999', '19', '9');
INSERT INTO `user_grup` VALUES ('825', '0999', '19', '20');
INSERT INTO `user_grup` VALUES ('824', '0999', '19', '8');
INSERT INTO `user_grup` VALUES ('823', '0999', '19', '14');
INSERT INTO `user_grup` VALUES ('282', '0290', '3', '3');
INSERT INTO `user_grup` VALUES ('783', '0369', '13', '7');
INSERT INTO `user_grup` VALUES ('248', '0524', '4', '2');
INSERT INTO `user_grup` VALUES ('247', '0515', '4', '2');
INSERT INTO `user_grup` VALUES ('943', '0240', '1', '18');
INSERT INTO `user_grup` VALUES ('944', '0240', '1', '1');
INSERT INTO `user_grup` VALUES ('258', '0077', '13', '18');
INSERT INTO `user_grup` VALUES ('260', '0370', '13', '18');
INSERT INTO `user_grup` VALUES ('262', '0302', '3', '3');
INSERT INTO `user_grup` VALUES ('965', '0087', '3', '11');
INSERT INTO `user_grup` VALUES ('266', '0082', '14', '4');
INSERT INTO `user_grup` VALUES ('269', '0516', '4', '2');
INSERT INTO `user_grup` VALUES ('273', '0043', '3', '3');
INSERT INTO `user_grup` VALUES ('976', '0997', '4', '2');
INSERT INTO `user_grup` VALUES ('579', '0276', '1', '7');
INSERT INTO `user_grup` VALUES ('929', '0525', '12', '3');
INSERT INTO `user_grup` VALUES ('315', '0069', '13', '18');
INSERT INTO `user_grup` VALUES ('953', '0058', '12', '18');
INSERT INTO `user_grup` VALUES ('979', '0473', '14', '2');
INSERT INTO `user_grup` VALUES ('381', '0166', '21', '19');
INSERT INTO `user_grup` VALUES ('998', '0136', '14', '3');
INSERT INTO `user_grup` VALUES ('334', '0140', '1', '4');
INSERT INTO `user_grup` VALUES ('335', '0140', '1', '1');
INSERT INTO `user_grup` VALUES ('980', '0473', '14', '11');
INSERT INTO `user_grup` VALUES ('342', '0061', '14', '3');
INSERT INTO `user_grup` VALUES ('981', '0473', '14', '4');
INSERT INTO `user_grup` VALUES ('577', '0333', '12', '19');
INSERT INTO `user_grup` VALUES ('997', '0831', '12', '10');
INSERT INTO `user_grup` VALUES ('362', '0265', '3', '3');
INSERT INTO `user_grup` VALUES ('969', '0179', '3', '18');
INSERT INTO `user_grup` VALUES ('968', '0179', '3', '11');
INSERT INTO `user_grup` VALUES ('365', '0061', '14', '20');
INSERT INTO `user_grup` VALUES ('366', '0061', '14', '4');
INSERT INTO `user_grup` VALUES ('367', '0061', '14', '3');
INSERT INTO `user_grup` VALUES ('376', '0193', '14', '3');
INSERT INTO `user_grup` VALUES ('996', '0831', '12', '11');
INSERT INTO `user_grup` VALUES ('995', '0831', '12', '19');
INSERT INTO `user_grup` VALUES ('988', '0375', '12', '11');
INSERT INTO `user_grup` VALUES ('384', '0166', '21', '3');
INSERT INTO `user_grup` VALUES ('695', '0057', '10', '11');
INSERT INTO `user_grup` VALUES ('395', '0078', '21', '19');
INSERT INTO `user_grup` VALUES ('958', '0080', '11', '10');
INSERT INTO `user_grup` VALUES ('447', '0298', '1', '18');
INSERT INTO `user_grup` VALUES ('396', '0078', '21', '18');
INSERT INTO `user_grup` VALUES ('399', '0163', '19', '8');
INSERT INTO `user_grup` VALUES ('400', '0163', '19', '20');
INSERT INTO `user_grup` VALUES ('401', '0163', '19', '9');
INSERT INTO `user_grup` VALUES ('402', '0163', '19', '15');
INSERT INTO `user_grup` VALUES ('403', '0163', '19', '2');
INSERT INTO `user_grup` VALUES ('404', '0163', '19', '19');
INSERT INTO `user_grup` VALUES ('405', '0163', '19', '17');
INSERT INTO `user_grup` VALUES ('406', '0163', '19', '7');
INSERT INTO `user_grup` VALUES ('407', '0163', '19', '11');
INSERT INTO `user_grup` VALUES ('408', '0163', '19', '10');
INSERT INTO `user_grup` VALUES ('409', '0163', '19', '6');
INSERT INTO `user_grup` VALUES ('410', '0163', '19', '16');
INSERT INTO `user_grup` VALUES ('411', '0163', '19', '12');
INSERT INTO `user_grup` VALUES ('412', '0163', '19', '18');
INSERT INTO `user_grup` VALUES ('413', '0163', '19', '5');
INSERT INTO `user_grup` VALUES ('414', '0163', '19', '4');
INSERT INTO `user_grup` VALUES ('415', '0163', '19', '3');
INSERT INTO `user_grup` VALUES ('416', '0163', '19', '1');
INSERT INTO `user_grup` VALUES ('417', '0163', '19', '13');
INSERT INTO `user_grup` VALUES ('479', '0189', '3', '3');
INSERT INTO `user_grup` VALUES ('446', '0298', '1', '7');
INSERT INTO `user_grup` VALUES ('449', '0298', '1', '3');
INSERT INTO `user_grup` VALUES ('450', '0298', '1', '1');
INSERT INTO `user_grup` VALUES ('700', '0094', '14', '4');
INSERT INTO `user_grup` VALUES ('457', '0601', '11', '3');
INSERT INTO `user_grup` VALUES ('780', '0361', '1', '7');
INSERT INTO `user_grup` VALUES ('544', '0124', '14', '4');
INSERT INTO `user_grup` VALUES ('543', '0124', '14', '11');
INSERT INTO `user_grup` VALUES ('541', '0527', '16', '15');
INSERT INTO `user_grup` VALUES ('480', '0528', '23', '5');
INSERT INTO `user_grup` VALUES ('520', '0998', '19', '13');
INSERT INTO `user_grup` VALUES ('519', '0998', '19', '1');
INSERT INTO `user_grup` VALUES ('518', '0998', '19', '3');
INSERT INTO `user_grup` VALUES ('517', '0998', '19', '4');
INSERT INTO `user_grup` VALUES ('516', '0998', '19', '5');
INSERT INTO `user_grup` VALUES ('515', '0998', '19', '18');
INSERT INTO `user_grup` VALUES ('514', '0998', '19', '12');
INSERT INTO `user_grup` VALUES ('513', '0998', '19', '16');
INSERT INTO `user_grup` VALUES ('512', '0998', '19', '6');
INSERT INTO `user_grup` VALUES ('511', '0998', '19', '10');
INSERT INTO `user_grup` VALUES ('510', '0998', '19', '11');
INSERT INTO `user_grup` VALUES ('509', '0998', '19', '7');
INSERT INTO `user_grup` VALUES ('508', '0998', '19', '17');
INSERT INTO `user_grup` VALUES ('507', '0998', '19', '19');
INSERT INTO `user_grup` VALUES ('506', '0998', '19', '2');
INSERT INTO `user_grup` VALUES ('505', '0998', '19', '15');
INSERT INTO `user_grup` VALUES ('504', '0998', '19', '9');
INSERT INTO `user_grup` VALUES ('503', '0998', '19', '20');
INSERT INTO `user_grup` VALUES ('502', '0998', '19', '8');
INSERT INTO `user_grup` VALUES ('501', '0998', '19', '14');
INSERT INTO `user_grup` VALUES ('554', '0115', '14', '3');
INSERT INTO `user_grup` VALUES ('930', '0093', '12', '19');
INSERT INTO `user_grup` VALUES ('558', '0156', '14', '18');
INSERT INTO `user_grup` VALUES ('559', '0156', '14', '3');
INSERT INTO `user_grup` VALUES ('560', '0156', '14', '1');
INSERT INTO `user_grup` VALUES ('745', '0531', '1', '3');
INSERT INTO `user_grup` VALUES ('744', '0531', '1', '4');
INSERT INTO `user_grup` VALUES ('743', '0531', '1', '18');
INSERT INTO `user_grup` VALUES ('742', '0531', '1', '7');
INSERT INTO `user_grup` VALUES ('565', '0530', '1', '7');
INSERT INTO `user_grup` VALUES ('566', '0530', '1', '18');
INSERT INTO `user_grup` VALUES ('567', '0530', '1', '3');
INSERT INTO `user_grup` VALUES ('568', '0530', '1', '1');
INSERT INTO `user_grup` VALUES ('736', '0532', '1', '3');
INSERT INTO `user_grup` VALUES ('735', '0532', '1', '4');
INSERT INTO `user_grup` VALUES ('734', '0532', '1', '18');
INSERT INTO `user_grup` VALUES ('733', '0532', '1', '7');
INSERT INTO `user_grup` VALUES ('573', '0533', '13', '7');
INSERT INTO `user_grup` VALUES ('574', '0533', '13', '18');
INSERT INTO `user_grup` VALUES ('575', '0533', '13', '3');
INSERT INTO `user_grup` VALUES ('576', '0533', '13', '1');
INSERT INTO `user_grup` VALUES ('582', '0276', '1', '1');
INSERT INTO `user_grup` VALUES ('731', '1000', '19', '1');
INSERT INTO `user_grup` VALUES ('730', '1000', '19', '3');
INSERT INTO `user_grup` VALUES ('729', '1000', '19', '4');
INSERT INTO `user_grup` VALUES ('728', '1000', '19', '5');
INSERT INTO `user_grup` VALUES ('727', '1000', '19', '18');
INSERT INTO `user_grup` VALUES ('726', '1000', '19', '12');
INSERT INTO `user_grup` VALUES ('725', '1000', '19', '16');
INSERT INTO `user_grup` VALUES ('724', '1000', '19', '6');
INSERT INTO `user_grup` VALUES ('723', '1000', '19', '10');
INSERT INTO `user_grup` VALUES ('722', '1000', '19', '11');
INSERT INTO `user_grup` VALUES ('721', '1000', '19', '7');
INSERT INTO `user_grup` VALUES ('720', '1000', '19', '21');
INSERT INTO `user_grup` VALUES ('719', '1000', '19', '17');
INSERT INTO `user_grup` VALUES ('718', '1000', '19', '19');
INSERT INTO `user_grup` VALUES ('717', '1000', '19', '2');
INSERT INTO `user_grup` VALUES ('716', '1000', '19', '15');
INSERT INTO `user_grup` VALUES ('715', '1000', '19', '9');
INSERT INTO `user_grup` VALUES ('714', '1000', '19', '20');
INSERT INTO `user_grup` VALUES ('713', '1000', '19', '8');
INSERT INTO `user_grup` VALUES ('712', '1000', '19', '14');
INSERT INTO `user_grup` VALUES ('638', '0522', '1', '7');
INSERT INTO `user_grup` VALUES ('618', '0340', '22', '4');
INSERT INTO `user_grup` VALUES ('619', '0340', '22', '3');
INSERT INTO `user_grup` VALUES ('620', '0340', '22', '1');
INSERT INTO `user_grup` VALUES ('621', '0520', '17', '20');
INSERT INTO `user_grup` VALUES ('622', '0520', '17', '7');
INSERT INTO `user_grup` VALUES ('623', '0520', '17', '5');
INSERT INTO `user_grup` VALUES ('624', '0520', '17', '4');
INSERT INTO `user_grup` VALUES ('625', '0520', '17', '3');
INSERT INTO `user_grup` VALUES ('628', '0534', '3', '7');
INSERT INTO `user_grup` VALUES ('629', '0534', '3', '18');
INSERT INTO `user_grup` VALUES ('630', '0534', '3', '3');
INSERT INTO `user_grup` VALUES ('957', '0080', '11', '11');
INSERT INTO `user_grup` VALUES ('646', '0990', '3', '18');
INSERT INTO `user_grup` VALUES ('647', '0990', '3', '3');
INSERT INTO `user_grup` VALUES ('926', '0982', '1', '1');
INSERT INTO `user_grup` VALUES ('922', '0981', '1', '1');
INSERT INTO `user_grup` VALUES ('990', '0967', '3', '3');
INSERT INTO `user_grup` VALUES ('989', '0967', '3', '18');
INSERT INTO `user_grup` VALUES ('652', '0536', '1', '18');
INSERT INTO `user_grup` VALUES ('653', '0536', '1', '4');
INSERT INTO `user_grup` VALUES ('654', '0536', '1', '3');
INSERT INTO `user_grup` VALUES ('655', '0536', '1', '1');
INSERT INTO `user_grup` VALUES ('814', '0066', '13', '4');
INSERT INTO `user_grup` VALUES ('813', '0066', '13', '18');
INSERT INTO `user_grup` VALUES ('773', '0142', '13', '4');
INSERT INTO `user_grup` VALUES ('772', '0142', '13', '18');
INSERT INTO `user_grup` VALUES ('994', '0537', '1', '1');
INSERT INTO `user_grup` VALUES ('993', '0537', '1', '3');
INSERT INTO `user_grup` VALUES ('992', '0537', '1', '4');
INSERT INTO `user_grup` VALUES ('991', '0537', '1', '18');
INSERT INTO `user_grup` VALUES ('788', '0073', '12', '7');
INSERT INTO `user_grup` VALUES ('900', '0269', '13', '4');
INSERT INTO `user_grup` VALUES ('906', '0330', '12', '4');
INSERT INTO `user_grup` VALUES ('928', '0525', '12', '18');
INSERT INTO `user_grup` VALUES ('952', '0058', '12', '10');
INSERT INTO `user_grup` VALUES ('951', '0058', '12', '11');
INSERT INTO `user_grup` VALUES ('950', '0058', '12', '19');
INSERT INTO `user_grup` VALUES ('697', '0057', '10', '18');
INSERT INTO `user_grup` VALUES ('698', '0057', '10', '4');
INSERT INTO `user_grup` VALUES ('699', '0057', '10', '3');
INSERT INTO `user_grup` VALUES ('702', '0185', '12', '10');
INSERT INTO `user_grup` VALUES ('703', '0103', '4', '2');
INSERT INTO `user_grup` VALUES ('704', '0103', '4', '4');
INSERT INTO `user_grup` VALUES ('705', '0103', '4', '3');
INSERT INTO `user_grup` VALUES ('706', '0104', '4', '2');
INSERT INTO `user_grup` VALUES ('707', '0104', '4', '4');
INSERT INTO `user_grup` VALUES ('708', '0104', '4', '3');
INSERT INTO `user_grup` VALUES ('709', '0612', '4', '2');
INSERT INTO `user_grup` VALUES ('710', '0612', '4', '4');
INSERT INTO `user_grup` VALUES ('711', '0612', '4', '3');
INSERT INTO `user_grup` VALUES ('732', '1000', '19', '13');
INSERT INTO `user_grup` VALUES ('737', '0532', '1', '1');
INSERT INTO `user_grup` VALUES ('771', '0142', '13', '7');
INSERT INTO `user_grup` VALUES ('746', '0531', '1', '1');
INSERT INTO `user_grup` VALUES ('876', '1234', '19', '15');
INSERT INTO `user_grup` VALUES ('875', '1234', '19', '9');
INSERT INTO `user_grup` VALUES ('874', '1234', '19', '20');
INSERT INTO `user_grup` VALUES ('873', '1234', '19', '8');
INSERT INTO `user_grup` VALUES ('872', '1234', '19', '14');
INSERT INTO `user_grup` VALUES ('752', '0614', '14', '20');
INSERT INTO `user_grup` VALUES ('753', '0614', '14', '11');
INSERT INTO `user_grup` VALUES ('754', '0614', '14', '10');
INSERT INTO `user_grup` VALUES ('755', '0614', '14', '18');
INSERT INTO `user_grup` VALUES ('756', '0614', '14', '4');
INSERT INTO `user_grup` VALUES ('757', '0614', '14', '3');
INSERT INTO `user_grup` VALUES ('898', '0028', '14', '3');
INSERT INTO `user_grup` VALUES ('897', '0028', '14', '4');
INSERT INTO `user_grup` VALUES ('896', '0028', '14', '18');
INSERT INTO `user_grup` VALUES ('762', '0131', '3', '3');
INSERT INTO `user_grup` VALUES ('956', '0080', '11', '19');
INSERT INTO `user_grup` VALUES ('775', '0142', '13', '1');
INSERT INTO `user_grup` VALUES ('776', '0102', '4', '2');
INSERT INTO `user_grup` VALUES ('777', '0102', '4', '18');
INSERT INTO `user_grup` VALUES ('778', '0102', '4', '4');
INSERT INTO `user_grup` VALUES ('779', '0102', '4', '3');
INSERT INTO `user_grup` VALUES ('782', '0361', '1', '1');
INSERT INTO `user_grup` VALUES ('785', '0369', '13', '4');
INSERT INTO `user_grup` VALUES ('790', '0992', '4', '2');
INSERT INTO `user_grup` VALUES ('791', '0992', '4', '11');
INSERT INTO `user_grup` VALUES ('792', '0992', '4', '4');
INSERT INTO `user_grup` VALUES ('793', '0992', '4', '3');
INSERT INTO `user_grup` VALUES ('794', '0993', '4', '2');
INSERT INTO `user_grup` VALUES ('795', '0993', '4', '11');
INSERT INTO `user_grup` VALUES ('796', '0993', '4', '4');
INSERT INTO `user_grup` VALUES ('797', '0993', '4', '3');
INSERT INTO `user_grup` VALUES ('798', '0994', '4', '2');
INSERT INTO `user_grup` VALUES ('799', '0994', '4', '4');
INSERT INTO `user_grup` VALUES ('800', '0994', '4', '3');
INSERT INTO `user_grup` VALUES ('801', '0995', '4', '2');
INSERT INTO `user_grup` VALUES ('802', '0995', '4', '4');
INSERT INTO `user_grup` VALUES ('803', '0995', '4', '3');
INSERT INTO `user_grup` VALUES ('804', '0996', '4', '2');
INSERT INTO `user_grup` VALUES ('805', '0996', '4', '4');
INSERT INTO `user_grup` VALUES ('806', '0996', '4', '3');
INSERT INTO `user_grup` VALUES ('807', '0997	', '4', '2');
INSERT INTO `user_grup` VALUES ('808', '0997	', '4', '4');
INSERT INTO `user_grup` VALUES ('809', '0997	', '4', '3');
INSERT INTO `user_grup` VALUES ('810', '0898', '4', '2');
INSERT INTO `user_grup` VALUES ('811', '0898', '4', '4');
INSERT INTO `user_grup` VALUES ('812', '0898', '4', '3');
INSERT INTO `user_grup` VALUES ('816', '0066', '13', '1');
INSERT INTO `user_grup` VALUES ('817', '0405', '14', '7');
INSERT INTO `user_grup` VALUES ('818', '0405', '14', '4');
INSERT INTO `user_grup` VALUES ('819', '0405', '14', '3');
INSERT INTO `user_grup` VALUES ('853', '0801', '21', '10');
INSERT INTO `user_grup` VALUES ('852', '0801', '21', '19');
INSERT INTO `user_grup` VALUES ('851', '0801', '21', '20');
INSERT INTO `user_grup` VALUES ('843', '0999', '19', '13');
INSERT INTO `user_grup` VALUES ('848', '0257', '13', '18');
INSERT INTO `user_grup` VALUES ('850', '0257', '13', '1');
INSERT INTO `user_grup` VALUES ('854', '0801', '21', '18');
INSERT INTO `user_grup` VALUES ('855', '0802', '4', '2');
INSERT INTO `user_grup` VALUES ('856', '0802', '4', '4');
INSERT INTO `user_grup` VALUES ('859', '0143', '1', '18');
INSERT INTO `user_grup` VALUES ('861', '0143', '1', '1');
INSERT INTO `user_grup` VALUES ('871', '0712', '14', '4');
INSERT INTO `user_grup` VALUES ('870', '0712', '14', '18');
INSERT INTO `user_grup` VALUES ('869', '0712', '14', '10');
INSERT INTO `user_grup` VALUES ('868', '0712', '14', '11');
INSERT INTO `user_grup` VALUES ('867', '0712', '14', '7');
INSERT INTO `user_grup` VALUES ('877', '1234', '19', '2');
INSERT INTO `user_grup` VALUES ('878', '1234', '19', '19');
INSERT INTO `user_grup` VALUES ('879', '1234', '19', '17');
INSERT INTO `user_grup` VALUES ('880', '1234', '19', '21');
INSERT INTO `user_grup` VALUES ('881', '1234', '19', '7');
INSERT INTO `user_grup` VALUES ('882', '1234', '19', '11');
INSERT INTO `user_grup` VALUES ('883', '1234', '19', '10');
INSERT INTO `user_grup` VALUES ('884', '1234', '19', '6');
INSERT INTO `user_grup` VALUES ('885', '1234', '19', '16');
INSERT INTO `user_grup` VALUES ('886', '1234', '19', '12');
INSERT INTO `user_grup` VALUES ('887', '1234', '19', '18');
INSERT INTO `user_grup` VALUES ('888', '1234', '19', '5');
INSERT INTO `user_grup` VALUES ('889', '1234', '19', '4');
INSERT INTO `user_grup` VALUES ('890', '1234', '19', '3');
INSERT INTO `user_grup` VALUES ('891', '1234', '19', '1');
INSERT INTO `user_grup` VALUES ('892', '1234', '19', '13');
INSERT INTO `user_grup` VALUES ('899', '0269', '13', '7');
INSERT INTO `user_grup` VALUES ('902', '0269', '13', '1');
INSERT INTO `user_grup` VALUES ('904', '0789', '2', '6');
INSERT INTO `user_grup` VALUES ('905', '0788', '2', '6');
INSERT INTO `user_grup` VALUES ('920', '0981', '1', '18');
INSERT INTO `user_grup` VALUES ('925', '0982', '1', '3');
INSERT INTO `user_grup` VALUES ('924', '0982', '1', '18');
INSERT INTO `user_grup` VALUES ('923', '0982', '1', '7');
INSERT INTO `user_grup` VALUES ('919', '0981', '1', '7');
INSERT INTO `user_grup` VALUES ('932', '0093', '12', '10');
INSERT INTO `user_grup` VALUES ('973', '1335', '21', '20');
INSERT INTO `user_grup` VALUES ('935', '0406', '2', '6');
INSERT INTO `user_grup` VALUES ('936', '0407', '2', '6');
INSERT INTO `user_grup` VALUES ('937', '0408', '2', '6');
INSERT INTO `user_grup` VALUES ('945', '0409', '2', '6');
INSERT INTO `user_grup` VALUES ('946', '0410', '2', '6');
INSERT INTO `user_grup` VALUES ('947', '0411', '2', '6');
INSERT INTO `user_grup` VALUES ('948', '0412', '2', '6');
INSERT INTO `user_grup` VALUES ('949', '0413', '2', '6');
INSERT INTO `user_grup` VALUES ('955', '0058', '12', '3');
INSERT INTO `user_grup` VALUES ('960', '0080', '11', '4');
INSERT INTO `user_grup` VALUES ('963', '0118', '7', '11');
INSERT INTO `user_grup` VALUES ('967', '0087', '3', '3');
INSERT INTO `user_grup` VALUES ('970', '0179', '3', '3');
