<?php

use Illuminate\Database\Seeder;
use App\Sm_meta;

class MetasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $metas = array(
                ['parameter' => 'VOICE', 'description' => '', 'link' => '', 'value' => 1],
                ['parameter' => 'FIRST', 'description' => 'BPJS Lantai 1', 'link' => 'B1.', 'value' => 2],
                ['parameter' => 'FIRST', 'description' => 'BPJS Lantai 1', 'link' => 'B2.', 'value' => 3],
                ['parameter' => 'FIRST', 'description' => 'BPJS Lantai 2', 'link' => 'A', 'value' => 4],
                ['parameter' => 'FIRST', 'description' => 'SKTM', 'link' => 'C', 'value' => 5],
                ['parameter' => 'FIRST', 'description' => 'UMUM', 'link' => 'D', 'value' => 6]
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($metas as $meta)
        {
            Sm_meta::create($meta);
        }
    }
}
