<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = ['username' => 'admin', 'email' => 'user1@teknolands.com', 'password' => bcrypt('1234'), 'empid' => '123456', 'empname' => 'admin'];
        \Illuminate\Support\Facades\DB::table('users')->insert([$admin]);

        $role1 = ['name' => 'administrator', 'display_name' => 'Administrator', 'description' => 'Role Administrator'];
        $role2 = ['name' => 'bpjs_lt_1', 'display_name' => 'BPJS Lt 1', 'description' => 'Role BPJS Lt 1'];
        $role3 = ['name' => 'bpjs_lt_2', 'display_name' => 'BPJS Lt 2', 'description' => 'Role BPJS Lt 2'];
        $role4 = ['name' => 'sktm', 'display_name' => 'SKTM', 'description' => 'Role SKTM'];
        $role5 = ['name' => 'umum', 'display_name' => 'Umum', 'description' => 'Role Umum'];

        \Illuminate\Support\Facades\DB::table('roles')->insert([$role1, $role2, $role3, $role4, $role5]);

        $admin = \App\User::where('username', 'admin')->first();
        $role_admin = \App\Role::where('name', 'administrator')->first();
        $admin->attachRole($role_admin);

        $As = array(
                ['username' => 'loketb1.1', 'password' => bcrypt('1234'), 'empid' => '21', 'empname' => 'Loket B1.1', 'loket_name' => 'Loket B1.1', 'userlevel' => '2'],
                ['username' => 'loketb1.2', 'password' => bcrypt('1234'), 'empid' => '22', 'empname' => 'Loket B1.2', 'loket_name' => 'Loket B1.2', 'userlevel' => '2'],
                ['username' => 'loketb2.1', 'password' => bcrypt('1234'), 'empid' => '23', 'empname' => 'Loket B2.1', 'loket_name' => 'Loket B2.1', 'userlevel' => '3'],
                ['username' => 'loketb2.2', 'password' => bcrypt('1234'), 'empid' => '24', 'empname' => 'Loket B2.2', 'loket_name' => 'Loket B2.2', 'userlevel' => '3']
        );
                    
        foreach ($As as $A)
        {
            $a = \App\User::create($A);
            $role_a = \App\Role::where('name', 'bpjs_lt_1')->first();
            $userA = \App\User::where('username', $A['username'])->first();
            $userA->attachRole($role_a);
        }

        $Bs = array(
                ['username' => 'loketa1', 'password' => bcrypt('1234'), 'empid' => '31', 'empname' => 'Loket A1', 'loket_name' => 'Loket A1', 'userlevel' => '4'],
                ['username' => 'loketa2', 'password' => bcrypt('1234'), 'empid' => '32', 'empname' => 'Loket A2', 'loket_name' => 'Loket A2', 'userlevel' => '4']
        );
                    
        foreach ($Bs as $B)
        {
            $b = \App\User::create($B);
            $role_b = \App\Role::where('name', 'bpjs_lt_2')->first();
            $userB = \App\User::where('username', $B['username'])->first();
            $userB->attachRole($role_b);
        }

        $Cs = array(
                ['username' => 'loketc1', 'password' => bcrypt('1234'), 'empid' => '41', 'empname' => 'Loket C1', 'loket_name' => 'Loket C1', 'userlevel' => '5']
        );
                    
        foreach ($Cs as $C)
        {
            $c = \App\User::create($C);
            $role_c = \App\Role::where('name', 'sktm')->first();
            $userC = \App\User::where('username', $C['username'])->first();
            $userC->attachRole($role_c);
        }

        $Ds = array(
                ['username' => 'loketd1', 'password' => bcrypt('1234'), 'empid' => '51', 'empname' => 'Loket D1', 'loket_name' => 'Loket D1', 'userlevel' => '6']
        );
                    
        foreach ($Ds as $D)
        {
            $d = \App\User::create($D);
            $role_d = \App\Role::where('name', 'umum')->first();
            $userD = \App\User::where('username', $D['username'])->first();
            $userD->attachRole($role_d);
        }
    }
}
