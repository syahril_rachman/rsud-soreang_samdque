<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmPasiens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_pasiens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('IdBankNomor', 50);
            $table->string('IdPasien', 50)->nullable();
            $table->string('NamaPasien', 50)->nullable();
            $table->string('IdJenisKelamin', 2)->nullable();
            $table->timestamp('TglLahir')->nullable();
            $table->timestamps();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_pasiens');
    }
}
