<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmRuangans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_ruangans', function (Blueprint $table) {
            // $table->increments('id');
            $table->increments('roomidx', 11);
            $table->string('idRuangPelayanan', 10)->nullable();
            $table->string('namaRuangPelayanan', 50)->nullable();
            $table->string('aliasRuangPelayanan', 150)->nullable();            
            $table->timestamps();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_ruangans');
    }
}
