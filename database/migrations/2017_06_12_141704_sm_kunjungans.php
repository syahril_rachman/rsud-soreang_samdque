<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmKunjungans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_kunjungans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('visitidx', 50);
            $table->string('IdBankNomor', 50)->nullable();
            $table->string('IdRuangPerujuk', 10)->nullable();
            $table->string('NamaRuangPerujuk', 150)->nullable();
            $table->string('AliasNamaRuangPerujuk', 50)->nullable();
            $table->string('IdDokterPerujuk', 50)->nullable();
            $table->string('NamaDokterPerujuk', 150)->nullable();
            $table->timestamp('TglPesan')->nullable();
            $table->timestamp('TglAntrian')->nullable();
            $table->timestamp('TglAmbilHasil')->nullable();
            $table->string('position', 50)->nullable();
            $table->integer('type')->nullable();
            $table->integer('noqueue');
            // $table->foreign('IdRuangPerujuk')->references('idRuangPelayanan')->on('sm_ruangans')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_kunjungans');
    }
}

