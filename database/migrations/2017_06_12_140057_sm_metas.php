<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmMetas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_metas', function (Blueprint $table) {
            // $table->increments('id');
            $table->integer('value', 11);
            $table->string('parameter', 50)->nullable();
            $table->text('description')->nullable();
            $table->text('link')->nullable();
            $table->timestamps();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_metas');
    }
}
