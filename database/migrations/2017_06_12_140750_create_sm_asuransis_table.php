<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmAsuransisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_asuransis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asuransiidx');
            $table->string('IdAsuransi');
            $table->string('NamaAsuransi');
            $table->text('keterangan')->nullable();
            $table->integer('metavalue')->nullable();
            // $table->foreign('metavalue')->references('value')->on('sm_metas')->onDelete('cascade');
            // ->unsigned()
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_asuransis');
    }
}
