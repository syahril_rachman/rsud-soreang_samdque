<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_settings', function (Blueprint $table) {
            $table->increments('settingid', 11);
            $table->string('settingtype', 10)->nullable();
            $table->text('settingvalue')->nullable();
            $table->string('settingstat', 2)->nullable();
            $table->longText('setting')->nullable();
            $table->timestamps();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_settings');
    }
}
