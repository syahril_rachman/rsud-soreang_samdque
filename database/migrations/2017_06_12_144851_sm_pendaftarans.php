<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmPendaftarans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_pendaftarans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('regid', 50);
            $table->integer('noqueue')->nullable();
            $table->timestamp('datequeue')->nullable();
            $table->string('timequeue', 8)->nullable();
            $table->string('status', 10)->nullable();            
            $table->integer('metavalue')->nullable();
            // $table->foreign('metavalue')->references('value')->on('sm_metas')->onDelete('cascade');
            // ->unsigned()
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_pendaftarans');
    }
}
